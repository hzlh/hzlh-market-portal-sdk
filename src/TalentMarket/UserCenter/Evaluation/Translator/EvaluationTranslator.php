<?php
namespace SuperMarket\TalentMarket\UserCenter\Evaluation\Translator;

use Sdk\TalentMarket\Evaluation\Model\Evaluation;
use Sdk\TalentMarket\Evaluation\Model\NullEvaluation;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use SuperMarket\TalentMarket\UserCenter\Member\Translator\MemberTranslator;
use SuperMarket\TalentMarket\UserCenter\Order\Translator\OrderTranslator;
use SuperMarket\TalentMarket\UserCenter\Evaluation\Translator\EvaluationScoreTranslator;

use SuperMarket\TalentMarket\UserCenter\Enterprise\Translator\EnterpriseTranslator;

class EvaluationTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $evaluation = null)
    {
        unset($evaluation);
        unset($expression);
        return NullEvaluation::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    protected function getEvaluationScoreTranslator() : EvaluationScoreTranslator
    {
        return new EvaluationScoreTranslator();
    }

    protected function getOrderTranslator() : OrderTranslator
    {
        return new OrderTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function objectToArray($evaluation, array $keys = array())
    {
        if (!$evaluation instanceof Evaluation) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'content',
                'reason',
                'picture',
                'relation',
                'evaluationScore' =>[],
                'commodityName',
                'member' => [],
                'order'=> [],
                'enterprise'=> [],
                'isAuto',
                'isReply',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($evaluation->getId());
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars(str_replace("ℑ", "", $evaluation->getContent()));
            //phpcs:ignore
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $evaluation->getReason();
        }
        if (in_array('picture', $keys)) {
            $expression['picture'] = $evaluation->getPicture();
        }
        if (in_array('commodityName', $keys)) {
            $expression['commodityName'] = $evaluation->getCommodityName();
        }
        if (in_array('isAuto', $keys)) {
            $expression['isAuto'] =$evaluation->getIsAuto();
        }
        if (in_array('isReply', $keys)) {
            $expression['isReply'] = $evaluation->getIsReply();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $evaluation->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $evaluation->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $evaluation->getCreateTime());
            $expression['createTimeFullFormat'] = date('Y-m-d H:i:s', $evaluation->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $evaluation->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $evaluation->getUpdateTime());
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $evaluation->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['order'])) {
            $expression['order'] = $this->getOrderTranslator()->objectToArray(
                $evaluation->getOrder(),
                $keys['order']
            );
        }
        if (isset($keys['evaluationScore'])) {
            $expression['evaluationScore'] = $this->getEvaluationScoreTranslator()->objectToArray(
                $evaluation->getEvaluationScore(),
                $keys['evaluationScore']
            );
        }

        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $evaluation->getEnterprise(),
                $keys['enterprise']
            );
        }

        return $expression;
    }
}
