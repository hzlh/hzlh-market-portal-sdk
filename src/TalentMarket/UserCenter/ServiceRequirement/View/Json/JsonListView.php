<?php
namespace SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\StatusTrait;
use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\ListViewTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait, StatusTrait;

    //自定义状态
    const STATUS = array(
        'REVOKED' => -4, //撤销
        'CLOSED' => -6, //关闭
        'DELETED' => -8 //删除
    );

    public function display() : void
    {
        $list = $this->getList();

        if (!empty($list)) {
            $list = $this->stateTransitionByArray($list);
        }

        $data = array(
            'list' => $list,
            'total' => $this->getTotal()
        );

        $this->encode($data);
    }
}
