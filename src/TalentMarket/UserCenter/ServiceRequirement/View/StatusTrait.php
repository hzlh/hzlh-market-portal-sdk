<?php
namespace SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View;

use Sdk\Common\Model\IModifyStatusAble;

trait StatusTrait
{
    public function stateTransitionByOne($data)
    {
        $requirementStatus = $this->stateTransition($data['status'], $data['applyStatus']);
        $data['requirementStatus'] = $requirementStatus;

        return $data;
    }

    public function stateTransitionByArray($data)
    {
        foreach ($data as $key => $item) {
            $data[$key]['requirementStatus'] = $this->stateTransition($item['status'], $item['applyStatus']);
        }

        return $data;
    }

    protected function stateTransition($status, $applyStatus)
    {
        switch ($status) {
            case IModifyStatusAble::STATUS['REVOKED']:
                $requirementStatus = self::STATUS['REVOKED'];
                break;
            case IModifyStatusAble::STATUS['CLOSED']:
                $requirementStatus = self::STATUS['CLOSED'];
                break;
            case IModifyStatusAble::STATUS['DELETED']:
                $requirementStatus = self::STATUS['DELETED'];
                break;
            default:
                $requirementStatus = $applyStatus;
        }

        return $requirementStatus;
    }
}
