<?php
namespace SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\TalentMarket\ServiceRequirement\Model\ServiceRequirement;
use Sdk\TalentMarket\ServiceCategory\Repository\ServiceCategoryRepository;

use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class AddServiceRequirementCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    private $requirements;

    public function __construct()
    {
        $this->requirements = new ServiceRequirement();
    }

    public function __destruct()
    {
        unset($this->requirements);
    }

    protected function getRequirements() : ServiceRequirement
    {
        return $this->requirements;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchServiceCategory($id)
    {
        return $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    /**
     *
     */
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceRequirementCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceCategory = $this->fetchServiceCategory($command->serviceCategoryId);

        $requirements = $this->getRequirements();
        $requirements->setServiceCategory($serviceCategory);
        $requirements->setTitle($command->title);
        $requirements->setDetail($command->detail);
        $requirements->setMinPrice($command->minPrice);
        $requirements->setMaxPrice($command->maxPrice);
        $requirements->setValidityStartTime($command->validityStartTime);
        $requirements->setValidityEndTime($command->validityEndTime);
        $requirements->setContactName($command->contactName);
        $requirements->setContactPhone($command->contactPhone);

        if ($requirements->add()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            $this->getRequirements()->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->getRequirements()->getNumber()
        );
    }
}
