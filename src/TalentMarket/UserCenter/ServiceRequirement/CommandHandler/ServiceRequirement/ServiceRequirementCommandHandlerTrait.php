<?php
namespace SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Sdk\TalentMarket\ServiceRequirement\Model\ServiceRequirement;
use Sdk\TalentMarket\ServiceRequirement\Repository\ServiceRequirementRepository;

trait ServiceRequirementCommandHandlerTrait
{
    private $serviceRequirement;

    private $repository;

    public function __construct()
    {
        $this->serviceRequirement = new ServiceRequirement();
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        unset($this->serviceRequirement);
        unset($this->repository);
    }

    protected function getServiceRequirement() : ServiceRequirement
    {
        return $this->serviceRequirement;
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    protected function fetchRequirement(int $id) : ServiceRequirement
    {
        return $this->getRepository()->fetchOne($id);
    }
}
