<?php
namespace SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'SuperMarket\TalentMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
