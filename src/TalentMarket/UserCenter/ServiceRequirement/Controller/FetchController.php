<?php
namespace SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\Json\JsonListView;
use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\Template\SearchView;
use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\Template\ListView;
use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\View\Template\DetailView;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\TalentMarket\Service\Model\Service;
use Sdk\TalentMarket\Service\Repository\ServiceRepository;
use Sdk\TalentMarket\ServiceRequirement\Repository\ServiceRequirementRepository;

use SuperMarket\TalentMarket\Workbench\ServiceCategory\Controller\CategoryTrait;
use SuperMarket\Statistical\Controller\StatisticalControllerTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait, CategoryTrait, StatisticalControllerTrait;

    const TYPE = 'staticsTalentMarketRequirementCount';

    const SIZE = array(
        'REQUIREMENT_SIZE' => 10, //获取需求数据条数
        'SERVICE_SIZE' => 3 //获取服务数据条数
    );

    const SCENE = array(
        'PENDING' => 1, //待审核
        'APPROVE' => 2, //已通过
        'REJECT' => 3, //已驳回
        'ALL' => 4 //全部
    );

    private $serviceRepository;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
        $this->serviceRepository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->serviceRepository);
    }

    protected function getRequirementsRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /serviceRequirements
     * 发布需求列表
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        list($page, $size) = $this->getPageAndSize(self::SIZE['REQUIREMENT_SIZE']);
        list($filter, $sort) = $this->filterFormatChange();

        $requirementsList = array();
        list($count, $requirementsList) = $this->getRequirementsRepository()
            ->scenario(ServiceRequirementRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($count, $requirementsList));
            return true;
        }

        $this->render(new ListView($count, $requirementsList));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {
        $scene = $this->getRequest()->get('scene', '');
        if (empty($scene)) {
            $scene = 'Mw';
        }
        $scene = marmot_decode($scene);

        $serviceCategoryId = $this->getRequest()->get('serviceCategory', '');
        $serviceCategoryId = marmot_decode($serviceCategoryId);

        $title = $this->getRequest()->get('title', '');

        $validityStartTime = $this->getRequest()->get('validityStartTime', '');
        $validityEndTime = $this->getRequest()->get('validityEndTime', '');

        $sort = ['-updateTime'];
        $filter = array();
        $filter['member'] = Core::$container->get('user')->getId();
        $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];

        if (!empty($serviceCategoryId)) {
            $filter['serviceCategory'] = $serviceCategoryId;
        }

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if (!empty($validityStartTime)) {
            $filter['validityStartTime'] = $validityStartTime;
        }

        if (!empty($validityEndTime)) {
            $filter['validityEndTime'] = $validityEndTime;
        }

        if ($scene == self::SCENE['ALL']) {
            $status = [
                IModifyStatusAble::STATUS['NORMAL'],
                IModifyStatusAble::STATUS['REVOKED'],
                IModifyStatusAble::STATUS['CLOSED']
            ];
            $filter['status'] = implode(",", $status);
        }

        if ($scene == self::SCENE['PENDING']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'];
        }

        if ($scene == self::SCENE['APPROVE']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        }

        if ($scene == self::SCENE['REJECT']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['REJECT'];
        }

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /serviceRequirements/MA
     * 发布需求详情
     */
    protected function fetchOneAction($id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $requirements = $this->getRequirementsRepository()
            ->scenario(ServiceRequirementRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($requirements instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        // 获取推荐服务列表
        $applyStatus =  $requirements->getApplyStatus();
        $serviceCategoryId =  $requirements->getServiceCategory()->getId();
        $servicesList = $this->fetchServiceList($serviceCategoryId, $applyStatus);

        $this->render(new DetailView($requirements, $servicesList));
        return true;
    }

    /**
     * @param $serviceCategoryId
     * @return array
     * 获取条件为[服务分类id]的服务列表
     */
    protected function fetchServiceList($serviceCategoryId, $applyStatus)
    {
        $servicesList = array();

        if ($applyStatus != IApplyAble::APPLY_STATUS['APPROVE']) {
            return $servicesList;
        }

        list($filter, $sort) = $this->serviceFilterFormatChange($serviceCategoryId);

        list($count, $servicesList) = $this->getServiceRepository()
            ->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, self::SIZE['SERVICE_SIZE']);
        unset($count);

        return $servicesList;
    }

    protected function serviceFilterFormatChange($serviceCategoryId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['serviceCategory'] = $serviceCategoryId;
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /serviceRequirements/requirementSearch
     * 需求列表查询
     */
    public function requirementSearch()
    {
        $serviceCategory = $this->fetchServiceCategory();

        $filterCount['memberId'] = Core::$container->get('user')->getId();

        $staticsServiceRequirementCount = $this->analyse(self::TYPE, $filterCount);

        $this->render(new SearchView($serviceCategory, $staticsServiceRequirementCount, self::TYPE));
        return true;
    }
}
