<?php
namespace SuperMarket\TalentMarket\UserCenter\Contract\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Qxy\Contract\Contract\Model\Contract;
use Qxy\Contract\Contract\Model\NullContract;

use Qxy\Contract\Template\Model\Template;

use SuperMarket\TalentMarket\Workbench\ContractTemplate\Translator\ContractTemplateTranslator;
use SuperMarket\TalentMarket\UserCenter\Enterprise\Translator\EnterpriseTranslator;
use SuperMarket\TalentMarket\UserCenter\Order\Translator\OrderTranslator;
use SuperMarket\TalentMarket\Workbench\Performance\Translator\PerformanceTranslator;

class ContractTranslator implements ITranslator
{
    const STATUS_ZN = [
        Contract::STATUS['NORMAL'] => '待确认',
        Contract::STATUS['WAIT_PERFORMANCE'] => '待履约',
        Contract::STATUS['IS_PERFORMANCE'] => '已履约',
        Contract::STATUS['BREAK_CONTRACT'] => '已违约'
    ];

    public function arrayToObject(array $expression, $contract = null)
    {
        unset($contract);
        unset($expression);
        return NullContract::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getTemplateTranslator() : ContractTemplateTranslator
    {
        return new ContractTemplateTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getOrderTranslator() : OrderTranslator
    {
        return new OrderTranslator();
    }

    protected function getPerformanceTranslator() : PerformanceTranslator
    {
        return new PerformanceTranslator();
    }
    /**
     * 屏蔽类中所有PMD警告
     *
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($contract, array $keys = array())
    {
        if (!$contract instanceof Contract) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'orderNumber',
                'title',
                'content',
                'signTime',
                'status',
                'updateTime',
                'createTime',
                'partA' => [],
                'partB' => [],
                'order' => [],
                'performanceItems'=>[],
                'earlyWarningStatus',
                'partAEvaluateStatus',
                'partBEvaluateStatus'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($contract->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $contract->getNumber();
        }
        if (in_array('orderNumber', $keys)) {
            $expression['orderNumber'] = $contract->getOrderNumber();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $contract->getTitle();
        }
        if (in_array('signTime', $keys)) {
            $expression['signTime'] = $contract->getSignTime();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars($contract->getContent());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $contract->getStatus();
            $expression['statusFormat'] = self::STATUS_ZN[$contract->getStatus()];
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $contract->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $contract->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $contract->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $contract->getUpdateTime());
        }
        if (isset($keys['partB'])) {
            $expression['partB'] = $this->getEnterpriseTranslator()->objectToArray(
                $contract->getPartB(),
                $keys['partB']
            );
        }
        if (isset($keys['partA'])) {
            $expression['partA'] = $this->getEnterpriseTranslator()->objectToArray(
                $contract->getPartA(),
                $keys['partA']
            );
        }
        if (isset($keys['partB'])) {
            $expression['partB'] = $this->getEnterpriseTranslator()->objectToArray(
                $contract->getPartB(),
                $keys['partB']
            );
        }
        if (isset($keys['order'])) {
            $expression['order'] = $this->getOrderTranslator()->objectToArray(
                $contract->getOrder(),
                $keys['order']
            );
        }
        if (isset($keys['performanceItems'])) {
            $performanceItemsArray = [];
            foreach ($contract->getPerformance() as $performanceItems) {
                $performanceItemsArray[] = $this->getPerformanceTranslator()->objectToArray(
                    $performanceItems,
                    $keys['performanceItems']
                );
            }
            $expression['performanceItems'] = $performanceItemsArray;
        }
        if (in_array('earlyWarningStatus', $keys)) {
            $expression['earlyWarningStatus'] = $contract->getEarlyWarningStatus();
        }
        if (in_array('partAEvaluateStatus', $keys)) {
            $expression['partAEvaluateStatus'] = $contract->getPartAEvaluateStatus();
        }
        if (in_array('partBEvaluateStatus', $keys)) {
            $expression['partBEvaluateStatus'] = $contract->getPartBEvaluateStatus();
        }

        return $expression;
    }
}
