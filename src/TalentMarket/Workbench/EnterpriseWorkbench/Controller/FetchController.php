<?php
namespace SuperMarket\TalentMarket\Workbench\EnterpriseWorkbench\Controller;

use Common\Controller\Traits\GlobalCheckRolesTrait;
use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IEnableAble;
use Sdk\Staff\Repository\StaffRepository;
use Sdk\Authentication\Repository\AuthenticationRepository;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\FinanceAuthentication\Repository\FinanceAuthenticationRepository;
use Sdk\FinanceAuthentication\Repository\UnAuditedFinanceAuthenticationRepository;

use Workbench\EnterpriseWorkbench\View\Json\EnterpriseInfoJsonView;
use Workbench\EnterpriseWorkbench\View\Template\IndexView;
use Workbench\EnterpriseWorkbench\View\Template\WidgetServiceTopInfoView;

use UserCenter\Tag\Controller\TagControllerTrait;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait,
      FetchControllerTrait,
      GlobalCheckTrait,
      GlobalCheckRolesTrait,
      TagControllerTrait;

    private $staffRepository;

    private $enterpriseRepository;

    private $authenticationRepository;

    private $financeAuthenticationRepository;

    private $unAuditedRepository;

    private $concurrentAdapter;

    public function __construct()
    {
        parent::__construct();
        $this->staffRepository = new StaffRepository();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->authenticationRepository = new AuthenticationRepository();
        $this->financeAuthenticationRepository = new FinanceAuthenticationRepository();
        $this->unAuditedRepository = new UnAuditedFinanceAuthenticationRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->staffRepository);
        unset($this->enterpriseRepository);
        unset($this->authenticationRepository);
        unset($this->financeAuthenticationRepository);
        unset($this->unAuditedRepository);
        unset($this->concurrentAdapter);
    }

    protected function getStaffRepository() : StaffRepository
    {
        return $this->staffRepository;
    }

    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return $this->authenticationRepository;
    }

    protected function getFinanceAuthenticationRepository() : FinanceAuthenticationRepository
    {
        return $this->financeAuthenticationRepository;
    }

    protected function getUnAuditedRepository() : UnAuditedFinanceAuthenticationRepository
    {
        return $this->unAuditedRepository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /enterpriseWorkbenchs
     * 企业工作台认证列表
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }
        $enterpriseId =
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        if (empty($enterpriseId)) {
            $this->resetEnterpriseId();
        }

        if (!$this->globalCheckEnterprise()) {
            return false;
        }

        $repository = $this->getAuthenticationRepository();
        $unAuditedRepository = $this->getUnAuditedRepository();

        $sort = ['-updateTime'];
        // 认证服务商条件检索
        $filter['enterprise'] = $enterpriseId;
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        // 认证金融机构
        $unAuditedFilter['relation'] = $enterpriseId;

        $this->getConcurrentAdapter()->addPromise(
            'authentications',
            $repository->scenario(AuthenticationRepository::LIST_MODEL_UN)
                ->searchAsync($filter, $sort, PAGE, SIZE),
            $repository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'unAuditedFinanceAuthentication',
            $unAuditedRepository->scenario(UnAuditedFinanceAuthenticationRepository::LIST_MODEL_UN)
                ->searchAsync($unAuditedFilter, $sort, PAGE, SIZE),
            $unAuditedRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        $this->render(new IndexView($data));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /workbenches/enterpriseInfo
     * 获取员工企业信息
     */
    public function enterpriseInfo()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $filter['member'] = Core::$container->get('user')->getId();
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $staff = array();
        list($count, $staff) = $this->getStaffRepository()
            ->scenario(StaffRepository::LIST_MODEL_UN)
            ->search($filter, ['-updateTime'], PAGE, COMMON_SIZE);

        $this->render(new EnterpriseInfoJsonView($count, $staff));
        return true;
    }

    /**
     * @return bool
     * @param [POST]
     * @method /workbenches/saveEnterpriseByCore
     * 存储企业信息
     */
    public function saveEnterpriseByCore()
    {
        $enterpriseId = $this->getRequest()->post('enterpriseId', '');
        $enterpriseId = marmot_decode($enterpriseId);

        if (Core::$cacheDriver->save(
            'staffEnterpriseId:' .Core::$container->get('user')->getId(),
            $enterpriseId
        )
        ) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /enterpriseWorkbenches/serviceTopInfo
     * 企业工作台顶部信息公共展示
     */
    public function serviceTopInfo()
    {
        $enterpriseInfo = $this->getEnterpriseInfos();
        $authentication = $this->getAuthentication($enterpriseInfo->getId());
        $financeAuthentication = $this->getFinanceAuthentication($enterpriseInfo->getId());

        $tag = $enterpriseInfo->getTag();

        $tagList = $this->getTags($tag);

        $this->render(new WidgetServiceTopInfoView($enterpriseInfo, $authentication, $financeAuthentication, $tagList));
        return true;
    }

    protected function getAuthentication($enterpriseId)
    {
        list($filter, $sort) = $this->filterFormatChange($enterpriseId);

        $authentication = array();
        list($count, $authentication) = $this->getAuthenticationRepository()
            ->scenario(AuthenticationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $authentication;
    }

    protected function getFinanceAuthentication($enterpriseId)
    {
        $data = 1;
        $financeAuthentication = $this->getFinanceAuthenticationRepository()
            ->scenario(FinanceAuthenticationRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($enterpriseId);

        if ($financeAuthentication instanceof INull) {
            $data = 0;
        }

        return $data;
    }

    protected function filterFormatChange($enterpriseId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['enterprise'] = $enterpriseId;
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        return [$filter, $sort];
    }

    protected function fetchOneAction($id)
    {
        unset($id);
        return false;
    }
}
