<?php
namespace SuperMarket\TalentMarket\Workbench\EnterpriseWorkbench\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Workbench\Staff\Translator\StaffTranslator;

class EnterpriseInfoJsonView extends JsonView implements IView
{
    private $staff;

    private $count;

    private $translator;

    public function __construct(
        $count,
        array $staff
    ) {
        parent::__construct();
        $this->count = $count;
        $this->staff = $staff;
        $this->translator = new StaffTranslator();
    }

    protected function getStaff() : array
    {
        return $this->staff;
    }

    protected function getTotal()
    {
        return $this->count;
    }

    protected function getTranslator() : StaffTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $list = array();
        foreach ($this->getStaff() as $staff) {
            $list[] = $this->getTranslator()->objectToArray(
                $staff,
                array(
                    'id',
                    'realName',
                    'status',
                    'enterprise'=>['id','name','logo'],
                )
            );
        }

        $dataTable = array(
            'total' => $this->getTotal(),
            'list' => $list
        );

        $this->encode($dataTable);
    }
}
