<?php
namespace SuperMarket\TalentMarket\Workbench\EnterpriseWorkbench\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use UserCenter\Enterprise\Translator\EnterpriseTranslator;

use Workbench\ServiceProvider\Translator\ServiceProviderTranslator;

use UserCenter\Tag\View\TagTrait;

class WidgetServiceTopInfoView extends TemplateView implements IView
{
    use TagTrait;

    private $translator;

    private $enterprise;

    private $tagList;

    private $authentication;

    private $financeAuthentication;

    private $serviceProviderTranslator;

    public function __construct($enterprise, $authentication, $financeAuthentication, $tagList)
    {
        parent::__construct();
        $this->enterprise = $enterprise;
        $this->tagList = $tagList;
        $this->authentication = $authentication;
        $this->financeAuthentication = $financeAuthentication;
        $this->translator = new EnterpriseTranslator();
        $this->serviceProviderTranslator = new ServiceProviderTranslator();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->enterprise);
        unset($this->tagList);
        unset($this->authentication);
        unset($this->financeAuthentication);
        unset($this->translator);
        unset($this->serviceProviderTranslator);
    }

    public function getTranslator() : EnterpriseTranslator
    {
        return $this->translator;
    }

    public function getServiceProviderTranslator() : ServiceProviderTranslator
    {
        return $this->serviceProviderTranslator;
    }

    public function getEnterprise()
    {
        return $this->enterprise;
    }

    public function getAuthentication()
    {
        return $this->authentication;
    }

    public function getFinanceAuthentication()
    {
        return $this->financeAuthentication;
    }

    protected function getServiceCategoryNames($authentications)
    {
        $translator = $this->getServiceProviderTranslator();

        $serviceCategoryList = array();
        foreach ($authentications as $authentication) {
            $serviceCategoryList[] = $translator->objectToArray(
                $authentication,
                array('serviceCategory'=>[])
            );
        }

        $serviceCategoryName = array();
        foreach ($serviceCategoryList as $serviceCategory) {
            $serviceCategoryName[] = $serviceCategory['serviceCategory']['name'];
        }

        return $serviceCategoryName;
    }

    public function display() : void
    {
        $translator = $this->getTranslator();

        $enterpriseInfo = $translator->objectToArray(
            $this->getEnterprise(),
            array('id', 'name', 'logo', 'contactsInfo'=>[])
        );

        $authentications = $this->getAuthentication();
        $authentications = !empty($authentications) ? $authentications : array();

        $serviceCategoryNames = array();
        if (!empty($authentications)) {
            $serviceCategoryNames = $this->getServiceCategoryNames($authentications);
        }

        $financeAuthentication = $this->getFinanceAuthentication();

        $tag = $this->tagDetailList();
        
        $this->getView()->display(
            'TalentMarket/Workbench/Layout/WidgetServiceTopInfo.tpl',
            [
                'serviceCategoryNames' => $serviceCategoryNames,
                'financeAuthentication' => $financeAuthentication,
                'data' => $enterpriseInfo,
                'tags' => !empty($tag) ? $tag : []
            ]
        );
    }
}
