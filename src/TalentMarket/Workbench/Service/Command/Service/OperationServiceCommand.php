<?php
namespace SuperMarket\TalentMarket\Workbench\Service\Command\Service;

use Marmot\Interfaces\ICommand;

abstract class OperationServiceCommand implements ICommand
{
    public $title;

    public $cover;

    public $serviceObjects;

    public $price;

    public $detail;

    public $contract;

    public $serviceCategory;

    public $tag;

    public $id;

    public function __construct(
        string $title,
        string $tag,
        array $cover,
        array $serviceObjects,
        array $price,
        array $detail,
        array $contract,
        int $serviceCategory = 0,
        int $id = 0
    ) {
        $this->title = $title;
        $this->tag = $tag;
        $this->cover = $cover;
        $this->serviceObjects = $serviceObjects;
        $this->price = $price;
        $this->detail = $detail;
        $this->contract = $contract;
        $this->serviceCategory = $serviceCategory;
        $this->id = $id;
    }
}
