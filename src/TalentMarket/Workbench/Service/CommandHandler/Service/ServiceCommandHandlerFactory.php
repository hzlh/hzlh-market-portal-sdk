<?php
namespace SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\AddServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\AddServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\EditServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\EditServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\ResubmitServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\RevokeServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\DeleteServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\CloseServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\OnShelfServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'SuperMarket\TalentMarket\Workbench\Service\Command\Service\OffStockServiceCommand'=>
        'SuperMarket\TalentMarket\Workbench\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
