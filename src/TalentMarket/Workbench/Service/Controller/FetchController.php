<?php
namespace SuperMarket\TalentMarket\Workbench\Service\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\TalentMarket\Workbench\Service\View\Template\SearchView;
use SuperMarket\TalentMarket\Workbench\Service\View\Json\JsonListView;
use SuperMarket\TalentMarket\Workbench\Service\View\Template\ListView;
use SuperMarket\TalentMarket\Workbench\Service\View\Template\DetailView;
use SuperMarket\TalentMarket\Workbench\Service\View\Json\JsonDetailView;

use Sdk\Common\Model\IApplyAble;
use Sdk\TalentMarket\Service\Model\Service;
use Sdk\TalentMarket\Service\Model\NullService;
use Sdk\TalentMarket\Service\Repository\ServiceRepository;

use Sdk\Collection\Model\Collection;

use Statistical\Controller\StatisticalControllerTrait;

use UserCenter\Tag\Controller\TagControllerTrait;

use UserCenter\Collection\Controller\CollectionTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait,
        FetchControllerTrait,
        GlobalCheckTrait,
        ServiceTrait,
        ContractTemplateItemTrait,
        TagControllerTrait,
        CollectionTrait;

    const TYPE = 'staticsServiceCount';

    const SERVICE_SIZE = 10;

    const SCENE = array(
        'ALL' => 1, //全部
        'PENDING' => 2, //待审核
        'ONSHELF' => 3, //已上架
        'OFFSTOCK' => 4, //已下架
        'REJECT' => 5, //已驳回
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }



    /**
     * @return bool
     * @param [GET]
     * @method /services
     * 发布服务列表
     *
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        if (!$this->globalCheckEnterprise()) {
            return false;
        }

        $enterpriseId = Core::$cacheDriver->fetch(
            'staffEnterpriseId:'.Core::$container->get('user')->getId()
        );
        list($page, $size) = $this->getPageAndSize(self::SERVICE_SIZE);
        list($filter, $sort) = $this->filterFormatChange($enterpriseId);

        $servicesList = array();
        list($count, $servicesList) = $this->getServiceRepository()
            ->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $filterCount['enterpriseId'] = $enterpriseId;

        $staticsServiceCount = $this->analyse(self::TYPE, $filterCount);

        $contractTemplate = [];
        if (!empty($servicesList)) {
            $contractTemplate = $this->filterContractTemplateItem($servicesList);
        }

        if ($this->getRequest()->isAjax()) {
            $this->render(
                new JsonListView(
                    $count,
                    $servicesList,
                    $staticsServiceCount,
                    self::TYPE,
                    $contractTemplate
                )
            );
            return true;
        }

        $this->render(
            new ListView(
                $count,
                $servicesList,
                $staticsServiceCount,
                self::TYPE,
                $contractTemplate
            )
        );
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange($enterpriseId)
    {
        $scene = $this->getRequest()->get('scene', '');
        if (empty($scene)) {
            $scene = 'MA';
        }
        $scene = marmot_decode($scene);

        $serviceCategoryId = $this->getRequest()->get('serviceCategory', '');
        $serviceCategoryId = marmot_decode($serviceCategoryId);

        $title = $this->getRequest()->get('title', '');

        $sort = ['-updateTime'];
        $filter = array();
        $filter['enterprise'] = $enterpriseId;

        if (!empty($serviceCategoryId)) {
            $filter['serviceCategory'] = $serviceCategoryId;
        }

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if ($scene == self::SCENE['ALL']) {
            $status = [
                Service::SERVICE_STATUS['ONSHELF'], Service::SERVICE_STATUS['OFFSTOCK'],
                Service::SERVICE_STATUS['REVOKED'], Service::SERVICE_STATUS['CLOSED']
            ];
            $filter['status'] = implode(",", $status);
        }

        if ($scene == self::SCENE['PENDING']) {
            $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'];
        }

        if ($scene == self::SCENE['ONSHELF']) {
            $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        }

        if ($scene == self::SCENE['OFFSTOCK']) {
            $filter['status'] = Service::SERVICE_STATUS['OFFSTOCK'];
        }

        if ($scene == self::SCENE['REJECT']) {
            $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['REJECT'];
        }

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /services/MA
     * 发布服务详情
     */
    protected function fetchOneAction($id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $service = $this->getServiceRepository()
            ->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($service instanceof NullService) {
            return false;
        }

        $tag = $service->getTag();
        $tagList = $this->getTags($tag);

        $contractTemplate = $this->filterContractTemplateItem($service);
        // 服务收藏
        list($collectionList, $collectionCount) = $this->collectionInfoList(
            Collection::COLLECTION_CATEGORY['SERVICE'],
            $id
        );
        unset($collectionList);

        // 注意: 此处应用ajax用于修改页面回显服务描述
        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonDetailView($service, $contractTemplate, $tagList, $collectionCount));
            return true;
        }

        $this->render(new DetailView($service, $contractTemplate, $tagList, $collectionCount));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /services/serviceSearch
     * 服务列表查询
     *
     */
    public function serviceSearch()
    {
        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $serviceCategoryIds  = $this->getServiceCategoryIds($enterpriseId);
        $serviceCategoryArray = $this->fetchServiceCategoryByIds($serviceCategoryIds);

        $this->render(new SearchView($serviceCategoryArray));
        return true;
    }

    public function fetchServiceInfo(string $id)
    {
        $id = marmot_decode($id);
        $service = $this->getServiceDetail($id);

        if ($service instanceof NullService) {
            return false;
        }

        $this->render(new JsonDetailView($service));
        return true;
    }
}
