<?php
namespace SuperMarket\TalentMarket\Workbench\Service\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Workbench\Service\View\ViewTrait;
use SuperMarket\TalentMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

use Common\Controller\Traits\GlobalCheckRolesTrait;

use UserCenter\Tag\View\TagTrait;

class EditView extends TemplateView implements IView
{
    use ViewTrait, ServiceCategoryViewTrait, GlobalCheckRolesTrait, TagTrait;

    public function display()
    {
        $data = $this->getData();

        $tag = $this->tagDetailList();

        $data['tag'] = !empty($tag) ? $tag : [];
        
        $serviceCategories = $this->getServiceCategory();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $serviceObjects = $this->getServiceObjects();

        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'TalentMarket/Service/Workbench/Service/Edit.tpl',
            [
                'data' => $data,
                'permission' => $permission,
                'serviceCategories' => $serviceCategories,
                'serviceObjects' => $serviceObjects,
                'nav_left' => NavTrait::NAV_WORKBENCH['SERVICE'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_SERVICE']
            ]
        );
    }
}
