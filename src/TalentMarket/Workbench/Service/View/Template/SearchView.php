<?php
namespace SuperMarket\TalentMarket\Workbench\Service\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

class SearchView extends TemplateView implements IView
{
    use ServiceCategoryViewTrait;

    private $serviceCategories;

    public function __construct($serviceCategories)
    {
        parent::__construct();
        $this->serviceCategories = $serviceCategories;
    }

    public function __destruct()
    {
        unset($this->serviceCategory);
    }

    public function getServiceCategories() : array
    {
        return $this->serviceCategories;
    }

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $this->getView()->display(
            'TalentMarket/Service/Workbench/Service/Search.tpl',
            [
                'serviceCategories' => $serviceCategories
            ]
        );
    }
}
