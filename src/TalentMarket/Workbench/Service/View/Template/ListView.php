<?php
namespace SuperMarket\TalentMarket\Workbench\Service\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Workbench\Service\View\StatusTrait;
use SuperMarket\TalentMarket\Workbench\Service\View\ListViewTrait;

use Common\Controller\Traits\GlobalCheckRolesTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait, StatusTrait, GlobalCheckRolesTrait;

    const STATUS = array(
        'OFFSTOCK' => -3,
        'REVOKED' => -4,
        'CLOSED' => -6,
        'DELETED' => -8
    );

    const TEMPLATE_STATUS = [
        'UNCORRELATED' => 0,
        'ASSOCIATED' => 2
    ];

    const TEMPLATE_STATUS_ZN = [
        self::TEMPLATE_STATUS['UNCORRELATED'] => '去关联',
        self::TEMPLATE_STATUS['ASSOCIATED'] => '已关联'
    ];

    public function display()
    {
        $list = $this->getList();

        if (!empty($list)) {
            $list = $this->stateTransitionByArray($list);
        }

        $contractTemplateItem = $this->getContractTemplateItemList();

        $template = [];
        foreach ($list as $key => $val) {
            $template['id'] = '';
            $template['templateStatus'] = marmot_encode(self::TEMPLATE_STATUS['UNCORRELATED']);
            $template['templateStatusFormat'] = self::TEMPLATE_STATUS_ZN[self::TEMPLATE_STATUS['UNCORRELATED']];

            foreach ($contractTemplateItem as $itemVal) {
                if ($val['id'] == $itemVal['item']) {
                    $template['id'] = $itemVal['template']['id'];
                    $template['templateStatus'] = marmot_encode(self::TEMPLATE_STATUS['ASSOCIATED']);
                    $template['templateStatusFormat'] = self::TEMPLATE_STATUS_ZN[self::TEMPLATE_STATUS['ASSOCIATED']];
                }
            }

            $list[$key]['template'] = $template;
        }
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'TalentMarket/Service/Workbench/Service/Audit.tpl',
            [
                'list' => $list,
                'total' => $this->getTotal(),
                'permission' => $permission,
                'serviceCount' =>$this->getStaticsService(),
                'nav_left' => NavTrait::NAV_WORKBENCH['SERVICE'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_SERVICE']
            ]
        );
    }
}
