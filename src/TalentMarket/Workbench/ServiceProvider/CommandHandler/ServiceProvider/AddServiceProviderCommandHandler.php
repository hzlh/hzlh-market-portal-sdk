<?php
namespace SuperMarket\TalentMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Authentication\Model\Authentication;

use SuperMarket\TalentMarket\Workbench\ServiceProvider\Command\ServiceProvider\AddServiceProviderCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class AddServiceProviderCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceProviderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    private $authentication;

    public function __construct()
    {
        $this->authentication = new Authentication();
    }

    public function __destruct()
    {
        unset($this->authentication);
    }

    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceProviderCommand)) {
            throw new \InvalidArgumentException;
        }

        $authentication = $this->getAuthentication();
        $authentication = $this->executeAction($command, $authentication);

        if ($authentication->add()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['SERVICE_PROVIDER'],
            $this->getAuthentication()->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->getAuthentication()->getNumber(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
