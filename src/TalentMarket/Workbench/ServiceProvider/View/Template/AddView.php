<?php
namespace SuperMarket\TalentMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Workbench\ServiceProvider\View\ViewTrait;
use SuperMarket\TalentMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

class AddView extends TemplateView implements IView
{
    use ViewTrait, ServiceCategoryViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);
        $authenticationIds  = $this->getAuthenticationIds();
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'TalentMarket/Service/Workbench/ServiceProvider/Add.tpl',
            [
                'authenticationIds' => $authenticationIds,
                'serviceCategories' => $serviceCategories,
                'permission' => $permission,
                'nav_left' => NavTrait::NAV_WORKBENCH['AUTHENTICATION'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_AUTHENTICATION']
            ]
        );
    }
}
