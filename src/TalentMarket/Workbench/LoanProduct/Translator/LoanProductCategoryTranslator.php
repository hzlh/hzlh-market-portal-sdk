<?php
namespace SuperMarket\TalentMarket\Workbench\LoanProduct\Translator;

use Sdk\Common\Translator\CategoryTranslator;

use Sdk\ProductMarket\LoanProduct\Model\NullLoanProductCategory;

class LoanProductCategoryTranslator extends CategoryTranslator
{
    public function arrayToObject(array $expression, $category = null)
    {
        unset($category);
        unset($expression);
        return new NullLoanProductCategory(0, '');
    }
}
