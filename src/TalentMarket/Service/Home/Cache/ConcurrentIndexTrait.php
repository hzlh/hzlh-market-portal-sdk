<?php
namespace SuperMarket\TalentMarket\Service\Home\Cache;

use SuperMarket\TalentMarket\Workbench\Service\Translator\ServiceTranslator;
use SuperMarket\TalentMarket\UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;
use Portal\Policy\Translator\PolicyTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;

trait ConcurrentIndexTrait
{
    protected function getServiceTranslator() : ServiceTranslator
    {
        return new ServiceTranslator();
    }

    protected function getServiceRequirementTranslator() : ServiceRequirementTranslator
    {
        return new ServiceRequirementTranslator();
    }

    protected function getPolicyTranslator() : PolicyTranslator
    {
        return new PolicyTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function indexArray($data)
    {
        $serviceCount = empty($data['service'][0]) ? 0 : $data['service'][0];
        $serviceList = empty($data['service'][0]) ? array() : $data['service'][1];
        $services = array();

        foreach ($serviceList as $key => $val) {
            $services[] = $this->getServiceTranslator()->objectToArray(
                $val,
                array(
                    'id',
                    'title',
                    'minPrice',
                    'volume',
                    'cover',
                    'enterprise'=>['id','logo','name'],
                    'serviceCategory'=>['name'],
                    'attentionDegree',
                    'pageViews',
                    'createTime'
                )
            );
        }

        $requirementCount = empty($data['requirements'][0]) ? 0 : $data['requirements'][0];
        $requirementsList = empty($data['requirements'][0]) ? array() : $data['requirements'][1];
        $requirements = array();
        foreach ($requirementsList as $key => $val) {
            $requirements[] = $this->getServiceRequirementTranslator()->objectToArray(
                $val,
                array(
                    'id',
                    'title',
                    'validityStartTime',
                    'validityEndTime',
                    'serviceCategory'=>['name'],
                    'minPrice',
                    'maxPrice',
                    'member'=>['id','avatar','nickName'],
                    'createTime'
                )
            );
        }

        $policyCount = empty($data['policy'][0]) ? 0 : $data['policy'][0];
        $policyList = empty($data['policy'][0]) ? array() : $data['policy'][1];
        $policies = array();
        foreach ($policyList as $key => $val) {
            $policies[] = $this->getPolicyTranslator()->objectToArray(
                $val,
                array(
                    'id',
                    'title',
                    'image',
                    'description',
                    'level',
                    'updateTime'
                )
            );
        }
        
        $enterpriseCount = empty($data['enterprise'][0]) ? 0 : $data['enterprise'][0];
        $enterpriseList = empty($data['enterprise'][0]) ? array() : $data['enterprise'][1];
        $enterprises = array();
        foreach ($enterpriseList as $key => $val) {
            $enterprises[] = $this->getEnterpriseTranslator()->objectToArray(
                $val
            );
        }

        $data['enterprise'] = $enterprises;
        $data['enterpriseCount'] = $enterpriseCount;
        $data['policy'] = $policies;
        $data['policyCount'] = $policyCount;
        $data['requirements'] = $requirements;
        $data['requirementCount'] = $requirementCount;
        $data['service'] = $services;
        $data['serviceCount'] = $serviceCount;

        return $data;
    }
}
