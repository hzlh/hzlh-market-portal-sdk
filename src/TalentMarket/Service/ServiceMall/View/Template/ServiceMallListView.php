<?php
namespace SuperMarket\TalentMarket\Service\ServiceMall\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Service\ServiceMall\View\ListViewTrait;

class ServiceMallListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $searchParameters = $this->searchParameters();

        $serviceList = $this->serviceList();

        $this->getView()->display(
            'TalentMarket/Service/ServiceMall/Index.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['TALENT_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['JOB_MALL'],
                'nav_phone' => NavTrait::NAV_PHONE['TALENT_HOME'],
                'searchParameters' => $searchParameters,
                'list' => $serviceList,
                'count' => $this->getCount()
            ]
        );
    }
}
