<?php
namespace SuperMarket\TalentMarket\Service\ServiceMall\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Service\ServiceMall\View\ViewTrait;

use Qxy\Contract\Statistical\Model\Statistical;

class ServiceMallView extends TemplateView implements IView
{
    use ViewTrait;

    const USAGE_NUM = 0;

    public function display()
    {
        $data = $this->data();

        $contractTemplate = $this->getContractTemplateItemList();

        $data['service'] = $this->contractTemplateItemStatus($data['service'], $contractTemplate);
       
        $collection = $this->getCollectionList();
        $collectionAll = $this->getCollectionAll();

        $enterpriseCollection = $this->getEnterpriseCollectionList();
        $enterpriseCollectionAll = $this->getEnterpriseCollectionAll();
     
        $this->getView()->display(
            'TalentMarket/Service/ServiceMall/ServiceDetail.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['TALENT_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['JOB_MALL'],
                'nav_phone' => NavTrait::NAV_PHONE['TALENT_HOME'],
                'service' => $data['service'],
                'storeServices' => $data['storeServices'],
                'sameServices' => $data['sameServices'],
                'evaluation' => $data['evaluation'],
                'commodityScore' => $data['commodityScore'],
                'enterpriseScore' => $data['enterpriseScore'],
                'myCoupon' => $this->coupon(),
                'total' => $data['evaluationCount'],
                'collection' => $collection,
                'collectionAll' => $collectionAll,
                'enterpriseCollection' => $enterpriseCollection,
                'enterpriseCollectionAll' => $enterpriseCollectionAll,
                'contractDisputeList'=>$this->getContractDisputeList(),
                'contractPerformanceCount'=>$this->getPerformanceStatics(),
                'enterpriseContractStatics'=>$this->getEnterpriseContractStaticsData(),
                'shareTemplateCount'=>intval($this->getShareTemplateCount()),
            ]
        );
    }

    protected function contractTemplateItemStatus(array $service, array $templateItem) : array
    {
        $service['template'] = [];
        foreach ($templateItem as $value) {
            if ($value['item'] == $service['id']) {
                $service['template'] = $value;
            }
        }

        return $service;
    }

    protected function getPerformanceStatics()
    {
        $performanceStatics = [];

        if (!empty($this->getContractPerformanceStatics())) {
            $performanceStatics = $this->contractStatisticalArray(
                $this->getContractPerformanceStatics()['type'],
                $this->getContractPerformanceStatics()['data']
            );
        }

        return array_values($performanceStatics);
    }

    protected function getContractUsageListAll()
    {
        $contractUsageLis = [];

        if (!empty($this->getContractUsageList())) {
            $contractUsageLis = $this->contractStatisticalArray(
                $this->getContractUsageList()['type'],
                $this->getContractUsageList()['data']
            );
        }

        return $contractUsageLis;
    }

    public function contractStatisticalArray(string $type, Statistical $statistical)
    {
        $data = array();

        $translator = $this->getStatisticalTranslator($type);

        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }

    protected function getContractDisputeList():array
    {

        $contractTemplateItemList = $this->getContractUsageListAll();
        $enterpriseList = $this->getAllEnterpriseList();

        $resultList = [];
        if (!empty($contractTemplateItemList) && !empty($enterpriseList)) {
            foreach ($enterpriseList as $key => $value) {
                foreach ($contractTemplateItemList as $tempKey => $tempVal) {
                    if ($tempVal['usageNum'] != self::USAGE_NUM) {
                        if (marmot_decode($value['id']) == $tempKey) {
                            $resultList[$key]=array_merge($value, $tempVal);
                        }
                    }
                }
            }
            $resultList = array_values($resultList);
        }

        return $resultList;
    }

    protected function getEnterpriseContractStaticsData()
    {
        $enterpriseContractStatics = [];

        if (!empty($this->getEnterpriseContractStatics())) {
            $enterpriseContractStatics = $this->contractStatisticalArray(
                $this->getEnterpriseContractStatics()['type'],
                $this->getEnterpriseContractStatics()['data']
            );
        }

        return array_values($enterpriseContractStatics);
    }
}
