<?php
namespace SuperMarket\TalentMarket\Service\RequirementSquare\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\TalentMarket\Service\RequirementSquare\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getList();

        $this->getView()->display(
            'TalentMarket/Service/RequirementSquare/List.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['TALENT_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['POSITION_MALL'],
                'nav_phone' => NavTrait::NAV_PHONE['TALENT_HOME'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
