<?php
namespace SuperMarket\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

class StaticsTranslatorFactory
{
    const MAPS = array(
        //产销超市
        'staticsProductMarketAuthenticationCount'=>
        'SuperMarket\Statistical\Translator\ProductMarket\StaticsServiceAuthenticationCountTranslator',
        'staticsProductMarketRequirementCount'=>
        'SuperMarket\Statistical\Translator\ProductMarket\StaticsServiceRequirementCountTranslator',
        'staticsProductMarketCount'=>
        'SuperMarket\Statistical\Translator\ProductMarket\StaticsServiceCountTranslator',

        //人才超市
        'staticsTalentMarketRequirementCount'=>
        'SuperMarket\Statistical\Translator\TalentMarket\StaticsServiceRequirementCountTranslator',

        //学习港
        'staticsLearnMarketRequirementCount'=>
        'SuperMarket\Statistical\Translator\LearnMarket\StaticsServiceRequirementCountTranslator',

    );

    public function getTranslator(string $type) : ITranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : false;
    }
}
