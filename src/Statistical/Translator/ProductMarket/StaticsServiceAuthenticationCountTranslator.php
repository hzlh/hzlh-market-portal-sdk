<?php
namespace SuperMarket\Statistical\Translator\ProductMarket;

use Marmot\Interfaces\ITranslator;

use Sdk\MarketStatical\Model\Statistical;
use Sdk\MarketStatical\Model\NullStatistical;

class StaticsServiceAuthenticationCountTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return NullStatistical::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();

        if (in_array('result', $keys)) {
            $expression = $statistical->getResult();
        }

        return $expression;
    }
}
