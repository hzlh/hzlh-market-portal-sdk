<?php
namespace SuperMarket\Statistical\Controller;

use Sdk\MarketStatical\Repository\StatisticalRepository;
use Sdk\MarketStatical\Adapter\StatisticalAdapterFactory;

trait StatisticalControllerTrait
{
    protected function getRepository(string $type) : StatisticalRepository
    {
        $adapterFactory = new StatisticalAdapterFactory();
        $adapter = $adapterFactory->getAdapter($type);

        return new StatisticalRepository(new $adapter);
    }

    public function analyse(string $type, array $filter = [])
    {
        $repository = $this->getRepository($type);

        $statistical = $repository->analyse($filter);

        return $statistical;
    }
}
