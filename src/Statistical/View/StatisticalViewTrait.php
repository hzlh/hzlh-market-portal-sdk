<?php
namespace SuperMarket\Statistical\View;

use SuperMarket\Statistical\Translator\StaticsTranslatorFactory;

use Sdk\MarketStatical\Model\Statistical;

trait StatisticalViewTrait
{
    protected function getStatisticalTranslator(string $type)
    {
        $translatorFactory = new StaticsTranslatorFactory();

        $translator = $translatorFactory->getTranslator($type);

        return new $translator;
    }

    public function statisticalArray(string $type, Statistical $statistical)
    {
        $data = array();

        $translator = $this->getStatisticalTranslator($type);

        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }
}
