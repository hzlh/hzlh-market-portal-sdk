<?php
namespace SuperMarket\StockMarkets\ServiceMall\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use Sdk\Collection\Model\Collection;
use Sdk\Common\Model\IApplyAble;
use Sdk\Service\Model\Service;
use Sdk\Service\Repository\ServiceRepository;
use Sdk\ServiceCategory\Repository\ServiceCategoryRepository;
use Sdk\MerchantCoupon\Repository\MerchantCouponRepository;
use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\Evaluation\Model\Evaluation;
use Sdk\Evaluation\Repository\EvaluationRepository;
use Sdk\EvaluationReply\Repository\EvaluationReplyRepository;

use SuperMarket\StockMarkets\ServiceMall\View\Template\ServiceMallListView;
use SuperMarket\StockMarkets\ServiceMall\View\Template\ServiceMallView;
use SuperMarket\StockMarkets\ServiceMall\View\Json\ServiceMallJsonListView;
use SuperMarket\StockMarkets\ServiceMall\View\Template\RelevantCouponsListView;
use SuperMarket\StockMarkets\ServiceMall\View\Json\RelevantCouponsJsonListView;
use SuperMarket\StockMarkets\ServiceMall\View\Json\ServiceEvaluationListView;

use UserCenter\Collection\Controller\CollectionTrait;

use Workbench\Service\Controller\ContractTemplateItemTrait;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait,
        FetchControllerTrait,
        ServiceMallControllerTrait,
        CollectionTrait,
        ContractTemplateItemTrait;

    const STATICS_TYPE = [
      'GOODS_SCORE' => 'staticsCommodityScore',
      'ENTERPRISE_SCORE' => 'staticsEnterpriseScore',
      'CONTRACT_PERFORMANCE_COUNT' => 'staticsContractPerformanceCount',
      'CONTRACT_USAGE_COUNT' =>'staticsContractUsageCount',
      'ENTERPRISE_CONTRACT_COUNT' =>'staticsEnterpriseContractCount'
    ];

    const PAGE = 0;

    const SIZE = [
        'SERVICE_LIST' => 20, //服务商城列表数量
        'RECOMMENDED_SERVICES' => 4 //详情页面同企业推荐服务数量
    ];

    private $serviceCategoryRepository;

    private $serviceRepository;

    private $merchantCouponRepository;

    private $concurrentAdapter;

    public function __construct()
    {
        parent::__construct();
        $this->merchantCouponRepository = new MerchantCouponRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->merchantCouponRepository);
        unset($this->serviceCategoryRepository);
        unset($this->serviceRepository);
        unset($this->concurrentAdapter);
    }

    protected function getMerchantCouponRepository() : MerchantCouponRepository
    {
        return $this->merchantCouponRepository;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function getConcurrentAdapter()
    {
        return $this->concurrentAdapter;
    }

    protected function getEvaluationRepository() : EvaluationRepository
    {
        return new EvaluationRepository();
    }

    protected function getEvaluationReplyRepository() : EvaluationReplyRepository
    {
        return new EvaluationReplyRepository();
    }
    /**
     *
     */
    protected function filterAction()
    {
        // 获取搜索参数列表
        $searchParameters = $this->fetchSearchParameters();

        $list =json_decode('[
          {
              "id": "Mywt",
              "number": "FW20210413414",
              "title": "北京商旅机构融资",
              "cover": {
                  "name": "siinlswj6d_1598923695917",
                  "identify": "siinlswj6d_1598923695917.png"
              },
              "detail": [
                  {
                      "old": "",
                      "type": "image",
                      "value": "block.jpeg",
                      "checked": "-2"
                  },
                  {
                      "type": "image",
                      "value": "5vi3acp2a2_1618283296362.png"
                  }
              ],
              "price": [
                  {
                      "name": "20",
                      "value": "300.00"
                  }
              ],
              "minPrice": "3.00",
              "maxPrice": "300.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 26,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1618283395,
              "createTimeFormat": "2021-04-13 11:09:55",
              "updateTime": 1619491561,
              "updateTimeFormat": "2021-04-27 10:46:01",
              "serviceObjects": [
                  {
                      "id": 7,
                      "name": "其他"
                  }
              ],
              "serviceCategory": {
                  "id": "NA",
                  "name": "企业变更",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MQ",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "22"
          },
          {
              "id": "Mysw",
              "number": "FW20210413407",
              "title": "河南供应链新零售项目融资",
              "cover": {
                  "name": "ukzys8lbt7_1618282008130",
                  "identify": "ukzys8lbt7_1618282008130.jpg"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>河南供应链新零售项目融资</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "50000.00"
                  }
              ],
              "minPrice": "50000.00",
              "maxPrice": "50000.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 67,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "bohui",
              "createTime": 1618282021,
              "createTimeFormat": "2021-04-13 10:47:01",
              "updateTime": 1619415584,
              "updateTimeFormat": "2021-04-26 13:39:44",
              "serviceObjects": [],
              "serviceCategory": {
                  "id": "MA",
                  "name": "人力资源管理咨询",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 0,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "162"
          },
          {
              "id": "My0x",
              "number": "FW20210413428",
              "title": "广西品牌营销管理系统融资",
              "cover": {
                  "name": "siinlswj6d_1598923695917",
                  "identify": "siinlswj6d_1598923695917.png"
              },
              "detail": [
                  {
                      "type": "image",
                      "value": "m73q5c6hb7_1618314871431.png"
                  },
                  {
                      "type": "image",
                      "value": "tg18xggsww_1618314886516.png"
                  }
              ],
              "price": [
                  {
                      "name": "gg",
                      "value": "1.00"
                  }
              ],
              "minPrice": "1.00",
              "maxPrice": "1.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 18,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1618314895,
              "createTimeFormat": "2021-04-13 19:54:55",
              "updateTime": 1619341814,
              "updateTimeFormat": "2021-04-25 17:10:14",
              "serviceObjects": [],
              "serviceCategory": {
                  "id": "NA",
                  "name": "企业变更",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MQ",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "218,267"
          },
          {
              "id": "MTEw",
              "number": "FW20201224267",
              "title": "浙江线上教育平台项目融资",
              "cover": {
                  "name": "siinlswj6d_1598923695917",
                  "identify": "siinlswj6d_1598923695917.png"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>浙江线上教育平台项目融资</p>"
                  }
              ],
              "price": [
                  {
                      "name": "10",
                      "value": "0.10"
                  }
              ],
              "minPrice": "0.10",
              "maxPrice": "0.10",
              "contract": [],
              "attentionDegree": 0,
              "volume": 1,
              "pageViews": 84,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "212",
              "createTime": 1608779035,
              "createTimeFormat": "2020-12-24 11:03:55",
              "updateTime": 1618984438,
              "updateTimeFormat": "2021-04-21 13:53:58",
              "serviceObjects": [
                  {
                      "id": 2,
                      "name": "小型企业"
                  },
                  {
                      "id": 6,
                      "name": "个体商户"
                  }
              ],
              "serviceCategory": {
                  "id": "NS0",
                  "name": "其他",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "NA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
              },
              "tag": "15,22,29,85,92"
          },
          {
              "id": "MDQ",
              "number": "FW2020090119",
              "title": "浙江线下时尚平台实体店融资",
              "cover": {
                  "name": "zqk7w29ciu_1598952000004",
                  "identify": "zqk7w29ciu_1598952000004.jpg"
              },
              "detail": [
                {
                  "type": "text",
                  "value": "<p>浙江线下时尚平台实体店融资</p>"
              }
              ],
              "price": [
                  {
                      "name": "一次",
                      "value": "7000.00"
                  }
              ],
              "minPrice": "7000.00",
              "maxPrice": "7000.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 232,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1598952018,
              "createTimeFormat": "2020-09-01 17:20:18",
              "updateTime": 1618984430,
              "updateTimeFormat": "2021-04-21 13:53:50",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  }
              ],
              "serviceCategory": {
                  "id": "Mis",
                  "name": "核心专利代理",
                  "qualificationName": "专利代理机构注册证",
                  "isQualification": 2,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
              },
              "tag": "15,22,29,85,92"
          },
          {
              "id": "Niw",
              "number": "FW2020102971",
              "title": "四川家政服务和废品回收信息服务平台融资",
              "cover": {
                  "name": "15c3ocyj54_1603962535439",
                  "identify": "15c3ocyj54_1603962535439.jpg"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>四川家政服务和废品回收信息服务平台融资</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "300.00"
                  }
              ],
              "minPrice": "300.00",
              "maxPrice": "300.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 2,
              "pageViews": 240,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1603962667,
              "createTimeFormat": "2020-10-29 17:11:07",
              "updateTime": 1618984427,
              "updateTimeFormat": "2021-04-21 13:53:47",
              "serviceObjects": [],
              "serviceCategory": {
                  "id": "NS0",
                  "name": "其他",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "NA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
              },
              "tag": "43,50,99,106,113"
          },
          {
              "id": "MTM",
              "number": "FW2020091828",
              "title": "四川太阳能浮台无线水质监测站融资",
              "cover": {
                  "name": "xgjehyrr64_1616639979206",
                  "identify": "xgjehyrr64_1616639979206.png"
              },
              "detail": [
                {
                  "type": "text",
                  "value": "<p>四川太阳能浮台无线水质监测站融资</p>"
                },
                  {
                      "type": "image",
                      "value": "w39o1otu32_1616640038136.jpg"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "5888.00"
                  }
              ],
              "minPrice": "5888.00",
              "maxPrice": "5888.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 4,
              "pageViews": 408,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "caichaocoajvad",
              "createTime": 1600416014,
              "createTimeFormat": "2020-09-18 16:00:14",
              "updateTime": 1618984424,
              "updateTimeFormat": "2021-04-21 13:53:44",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 2,
                      "name": "小型企业"
                  },
                  {
                      "id": 3,
                      "name": "中型企业"
                  },
                  {
                      "id": 4,
                      "name": "大型企业"
                  },
                  {
                      "id": 5,
                      "name": "创业个人"
                  },
                  {
                      "id": 6,
                      "name": "个体商户"
                  },
                  {
                      "id": 7,
                      "name": "其他"
                  }
              ],
              "serviceCategory": {
                  "id": "Mg",
                  "name": "招聘外包",
                  "qualificationName": "人才中介服务许可证",
                  "isQualification": 0,
                  "isEnterpriseVerify": 0,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "OA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
              },
              "tag": "43,50,99,106,113"
          },
          {
              "id": "MS8v",
              "number": "FW20201212246",
              "title": "江西奶茶融资",
              "cover": {
                  "name": "7fvqzs6xpo_1616640603210",
                  "identify": "7fvqzs6xpo_1616640603210.png"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p><span style=\"color: #333333; font-family: 微软雅黑; font-size: 15px; background-color: #ffffff;\">一次性提交资料，全程托管，手机随时查看办理进度；<br /></span><span style=\"color: #333333; font-family: 微软雅黑; font-size: 15px; background-color: #ffffff;\">价格优惠透明，明码标价签订记账合同，全程无隐形收费；<br /></span><span style=\"color: #333333; font-family: 微软雅黑; font-size: 15px; background-color: #ffffff;\">专业业务上门收取票据，为您省时省心。</span></p>"
                  },
                  {
                      "type": "image",
                      "value": "sgrhf441hc_1616639706612.jpg"
                  }
              ],
              "price": [
                  {
                      "name": "小规模纳税人0申报/年",
                      "value": "1000.00"
                  },
                  {
                      "name": "小规模纳税人正常户/两年",
                      "value": "4000.00"
                  }
              ],
              "minPrice": "1000.00",
              "maxPrice": "4000.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 4,
              "pageViews": 350,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1607761221,
              "createTimeFormat": "2020-12-12 16:20:21",
              "updateTime": 1618984416,
              "updateTimeFormat": "2021-04-21 13:53:36",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 2,
                      "name": "小型企业"
                  },
                  {
                      "id": 3,
                      "name": "中型企业"
                  },
                  {
                      "id": 4,
                      "name": "大型企业"
                  },
                  {
                      "id": 5,
                      "name": "创业个人"
                  },
                  {
                      "id": 6,
                      "name": "个体商户"
                  }
              ],
              "serviceCategory": {
                  "id": "NCw",
                  "name": "财税管理培训",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MQ",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
              },
              "tag": "43,50,99,106,113"
          },
          {
              "id": "My0q",
              "number": "FW20210413421",
              "title": "广东高校服务通融资",
              "cover": {
                  "name": "ooyxdy738j_1612924397346",
                  "identify": "ooyxdy738j_1612924397346.jpg"
              },
              "detail": [
                  {
                      "old": "grixshmcrr_1618283287819.jpeg",
                      "type": "image",
                      "value": "block.jpeg",
                      "checked": "-2"
                  },
                  {
                      "type": "image",
                      "value": "5vi3acp2a2_1618283296362.png"
                  }
              ],
              "price": [
                  {
                      "name": "20",
                      "value": "3.00"
                  }
              ],
              "minPrice": "3.00",
              "maxPrice": "3.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 35,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1618283489,
              "createTimeFormat": "2021-04-13 11:11:29",
              "updateTime": 1618293324,
              "updateTimeFormat": "2021-04-13 13:55:24",
              "serviceObjects": [
                  {
                      "id": 7,
                      "name": "其他"
                  }
              ],
              "serviceCategory": {
                  "id": "NA",
                  "name": "企业变更",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MQ",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "22"
          },
          {
              "id": "MjIy",
              "number": "FW20210223379",
              "title": "广东3c电子项目融资",
              "cover": {
                  "name": "ggrvabd5im_1614060894527",
                  "identify": "ggrvabd5im_1614060894527.png"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>广东3c电子项目融资</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "1.00"
                  }
              ],
              "minPrice": "1.00",
              "maxPrice": "1.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 11,
              "pageViews": 269,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1614060959,
              "createTimeFormat": "2021-02-23 14:15:59",
              "updateTime": 1614060976,
              "updateTimeFormat": "2021-02-23 14:16:16",
              "serviceObjects": [],
              "serviceCategory": {
                  "id": "MA",
                  "name": "人力资源管理咨询",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 0,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "8,57,155,204,253"
          },
          {
              "id": "MjAx",
              "number": "FW20210210358",
              "title": "江西高校服务通融资",
              "cover": {
                  "name": "www4ntnomc_1612924025460",
                  "identify": "www4ntnomc_1612924025460.png"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>人事招聘</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "1.00"
                  }
              ],
              "minPrice": "1.00",
              "maxPrice": "1.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 136,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1612924157,
              "createTimeFormat": "2021-02-10 10:29:17",
              "updateTime": 1612924459,
              "updateTimeFormat": "2021-02-10 10:34:19",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 5,
                      "name": "创业个人"
                  }
              ],
              "serviceCategory": {
                  "id": "Mg",
                  "name": "招聘外包",
                  "qualificationName": "人才中介服务许可证",
                  "isQualification": 0,
                  "isEnterpriseVerify": 0,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "OA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "8,267,274,134,127"
          },
          {
              "id": "MjEu",
              "number": "FW20210210365",
              "title": "浙江药本药中药饮片线上批发平台融资",
              "cover": {
                  "name": "rpvbarnfzk_1612924304533",
                  "identify": "rpvbarnfzk_1612924304533.jpg"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>浙江药本药中药饮片线上批发平台融资</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "1.00"
                  }
              ],
              "minPrice": "1.00",
              "maxPrice": "1.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 101,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1612924367,
              "createTimeFormat": "2021-02-10 10:32:47",
              "updateTime": 1612924454,
              "updateTimeFormat": "2021-02-10 10:34:14",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 2,
                      "name": "小型企业"
                  },
                  {
                      "id": 3,
                      "name": "中型企业"
                  },
                  {
                      "id": 4,
                      "name": "大型企业"
                  },
                  {
                      "id": 5,
                      "name": "创业个人"
                  },
                  {
                      "id": 6,
                      "name": "个体商户"
                  },
                  {
                      "id": 7,
                      "name": "其他"
                  }
              ],
              "serviceCategory": {
                  "id": "NDQ",
                  "name": "场地租赁",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "OA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "8,267,274,134,127"
          },
          {
              "id": "MjIr",
              "number": "FW20210210372",
              "title": "浙江防火系统融资",
              "cover": {
                  "name": "ooyxdy738j_1612924397346",
                  "identify": "ooyxdy738j_1612924397346.jpg"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>浙江防火系统融资</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "1.00"
                  }
              ],
              "minPrice": "1.00",
              "maxPrice": "1.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 90,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1612924441,
              "createTimeFormat": "2021-02-10 10:34:01",
              "updateTime": 1612924451,
              "updateTimeFormat": "2021-02-10 10:34:11",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 2,
                      "name": "小型企业"
                  },
                  {
                      "id": 3,
                      "name": "中型企业"
                  },
                  {
                      "id": 4,
                      "name": "大型企业"
                  },
                  {
                      "id": 5,
                      "name": "创业个人"
                  },
                  {
                      "id": 6,
                      "name": "个体商户"
                  },
                  {
                      "id": 7,
                      "name": "其他"
                  }
              ],
              "serviceCategory": {
                  "id": "Mw",
                  "name": "公司设立",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MQ",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "8,267,274,134,127"
          },
          {
              "id": "MjAq",
              "number": "FW20210210351",
              "title": "浙江笑迎计划融资",
              "cover": {
                  "name": "aiyg9npd2m_1612921889210",
                  "identify": "aiyg9npd2m_1612921889210.png"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>浙江笑迎计划融资</p><p>您的第一选择</p>"
                  },
                  {
                      "type": "image",
                      "value": "g1ayan4kaq_1612921931303.png"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "1.00"
                  }
              ],
              "minPrice": "1.00",
              "maxPrice": "1.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 126,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1612921948,
              "createTimeFormat": "2021-02-10 09:52:28",
              "updateTime": 1612921960,
              "updateTimeFormat": "2021-02-10 09:52:40",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 2,
                      "name": "小型企业"
                  },
                  {
                      "id": 3,
                      "name": "中型企业"
                  },
                  {
                      "id": 4,
                      "name": "大型企业"
                  },
                  {
                      "id": 5,
                      "name": "创业个人"
                  },
                  {
                      "id": 6,
                      "name": "个体商户"
                  },
                  {
                      "id": 7,
                      "name": "其他"
                  }
              ],
              "serviceCategory": {
                  "id": "MQ",
                  "name": "人事外包",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 2,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "8,267,274,134,141,148,85,99,113"
          },
          {
              "id": "Mi8t",
              "number": "FW20210204344",
              "title": "湖北校园生活服务社交平台项目",
              "cover": {
                  "name": "wpyttzl9zf_1612411134004",
                  "identify": "wpyttzl9zf_1612411134004.png"
              },
              "detail": [
                  {
                      "type": "text",
                      "value": "<p>湖北校园生活服务社交平台项目</p>"
                  },
                  {
                      "type": "image",
                      "value": "ibafxdknrd_1612861167196.png"
                  },
                  {
                      "type": "text",
                      "value": "<p>tableview 加载html标签自适应</p>"
                  },
                  {
                      "type": "text",
                      "value": "<p>tableview 加载html标签自适应tableview 加载html标签自适应tableview 加载html标签自适应tableview 加载html标签自适应tableview 加载html标签自适应</p>"
                  }
              ],
              "price": [
                  {
                      "name": "1",
                      "value": "50.00"
                  }
              ],
              "minPrice": "50.00",
              "maxPrice": "50.00",
              "contract": [],
              "attentionDegree": 0,
              "volume": 0,
              "pageViews": 121,
              "applyStatus": 2,
              "status": 0,
              "rejectReason": "",
              "createTime": 1612411147,
              "createTimeFormat": "2021-02-04 11:59:07",
              "updateTime": 1612861221,
              "updateTimeFormat": "2021-02-09 17:00:21",
              "serviceObjects": [
                  {
                      "id": 1,
                      "name": "微型企业"
                  },
                  {
                      "id": 2,
                      "name": "小型企业"
                  }
              ],
              "serviceCategory": {
                  "id": "MA",
                  "name": "人力资源管理咨询",
                  "qualificationName": "",
                  "isQualification": 0,
                  "isEnterpriseVerify": 0,
                  "commission": 5,
                  "status": 0,
                  "parentCategory": {
                      "id": "MA",
                      "name": "",
                      "status": 0
                  }
              },
              "enterprise": {
                "id": "NA",
                "name": "湖北翰林建设劳务有限公司",
                "enterpriseName": "湖***司",
                "unifiedSocialCreditCode": "91420500691796H522",
                "tag": "",
                "logo": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "businessLicense": {
                    "name": "1c6x13xpjy_1598923763457",
                    "identify": "1c6x13xpjy_1598923763457.jpg"
                },
                "powerAttorney": {
                    "name": "",
                    "identify": ""
                },
                "contactsName": "王洋",
                "contactsCellphone": "13621267859",
                "contactsArea": "湖北省,宜昌市,西陵区",
                "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                "legalPersonName": "",
                "legalPersonCardId": "",
                "legalPersonPositivePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonReversePhoto": {
                    "name": "",
                    "identify": ""
                },
                "legalPersonHandheldPhoto": {
                    "name": "",
                    "identify": ""
                },
                "member": {
                    "id": "NA",
                    "avatar": [],
                    "nickName": "",
                    "gender": 1,
                    "birthday": "0000-00-00",
                    "area": "",
                    "address": "",
                    "briefIntroduction": "",
                    "cellphone": "",
                    "cellphoneMask": "",
                    "realName": "",
                    "userName": "",
                    "createTime": 0,
                    "createTimeFormat": "1970-01-01",
                    "updateTime": 0,
                    "updateTimeFormat": "1970-01-01",
                    "tag": ""
                },
                "realNameAuthenticationStatus": 2
              },
              "tag": "162,169"
          }
      ]',true);
      $searchParameters['parentCategories']= array(
        array(
            "id" => "MA",
            "name" => "债权融资"
        ),
        array(
            "id" => "MQ",
            "name" => "股权融资"
        ),
        array(
            "id" => "Mg",
            "name" => "内部融资"
        ),
        array(
            "id" => "Mw",
            "name" => "项目融资"
        ),
        array(
            "id" => "NA",
            "name" => "政策融资"
            ),
        array(
            "id" => "NQ",
            "name" => "贸易融资"
        ),
      );
      $searchParameters['serviceCategories'] = array(
        array(
            "id" => "MA",
            "name" => "国内银行贷款",
            "pid" => "MA"
        ),
       array(
               "id" => "MQ",
               "name" => "国外银行贷款",
               "pid" => "MA"
       ),
       array(
               "id" => "Mg",
               "name" => "企业发行债券融资",
               "pid" => "MA"
       ),
       array(
               "id" => "Mw",
               "name" => "民间借贷融资",
               "pid" => "MA"
       ),
       array(
               "id" => "NA",
               "name" => "信用担保融资",
               "pid" => "MA"
           ),
       array(
              "id" => "NQ",
              "name" => "租赁融资",
              "pid" => "MA"
       ),
       array(
            "id" => "Ng",
            "name" => "股权出让融资",
            "pid" => "MQ"
      ),
      array(
              "id" => "Nw",
              "name" => "产权交易融资",
              "pid" => "MQ"
      ),
      array(
              "id" => "OA",
              "name" => "增资扩股融资",
              "pid" => "MQ"
      ),
      array(
              "id" => "MCs",
              "name" => "杠杠收购融资",
              "pid" => "MQ"
      ),
      array(
              "id" => "MCw",
              "name" => "风险投资融资",
              "pid" => "MQ"
          ),
      array(
            "id" => "MC0",
            "name" => "私募股权融资",
            "pid" => "MQ"
      ),

      array(
        "id" => "MC4",
        "name" => "留存盈余融资",
        "pid" => "Mg"
      ),
      array(
              "id" => "MC8",
              "name" => "应收账款融资",
              "pid" => "Mg"
      ),
      array(
              "id" => "MDA",
              "name" => "资产典当融资",
              "pid" => "Mg"
      ),
      array(
              "id" => "MDE",
              "name" => "商业信用融资",
              "pid" => "Mg"
      ),
      array(
              "id" => "MDI",
              "name" => "信用证融资",
              "pid" => "Mg"
          ),
      array(
            "id" => "MDM",
            "name" => "福费廷融资",
            "pid" => "Mg"
      ),

      array(
        "id" => "MDQ",
        "name" => "项目包装融资",
        "pid" => "Mw"
      ),
      array(
              "id" => "MSs",
              "name" => "BOT项目融资",
              "pid" => "Mw"
      ),
      array(
              "id" => "MSw",
              "name" => "IFC国际融资",
              "pid" => "Mw"
      ),
      array(
              "id" => "MS0",
              "name" => "高新技术融资",
              "pid" => "Mw"
      ),
      array(
              "id" => "MDI",
              "name" => "专项资金融资",
              "pid" => "Mw"
          ),
      array(
            "id" => "MS4",
            "name" => "产业政策融资",
            "pid" => "NA"
      ),

      array(
        "id" => "MS8",
        "name" => "国际保理融资",
        "pid" => "NQ"
      ),
      array(
              "id" => "MTA",
              "name" => "打包放款融资",
              "pid" => "NQ"
      ),
      array(
              "id" => "MTE",
              "name" => "补偿贸易融资",
              "pid" => "NQ"
          ),
      array(
            "id" => "MTI",
            "name" => "出口信贷融资",
            "pid" => "NQ"
      )
    );

        $this->render(new ServiceMallListView(15, $list, $searchParameters));
        return true;
    }

    protected function filterFormatChange()
    {
        $parentCategory = $this->getRequest()->get('parentCategory', '');
        $serviceCategory = $this->getRequest()->get('serviceCategory', array());
        $serviceObjects = $this->getRequest()->get('serviceObject', array());
        $minPrice = $this->getRequest()->get('minPrice', 0);
        $maxPrice = $this->getRequest()->get('maxPrice', 0);
        $contract = $this->getRequest()->get('contract', 0);
        $title = $this->getRequest()->get('title', '');
        $sort = $this->getRequest()->get('sort', array());

        $tag = Core::$container->get('user')->getBinaries();
        if (!empty($tag)) {
            $sort['tag'] = $tag;
        }

        $sort = empty($sort) ? ['-updateTime'] : $sort;

        $filter = [];
        $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        $filter['title'] = $title;
        $filter['minPriceRange'] = $minPrice;
        $filter['maxPriceRange'] = $maxPrice;
        $filter['contract'] = $contract;

        if (!empty($parentCategory) && !empty($serviceCategory)) {
            unset($parentCategory);
        }

        if (!empty($parentCategory)) {
            $filter['parentCategory'] = marmot_decode($parentCategory);
        }
        if (!empty($serviceCategory)) {
            $filter['serviceCategory'] = $this->implodeArray($serviceCategory);
        }
        if (!empty($serviceObjects)) {
            $filter['serviceObjects'] = $this->implodeArray($serviceObjects);
        }

        return [$filter, $sort];
    }
    /**
     *
     */
    protected function implodeArray(array $array) : string
    {
        $idDecodeData = array();

        foreach ($array as $val) {
            $idDecodeData[] = marmot_decode($val);
        }

        return implode(',', $idDecodeData);
    }
    /**
     *
     */
    protected function fetchOneAction(int $id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        $serviceRepository = $this->getServiceRepository();

        $service = $serviceRepository->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)->portalFetchOne($id);

        if ($service instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        list($filterStore, $filterSame ,$sort) = $this->filterRecommendedFormatChange($service);

        // 获取推荐服务列表
        $this->getConcurrentAdapter()->addPromise(
            'storeServices',
            $serviceRepository->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($filterStore, $sort, self::PAGE, self::SIZE['RECOMMENDED_SERVICES']),
            $serviceRepository->getAdapter()
        );
        $this->getConcurrentAdapter()->addPromise(
            'sameServices',
            $serviceRepository->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($filterSame, $sort, self::PAGE, self::SIZE['RECOMMENDED_SERVICES']),
            $serviceRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        list($count, $couponList) = $this->fetchCoupon($service->getEnterprise()->getId());
        unset($count);

        //获取商品评价数据
        list($evaluationCount,$evaluations) = $this->filterEvaluation($id);
        $staticsList = [];
        $staticsList = $this->fetchStatics($id);
        // 服务收藏
        list($collection, $collectionAll) = $this->collectionInfoList(
            Collection::COLLECTION_CATEGORY['SERVICE'],
            $id
        );
        list($enterpriseCollection, $enterpriseCollectionAll) = $this->collectionInfoList(
            Collection::COLLECTION_CATEGORY['ENTERPRISE'],
            $service->getEnterprise()->getId()
        );

        $evaluationReplies = array();
        $enterpriseStaticsList = array();

        if (!empty($evaluations)) {
            $evaluationReplies = $this->filterEvaluationReplies($evaluations);
            $enterpriseStaticsList = $this->filterEnterpriseStatics($evaluations);
        }

        $collectionArray = array(
            'collection' => $collection,//用户收藏列表
            'collectionAll' => $collectionAll,//商品收藏总数
            'enterpriseCollection' => $enterpriseCollection,
            'enterpriseCollectionAll' => $enterpriseCollectionAll,
        );
        $contractTemplate = [];
        $contractTemplate = $this->filterContractTemplateItem($service);

        $contractPerformanceStatics = [];
        $enterpriseList = [];
        $contractUsageList = [];
        if (!empty($contractTemplate)) {
            //获取合同模版履约情况和纠纷率的统计
            $templateId = $this->getTemplateIds($contractTemplate);
            $contractPerformanceStatics = $this->fetchPerformanceStatics($templateId, self::STATICS_TYPE['CONTRACT_PERFORMANCE_COUNT']);//phpcs:ignore

            //获取合同模版的违约次数和使用次数
            $contractUsageList = $this->fetchPerformanceStatics($templateId, self::STATICS_TYPE['CONTRACT_USAGE_COUNT']);//phpcs:ignore
            if (!empty($contractUsageList)) {
                $enterpriseIds = $this->getListIds($contractUsageList['data']->getResult());
                $enterpriseList = $this->getEnterpriseList([$enterpriseIds]);
            }
        }

        //获取商家履约情况数据
        $enterpriseContractStatics = [];
        $enterpriseContractStatics = $this->fetchPerformanceStatics($service->getEnterprise()->getId(), self::STATICS_TYPE['ENTERPRISE_CONTRACT_COUNT']);//phpcs:ignore

        //获取系统模版数量
        $shareTemplateCount = $this->getSystemTemplateData();

        if ($this->getRequest()->isAjax()) {
            $this->render(new ServiceEvaluationListView(
                $service,
                $data,
                $couponList,
                $staticsList,
                $evaluationReplies,
                $enterpriseStaticsList,
                $evaluations,
                $evaluationCount,
                $collectionArray,
                $contractTemplate
            ));
            return true;
        }

        $this->render(new ServiceMallView(
            $service,
            $data,
            $couponList,
            $staticsList,
            $evaluationReplies,
            $enterpriseStaticsList,
            $evaluations,
            $evaluationCount,
            $collectionArray,
            $contractTemplate,
            $enterpriseList,
            $contractUsageList,
            $contractPerformanceStatics,
            $enterpriseContractStatics,
            $shareTemplateCount
        ));
        return true;
    }
    /**
     *
     */
    public function filterRecommendedFormatChange(Service $service)
    {
        $enterpriseId = $service->getEnterprise()->getId();
        $serviceCategoryId = $service->getServiceCategory()->getId();

        $sort = ['-updateTime'];

        $filterStore['enterprise'] = $enterpriseId;
        $filterStore['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $filterStore['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        $filterSame['serviceCategory'] = $serviceCategoryId;
        $filterSame['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $filterSame['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        return [$filterStore, $filterSame, $sort];
    }

    /**
     *
     */
    public function fetchRelevantCoupons()
    {
        $enterprise = $this->getRequest()->get('enterpriseId', '');

        list($count, $couponList) = $this->fetchCoupon(marmot_decode($enterprise));

        if ($this->getRequest()->isAjax()) {
            $this->render(new RelevantCouponsJsonListView($count, $couponList));
            return true;
        }

        $this->render(new RelevantCouponsListView($count, $couponList));
        return true;
    }

    /**
     *
     */
    protected function fetchCoupon($enterpriseId)
    {
        list($page, $size) = $this->getPageAndSize(COMMON_SIZE);
        list($filter, $sort) = $this->filterRelevantCouponsFormatChange($enterpriseId);

        $couponList = array();
        list($count, $couponList) = $this->getMerchantCouponRepository()
            ->scenario(MerchantCouponRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        return [$count, $couponList];
    }

    /**
     *
     */
    protected function filterRelevantCouponsFormatChange($enterpriseId)
    {
        $sort = ['-updateTime'];
        $filter = array();

        $filter['releaseType'] = MerchantCoupon::RELEASE_TYPE['MERCHANT'];  //默认商家优惠劵
        $filter['status'] = MerchantCoupon::STATUS['NORMAL'];   //默认正常使用
        $filter['enterpriseId'] = $enterpriseId;

        return [$filter, $sort];
    }

    protected function filterEvaluation(int $serviceId) : array
    {
        $filter = [];
        $filter['relation'] = $serviceId;
        $filter['status'] = Evaluation::STATUS['NORMAL'];
        $filter['relationCategory'] = Evaluation::RELATION_CATEGORY['SERVICE'];
        $sort = ['-createTime'];

        list($page, $size) = $this->getPageAndSize(FORM_SIZE);

        $evaluationType = marmot_decode($this->getRequest()->get('evaluationType', ''));

        if (!empty($evaluationType)) {
            $filter['evaluationType'] = $evaluationType;
        }

        list($count, $list) = $this->getEvaluationRepository()
            ->scenario(EvaluationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        return [$count,$list];
    }

    protected function filterEvaluationReplies(array $evaluations) : array
    {
        $evaluationIds = [];
        foreach ($evaluations as $evaluation) {
            $evaluationIds[] = $evaluation->getId();
        }
        $sort = ['-createTime'];

        $filter=[];
        $filter['evaluation'] = join(',', $evaluationIds);

        list($count, $evaluationReplyList) = $this->getEvaluationReplyRepository()
             ->scenario(EvaluationReplyRepository::LIST_MODEL_UN)
             ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $evaluationReplyList;
    }

    protected function filterEnterpriseStatics(array $evaluations) : array
    {
        $enterpriseIds = [];
        foreach ($evaluations as $evaluation) {
            $enterpriseIds[] = $evaluation->getEnterprise()->getId();
        }

        $filter=[];
        $filter['enterpriseId'] = $enterpriseIds[0];

        $enterpriseStaticsList = $this->analyse(self::STATICS_TYPE['ENTERPRISE_SCORE'], $filter);

        return  ["data"=>$enterpriseStaticsList,"type"=>self::STATICS_TYPE['ENTERPRISE_SCORE']];
    }

    protected function fetchStatics(int $serviceId)
    {
        $filter = [];
        $filter['serviceId'] = $serviceId;
        $filter['relationCategory'] = Evaluation::RELATION_CATEGORY['SERVICE'];

        $staticsList = $this->analyse(self::STATICS_TYPE['GOODS_SCORE'], $filter);
        return ["data"=>$staticsList,"type"=>self::STATICS_TYPE['GOODS_SCORE']];
    }
}
