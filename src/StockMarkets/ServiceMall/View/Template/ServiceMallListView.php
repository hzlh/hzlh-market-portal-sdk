<?php
namespace SuperMarket\StockMarkets\ServiceMall\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\StockMarkets\ServiceMall\View\ListViewTrait;

class ServiceMallListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $searchParameters = $this->searchParameters();

        $serviceList = $this->getList();

        $this->getView()->display(
            'StockMarkets/ServiceList.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['ROADSHOW_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['PROJECT'],
                'nav_phone' => NavTrait::NAV_PHONE['PROJECT'],
                'searchParameters' => $searchParameters,
                'list' => $serviceList,
                'count' => $this->getCount()
            ]
        );
    }
}
