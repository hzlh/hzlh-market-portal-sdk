<?php
namespace SuperMarket\StockMarkets\ServiceMall\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\StockMarkets\ServiceMall\View\ViewTrait;

use Qxy\Contract\Statistical\Model\Statistical;

class ServiceMallView extends TemplateView implements IView
{
    use ViewTrait;

    const USAGE_NUM = 0;

    public function display()
    {
        $data = $this->data();

        $contractTemplate = $this->getContractTemplateItemList();

        $data['service'] = $this->contractTemplateItemStatus($data['service'], $contractTemplate);

        $collection = $this->getCollectionList();
        $collectionAll = $this->getCollectionAll();

        $enterpriseCollection = $this->getEnterpriseCollectionList();
        $enterpriseCollectionAll = $this->getEnterpriseCollectionAll();

        $data['service'] = $this->getDataTitle($data['service']);

        $this->getView()->display(
            'StockMarkets/ServiceDetail.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['ROADSHOW_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['PROJECT'],
                'nav_phone' => NavTrait::NAV_PHONE['PROJECT'],
                'service' => $data['service'],
                'storeServices' => $data['storeServices'],
                'sameServices' => $data['sameServices'],
                'evaluation' => $data['evaluation'],
                'commodityScore' => $data['commodityScore'],
                'enterpriseScore' => $data['enterpriseScore'],
                'myCoupon' => $this->coupon(),
                'total' => $data['evaluationCount'],
                'collection' => $collection,
                'collectionAll' => $collectionAll,
                'enterpriseCollection' => $enterpriseCollection,
                'enterpriseCollectionAll' => $enterpriseCollectionAll,
                'contractDisputeList'=>$this->getContractDisputeList(),
                'contractPerformanceCount'=>$this->getPerformanceStatics(),
                'enterpriseContractStatics'=>$this->getEnterpriseContractStaticsData(),
                'shareTemplateCount'=>intval($this->getShareTemplateCount()),
            ]
        );
    }

    protected function contractTemplateItemStatus(array $service, array $templateItem) : array
    {
        $service['template'] = [];
        foreach ($templateItem as $value) {
            if ($value['item'] == $service['id']) {
                $service['template'] = $value;
            }
        }

        return $service;
    }

    protected function getPerformanceStatics()
    {
        $performanceStatics = [];

        if (!empty($this->getContractPerformanceStatics())) {
            $performanceStatics = $this->contractStatisticalArray(
                $this->getContractPerformanceStatics()['type'],
                $this->getContractPerformanceStatics()['data']
            );
        }

        return array_values($performanceStatics);
    }

    protected function getContractUsageListAll()
    {
        $contractUsageLis = [];

        if (!empty($this->getContractUsageList())) {
            $contractUsageLis = $this->contractStatisticalArray(
                $this->getContractUsageList()['type'],
                $this->getContractUsageList()['data']
            );
        }

        return $contractUsageLis;
    }

    public function contractStatisticalArray(string $type, Statistical $statistical)
    {
        $data = array();

        $translator = $this->getStatisticalTranslator($type);

        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }

    protected function getContractDisputeList():array
    {

        $contractTemplateItemList = $this->getContractUsageListAll();
        $enterpriseList = $this->getAllEnterpriseList();

        $resultList = [];
        if (!empty($contractTemplateItemList) && !empty($enterpriseList)) {
            foreach ($enterpriseList as $key => $value) {
                foreach ($contractTemplateItemList as $tempKey => $tempVal) {
                    if ($tempVal['usageNum'] != self::USAGE_NUM) {
                        if (marmot_decode($value['id']) == $tempKey) {
                            $resultList[$key]=array_merge($value, $tempVal);
                        }
                    }
                }
            }
            $resultList = array_values($resultList);
        }

        return $resultList;
    }

    protected function getEnterpriseContractStaticsData()
    {
        $enterpriseContractStatics = [];

        if (!empty($this->getEnterpriseContractStatics())) {
            $enterpriseContractStatics = $this->contractStatisticalArray(
                $this->getEnterpriseContractStatics()['type'],
                $this->getEnterpriseContractStatics()['data']
            );
        }

        return array_values($enterpriseContractStatics);
    }

    protected function getDataTitle($data)
    {
        $enterprise = json_decode('
          {
            "id": "NA",
            "name": "湖北翰林建设劳务有限公司",
            "enterpriseName": "湖***司",
            "unifiedSocialCreditCode": "91420500691796H522",
            "tag": "",
            "logo": {
                "name": "siinlswj6d_1598923695917",
                "identify": "siinlswj6d_1598923695917.png"
            },
            "businessLicense": {
                "name": "1c6x13xpjy_1598923763457",
                "identify": "1c6x13xpjy_1598923763457.jpg"
            },
            "powerAttorney": {
                "name": "",
                "identify": ""
            },
            "contactsName": "王洋",
            "contactsCellphone": "13621267859",
            "contactsArea": "湖北省,宜昌市,西陵区",
            "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
            "legalPersonName": "",
            "legalPersonCardId": "",
            "legalPersonPositivePhoto": {
                "name": "",
                "identify": ""
            },
            "legalPersonReversePhoto": {
                "name": "",
                "identify": ""
            },
            "legalPersonHandheldPhoto": {
                "name": "",
                "identify": ""
            },
            "member": {
                "id": "NA",
                "avatar": [],
                "nickName": "",
                "gender": 1,
                "birthday": "0000-00-00",
                "area": "",
                "address": "",
                "briefIntroduction": "",
                "cellphone": "",
                "cellphoneMask": "",
                "realName": "",
                "userName": "",
                "createTime": 0,
                "createTimeFormat": "1970-01-01",
                "updateTime": 0,
                "updateTimeFormat": "1970-01-01",
                "tag": ""
            },
            "realNameAuthenticationStatus": 2
          }',true);

        if($data['id'] == 'Mywt'){
            $data['title']='北京商旅机构融资';
            $data['cover']= [
              "name"=>"siinlswj6d_1598923695917",
              "identify"=>"siinlswj6d_1598923695917.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'Mysw'){
            $data['title']='河南供应链新零售项目融资';
            $data['cover']= [
              "name"=>"ukzys8lbt7_1618282008130",
              "identify"=>"ukzys8lbt7_1618282008130.jpg"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'My0x'){
            $data['title']='广西品牌营销管理系统融资';
            $data['cover']= [
              "name"=>"siinlswj6d_1598923695917",
              "identify"=>"siinlswj6d_1598923695917.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MTEw'){
            $data['title']='浙江线上教育平台项目融资';
            $data['cover']= [
                "name"=>"siinlswj6d_1598923695917",
                "identify"=>"siinlswj6d_1598923695917.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MDQ'){
            $data['title']='浙江线下时尚平台实体店融资';
            $data['cover']= [
              "name"=>"zqk7w29ciu_1598952000004",
              "identify"=>"zqk7w29ciu_1598952000004.jpg"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'Niw'){
            $data['title']='四川家政服务和废品回收信息服务平台融资';
            $data['cover']= [
              "name"=>"15c3ocyj54_1603962535439",
              "identify"=>"15c3ocyj54_1603962535439.jpg"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MTM'){
            $data['title']='四川太阳能浮台无线水质监测站融资';
            $data['cover']= [
              "name"=>"xgjehyrr64_1616639979206",
              "identify"=>"xgjehyrr64_1616639979206.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MS8v'){
            $data['title']='江西奶茶融资';
            $data['cover']= [
              "name"=>"7fvqzs6xpo_1616640603210",
              "identify"=>"7fvqzs6xpo_1616640603210.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'My0q'){
            $data['title']='广东高校服务通融资';
            $data['cover']= [
                "name"=>"ooyxdy738j_1612924397346",
                "identify"=>"ooyxdy738j_1612924397346.jpg"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MjIy'){
            $data['title']='广东3c电子项目融资';
            $data['cover']= [
              "name"=>"ggrvabd5im_1614060894527",
              "identify"=>"ggrvabd5im_1614060894527.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MjAx'){
            $data['title']='江西高校服务通融资';
            $data['cover']= [
              "name"=>"www4ntnomc_1612924025460",
              "identify"=>"www4ntnomc_1612924025460.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MjEu'){
            $data['title']='浙江药本药中药饮片线上批发平台融资';
            $data['cover']= [
              "name"=>"rpvbarnfzk_1612924304533",
              "identify"=>"rpvbarnfzk_1612924304533.jpg"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MjIr'){
            $data['title']='浙江防火系统融资';
            $data['cover']= [
              "name"=>"ooyxdy738j_1612924397346",
              "identify"=>"ooyxdy738j_1612924397346.jpg"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'MjAq'){
            $data['title']='浙江笑迎计划融资';
            $data['cover']= [
              "name"=>"aiyg9npd2m_1612921889210",
              "identify"=>"aiyg9npd2m_1612921889210.png"
            ];
            $data['enterprise'] = $enterprise;
        }
        if($data['id'] == 'Mi8t'){
            $data['title']='湖北校园生活服务社交平台项目';
            $data['cover']= [
              "name"=>"wpyttzl9zf_1612411134004",
              "identify"=>"wpyttzl9zf_1612411134004.png"
            ];
            $data['enterprise'] = $enterprise;
        }

        return $data;
    }
}
