<?php
namespace SuperMarket\StockMarkets\ServiceMall\View;

use Sdk\Service\Model\Service;
use Sdk\Statistical\Model\Statistical;
use Sdk\Evaluation\Model\Evaluation;

use Workbench\Service\Translator\ServiceTranslator;
use Workbench\MerchantCoupon\Translator\MerchantCouponTranslator;
use Workbench\EvaluationReply\Translator\EvaluationReplyTranslator;
use Workbench\ContractTemplate\Translator\ContractTemplateItemTranslator;

use Statistical\Translator\StaticsTranslatorFactory;

use UserCenter\Evaluation\Translator\EvaluationTranslator;
use UserCenter\Order\Translator\OrderTranslator;
use UserCenter\Collection\Translator\CollectionTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
trait ViewTrait
{
    private $service;

    private $recommendedServices;

    private $couponList;

    private $serviceTranslator;

    private $couponTranslator;

    private $evaluations;

    private $staticsList;

    private $evaluationReplies;

    private $enterpriseStaticsList;

    private $collection;

    private $collectionTranslator;

    private $collectionAll;

    private $contractTemplateItem;

    private $contractTemplateItemTranslator;

    private $enterpriseList;

    private $contractPerformanceStatics;

    private $contractUsageList;

    private $shareTemplateCount;

    public function __construct(
        Service $service,
        array $recommendedServices,
        array $couponList,
        $staticsList,
        $evaluationReplies,
        $enterpriseStaticsList,
        $evaluations = array(),
        $evaluationCount = 0,
        $collections = array(),
        $contractTemplateItem = array(),
        $enterpriseList = array(),
        $contractUsageList = array(),
        $contractPerformanceStatics = array(),
        $enterpriseContractStatics = array(),
        $shareTemplateCount = 0
    ) {
        parent::__construct();
        $this->service = $service;
        $this->recommendedServices = $recommendedServices;
        $this->couponList = $couponList;
        $this->evaluations = $evaluations;
        $this->staticsList = $staticsList;
        $this->evaluationReplies = $evaluationReplies;
        $this->serviceTranslator = new ServiceTranslator();
        $this->couponTranslator = new MerchantCouponTranslator();
        $this->evaluationCount = $evaluationCount;
        $this->enterpriseStaticsList = $enterpriseStaticsList;
        $this->collection =
          isset($collections['collection'])
          ? $collections['collection']
          : array();
        $this->enterpriseCollection =
          isset($collections['enterpriseCollection'])
          ? $collections['enterpriseCollection']
          : array();
        ;
        $this->collectionTranslator = new CollectionTranslator();
        $this->collectionAll =
          isset($collections['collectionAll'])
          ? $collections['collectionAll']
          : 0;
        $this->enterpriseCollectionAll =
          isset($collections['enterpriseCollectionAll'])
          ? $collections['enterpriseCollectionAll']
          : 0;
        $this->contractTemplateItem = $contractTemplateItem;
        $this->contractTemplateItemTranslator = new ContractTemplateItemTranslator();
        $this->enterpriseList = $enterpriseList;
        $this->contractPerformanceStatics = $contractPerformanceStatics;
        $this->contractUsageList = $contractUsageList;
        $this->enterpriseContractStatics = $enterpriseContractStatics;
        $this->shareTemplateCount = $shareTemplateCount;
    }

    public function __destruct()
    {
        // parent::__destruct();
        unset($this->service);
        unset($this->recommendedServices);
        unset($this->couponList);
        unset($this->serviceTranslator);
        unset($this->couponTranslator);
        unset($this->evaluations);
        unset($this->staticsList);
        unset($this->evaluationReplies);
        unset($this->evaluationCount);
        unset($this->enterpriseStaticsList);
        unset($this->collection);
        unset($this->enterpriseCollection);
        unset($this->collectionTranslator);
        unset($this->collectionAll);
        unset($this->enterpriseCollectionAll);
        unset($this->contractTemplateItem);
        unset($this->contractTemplateItemTranslator);
        unset($this->contractPerformanceStatics);
        unset($this->enterpriseList);
        unset($this->contractUsageList);
        unset($this->enterpriseContractStatics);
        unset($this->shareTemplateCount);
    }

    public function getService() : Service
    {
        return $this->service;
    }

    public function getMyCoupon()
    {
        return $this->couponList;
    }

    public function getRecommendedServices() : array
    {
        return $this->recommendedServices;
    }

    public function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    public function getCouponTranslator() : MerchantCouponTranslator
    {
        return $this->couponTranslator;
    }

    protected function getStatisticalTranslator(string $type)
    {
        $translatorFactory = new StaticsTranslatorFactory();

        $translator = $translatorFactory->getTranslator($type);

        return new $translator;
    }

    public function statisticalArray(string $type, Statistical $statistical)
    {
        $data = array();
        $translator = $this->getStatisticalTranslator($type);

        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }

    public function getStatics()
    {
        if (!empty($this->getStaticsList())) {
            $staticsList = $this->statisticalArray(
                $this->getStaticsList()['type'],
                $this->getStaticsList()['data']
            );
        }

        return empty($staticsList) ? [] : $staticsList;
    }

    public function getEnterpriseStatics()
    {
        $enterpriseStaticsList = [];

        if (!empty($enterpriseStaticsList)) {
            $enterpriseStaticsList = $this->statisticalArray(
                $this->getEnterpriseStaticsList()['type'],
                $this->getEnterpriseStaticsList()['data']
            );
        }

        return $enterpriseStaticsList;
    }

    public function getEnterpriseStaticsList()
    {
        return $this->enterpriseStaticsList;
    }

    protected function getEvaluationTranslator() : EvaluationTranslator
    {
        return new EvaluationTranslator();
    }

    protected function getEvaluationReplyTranslator() : EvaluationReplyTranslator
    {
        return new EvaluationReplyTranslator();
    }

    protected function getOrderTranslator() : OrderTranslator
    {
        return new OrderTranslator();
    }

    protected function getType()
    {
        return $this->type;
    }

    protected function getSnapshot()
    {
        return $this->snapshot;
    }

    protected function getEvaluations()
    {
        return $this->evaluations;
    }

    protected function getStaticsList()
    {
        return $this->staticsList;
    }

    protected function getEvaluationReplies()
    {
        return $this->evaluationReplies;
    }

    protected function getEvaluationCount()
    {
        return $this->evaluationCount;
    }

    protected function getCollection()
    {
        return $this->collection;
    }

    protected function getEnterpriseCollection()
    {
        return $this->enterpriseCollection;
    }

    protected function getContractTemplateItem()
    {
        return $this->contractTemplateItem;
    }

    public function getContractTemplateItemTranslator() : ContractTemplateItemTranslator
    {
        return $this->contractTemplateItemTranslator;
    }

    public function getCollectionTranslator() : CollectionTranslator
    {
        return $this->collectionTranslator;
    }

    public function getContractTemplateItemList()
    {
        $translator = $this->getContractTemplateItemTranslator();

        $list = array();
        foreach ($this->getContractTemplateItem() as $contractTemplateItem) {
            $list[] = $translator->objectToArray(
                $contractTemplateItem
            );
        }

        return $list;
    }

    protected function getAllEvaluationList() : array
    {
        $evaluationList = [];
        foreach ($this->getEvaluations() as $evaluation) {
            $evaluationList[] = $this->getEvaluationTranslator()->objectToArray(
                $evaluation,
                array(
                'id',
                'content',
                'reason',
                'picture',
                'evaluationScore' => [
                'id',
                'commodityStarReview',
                'enterpriseStarReview'
                ],
                'commodityName',
                'member' => [
                'id',
                'nickName',
                'nickNameMask',
                'avatar'
                ],
                'order'=> [
                  'id',
                  'orderno',
                  'totalPrice',
                ],
                'enterprise' => [
                  'id',
                  'name',
                  'logo',
                ],
                'isAuto',
                'isReply',
                'status',
                'createTime',
                'updateTime'
                )
            );
        }
        $evaluationReplies = [];
        foreach ($this->getEvaluationReplies() as $evaluationReply) {
            $evaluationReplies[] = $this->getEvaluationReplyTranslator()->objectToArray(
                $evaluationReply,
                array(
                'id',
                'content',
                'reason',
                'replyType',
                'fromReplyId',
                'evaluation'=>['id'],
                'status',
                'createTime',
                'updateTime'
                )
            );
        }
        if (!empty($evaluationList)) {
            foreach ($evaluationList as $key => $evaluation) {
                $evaluationList[$key]['reply'] = [];
                if ($evaluation['isReply'] == Evaluation::IS_REPLY['REPLIED']) {
                    foreach ($evaluationReplies as $evaluationReply) {
                        if ($evaluationReply['evaluation']['id'] == $evaluation['id']) {
                            $evaluationList[$key]['reply'] = $evaluationReply;
                        }
                    }
                }
            }
        }

        return $evaluationList;
    }

    public function data()
    {
        $serviceTranslator = $this->getServiceTranslator();

        $service = $serviceTranslator->objectToArray($this->getService());

        foreach ($service['price'] as $key => $val) {
            if (strpos($val['value'], '.') === false) {
                $service['price'][$key]['value'] = $val['value'].'.00';
            }
        }

        //获取推荐服务
        $recommendedServices = $this->getRecommendedServices();

        $storeServices = !empty($recommendedServices['storeServices']) ?
            $recommendedServices['storeServices'][1] : [];
        $sameServices = !empty($recommendedServices['storeServices']) ?
            $recommendedServices['sameServices'][1] : [];

        //店铺推荐
        foreach ($storeServices as $key => $storeService) {
            $storeServices[$key] = $serviceTranslator->objectToArray(
                $storeService,
                array('id','title','minPrice','volume','pageViews','cover','serviceCategory'=>['name'])
            );
            if ($storeServices[$key]['id'] == $service['id']) {
                unset($storeServices[$key]);
            }
        }
        //同款推荐
        foreach ($sameServices as $key => $sameService) {
            $sameServices[$key] = $serviceTranslator->objectToArray(
                $sameService,
                array(
                    'id',
                    'title',
                    'minPrice',
                    'volume',
                    'pageViews',
                    'cover',
                    'serviceCategory'=>['name'],
                    'enterprise'=>['id','name','logo']
                )
            );
            if ($sameServices[$key]['id'] == $service['id']) {
                unset($sameServices[$key]);
            }
        }

        $evaluation = $this->getAllEvaluationList();
        $commodityScore = $this->getStatics();

        return array(
            'service' => $service,
            'storeServices' => $storeServices,
            'sameServices' => $sameServices,
            'evaluation' => $evaluation,
            'commodityScore' => $commodityScore,
            'enterpriseScore' => $this->getEnterpriseStatics(),
            'evaluationCount'=>$this->getEvaluationCount()
        );
    }

    public function coupon()
    {
        $myCoupon = $this->getMyCoupon();

        $couponTranslator = $this->getCouponTranslator();

        //我的优惠券
        foreach ($myCoupon as $key => $coupon) {
            $myCoupon[$key] = $couponTranslator->objectToArray(
                $coupon,
                array('id')
            );
        }

        return $myCoupon;
    }

    protected function getCollectionList() : array
    {
        $collectionList = [];
        foreach ($this->getCollection() as $collection) {
            $collectionList[] = $this->getCollectionTranslator()->objectToArray(
                $collection,
                array()
            );
        }

        return $collectionList;
    }

    protected function getEnterpriseCollectionList() : array
    {
        $collectionList = [];
        foreach ($this->getEnterpriseCollection() as $collection) {
            $collectionList[] = $this->getCollectionTranslator()->objectToArray(
                $collection,
                array()
            );
        }

        return $collectionList;
    }

    protected function getCollectionAll() : int
    {
        return $this->collectionAll;
    }

    protected function getEnterpriseCollectionAll() : int
    {
        return $this->enterpriseCollectionAll;
    }

    public function getEnterpriseList()
    {
        return $this->enterpriseList;
    }

    public function getContractPerformanceStatics()
    {
        return $this->contractPerformanceStatics;
    }

    public function getContractUsageList()
    {
        return $this->contractUsageList;
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    public function getAllEnterpriseList():array
    {
        $translator = $this->getEnterpriseTranslator();

        $list = array();
        foreach ($this->getEnterpriseList() as $enterprise) {
            $list[] = $translator->objectToArray(
                $enterprise,
                array(
                  'id',
                  'name',
                )
            );
        }

        return $list;
    }

    public function getEnterpriseContractStatics()
    {
        return $this->enterpriseContractStatics;
    }

    public function getShareTemplateCount()
    {
        return $this->shareTemplateCount;
    }
}
