<?php
namespace SuperMarket\StockMarkets\ServiceMall\View;

trait ListViewTrait
{
    private $count;

    private $list;

    private $searchParameters;

    public function __construct(int $count, array $list, array $searchParameters)
    {
        parent::__construct();
        $this->count = $count;
        $this->list = $list;
        $this->searchParameters = $searchParameters;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->count);
        unset($this->list);
        unset($this->searchParameters);
    }

    public function getCount() : int
    {
        return $this->count;
    }

    public function getList() : array
    {
        return $this->list;
    }

    public function getSearchParameters() : array
    {
        return $this->searchParameters;
    }

    public function searchParameters()
    {
        $searchParameters = $this->getSearchParameters();

        $serviceCategoryArray = $this->formatArray($searchParameters);

        $searchParameters['serviceCategories'] = $serviceCategoryArray;

        return $searchParameters;
    }

    private function formatArray(array $array)
    {
        $parentCategories = $array['parentCategories'];
        $serviceCategories = $array['serviceCategories'];

        foreach ($parentCategories as $key => $parentCategory) {
            foreach ($serviceCategories as $val) {
                if ($val['pid'] == $parentCategory['id']) {
                    unset($val['pid']);
                    $parentCategories[$key]['serviceCategory'][] = $val;
                }
            }
        };

        return $parentCategories;
    }
}
