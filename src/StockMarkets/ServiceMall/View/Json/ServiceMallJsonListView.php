<?php
namespace SuperMarket\StockMarkets\ServiceMall\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Workbench\Service\Translator\ServiceTranslator;

class ServiceMallJsonListView extends JsonView implements IView
{
    private $total;

    private $list;

    private $serviceTranslator;

    public function __construct($total, $list)
    {
        parent::__construct();
        $this->total = $total;
        $this->list = $list;
        $this->serviceTranslator = new ServiceTranslator();
    }

    public function __destruct()
    {
        unset($this->total);
        unset($this->list);
        unset($this->serviceTranslator);
    }

    protected function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    protected function getList()
    {
        return $this->list;
    }

    protected function getTotal()
    {
        return $this->total;
    }

    public function display() : void
    {
        $service = $this->getList();

        $serviceArray = [];
        if (!empty($service)) {
            foreach ($service as $value) {
                $serviceArray[] = $this->getServiceTranslator()->objectToArray(
                    $value
                );
            }
        }

        $data = array(
            'list' => $serviceArray,
            'total' => $this->getTotal()
        );

        $this->encode($data);
    }
}
