<?php
namespace Service\ServiceMall\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;
use Service\ServiceMall\View\RelevantCouponsViewTrait;

class RelevantCouponsJsonListView extends JsonView implements IView
{
    use RelevantCouponsViewTrait;
    
    public function display() : void
    {
         $list = $this->getCouponList();

         $data = array(
             'list' => $list,
             'total' => $this->getTotal()
         );

         $this->encode($data);
    }
}
