<?php
namespace SuperMarket\StockMarkets\RequirementSquare\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\StockMarkets\RequirementSquare\View\Template\CategoryView;
use SuperMarket\StockMarkets\RequirementSquare\View\Template\ListView;
use SuperMarket\StockMarkets\RequirementSquare\View\Template\DetailView;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\ServiceRequirement\Repository\ServiceRequirementRepository;

use Workbench\ServiceCategory\Controller\CategoryTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait, CategoryTrait;

    const SIZE = array(
        'REQUIREMENT_SIZE' => 12, //获取需求数据条数
        'SEARCH_SIZE' => 5 //获取推荐需求数
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /requirementSquare
     * 需求广场列表
     *
     */
    protected function filterAction()
    {

        $requirementsList = json_decode('[
          {
              "id": "MTAs",
              "title": "安徽旅游平台融资",
              "minPrice": "1.00",
              "maxPrice": "100.00",
              "validityStartTime": 1621209600,
              "validityStartTimeFormat": "2021.05.17",
              "validityEndTime": 1622678400,
              "validityEndTimeFormat": "2021.06.03",
              "createTime": 1619814705,
              "createTimeFormat": "2021-05-01 04:31:45",
              "serviceCategory": {
                  "id": "MCws",
                  "name": "房地产规划",
                  "parentCategory": {
                      "id": "MS0"
                  }
              }
          },
          {
              "id": "MTEw",
              "title": "北京外语情景教学APP商业融资",
              "minPrice": "1.00",
              "maxPrice": "300.00",
              "validityStartTime": 1622073600,
              "validityStartTimeFormat": "2021.05.27",
              "validityEndTime": 1622246400,
              "validityEndTimeFormat": "2021.05.29",
              "createTime": 1619815606,
              "createTimeFormat": "2021-05-01 04:46:46",
              "serviceCategory": {
                  "id": "MA",
                  "name": "人力资源管理咨询",
                  "parentCategory": {
                      "id": "MA"
                  }
              }
          },
          {
              "id": "MDEy",
              "title": "广西品牌营销管理系统融资",
              "minPrice": "1.00",
              "maxPrice": "560.00",
              "validityStartTime": 1619308800,
              "validityStartTimeFormat": "2021.04.25",
              "validityEndTime": 1619568000,
              "validityEndTimeFormat": "2021.04.28",
              "createTime": 1619426142,
              "createTimeFormat": "2021-04-26 16:35:42",
              "serviceCategory": {
                  "id": "MA",
                  "name": "人力资源管理咨询",
                  "parentCategory": {
                      "id": "MA"
                  }
              }
          },
          {
              "id": "MDEr",
              "title": "重庆康养田园综合体项目融资",
              "minPrice": "200.00",
              "maxPrice": "700.00",
              "validityStartTime": "",
              "validityStartTimeFormat": "",
              "validityEndTime": "",
              "validityEndTimeFormat": "",
              "createTime": 1619422208,
              "createTimeFormat": "2021-04-26 15:30:08",
              "serviceCategory": {
                  "id": "NCs",
                  "name": "经营管理培训",
                  "parentCategory": {
                      "id": "Mw"
                  }
              }
          },
          {
              "id": "MDAu",
              "title": "山西社区服务平台项目融资",
              "minPrice": "1.00",
              "maxPrice": "900.00",
              "validityStartTime": "",
              "validityStartTimeFormat": "",
              "validityEndTime": "",
              "validityEndTimeFormat": "",
              "createTime": 1619414187,
              "createTimeFormat": "2021-04-26 13:16:27",
              "serviceCategory": {
                  "id": "MDQ",
                  "name": "法律援助",
                  "parentCategory": {
                      "id": "MA"
                  }
              }
          },
          {
              "id": "MC8x",
              "title": "浙江无人机智能巡检融资",
              "minPrice": "10.00",
              "maxPrice": "1300.00",
              "validityStartTime": "",
              "validityStartTimeFormat": "",
              "validityEndTime": "",
              "validityEndTimeFormat": "",
              "createTime": 1619414053,
              "createTimeFormat": "2021-04-26 13:14:13",
              "serviceCategory": {
                  "id": "Mg",
                  "name": "招聘外包",
                  "parentCategory": {
                      "id": "MQ"
                  }
              }
          },
          {
              "id": "MC0p",
              "title": "深圳智慧猪场物联网解决方案项目融资",
              "minPrice": "1.00",
              "maxPrice": "2000.00",
              "validityStartTime": 1618790400,
              "validityStartTimeFormat": "2021.04.19",
              "validityEndTime": 1619740800,
              "validityEndTimeFormat": "2021.04.30",
              "createTime": 1618814222,
              "createTimeFormat": "2021-04-19 14:37:02",
              "serviceCategory": {
                  "id": "NA",
                  "name": "企业变更",
                  "parentCategory": {
                      "id": "Mg"
                  }
              }
          },
          {
              "id": "NzA",
              "title": "江苏宠物电商服务平台项目融资",
              "minPrice": "20.00",
              "maxPrice": "300.00",
              "validityStartTime": 1616284800,
              "validityStartTimeFormat": "2021.03.21",
              "validityEndTime": 1617062400,
              "validityEndTimeFormat": "2021.03.30",
              "createTime": 1616296693,
              "createTimeFormat": "2021-03-21 11:18:13",
              "serviceCategory": {
                  "id": "MCw",
                  "name": "年度财务审计",
                  "parentCategory": {
                      "id": "Ng"
                  }
              }
          },
          {
              "id": "NS8",
              "title": "湖北励志品牌白酒融资",
              "minPrice": "100.00",
              "maxPrice": "200.00",
              "validityStartTime": 1611100800,
              "validityStartTimeFormat": "2021.01.20",
              "validityEndTime": 1621468800,
              "validityEndTimeFormat": "2021.05.20",
              "createTime": 1611126565,
              "createTimeFormat": "2021-01-20 15:09:25",
              "serviceCategory": {
                  "id": "MCw",
                  "name": "年度财务审计",
                  "parentCategory": {
                      "id": "Ng"
                  }
              }
          },
          {
              "id": "MjE",
              "title": "浙江互联网＋法律平台融资",
              "minPrice": "1.00",
              "maxPrice": "230.00",
              "validityStartTime": 1606694400,
              "validityStartTimeFormat": "2020.11.30",
              "validityEndTime": 1609372800,
              "validityEndTimeFormat": "2020.12.31",
              "createTime": 1606725229,
              "createTimeFormat": "2020-11-30 16:33:49",
              "serviceCategory": {
                  "id": "MzI",
                  "name": "计算机网络服务",
                  "parentCategory": {
                      "id": "Mg"
                  }
              }
          },
          {
              "id": "MTA",
              "title": "河南供应链新零售项目融资",
              "minPrice": "10.00",
              "maxPrice": "500.00",
              "validityStartTime": "",
              "validityStartTimeFormat": "",
              "validityEndTime": "",
              "validityEndTimeFormat": "",
              "createTime": 1603086309,
              "createTimeFormat": "2020-10-19 13:45:09",
              "serviceCategory": {
                  "id": "MDE",
                  "name": "税务代理",
                  "parentCategory": {
                      "id": "MQ"
                  }
              }
          },
          {
              "id": "MTI",
              "title": "通渭县生态种养殖项目融资",
              "minPrice": "2.00",
              "maxPrice": "50.00",
              "validityStartTime": "",
              "validityStartTimeFormat": "",
              "validityEndTime": "",
              "validityEndTimeFormat": "",
              "createTime": 1603090096,
              "createTimeFormat": "2020-10-19 14:48:16",
              "serviceCategory": {
                  "id": "MC4",
                  "name": "资产评估",
                  "parentCategory": {
                      "id": "Mg"
                  }
              }
          }
        ]',true);

        $this->render(new ListView(12, $requirementsList));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {

        $sort = $this->getRequest()->get('sort', array());
        $sort = empty($sort) ? ['-updateTime'] : $sort;
        //需求标题
        $title = $this->getRequest()->get('title', '');
        //需求分类
        $parentCategory = $this->getRequest()->get('parentCategory', '');
        $serviceCategory = $this->getRequest()->get('serviceCategory', array());
        //价格区间
        $minPrice = $this->getRequest()->get('minPrice', '');
        $maxPrice = $this->getRequest()->get('maxPrice', '');
        //有效时间
        $validityStartTime = $this->getRequest()->get('validityStartTime', '');
        $validityEndTime = $this->getRequest()->get('validityEndTime', '');
        //检索条件
        $filter = array();
        $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if (!empty($parentCategory) && !empty($serviceCategory)) {
            unset($parentCategory);
        }

        if (!empty($parentCategory)) {
            $filter['parentCategory'] = marmot_decode($parentCategory);
        }
        if (!empty($serviceCategory)) {
            if (is_string($serviceCategory)) {
                $serviceCategory = array($serviceCategory);
            }
            $filter['serviceCategory'] = $this->implodeArray($serviceCategory);
        }
        if (!empty($minPrice)) {
            $filter['minPrice'] = $minPrice;
        }
        if (!empty($maxPrice)) {
            $filter['maxPrice'] = $maxPrice;
        }
        if (!empty($validityStartTime)) {
            $filter['validityStartTime'] = $validityStartTime;
        }
        if (!empty($validityEndTime)) {
            $filter['validityEndTime'] = $validityEndTime;
        }

        return [$filter, $sort];
    }

    /**
     *
     */
    protected function implodeArray(array $array) : string
    {
        $dataDecode = array();
        foreach ($array as $val) {
            $dataDecode[] = marmot_decode($val);
        }

        return implode(',', $dataDecode);
    }

    /**
     * @return bool
     * @param [GET]
     * @method /requirementSquare/MA
     * 需求详情
     *
     */
    protected function fetchOneAction($id)
    {
        $requirements = $this->getRepository()
            ->scenario(ServiceRequirementRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($requirements instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        //获取相似需求列表【按服务分类id检索】
        $serviceCategoryId =  $requirements->getServiceCategory()->getId();
        $requirementList = $this->fetchRequirementList($serviceCategoryId);

        $this->render(new DetailView($requirements, $requirementList));
        return true;
    }

    /**
     * @param $serviceCategoryId
     * @return array
     * 获取条件为[服务分类id]的需求列表
     *
     */
    protected function fetchRequirementList($serviceCategoryId)
    {
        list($filter, $sort) = $this->otherFilterFormatChange($serviceCategoryId);

        $requirementList = array();
        list($count, $requirementList) = $this->getRepository()
            ->scenario(ServiceRequirementRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, self::SIZE['SEARCH_SIZE']);
        unset($count);

        return $requirementList;
    }

    /**
     * @param $serviceCategoryId
     * @return array
     *
     * 获取需求详情中需求列表条件
     */
    protected function otherFilterFormatChange($serviceCategoryId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['serviceCategory'] = $serviceCategoryId;
        $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /requirementSquares/serviceCategory
     *
     * 需求分类列表
     */
    public function serviceCategory()
    {
      $serviceCategory['parentCategories']= array(
        array(
            "id" => "MA",
            "name" => "债权融资"
        ),
        array(
            "id" => "MQ",
            "name" => "股权融资"
        ),
        array(
            "id" => "Mg",
            "name" => "内部融资"
        ),
        array(
            "id" => "Mw",
            "name" => "项目融资"
        ),
        array(
            "id" => "NA",
            "name" => "政策融资"
            ),
        array(
            "id" => "NQ",
            "name" => "贸易融资"
        ),
      );
      $serviceCategory['serviceCategories'] = array(
        array(
            "id" => "MA",
            "name" => "国内银行贷款",
            "pid" => "MA"
        ),
       array(
               "id" => "MQ",
               "name" => "国外银行贷款",
               "pid" => "MA"
       ),
       array(
               "id" => "Mg",
               "name" => "企业发行债券融资",
               "pid" => "MA"
       ),
       array(
               "id" => "Mw",
               "name" => "民间借贷融资",
               "pid" => "MA"
       ),
       array(
               "id" => "NA",
               "name" => "信用担保融资",
               "pid" => "MA"
           ),
       array(
              "id" => "NQ",
              "name" => "租赁融资",
              "pid" => "MA"
       ),
       array(
            "id" => "Ng",
            "name" => "股权出让融资",
            "pid" => "MQ"
      ),
      array(
              "id" => "Nw",
              "name" => "产权交易融资",
              "pid" => "MQ"
      ),
      array(
              "id" => "OA",
              "name" => "增资扩股融资",
              "pid" => "MQ"
      ),
      array(
              "id" => "MCs",
              "name" => "杠杠收购融资",
              "pid" => "MQ"
      ),
      array(
              "id" => "MCw",
              "name" => "风险投资融资",
              "pid" => "MQ"
          ),
      array(
            "id" => "MC0",
            "name" => "私募股权融资",
            "pid" => "MQ"
      ),

      array(
        "id" => "MC4",
        "name" => "留存盈余融资",
        "pid" => "Mg"
      ),
      array(
              "id" => "MC8",
              "name" => "应收账款融资",
              "pid" => "Mg"
      ),
      array(
              "id" => "MDA",
              "name" => "资产典当融资",
              "pid" => "Mg"
      ),
      array(
              "id" => "MDE",
              "name" => "商业信用融资",
              "pid" => "Mg"
      ),
      array(
              "id" => "MDI",
              "name" => "信用证融资",
              "pid" => "Mg"
          ),
      array(
            "id" => "MDM",
            "name" => "福费廷融资",
            "pid" => "Mg"
      ),

      array(
        "id" => "MDQ",
        "name" => "项目包装融资",
        "pid" => "Mw"
      ),
      array(
              "id" => "MSs",
              "name" => "BOT项目融资",
              "pid" => "Mw"
      ),
      array(
              "id" => "MSw",
              "name" => "IFC国际融资",
              "pid" => "Mw"
      ),
      array(
              "id" => "MS0",
              "name" => "高新技术融资",
              "pid" => "Mw"
      ),
      array(
              "id" => "MDI",
              "name" => "专项资金融资",
              "pid" => "Mw"
          ),
      array(
            "id" => "MS4",
            "name" => "产业政策融资",
            "pid" => "NA"
      ),

      array(
        "id" => "MS8",
        "name" => "国际保理融资",
        "pid" => "NQ"
      ),
      array(
              "id" => "MTA",
              "name" => "打包放款融资",
              "pid" => "NQ"
      ),
      array(
              "id" => "MTE",
              "name" => "补偿贸易融资",
              "pid" => "NQ"
          ),
      array(
            "id" => "MTI",
            "name" => "出口信贷融资",
            "pid" => "NQ"
      )
    );

        $this->render(new CategoryView($serviceCategory));
        return true;
    }

    public function fetchList(string $ids)
    {
        unset($ids);
        $this->displayError();
        return false;
    }
}
