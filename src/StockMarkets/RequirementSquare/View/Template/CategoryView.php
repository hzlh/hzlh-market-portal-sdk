<?php
namespace SuperMarket\StockMarkets\RequirementSquare\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class CategoryView extends TemplateView implements IView
{
    private $serviceCategories;

    public function __construct($serviceCategories)
    {
        parent::__construct();
        $this->serviceCategories = $serviceCategories;
    }

    public function __destruct()
    {
        unset($this->serviceCategories);
    }

    public function getServiceCategories()
    {
        return $this->serviceCategories;
    }

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->formatArray($serviceCategories);
        $this->getView()->display(
            'StockMarkets/RequirementCategory.tpl',
            [
                'serviceCategories' => $serviceCategories
            ]
        );
    }

    private function formatArray(array $array)
    {
        $parentCategories = $array['parentCategories'];
        $serviceCategories = $array['serviceCategories'];

        foreach ($parentCategories as $key => $parentCategory) {
            foreach ($serviceCategories as $val) {
                if ($val['pid'] == $parentCategory['id']) {
                    unset($val['pid']);
                    $parentCategories[$key]['list'][] = $val;
                }
            }
        };

        return $parentCategories;
    }
}
