<?php
namespace SuperMarket\StockMarkets\RequirementSquare\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;

use Sdk\ServiceRequirement\Model\ServiceRequirement;

class DetailView extends TemplateView implements IView
{
    private $serviceRequirement;

    private $serviceRequirementList;

    private $translator;

    public function __construct(ServiceRequirement $serviceRequirement, $serviceRequirementList)
    {
        parent::__construct();
        $this->serviceRequirement = $serviceRequirement;
        $this->serviceRequirementList = $serviceRequirementList;
        $this->translator = new ServiceRequirementTranslator();
    }

    public function __destruct()
    {
        unset($this->serviceRequirement);
        unset($this->serviceRequirementList);
        unset($this->translator);
    }

    protected function getServiceRequirement() : ServiceRequirement
    {
        return $this->serviceRequirement;
    }

    protected function getServiceRequirementList() : array
    {
        return $this->serviceRequirementList;
    }

    protected function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray($this->getServiceRequirement());

        $requirementList = $this->getServiceRequirementList();

        $requirementArray = array();
        if (!empty($requirementList)) {
            foreach ($requirementList as $key => $requirement) {
                $requirementArray[$key] = $translator->objectToArray(
                    $requirement,
                    array(
                        'id', 'title', 'minPrice', 'maxPrice', 'serviceCategory'=>['name'],
                        'validityStartTime', 'validityEndTime', 'updateTime', 'createTime'
                    )
                );
                if ($requirementArray[$key]['id'] == $data['id']) {
                    unset($requirementArray[$key]);
                }
            }
        }

        $data = $this->getDataTitle($data);

        $this->getView()->display(
            'StockMarkets/RequirementDetail.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['ROADSHOW_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['FINANCE_REQUIREMENTS'],
                'nav_phone' => NavTrait::NAV_PHONE['FINANCE_REQUIREMENTS'],
                'data' => $data,
                'list' => $requirementArray
            ]
        );
    }

    protected function getDataTitle($data)
    {
        if($data['id'] == 'MTAs'){
            $data['title']='安徽旅游平台融资';
            $data['maxPrice']='100.00';
        }
        if($data['id'] == 'MTEw'){
            $data['title']='北京外语情景教学APP商业融资';
            $data['maxPrice']='300.00';
        }
        if($data['id'] == 'MDEy'){
            $data['title']='广西品牌营销管理系统融资';
            $data['maxPrice']='560.00';
        }
        if($data['id'] == 'MDEr'){
            $data['title']='重庆康养田园综合体项目融资';
            $data['maxPrice']='300.00';
        }
        if($data['id'] == 'MDAu'){
            $data['title']='山西社区服务平台项目融资';
            $data['maxPrice']='900.00';
        }
        if($data['id'] == 'MC8x'){
            $data['title']='浙江无人机智能巡检融资';
            $data['maxPrice']='1300.00';
        }
        if($data['id'] == 'MC0p'){
            $data['title']='深圳智慧猪场物联网解决方案项目融资';
            $data['maxPrice']='2000.00';
        }
        if($data['id'] == 'NzA'){
            $data['title']='江苏宠物电商服务平台项目融资';
            $data['maxPrice']='300.00';
        }
        if($data['id'] == 'NS8'){
            $data['title']='湖北励志品牌白酒融资';
            $data['maxPrice']='200.00';
        }
        if($data['id'] == 'MjE'){
            $data['title']='浙江互联网＋法律平台融资';
            $data['maxPrice']='230.00';
        }
        if($data['id'] == 'MTA'){
            $data['title']='河南供应链新零售项目融资';
            $data['maxPrice']='500.00';
        }
        if($data['id'] == 'MTI'){
            $data['title']='通渭县生态种养殖项目融资';
            $data['maxPrice']='50.00';
        }

        return $data;
    }
}
