<?php
namespace SuperMarket\StockMarkets\RequirementSquare\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\StockMarkets\RequirementSquare\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getServiceRequirements();

        $this->getView()->display(
            'StockMarkets/RequirementList.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['ROADSHOW_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['FINANCE_REQUIREMENTS'],
                'nav_phone' => NavTrait::NAV_PHONE['FINANCE_REQUIREMENTS'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
