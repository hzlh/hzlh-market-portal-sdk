<?php
namespace SuperMarket\StockMarkets\RequirementSquare\View;

use UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;

trait ListViewTrait
{
    private $count;

    private $data;

    private $translator;

    public function __construct($count, $data)
    {
        parent::__construct();
        $this->count = $count;
        $this->data = $data;
        $this->translator = new ServiceRequirementTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->data);
        unset($this->translator);
    }

    public function getServiceRequirements() : array
    {
        return $this->data;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }

    // public function getList()
    // {
    //     $translator = $this->getTranslator();
    //     $serviceRequirements = $this->getServiceRequirements();

    //     $list = array();
    //     foreach ($serviceRequirements as $serviceRequirement) {
    //         $list[] = $translator->objectToArray(
    //             $serviceRequirement,
    //             array(
    //                 'id', 'title', 'serviceCategory'=>['id','name','parentCategory'=>['id']], 'minPrice',
    //                 'maxPrice', 'validityStartTime', 'validityEndTime', 'createTime'
    //             )
    //         );
    //     }

    //     return $list;
    // }
}
