<?php
namespace SuperMarket\StockMarkets\Home\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class HomeView extends TemplateView implements IView
{
    private $data;

    public function __construct(array $data)
    {
        parent::__construct();
        $this->data = $data;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->data);
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function display()
    {
        $data = $this->getData();

        $this->getView()->display(
            'StockMarkets/Index.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['ROADSHOW_HOME'],
                'nav_phone' => NavTrait::NAV_PHONE['ROADSHOW_HOME'],
                'data' => $data,
                'count' => count($data),
            ]
        );
    }
}
