<?php
namespace SuperMarket\StockMarkets\Home\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\StockMarkets\Home\View\Template\HomeView;
use Service\Home\Cache\Query\HomeFragmentCacheQuery;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    protected function getHomeFragmentCacheQuery()
    {
        return new HomeFragmentCacheQuery();
    }

    protected function filterAction():bool
    {
        $data = array(
          'parentCategories'=>array(
              array(
                  "id" => "MA",
                  "name" => "债权融资"
              ),
              array(
                  "id" => "MQ",
                  "name" => "股权融资"
              ),
              array(
                  "id" => "Mg",
                  "name" => "内部融资"
              ),
              array(
                  "id" => "Mw",
                  "name" => "项目融资"
              ),
              array(
                  "id" => "NA",
                  "name" => "政策融资"
                  ),
              array(
                  "id" => "NQ",
                  "name" => "贸易融资"
              ),
            ),

            'serviceCategories'=>array(
              array(
                  "id" => "MA",
                  "name" => "国内银行贷款",
                  "pid" => "MA"
              ),
             array(
                     "id" => "MQ",
                     "name" => "国外银行贷款",
                     "pid" => "MA"
             ),
             array(
                     "id" => "Mg",
                     "name" => "企业发行债券融资",
                     "pid" => "MA"
             ),
             array(
                     "id" => "Mw",
                     "name" => "民间借贷融资",
                     "pid" => "MA"
             ),
             array(
                     "id" => "NA",
                     "name" => "信用担保融资",
                     "pid" => "MA"
                 ),
             array(
                    "id" => "NQ",
                    "name" => "租赁融资",
                    "pid" => "MA"
             ),
             array(
                  "id" => "Ng",
                  "name" => "股权出让融资",
                  "pid" => "MQ"
            ),
            array(
                    "id" => "Nw",
                    "name" => "产权交易融资",
                    "pid" => "MQ"
            ),
            array(
                    "id" => "OA",
                    "name" => "增资扩股融资",
                    "pid" => "MQ"
            ),
            array(
                    "id" => "MCs",
                    "name" => "杠杠收购融资",
                    "pid" => "MQ"
            ),
            array(
                    "id" => "MCw",
                    "name" => "风险投资融资",
                    "pid" => "MQ"
                ),
            array(
                  "id" => "MC0",
                  "name" => "私募股权融资",
                  "pid" => "MQ"
            ),

            array(
              "id" => "MC4",
              "name" => "留存盈余融资",
              "pid" => "Mg"
            ),
            array(
                    "id" => "MC8",
                    "name" => "应收账款融资",
                    "pid" => "Mg"
            ),
            array(
                    "id" => "MDA",
                    "name" => "资产典当融资",
                    "pid" => "Mg"
            ),
            array(
                    "id" => "MDE",
                    "name" => "商业信用融资",
                    "pid" => "Mg"
            ),
            array(
                    "id" => "MDI",
                    "name" => "信用证融资",
                    "pid" => "Mg"
                ),
            array(
                  "id" => "MDM",
                  "name" => "福费廷融资",
                  "pid" => "Mg"
            ),

            array(
              "id" => "MDQ",
              "name" => "项目包装融资",
              "pid" => "Mw"
            ),
            array(
                    "id" => "MSs",
                    "name" => "BOT项目融资",
                    "pid" => "Mw"
            ),
            array(
                    "id" => "MSw",
                    "name" => "IFC国际融资",
                    "pid" => "Mw"
            ),
            array(
                    "id" => "MS0",
                    "name" => "高新技术融资",
                    "pid" => "Mw"
            ),
            array(
                    "id" => "MDI",
                    "name" => "专项资金融资",
                    "pid" => "Mw"
                ),
            array(
                  "id" => "MS4",
                  "name" => "产业政策融资",
                  "pid" => "NA"
            ),

            array(
              "id" => "MS8",
              "name" => "国际保理融资",
              "pid" => "NQ"
            ),
            array(
                    "id" => "MTA",
                    "name" => "打包放款融资",
                    "pid" => "NQ"
            ),
            array(
                    "id" => "MTE",
                    "name" => "补偿贸易融资",
                    "pid" => "NQ"
                ),
            array(
                  "id" => "MTI",
                  "name" => "出口信贷融资",
                  "pid" => "NQ"
            ),
             ),
          'serviceCount'=>100,
          'policyCount'=> 88,
          'enterpriseCount' => 98,
          'serviceRequirementCount'=> 127,
          'banners' => array(),
          'services' =>json_decode('[
            {
                "id": "Mywt",
                "number": "FW20210413414",
                "title": "北京商旅机构融资",
                "cover": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "detail": [
                    {
                        "old": "",
                        "type": "image",
                        "value": "block.jpeg",
                        "checked": "-2"
                    },
                    {
                        "type": "image",
                        "value": "5vi3acp2a2_1618283296362.png"
                    }
                ],
                "price": [
                    {
                        "name": "20",
                        "value": "3.00"
                    }
                ],
                "minPrice": "3000.00",
                "maxPrice": "3000.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 0,
                "pageViews": 26,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "",
                "createTime": 1618283395,
                "createTimeFormat": "2021-04-13 11:09:55",
                "updateTime": 1619491561,
                "updateTimeFormat": "2021-04-27 10:46:01",
                "serviceObjects": [
                    {
                        "id": 7,
                        "name": "其他"
                    }
                ],
                "serviceCategory": {
                    "id": "NA",
                    "name": "企业变更",
                    "qualificationName": "",
                    "isQualification": 0,
                    "isEnterpriseVerify": 2,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "MQ",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
                },
                "tag": "22"
            },
            {
                "id": "Mysw",
                "number": "FW20210413407",
                "title": "河南供应链新零售项目融资",
                "cover": {
                    "name": "ukzys8lbt7_1618282008130",
                    "identify": "ukzys8lbt7_1618282008130.jpg"
                },
                "detail": [
                    {
                        "type": "text",
                        "value": "<p>河南供应链新零售项目融资</p>"
                    }
                ],
                "price": [
                    {
                        "name": "1",
                        "value": "50000.00"
                    }
                ],
                "minPrice": "50000.00",
                "maxPrice": "50000.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 0,
                "pageViews": 67,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "bohui",
                "createTime": 1618282021,
                "createTimeFormat": "2021-04-13 10:47:01",
                "updateTime": 1619415584,
                "updateTimeFormat": "2021-04-26 13:39:44",
                "serviceObjects": [],
                "serviceCategory": {
                    "id": "MA",
                    "name": "人力资源管理咨询",
                    "qualificationName": "",
                    "isQualification": 0,
                    "isEnterpriseVerify": 0,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "MA",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
                },
                "tag": "162"
            },
            {
                "id": "My0x",
                "number": "FW20210413428",
                "title": "广西品牌营销管理系统融资",
                "cover": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "detail": [
                    {
                        "type": "image",
                        "value": "m73q5c6hb7_1618314871431.png"
                    },
                    {
                        "old": "rxjs7niyix_1618314881098.jpeg",
                        "type": "image",
                        "value": "block.jpeg",
                        "checked": "-2"
                    },
                    {
                        "type": "image",
                        "value": "tg18xggsww_1618314886516.png"
                    }
                ],
                "price": [
                    {
                        "name": "gg",
                        "value": "1000.00"
                    }
                ],
                "minPrice": "1000.00",
                "maxPrice": "1000.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 0,
                "pageViews": 18,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "",
                "createTime": 1618314895,
                "createTimeFormat": "2021-04-13 19:54:55",
                "updateTime": 1619341814,
                "updateTimeFormat": "2021-04-25 17:10:14",
                "serviceObjects": [],
                "serviceCategory": {
                    "id": "NA",
                    "name": "企业变更",
                    "qualificationName": "",
                    "isQualification": 0,
                    "isEnterpriseVerify": 2,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "MQ",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                  "id": "NA",
                  "name": "湖北翰林建设劳务有限公司",
                  "enterpriseName": "湖***司",
                  "unifiedSocialCreditCode": "91420500691796H522",
                  "tag": "",
                  "logo": {
                      "name": "siinlswj6d_1598923695917",
                      "identify": "siinlswj6d_1598923695917.png"
                  },
                  "businessLicense": {
                      "name": "1c6x13xpjy_1598923763457",
                      "identify": "1c6x13xpjy_1598923763457.jpg"
                  },
                  "powerAttorney": {
                      "name": "",
                      "identify": ""
                  },
                  "contactsName": "王洋",
                  "contactsCellphone": "13621267859",
                  "contactsArea": "湖北省,宜昌市,西陵区",
                  "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                  "legalPersonName": "",
                  "legalPersonCardId": "",
                  "legalPersonPositivePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonReversePhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "legalPersonHandheldPhoto": {
                      "name": "",
                      "identify": ""
                  },
                  "member": {
                      "id": "NA",
                      "avatar": [],
                      "nickName": "",
                      "gender": 1,
                      "birthday": "0000-00-00",
                      "area": "",
                      "address": "",
                      "briefIntroduction": "",
                      "cellphone": "",
                      "cellphoneMask": "",
                      "realName": "",
                      "userName": "",
                      "createTime": 0,
                      "createTimeFormat": "1970-01-01",
                      "updateTime": 0,
                      "updateTimeFormat": "1970-01-01",
                      "tag": ""
                  },
                  "realNameAuthenticationStatus": 2
                },
                "tag": "218,267"
            },
            {
                "id": "MTEw",
                "number": "FW20201224267",
                "title": "浙江线上教育平台项目融资",
                "cover": {
                    "name": "siinlswj6d_1598923695917",
                    "identify": "siinlswj6d_1598923695917.png"
                },
                "detail": [
                    {
                        "type": "text",
                        "value": "<p>浙江线上教育平台项目融资</p>"
                    }
                ],
                "price": [
                    {
                        "name": "10",
                        "value": "8700.00"
                    }
                ],
                "minPrice": "8700.00",
                "maxPrice": "8700.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 1,
                "pageViews": 84,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "212",
                "createTime": 1608779035,
                "createTimeFormat": "2020-12-24 11:03:55",
                "updateTime": 1618984438,
                "updateTimeFormat": "2021-04-21 13:53:58",
                "serviceObjects": [
                    {
                        "id": 2,
                        "name": "小型企业"
                    },
                    {
                        "id": 6,
                        "name": "个体商户"
                    }
                ],
                "serviceCategory": {
                    "id": "NS0",
                    "name": "其他",
                    "qualificationName": "",
                    "isQualification": 0,
                    "isEnterpriseVerify": 2,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "NA",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                    "id": "NA",
                    "name": "湖北翰林建设劳务有限公司",
                    "enterpriseName": "湖***司",
                    "unifiedSocialCreditCode": "91420500691796H522",
                    "tag": "",
                    "logo": {
                        "name": "siinlswj6d_1598923695917",
                        "identify": "siinlswj6d_1598923695917.png"
                    },
                    "businessLicense": {
                        "name": "1c6x13xpjy_1598923763457",
                        "identify": "1c6x13xpjy_1598923763457.jpg"
                    },
                    "powerAttorney": {
                        "name": "",
                        "identify": ""
                    },
                    "contactsName": "王洋",
                    "contactsCellphone": "13621267859",
                    "contactsArea": "湖北省,宜昌市,西陵区",
                    "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                    "legalPersonName": "",
                    "legalPersonCardId": "",
                    "legalPersonPositivePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonReversePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonHandheldPhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "member": {
                        "id": "NA",
                        "avatar": [],
                        "nickName": "",
                        "gender": 1,
                        "birthday": "0000-00-00",
                        "area": "",
                        "address": "",
                        "briefIntroduction": "",
                        "cellphone": "",
                        "cellphoneMask": "",
                        "realName": "",
                        "userName": "",
                        "createTime": 0,
                        "createTimeFormat": "1970-01-01",
                        "updateTime": 0,
                        "updateTimeFormat": "1970-01-01",
                        "tag": ""
                    },
                    "realNameAuthenticationStatus": 2
                },
                "tag": "15,22,29,85,92"
            },
            {
                "id": "MDQ",
                "number": "FW2020090119",
                "title": "浙江线下时尚平台实体店融资",
                "cover": {
                    "name": "zqk7w29ciu_1598952000004",
                    "identify": "zqk7w29ciu_1598952000004.jpg"
                },
                "detail": [
                  {
                    "type": "text",
                    "value": "<p>浙江线下时尚平台实体店融资</p>"
                  }
                ],
                "price": [
                    {
                        "name": "一次",
                        "value": "7000.00"
                    }
                ],
                "minPrice": "7000.00",
                "maxPrice": "7000.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 0,
                "pageViews": 232,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "",
                "createTime": 1598952018,
                "createTimeFormat": "2020-09-01 17:20:18",
                "updateTime": 1618984430,
                "updateTimeFormat": "2021-04-21 13:53:50",
                "serviceObjects": [
                    {
                        "id": 1,
                        "name": "微型企业"
                    }
                ],
                "serviceCategory": {
                    "id": "Mis",
                    "name": "核心专利代理",
                    "qualificationName": "专利代理机构注册证",
                    "isQualification": 2,
                    "isEnterpriseVerify": 2,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "MA",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                    "id": "NA",
                    "name": "湖北翰林建设劳务有限公司",
                    "enterpriseName": "湖***司",
                    "unifiedSocialCreditCode": "91420500691796H522",
                    "tag": "",
                    "logo": {
                        "name": "siinlswj6d_1598923695917",
                        "identify": "siinlswj6d_1598923695917.png"
                    },
                    "businessLicense": {
                        "name": "1c6x13xpjy_1598923763457",
                        "identify": "1c6x13xpjy_1598923763457.jpg"
                    },
                    "powerAttorney": {
                        "name": "",
                        "identify": ""
                    },
                    "contactsName": "王洋",
                    "contactsCellphone": "13621267859",
                    "contactsArea": "湖北省,宜昌市,西陵区",
                    "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                    "legalPersonName": "",
                    "legalPersonCardId": "",
                    "legalPersonPositivePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonReversePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonHandheldPhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "member": {
                        "id": "NA",
                        "avatar": [],
                        "nickName": "",
                        "gender": 1,
                        "birthday": "0000-00-00",
                        "area": "",
                        "address": "",
                        "briefIntroduction": "",
                        "cellphone": "",
                        "cellphoneMask": "",
                        "realName": "",
                        "userName": "",
                        "createTime": 0,
                        "createTimeFormat": "1970-01-01",
                        "updateTime": 0,
                        "updateTimeFormat": "1970-01-01",
                        "tag": ""
                    },
                    "realNameAuthenticationStatus": 2
                },
                "tag": "15,22,29,85,92"
            },
            {
                "id": "Niw",
                "number": "FW2020102971",
                "title": "四川家政服务和废品回收信息服务平台融资",
                "cover": {
                    "name": "15c3ocyj54_1603962535439",
                    "identify": "15c3ocyj54_1603962535439.jpg"
                },
                "detail": [
                    {
                        "type": "text",
                        "value": "<p>四川家政服务和废品回收信息服务平台融资</p>"
                    }
                ],
                "price": [
                    {
                        "name": "1",
                        "value": "300.00"
                    }
                ],
                "minPrice": "300.00",
                "maxPrice": "300.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 2,
                "pageViews": 240,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "",
                "createTime": 1603962667,
                "createTimeFormat": "2020-10-29 17:11:07",
                "updateTime": 1618984427,
                "updateTimeFormat": "2021-04-21 13:53:47",
                "serviceObjects": [],
                "serviceCategory": {
                    "id": "NS0",
                    "name": "其他",
                    "qualificationName": "",
                    "isQualification": 0,
                    "isEnterpriseVerify": 2,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "NA",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                    "id": "NA",
                    "name": "湖北翰林建设劳务有限公司",
                    "enterpriseName": "湖***司",
                    "unifiedSocialCreditCode": "91420500691796H522",
                    "tag": "",
                    "logo": {
                        "name": "siinlswj6d_1598923695917",
                        "identify": "siinlswj6d_1598923695917.png"
                    },
                    "businessLicense": {
                        "name": "1c6x13xpjy_1598923763457",
                        "identify": "1c6x13xpjy_1598923763457.jpg"
                    },
                    "powerAttorney": {
                        "name": "",
                        "identify": ""
                    },
                    "contactsName": "王洋",
                    "contactsCellphone": "13621267859",
                    "contactsArea": "湖北省,宜昌市,西陵区",
                    "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                    "legalPersonName": "",
                    "legalPersonCardId": "",
                    "legalPersonPositivePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonReversePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonHandheldPhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "member": {
                        "id": "NA",
                        "avatar": [],
                        "nickName": "",
                        "gender": 1,
                        "birthday": "0000-00-00",
                        "area": "",
                        "address": "",
                        "briefIntroduction": "",
                        "cellphone": "",
                        "cellphoneMask": "",
                        "realName": "",
                        "userName": "",
                        "createTime": 0,
                        "createTimeFormat": "1970-01-01",
                        "updateTime": 0,
                        "updateTimeFormat": "1970-01-01",
                        "tag": ""
                    },
                    "realNameAuthenticationStatus": 2
                },
                "tag": "43,50,99,106,113"
            },
            {
                "id": "MTM",
                "number": "FW2020091828",
                "title": "四川太阳能浮台无线水质监测站融资",
                "cover": {
                    "name": "xgjehyrr64_1616639979206",
                    "identify": "xgjehyrr64_1616639979206.png"
                },
                "detail": [
                    {
                        "type": "image",
                        "value": "w39o1otu32_1616640038136.jpg"
                    }
                ],
                "price": [
                    {
                        "name": "1",
                        "value": "5888.00"
                    }
                ],
                "minPrice": "5888.00",
                "maxPrice": "5888.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 4,
                "pageViews": 408,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "caichaocoajvad",
                "createTime": 1600416014,
                "createTimeFormat": "2020-09-18 16:00:14",
                "updateTime": 1618984424,
                "updateTimeFormat": "2021-04-21 13:53:44",
                "serviceObjects": [
                    {
                        "id": 1,
                        "name": "微型企业"
                    },
                    {
                        "id": 2,
                        "name": "小型企业"
                    },
                    {
                        "id": 3,
                        "name": "中型企业"
                    },
                    {
                        "id": 4,
                        "name": "大型企业"
                    },
                    {
                        "id": 5,
                        "name": "创业个人"
                    },
                    {
                        "id": 6,
                        "name": "个体商户"
                    },
                    {
                        "id": 7,
                        "name": "其他"
                    }
                ],
                "serviceCategory": {
                    "id": "Mg",
                    "name": "招聘外包",
                    "qualificationName": "人才中介服务许可证",
                    "isQualification": 0,
                    "isEnterpriseVerify": 0,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "OA",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                    "id": "NA",
                    "name": "湖北翰林建设劳务有限公司",
                    "enterpriseName": "湖***司",
                    "unifiedSocialCreditCode": "91420500691796H522",
                    "tag": "",
                    "logo": {
                        "name": "siinlswj6d_1598923695917",
                        "identify": "siinlswj6d_1598923695917.png"
                    },
                    "businessLicense": {
                        "name": "1c6x13xpjy_1598923763457",
                        "identify": "1c6x13xpjy_1598923763457.jpg"
                    },
                    "powerAttorney": {
                        "name": "",
                        "identify": ""
                    },
                    "contactsName": "王洋",
                    "contactsCellphone": "13621267859",
                    "contactsArea": "湖北省,宜昌市,西陵区",
                    "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                    "legalPersonName": "",
                    "legalPersonCardId": "",
                    "legalPersonPositivePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonReversePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonHandheldPhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "member": {
                        "id": "NA",
                        "avatar": [],
                        "nickName": "",
                        "gender": 1,
                        "birthday": "0000-00-00",
                        "area": "",
                        "address": "",
                        "briefIntroduction": "",
                        "cellphone": "",
                        "cellphoneMask": "",
                        "realName": "",
                        "userName": "",
                        "createTime": 0,
                        "createTimeFormat": "1970-01-01",
                        "updateTime": 0,
                        "updateTimeFormat": "1970-01-01",
                        "tag": ""
                    },
                    "realNameAuthenticationStatus": 2
                },
                "tag": "43,50,99,106,113"
            },
            {
                "id": "MS8v",
                "number": "FW20201212246",
                "title": "江西奶茶融资",
                "cover": {
                    "name": "7fvqzs6xpo_1616640603210",
                    "identify": "7fvqzs6xpo_1616640603210.png"
                },
                "detail": [
                    {
                        "type": "text",
                        "value": "<p><span style=\"color: #333333; font-family: 微软雅黑; font-size: 15px; background-color: #ffffff;\">一次性提交资料，全程托管，手机随时查看办理进度；<br /></span><span style=\"color: #333333; font-family: 微软雅黑; font-size: 15px; background-color: #ffffff;\">价格优惠透明，明码标价签订记账合同，全程无隐形收费；<br /></span><span style=\"color: #333333; font-family: 微软雅黑; font-size: 15px; background-color: #ffffff;\">专业业务上门收取票据，为您省时省心。</span></p>"
                    },
                    {
                        "type": "image",
                        "value": "sgrhf441hc_1616639706612.jpg"
                    }
                ],
                "price": [
                    {
                        "name": "小规模纳税人0申报/年",
                        "value": "1000.00"
                    },
                    {
                        "name": "小规模纳税人正常户/两年",
                        "value": "4000.00"
                    }
                ],
                "minPrice": "1000.00",
                "maxPrice": "4000.00",
                "contract": [],
                "attentionDegree": 0,
                "volume": 4,
                "pageViews": 349,
                "applyStatus": 2,
                "status": 0,
                "rejectReason": "",
                "createTime": 1607761221,
                "createTimeFormat": "2020-12-12 16:20:21",
                "updateTime": 1618984416,
                "updateTimeFormat": "2021-04-21 13:53:36",
                "serviceObjects": [
                    {
                        "id": 1,
                        "name": "微型企业"
                    },
                    {
                        "id": 2,
                        "name": "小型企业"
                    },
                    {
                        "id": 3,
                        "name": "中型企业"
                    },
                    {
                        "id": 4,
                        "name": "大型企业"
                    },
                    {
                        "id": 5,
                        "name": "创业个人"
                    },
                    {
                        "id": 6,
                        "name": "个体商户"
                    }
                ],
                "serviceCategory": {
                    "id": "NCw",
                    "name": "财税管理培训",
                    "qualificationName": "",
                    "isQualification": 0,
                    "isEnterpriseVerify": 2,
                    "commission": 5,
                    "status": 0,
                    "parentCategory": {
                        "id": "MQ",
                        "name": "",
                        "status": 0
                    }
                },
                "enterprise": {
                    "id": "NA",
                    "name": "湖北翰林建设劳务有限公司",
                    "enterpriseName": "湖***司",
                    "unifiedSocialCreditCode": "91420500691796H522",
                    "tag": "",
                    "logo": {
                        "name": "siinlswj6d_1598923695917",
                        "identify": "siinlswj6d_1598923695917.png"
                    },
                    "businessLicense": {
                        "name": "1c6x13xpjy_1598923763457",
                        "identify": "1c6x13xpjy_1598923763457.jpg"
                    },
                    "powerAttorney": {
                        "name": "",
                        "identify": ""
                    },
                    "contactsName": "王洋",
                    "contactsCellphone": "13621267859",
                    "contactsArea": "湖北省,宜昌市,西陵区",
                    "contactsAddress": "湖北省宜昌市西陵区高新技术产业开发区分局",
                    "legalPersonName": "",
                    "legalPersonCardId": "",
                    "legalPersonPositivePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonReversePhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "legalPersonHandheldPhoto": {
                        "name": "",
                        "identify": ""
                    },
                    "member": {
                        "id": "NA",
                        "avatar": [],
                        "nickName": "",
                        "gender": 1,
                        "birthday": "0000-00-00",
                        "area": "",
                        "address": "",
                        "briefIntroduction": "",
                        "cellphone": "",
                        "cellphoneMask": "",
                        "realName": "",
                        "userName": "",
                        "createTime": 0,
                        "createTimeFormat": "1970-01-01",
                        "updateTime": 0,
                        "updateTimeFormat": "1970-01-01",
                        "tag": ""
                    },
                    "realNameAuthenticationStatus": 2
                },
                "tag": "43,50,99,106,113"
            }
        ]',true),
          'enterprises' =>array(
              array(
                  "id" => "MCs",
                  "name" => "星石科技",
                  "enterpriseName" => "星**技",
                  "logo" => array(
                          "name" => "u2dk4o8ggz_1598929093764",
                          "identify" => "u2dk4o8ggz_1598929093764.jpg"
                  ),
                  "serviceCount" => 9,
                  "serviceEmployer" => 4
              ),
              array(
                  "id" => "Mg",
                  "name" => "台州市椒江岳荣电子商务商行",
                  "enterpriseName" => "台***行",
                  "logo" => array(
                          "name" => "20201031160419ech0ro.png",
                          "identify" => "20201031160419ech0ro.png"
                  ),
                  "serviceCount" => 5,
                  "serviceEmployer" => 107
              ),
              array(
                  "id" => "NA",
                  "name" => "湖北翰林建设劳务有限公司",
                  "enterpriseName" => "湖***司",
                  "logo" => array(
                          "name" => "siinlswj6d_1598923695917",
                          "identify" => "siinlswj6d_1598923695917.png"
                      ),
                  "serviceCount" => 5,
                  "serviceEmployer" => 165
              ),
              array(
                  "id" => "MC0p",
                  "name" => "陕西中澳电子科技有限公司",
                  "enterpriseName" => "陕***司",
                  "logo" => array(
                          "name" => "7xuzc4wvsm_1607709449406",
                          "identify" => "7xuzc4wvsm_1607709449406.jpg",
                  ),
                  "serviceCount" => 4,
                  "serviceEmployer" => 5
              ),
              array(
                  "id" => "NjM",
                  "name" => "中国大陆工商局",
                  "enterpriseName" => "中***局",
                  "logo" => array(
                          "name" => "7dkyybaymi_1619023286013",
                          "identify" => "7dkyybaymi_1619023286013.jpeg"
                  ),
                  "serviceCount" => 3,
                  "serviceEmployer" => 0
              ),
              array(
                  "id" => "Nw",
                  "name" => "宜昌联合奥美五金责任有限公司",
                  "enterpriseName" => "宜***司",
                  "logo" => array(
                          "name" => "w7cs1qxm1k_1598928852494",
                          "identify" => "w7cs1qxm1k_1598928852494.png"
                  ),
                  "serviceCount" => 3,
                  "serviceEmployer" => 4
              ),
              array(
                    "id" => "MCw",
                    "name" => "湖北金麦会计代理有限公司",
                    "enterpriseName" => "湖***司",
                    "logo" => array (
                            "name" => "pv6isx3ylz_1598960286554",
                            "identify" =>" pv6isx3ylz_1598960286554.png",
                    ),
                    "serviceCount" => 2,
                    "serviceEmployer" => 19
              ),
              array(
                    "id" => "MjE",
                    "name" => "杭州网易严选贸易有限公司",
                    "enterpriseName" => "杭***司",
                    "logo" => array (
                            "name" => "20201102165729fu2x8k.png",
                            "identify" => "20201102165729fu2x8k.png"
                    ),
                    "serviceCount" => 1,
                    "serviceEmployer" => 9
              )
          ),
          'serviceRequirements' =>json_decode('[
            {
                "id": "MTAs",
                "title": "安徽旅游平台融资",
                "minPrice": "1.00",
                "maxPrice": "100.00",
                "validityStartTime": 1621209600,
                "validityStartTimeFormat": "2021.05.17",
                "validityEndTime": 1622678400,
                "validityEndTimeFormat": "2021.06.03",
                "createTime": 1619814705,
                "createTimeFormat": "2021-05-01 04:31:45",
                "serviceCategory": {
                    "id": "MCws",
                    "name": "房地产规划",
                    "parentCategory": {
                        "id": "MS0"
                    }
                }
            },
            {
                "id": "MTEw",
                "title": "北京外语情景教学APP商业融资",
                "minPrice": "1.00",
                "maxPrice": "300.00",
                "validityStartTime": 1622073600,
                "validityStartTimeFormat": "2021.05.27",
                "validityEndTime": 1622246400,
                "validityEndTimeFormat": "2021.05.29",
                "createTime": 1619815606,
                "createTimeFormat": "2021-05-01 04:46:46",
                "serviceCategory": {
                    "id": "MA",
                    "name": "人力资源管理咨询",
                    "parentCategory": {
                        "id": "MA"
                    }
                }
            },
            {
                "id": "MDEy",
                "title": "广西品牌营销管理系统融资",
                "minPrice": "1.00",
                "maxPrice": "560.00",
                "validityStartTime": 1619308800,
                "validityStartTimeFormat": "2021.04.25",
                "validityEndTime": 1619568000,
                "validityEndTimeFormat": "2021.04.28",
                "createTime": 1619426142,
                "createTimeFormat": "2021-04-26 16:35:42",
                "serviceCategory": {
                    "id": "MA",
                    "name": "人力资源管理咨询",
                    "parentCategory": {
                        "id": "MA"
                    }
                }
            },
            {
                "id": "MDEr",
                "title": "重庆康养田园综合体项目融资",
                "minPrice": "200.00",
                "maxPrice": "700.00",
                "validityStartTime": "",
                "validityStartTimeFormat": "",
                "validityEndTime": "",
                "validityEndTimeFormat": "",
                "createTime": 1619422208,
                "createTimeFormat": "2021-04-26 15:30:08",
                "serviceCategory": {
                    "id": "NCs",
                    "name": "经营管理培训",
                    "parentCategory": {
                        "id": "Mw"
                    }
                }
            },
            {
                "id": "MDAu",
                "title": "山西社区服务平台项目融资",
                "minPrice": "1.00",
                "maxPrice": "900.00",
                "validityStartTime": "",
                "validityStartTimeFormat": "",
                "validityEndTime": "",
                "validityEndTimeFormat": "",
                "createTime": 1619414187,
                "createTimeFormat": "2021-04-26 13:16:27",
                "serviceCategory": {
                    "id": "MDQ",
                    "name": "法律援助",
                    "parentCategory": {
                        "id": "MA"
                    }
                }
            },
            {
                "id": "MC8x",
                "title": "浙江无人机智能巡检融资",
                "minPrice": "10.00",
                "maxPrice": "1300.00",
                "validityStartTime": "",
                "validityStartTimeFormat": "",
                "validityEndTime": "",
                "validityEndTimeFormat": "",
                "createTime": 1619414053,
                "createTimeFormat": "2021-04-26 13:14:13",
                "serviceCategory": {
                    "id": "Mg",
                    "name": "招聘外包",
                    "parentCategory": {
                        "id": "MQ"
                    }
                }
            },
            {
                "id": "MC0p",
                "title": "深圳智慧猪场物联网解决方案项目融资",
                "minPrice": "1.00",
                "maxPrice": "2000.00",
                "validityStartTime": 1618790400,
                "validityStartTimeFormat": "2021.04.19",
                "validityEndTime": 1619740800,
                "validityEndTimeFormat": "2021.04.30",
                "createTime": 1618814222,
                "createTimeFormat": "2021-04-19 14:37:02",
                "serviceCategory": {
                    "id": "NA",
                    "name": "企业变更",
                    "parentCategory": {
                        "id": "Mg"
                    }
                }
            },
            {
                "id": "NzA",
                "title": "江苏宠物电商服务平台项目融资",
                "minPrice": "20.00",
                "maxPrice": "300.00",
                "validityStartTime": 1616284800,
                "validityStartTimeFormat": "2021.03.21",
                "validityEndTime": 1617062400,
                "validityEndTimeFormat": "2021.03.30",
                "createTime": 1616296693,
                "createTimeFormat": "2021-03-21 11:18:13",
                "serviceCategory": {
                    "id": "MCw",
                    "name": "年度财务审计",
                    "parentCategory": {
                        "id": "Ng"
                    }
                }
            },
            {
                "id": "NS8",
                "title": "湖北励志品牌白酒融资",
                "minPrice": "100.00",
                "maxPrice": "200.00",
                "validityStartTime": 1611100800,
                "validityStartTimeFormat": "2021.01.20",
                "validityEndTime": 1621468800,
                "validityEndTimeFormat": "2021.05.20",
                "createTime": 1611126565,
                "createTimeFormat": "2021-01-20 15:09:25",
                "serviceCategory": {
                    "id": "MCw",
                    "name": "年度财务审计",
                    "parentCategory": {
                        "id": "Ng"
                    }
                }
            },
            {
                "id": "MjE",
                "title": "浙江互联网＋法律平台融资",
                "minPrice": "1.00",
                "maxPrice": "230.0",
                "validityStartTime": 1606694400,
                "validityStartTimeFormat": "2020.11.30",
                "validityEndTime": 1609372800,
                "validityEndTimeFormat": "2020.12.31",
                "createTime": 1606725229,
                "createTimeFormat": "2020-11-30 16:33:49",
                "serviceCategory": {
                    "id": "MzI",
                    "name": "计算机网络服务",
                    "parentCategory": {
                        "id": "Ng"
                    }
                }
            },
            {
                "id": "MTA",
                "title": "河南供应链新零售项目融资",
                "minPrice": "10.00",
                "maxPrice": "500.00",
                "validityStartTime": "",
                "validityStartTimeFormat": "",
                "validityEndTime": "",
                "validityEndTimeFormat": "",
                "createTime": 1603086309,
                "createTimeFormat": "2020-10-19 13:45:09",
                "serviceCategory": {
                    "id": "MDE",
                    "name": "税务代理",
                    "parentCategory": {
                        "id": "Mg"
                    }
                }
            }
        ]',true),

        );
        $cacheData = $this->getHomeFragmentCacheQuery()->get();
        $data['policies'] = $cacheData['policy'];

        $this->render(new HomeView($data));
        return true;
    }

    protected function fetchOneAction($id) : bool
    {
        unset($id);
        return false;
    }
}
