<?php
namespace SuperMarket\ProductMarket\Crew\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\Crew\Model\Crew;

class CrewSessionTranslator implements ITranslator
{
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function arrayToObject(array $expression, $crew = null)
    {
        if ($crew == null) {
            $crew = new Crew($expression['id']);
        }
        if (isset($expression['cellphone'])) {
            $crew->setCellphone($expression['cellphone']);
        }
        if (isset($expression['userName'])) {
            $crew->setUserName($expression['userName']);
        }
        if (isset($expression['realName'])) {
            $crew->setRealName($expression['realName']);
        }
        if (isset($expression['identify'])) {
            $crew->setIdentify($expression['identify']);
        }
        if (isset($expression['avatar'])) {
            $crew->setAvatar($expression['avatar']);
        }
        if (isset($expression['roles'])) {
            $crew->setRoles($expression['roles']);
        }
        
        return $crew;
    }
    
    public function arrayToObjects(array $expression) : array
    {
        return [
            $expression['id'] => $this->arrayToObject($expression)
        ];
    }

    public function objectToArray($crew, array $keys = array())
    {
        unset($keys);
        $expression = array();

        $expression['id'] = $crew->getId();
        $expression['userName'] = $crew->getUserName();
        $expression['cellphone'] = $crew->getCellphone();
        $expression['realName'] = $crew->getRealName();
        $expression['identify'] = $crew->getIdentify();
        $expression['avatar'] = $crew->getAvatar();
        $expression['roles'] = $crew->getRoles();
        
        return $expression;
    }
}
