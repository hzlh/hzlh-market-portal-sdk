<?php
namespace SuperMarket\ProductMarket\Crew\Translator;

use Sdk\Crew\Model\NullCrew;
use Sdk\Crew\Model\Crew;

use SuperMarket\ProductMarket\User\Translator\UserTranslator;

use Marmot\Framework\Classes\Filter;

class CrewTranslator extends UserTranslator
{
    public function arrayToObject(array $expression, $crew = null)
    {
        unset($crew);
        unset($expression);
        return NullCrew::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        $user = parent::objectToArray($crew, $keys);

        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
               'workNumber',
               'cellphone',
               'status',
               'remark',
               'createTime',
               'signInTime',
            );
        }

        $expression = array();

        if (in_array('workNumber', $keys)) {
            $expression['workNumber'] = $crew->getWorkNumber();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $crew->getCellphone();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $crew->getStatus();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $crew->getRemark()));//phpcs:ignore
            $expression['remarkTextArea'] = Filter::dhtmlspecialchars(str_replace("ℑ", "\n", $crew->getRemark()));//phpcs:ignore
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $crew->getCreateTime();
        }
        if (in_array('signInTime', $keys)) {
            $expression['signInTime'] = $crew->getSignInTime();
        }
        
        $expression = array_merge($user, $expression);

        return $expression;
    }
}
