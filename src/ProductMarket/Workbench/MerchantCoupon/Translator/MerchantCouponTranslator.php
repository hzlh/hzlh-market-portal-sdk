<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantCoupon\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\MerchantCoupon\Model\NullMerchantCoupon;

use SuperMarket\ProductMarket\UserCenter\Enterprise\Translator\EnterpriseTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class MerchantCouponTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $merchantCoupon = null)
    {
        unset($merchantCoupon);
        unset($expression);
        return NullMerchantCoupon::getInstance();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($merchantCoupon, array $keys = array())
    {
        if (!$merchantCoupon instanceof MerchantCoupon) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'releaseType',
                'number',
                'name',
                'couponType',
                'denomination',
                'discount',
                'useStandard',
                'validityStartTime',
                'validityEndTime',
                'issueTotal',
                'distributeTotal',
                'perQuota',
                'useScenario',
                'receivingMode',
                'receivingUsers',
                'isSuperposition',
                'applyScope',
                'applySituation',
                'status',
                'createTime',
                'updateTime',
                'reference'=>['id','name'],
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($merchantCoupon->getId());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $merchantCoupon->getStatus();
        }
        if (in_array('releaseType', $keys)) {
            $expression['releaseType'] = $merchantCoupon->getReleaseType();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $merchantCoupon->getNumber();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $merchantCoupon->getName();
        }
        if (in_array('couponType', $keys)) {
            $expression['couponType'] = $merchantCoupon->getCouponType();
        }
        if (in_array('denomination', $keys)) {
            $expression['denomination'] = empty($merchantCoupon->getDenomination())
            ? '' : number_format($merchantCoupon->getDenomination(), 2, '.', '');
        }
        if (in_array('discount', $keys)) {
            $expression['discount'] = $merchantCoupon->getDiscount();
        }
        if (in_array('useStandard', $keys)) {
            $expression['useStandard'] = empty($merchantCoupon->getUseStandard())
            ? '' : number_format($merchantCoupon->getUseStandard(), 2, '.', '');
        }
        if (in_array('issueTotal', $keys)) {
            $expression['issueTotal'] = $merchantCoupon->getIssueTotal();
        }
        if (in_array('distributeTotal', $keys)) {
            $expression['distributeTotal'] = $merchantCoupon->getDistributeTotal();
        }
        if (in_array('perQuota', $keys)) {
            $expression['perQuota'] = $merchantCoupon->getPerQuota();
        }
        if (in_array('useScenario', $keys)) {
            $expression['useScenario'] = $merchantCoupon->getUseScenario();
        }
        if (in_array('receivingMode', $keys)) {
            $expression['receivingMode'] = $merchantCoupon->getReceivingMode();
        }
        if (in_array('receivingUsers', $keys)) {
            $expression['receivingUsers'] = $merchantCoupon->getReceivingUsers();
        }
        if (in_array('isSuperposition', $keys)) {
            $expression['isSuperposition'] = $merchantCoupon->getIsSuperposition();
        }
        if (in_array('applyScope', $keys)) {
            $expression['applyScope'] = $merchantCoupon->getApplyScope();
        }
        if (in_array('applySituation', $keys)) {
            $expression['applySituation'] = $merchantCoupon->getApplySituation();
        }
        if (in_array('validityStartTime', $keys)) {
            $expression['validityStartTime'] = intval($merchantCoupon->getValidityStartTime());
            $expression['validityStartTimeFormat'] = ($expression['validityStartTime'] != 0)
                ? date('Y.m.d', $expression['validityStartTime']) : 0;
        }
        if (in_array('validityEndTime', $keys)) {
            $expression['validityEndTime'] = intval($merchantCoupon->getValidityEndTime());
            $expression['validityEndTimeFormat'] = ($expression['validityEndTime'] != 0)
                ? date('Y.m.d', $expression['validityEndTime']) : 0;
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $merchantCoupon->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $merchantCoupon->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $merchantCoupon->getUpdateTime();
        }
        if (isset($keys['reference'])) {
            $expression['reference'] = $this->getEnterpriseTranslator()->objectToArray(
                $merchantCoupon->getReference(),
                $keys['reference']
            );
        }

        return $expression;
    }
}
