<?php
namespace SuperMarket\ProductMarket\Workbench\EnterpriseWorkbench\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Common\Controller\Traits\GlobalCheckRolesTrait;
use Workbench\EnterpriseWorkbench\View\IndexViewTrait;

class IndexView extends TemplateView implements IView
{
    use IndexViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $authentications = $this->getAuthentication();
        $permission = $this->workbenchesRoles();
 
        $this->getView()->display(
            'ProductMarket/EnterpriseWorkbench/Index.tpl',
            [
                'nav_left' => NavTrait::NAV_WORKBENCH['IDENTITY'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_IDENTITY'],
                'authentications' => $authentications['authentication'],
                'financeAuthentication' => $authentications['financeAuthentication'],
                'permission' => $permission
            ]
        );
    }
}
