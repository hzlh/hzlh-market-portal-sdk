<?php
namespace SuperMarket\ProductMarket\Workbench\EnterpriseWorkbench\View;

use Workbench\FinanceAuthentication\Translator\UnAuditedFinanceAuthenticationTranslator;

trait IndexViewTrait
{
    private $data;

    private $unAuditedFinanceTranslator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->unAuditedFinanceTranslator = new UnAuditedFinanceAuthenticationTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->unAuditedFinanceTranslator);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getUnAuditedFinanceTranslator() : UnAuditedFinanceAuthenticationTranslator
    {
        return $this->unAuditedFinanceTranslator;
    }

    public function getAuthentication()
    {
        $data = $this->getData();

        $authentication = !empty($data['authentications']) ? $data['authentications'][1] : array();

        $unAuditedFinance = !empty($data['unAuditedFinanceAuthentication'])
             ? $data['unAuditedFinanceAuthentication'][1] : array();

        $unAuditedFinanceTranslator = $this->getUnAuditedFinanceTranslator();

        $unAuditedFinanceArray = array();
        if (!empty($unAuditedFinance)) {
            foreach ($unAuditedFinance as $unAuditedFinanceVal) {
                $unAuditedFinanceArray = $unAuditedFinanceTranslator->objectToArray(
                    $unAuditedFinanceVal
                );
            }
        }

        $authenticationList = array(
            'authentication' => $authentication,
            'financeAuthentication' => $unAuditedFinanceArray
        );

        return $authenticationList;
    }
}
