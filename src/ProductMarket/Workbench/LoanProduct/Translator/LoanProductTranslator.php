<?php
namespace SuperMarket\ProductMarket\Workbench\LoanProduct\Translator;

use Marmot\Framework\Classes\Filter;
use Marmot\Interfaces\ITranslator;

use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\LoanProduct\Model\NullLoanProduct;

use SuperMarket\ProductMarket\Label\Translator\LabelTranslator;
use Snapshot\Translator\SnapshotTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;
use Workbench\LoanProduct\Translator\LoanProductCategoryTranslator;
use Dictionary\Translator\DictionaryTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class LoanProductTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $loanProduct = null)
    {
        unset($expression);
        unset($loanProduct);
        return NullLoanProduct::getInstance();
    }

    protected function getLabelTranslator() : LabelTranslator
    {
        return new LabelTranslator();
    }

    protected function getLoanProductCategoryTranslator() : LoanProductCategoryTranslator
    {
        return new LoanProductCategoryTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getDictionaryTranslator() : DictionaryTranslator
    {
        return new DictionaryTranslator();
    }

    protected function getSnapshotsTranslator() : SnapshotTranslator
    {
        return new SnapshotTranslator();
    }

    public function objectToArray($loanProduct, array $keys = array())
    {
        if (!$loanProduct instanceof LoanProduct) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'tag',
                'number',
                'introduction',
                'loanType',
                'associatedPolicies',
                'applicationMaterial',
                'applicationCondition',
                'cover',
                'supportCity',
                'contract',
                'application',
                'minLoanAmount',
                'maxLoanAmount',
                'loanInterestRate',
                'loanMonthInterestRate',
                'minLoanPeriod',
                'maxLoanPeriod',
                'minLoanTerm',
                'maxLoanTerm',
                'volume',
                'volumeSuccess',
                'pageViews',
                'loanTermUnit'=>[],
                'loanInterestRateUnit'=>[],
                'isSupportEarlyRepayment'=>[],
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost'=>[],
                'repaymentMethods'=>[],
                'guarantyStyles'=>[],
                'productObject'=>[],
                'labels' => ['id', 'name'],
                'enterprise'=>[],
                'snapshots'=>[],
                'rejectReason',
                'applyStatus',
                'status',
                'createTime',
                'updateTime',
                'attentionDegree',
                'againLoanType'
            );
        }
        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($loanProduct->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $loanProduct->getTitle();
        }
        if (in_array('tag', $keys)) {
            $expression['tag'] = $loanProduct->getTag();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $loanProduct->getNumber();
        }
        if (in_array('introduction', $keys)) {
            $expression['introduction'] = Filter::dhtmlspecialchars(
                str_replace("ℑ", "&lt;br&gt;", $loanProduct->getIntroduction())
            );
        }
        if (in_array('loanType', $keys)) {
            $expression['loanType'] = $loanProduct->getLoanType();
        }
        if (in_array('associatedPolicies', $keys)) {
            $associatedPolicies = $loanProduct->getAssociatedPolicies();
            foreach ($associatedPolicies as $key => $value) {
                $associatedPolicies[$key]['description'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $value['description'])); //phpcs:ignore
                $associatedPolicies[$key]['releaseTimeFormat'] = date('Y年m月d日', $value['releaseTime']);
            }
            $expression['associatedPolicies'] = $associatedPolicies;
        }
        if (in_array('applicationMaterial', $keys)) {
            $expression['applicationMaterial'] = $loanProduct->getApplicationMaterial();
        }
        if (in_array('applicationCondition', $keys)) {
            $applicationCondition = $loanProduct->getApplicationCondition();
            foreach ($applicationCondition as $item => $value) {
                foreach ($value as $key => $each) {
                    $applicationCondition[$item][$key]['description'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $each['description'])); //phpcs:ignore
                }
            }
            $expression['applicationCondition'] = $applicationCondition;
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $loanProduct->getCover();
        }
        if (in_array('supportCity', $keys)) {
            $expression['supportCity'] = $loanProduct->getSupportCity();

            $supportCities = array();
            foreach ($expression['supportCity'] as $supportCity) {
                $supportCities[] = str_replace(',', '-', $supportCity);
            }

            $expression['supportCityString'] = join('、', $supportCities);
            $expression['supportCityStringLess'] = join('、', array_slice($supportCities, 0, 2));
        }
        if (in_array('contract', $keys)) {
            $expression['contract'] = $loanProduct->getContract();
        }
        if (in_array('application', $keys)) {
            $expression['application'] = $loanProduct->getApplication();
        }
        if (in_array('minLoanAmount', $keys)) {
            $expression['minLoanAmount'] = $loanProduct->getMinLoanAmount();
        }
        if (in_array('maxLoanAmount', $keys)) {
            $expression['maxLoanAmount'] = $loanProduct->getMaxLoanAmount();
        }
        if (in_array('loanInterestRate', $keys)) {
            $expression['loanInterestRate'] = $loanProduct->getLoanInterestRate();
        }
        if (in_array('loanMonthInterestRate', $keys)) {
            $expression['loanMonthInterestRate'] = $loanProduct->getLoanMonthInterestRate();
        }
        if (in_array('minLoanPeriod', $keys)) {
            $expression['minLoanPeriod'] = $loanProduct->getMinLoanPeriod();
        }
        if (in_array('maxLoanPeriod', $keys)) {
            $expression['maxLoanPeriod'] = $loanProduct->getMaxLoanPeriod();
        }
        if (in_array('minLoanTerm', $keys)) {
            $expression['minLoanTerm'] = $loanProduct->getMinLoanTerm();
        }
        if (in_array('maxLoanTerm', $keys)) {
            $expression['maxLoanTerm'] = $loanProduct->getMaxLoanTerm();
        }
        if (in_array('earlyRepaymentTerm', $keys)) {
            $expression['earlyRepaymentTerm'] = $loanProduct->getEarlyRepaymentTerm();
        }
        if (in_array('volume', $keys)) {
            $expression['volume'] = $loanProduct->getVolume();
        }
        if (in_array('volumeSuccess', $keys)) {
            $expression['volumeSuccess'] = $loanProduct->getVolumeSuccess();
        }
        if (in_array('pageViews', $keys)) {
            $expression['pageViews'] = $loanProduct->getPageViews();
        }
        if (isset($keys['loanTermUnit'])) {
            $expression['loanTermUnit'] = $this->getLoanProductCategoryTranslator()->objectToArray(
                $loanProduct->getLoanTermUnit(),
                $keys['loanTermUnit']
            );
        }
        if (isset($keys['loanInterestRateUnit'])) {
            $expression['loanInterestRateUnit'] = $this->getLoanProductCategoryTranslator()->objectToArray(
                $loanProduct->getLoanInterestRateUnit(),
                $keys['loanInterestRateUnit']
            );
        }
        if (isset($keys['isSupportEarlyRepayment'])) {
            $expression['isSupportEarlyRepayment'] = $this->getLoanProductCategoryTranslator()->objectToArray(
                $loanProduct->getIsSupportEarlyRepayment(),
                $keys['isSupportEarlyRepayment']
            );
        }
        if (isset($keys['isExistEarlyRepaymentCost'])) {
            $expression['isExistEarlyRepaymentCost'] = $this->getLoanProductCategoryTranslator()->objectToArray(
                $loanProduct->getIsExistEarlyRepaymentCost(),
                $keys['isExistEarlyRepaymentCost']
            );
        }
        if (isset($keys['repaymentMethods'])) {
            $expression['repaymentMethods'] = $this->repaymentMethods($keys, $loanProduct);
        }
        if (isset($keys['guarantyStyles'])) {
            $expression['guarantyStyles'] = $this->guarantyStyles($keys, $loanProduct);
        }
        if (isset($keys['productObject'])) {
            $expression['productObject'] = $this->productObject($keys, $loanProduct);
        }
        if (isset($keys['labels'])) {
            $expression['labels'] = $this->labels($keys, $loanProduct);
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $loanProduct->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $loanProduct->getRejectReason();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $loanProduct->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $loanProduct->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $loanProduct->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $expression['updateTime']);
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $loanProduct->getEnterprise(),
                $keys['enterprise']
            );
        }
        if (isset($keys['snapshots'])) {
            $expression['snapshots'] = $this->getSnapshotsTranslator()->objectToArray($loanProduct->getSnapshot());
            // foreach ($loanProduct->getSnapshots() as $snapshot) {
            //     $expression['snapshots'] = $this->getSnapshotsTranslator()->objectToArray($snapshot);
            // }
        }
        if (in_array('attentionDegree', $keys)) {
            $expression['attentionDegree'] = $loanProduct->getAttentionDegree();
        }
        if (in_array('againLoanType', $keys)) {
            $expression['againLoanType'] = $loanProduct->getAgainLoanType();
        }

        return $expression;
    }

    private function repaymentMethods($keys, $loanProduct)
    {
        $repaymentMethodsArray = array();

        $repaymentMethods = $loanProduct->getRepaymentMethods();
        foreach ($repaymentMethods as $repaymentMethod) {
            $repaymentMethodsArray[] = $this->getLoanProductCategoryTranslator()->objectToArray(
                $repaymentMethod,
                $keys['repaymentMethods']
            );
        }

        return $repaymentMethodsArray;
    }

    private function guarantyStyles($keys, $loanProduct)
    {
        $guarantyStylesArray = array();

        $guarantyStyles = $loanProduct->getGuarantyStyles();
        foreach ($guarantyStyles as $guarantyStyle) {
            $guarantyStylesArray[] = $this->getDictionaryTranslator()->objectToArray(
                $guarantyStyle,
                $keys['guarantyStyles']
            );
        }

        return $guarantyStylesArray;
    }

    private function productObject($keys, $loanProduct)
    {
        $productObjectArray = array();

        $productObject = $loanProduct->getProductObject();
        foreach ($productObject as $value) {
            $productObjectArray[] = $this->getLoanProductCategoryTranslator()->objectToArray(
                $value,
                $keys['productObject']
            );
        }

        return $productObjectArray;
    }

    private function labels($keys, $loanProduct)
    {
        $labelsArray = array();

        $labels = $loanProduct->getLabels();
        foreach ($labels as $label) {
            $labelsArray[] = $this->getLabelTranslator()->objectToArray(
                $label,
                $keys['labels']
            );
        }

        return $labelsArray;
    }
}
