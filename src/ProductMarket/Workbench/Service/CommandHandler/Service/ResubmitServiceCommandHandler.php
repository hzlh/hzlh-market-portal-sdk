<?php
namespace SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\Service\Command\Service\ResubmitServiceCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\ProductMarket\Log\Model\Log;
use Sdk\ProductMarket\Log\Model\ILogAble;

class ResubmitServiceCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->service = $this->fetchService($command->id);
        $this->service = $this->executeAction($command, $this->service);

        if ($this->service->resubmit()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_RESUBMIT'],
            ILogAble::CATEGORY['SERVICE'],
            $this->service->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->service->getNumber(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
