<?php
namespace SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\AddServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\AddServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\EditServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\EditServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\ResubmitServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\RevokeServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\DeleteServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\CloseServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\OnShelfServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Service\Command\Service\OffStockServiceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
