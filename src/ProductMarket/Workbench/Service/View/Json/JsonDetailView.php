<?php
namespace SuperMarket\ProductMarket\Workbench\Service\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\Workbench\Service\View\DetailViewTrait;
use SuperMarket\ProductMarket\Workbench\Service\View\StatusTrait;

use SuperMarket\ProductMarket\UserCenter\Tag\View\TagTrait;

class JsonDetailView extends JsonView implements IView
{
    use DetailViewTrait, StatusTrait, TagTrait;

    const STATUS = array(
        'OFFSTOCK' => -3,
        'REVOKED' => -4,
        'CLOSED' => -6,
        'DELETED' => -8
    );

    public function display() : void
    {

        $data = $this->getDetail();

        $tag = $this->tagDetailList();
        $data['tag'] = !empty($tag) ? $tag : [];

        $data['collectionCount'] = $this->getCollectionCount();
        
        $this->encode($data);
    }
}
