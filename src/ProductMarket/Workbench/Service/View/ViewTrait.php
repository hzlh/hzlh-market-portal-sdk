<?php
namespace SuperMarket\ProductMarket\Workbench\Service\View;

use Sdk\ProductMarket\Service\Model\Service;

use SuperMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;

trait ViewTrait
{
    private $service;

    private $tagList;

    private $serviceCategories;

    private $serviceObjects;

    private $translator;

    public function __construct(Service $service, $serviceCategories, $serviceObjects, $tagList)
    {
        parent::__construct();
        $this->service = $service;
        $this->tagList = $tagList;
        $this->serviceCategories = $serviceCategories;
        $this->serviceObjects = $serviceObjects;
        $this->translator = new ServiceTranslator();
    }

    public function __destruct()
    {
        unset($this->service);
        unset($this->tagList);
        unset($this->serviceCategories);
        unset($this->serviceObjects);
        unset($this->translator);
    }

    protected function getService() : Service
    {
        return $this->service;
    }

    protected function getServiceCategory()
    {
        return $this->serviceCategories;
    }

    protected function getServiceObjects() : array
    {
        return $this->serviceObjects;
    }

    protected function getTranslator() : ServiceTranslator
    {
        return $this->translator;
    }

    protected function getData()
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($this->getService());

        // 将二维数组转化为一维数组，获取其中的id集合
        $data['serviceObjects'] = array_column($data['serviceObjects'], 'id');

        return $data;
    }
}
