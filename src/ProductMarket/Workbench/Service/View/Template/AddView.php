<?php
namespace SuperMarket\ProductMarket\Workbench\Service\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

use Common\Controller\Traits\GlobalCheckRolesTrait;

class AddView extends TemplateView implements IView
{
    use ServiceCategoryViewTrait, GlobalCheckRolesTrait;

    private $serviceCategories;

    private $serviceObjects;

    public function __construct($serviceCategories, $serviceObjects)
    {
        parent::__construct();
        $this->serviceCategories = $serviceCategories;
        $this->serviceObjects = $serviceObjects;
    }

    public function __destruct()
    {
        unset($this->serviceCategories);
        unset($this->serviceObjects);
    }

    public function getServiceCategories() : array
    {
        return $this->serviceCategories;
    }

    protected function getServiceObjects() : array
    {
        return $this->serviceObjects;
    }

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $serviceObjects = $this->getServiceObjects();

        $permission = $this->workbenchesRoles();
        
        $this->getView()->display(
            'ProductMarket/Service/Workbench/Service/Add.tpl',
            [
                'permission' => $permission,
                'serviceCategories' => $serviceCategories,
                'serviceObjects' => $serviceObjects,
                'nav_left' => NavTrait::NAV_WORKBENCH['PRODUCT_MARKET_HOME'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['PRODUCT_MARKET_HOME'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_HOME']
            ]
        );
    }
}
