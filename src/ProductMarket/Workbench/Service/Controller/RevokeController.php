<?php
namespace SuperMarket\ProductMarket\Workbench\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\RevokeControllerTrait;
use Common\Controller\Interfaces\IRevokeAbleController;

use SuperMarket\ProductMarket\Workbench\Service\Command\Service\RevokeServiceCommand;
use SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

class RevokeController extends Controller implements IRevokeAbleController
{
    use WebTrait, RevokeControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function revokeAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new RevokeServiceCommand($id));
    }
}
