<?php
namespace SuperMarket\ProductMarket\Workbench\Service\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\Service\WidgetRules as ServiceWidgetRules;

trait OperationValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getServiceWidgetRules() : ServiceWidgetRules
    {
        return ServiceWidgetRules::getInstance();
    }

    protected function validateOperationScenario(
        $title,
        $cover,
        $price,
        $contract,
        $serviceObjects,
        $detail,
        $serviceCategoryId
    ) : bool {
        return $this->getServiceWidgetRules()->title($title)
            && $this->getWidgetRules()->image($cover)
            && $this->getServiceWidgetRules()->servicePrice($price)
            && (empty($contract) ? true : $this->getServiceWidgetRules()->contract($contract))
            && (empty($serviceObjects) ? true : $this->getServiceWidgetRules()->serviceObjects($serviceObjects))
            && $this->getWidgetRules()->detail($detail)
            && $this->getWidgetRules()->formatNumeric($serviceCategoryId, 'serviceCategoryId');
    }
}
