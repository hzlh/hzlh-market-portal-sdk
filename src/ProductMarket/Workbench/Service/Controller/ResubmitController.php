<?php
namespace SuperMarket\ProductMarket\Workbench\Service\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\ResubmitControllerTrait;
use Common\Controller\Interfaces\IResubmitAbleController;

use SuperMarket\ProductMarket\Workbench\Service\View\Template\ResubmitView;
use SuperMarket\ProductMarket\Workbench\Service\Command\Service\ResubmitServiceCommand;
use SuperMarket\ProductMarket\Workbench\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

use Sdk\ProductMarket\Service\Repository\ServiceRepository;

use SuperMarket\ProductMarket\UserCenter\Tag\Controller\TagControllerTrait;

class ResubmitController extends Controller implements IResubmitAbleController
{
    use WebTrait, ResubmitControllerTrait, GlobalCheckTrait,
    ServiceTrait, OperationValidateTrait, TagControllerTrait;

    private $commandBus;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
        unset($this->repository);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->repository;
    }

    /**
     * @param $id
     * @return bool
     * @param [GET, POST]
     * @method /services/{id}/resubmit
     * 重新认证
     */
    protected function resubmitView(int $id) : bool
    {
        $service = $this->getServiceRepository()
            ->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($service instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $tag = $service->getTag();
        $tagList = $this->getTags($tag);

        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $serviceCategoryIds  = $this->getServiceCategoryIds($enterpriseId);
        $serviceCategoryArray = $this->fetchServiceCategoryByIds($serviceCategoryIds);
        $serviceObjects = $this->fetchServiceObjects();

        $this->render(new ResubmitView($service, $serviceCategoryArray, $serviceObjects, $tagList));
        return true;
    }

    protected function resubmitAction(int $id)
    {
        $request = $this->getCommonRequest();

        if ($this->validateOperationScenario(
            $request['title'],
            $request['cover'],
            $request['price'],
            $request['contract'],
            $request['serviceObjects'],
            $request['detail'],
            $request['serviceCategoryId']
        ) ) {
            list($cover, $detail) = $this->getCoverAndDetail($request['cover'], $request['detail']);
            $command = new ResubmitServiceCommand(
                $request['title'],
                $request['tag'],
                $cover,
                $request['serviceObjects'],
                $request['price'],
                $detail,
                $request['contract'],
                $request['serviceCategoryId'],
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
