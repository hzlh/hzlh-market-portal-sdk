<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\CommandHandler;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;
use Sdk\ProductMarket\Order\RefundOrder\Model\NullRefundOrder;
use Sdk\ProductMarket\Order\RefundOrder\Repository\RefundOrderRepository;

trait RefundCommandHandlerTrait
{
    private $refundOrder;

    private $repository;

    public function __construct()
    {
        $this->refundOrder = NullRefundOrder::getInstance();
        $this->repository = new RefundOrderRepository();
    }

    public function __destruct()
    {
        unset($this->refundOrder);
        unset($this->repository);
    }

    protected function getRefundOrder() : RefundOrder
    {
        return $this->refundOrder;
    }

    protected function getRepository() : RefundOrderRepository
    {
        return $this->repository;
    }

    protected function fetchRefundOrder($id) : RefundOrder
    {
        return $this->getRepository()->fetchOne($id);
    }
}
