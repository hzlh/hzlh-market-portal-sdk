<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\Refund\Command\RefuseCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class RefuseCommandHandler implements ICommandHandler, ILogAble
{
    use RefundCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RefuseCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->refundOrder = $this->fetchRefundOrder($command->id);
        $this->refundOrder->setRefuseReason($command->refuseReason);

        if ($this->refundOrder->reject()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REJECT'],
            ILogAble::CATEGORY['REFUND_ORDER'],
            $this->refundOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->refundOrder->getRefundReason(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
