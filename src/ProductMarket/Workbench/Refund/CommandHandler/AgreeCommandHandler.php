<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\Refund\Command\AgreeCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class AgreeCommandHandler implements ICommandHandler, ILogAble
{
    use RefundCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AgreeCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->refundOrder = $this->fetchRefundOrder($command->id);

        if ($this->refundOrder->approve()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::CATEGORY['REFUND_ORDER'],
            $this->refundOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->refundOrder->getRefundReason(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
