<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class RefundCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
            'SuperMarket\ProductMarket\Workbench\Refund\Command\AgreeCommand'=>
                'SuperMarket\ProductMarket\Workbench\Refund\CommandHandler\AgreeCommandHandler',
            'SuperMarket\ProductMarket\Workbench\Refund\Command\RefuseCommand'=>
                'SuperMarket\ProductMarket\Workbench\Refund\CommandHandler\RefuseCommandHandler'
        );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);

        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
