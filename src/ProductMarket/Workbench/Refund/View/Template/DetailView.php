<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;

use SuperMarket\ProductMarket\Workbench\Refund\Translator\RefundTranslator;

class DetailView extends TemplateView implements IView
{
    use GlobalCheckRolesTrait;

    private $refund;

    private $translator;

    public function __construct(RefundOrder $refund)
    {
        parent::__construct();
        $this->refund = $refund;
        $this->translator = new RefundTranslator();
    }

    public function __destruct()
    {
        unset($this->refund);
        unset($this->translator);
    }

    protected function getRefund() : RefundOrder
    {
        return $this->refund;
    }

    protected function getTranslator() : RefundTranslator
    {
        return $this->translator;
    }

    public function getData()
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray(
            $this->getRefund()
        );

        return $data;
    }

    public function display()
    {
        $data = $this->getData();
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Workbench/Refund/Detail.tpl',
            [
                'data' => $data,
                'permission' => $permission,
                'nav_left' => NavTrait::NAV_WORKBENCH['PRODUCT_MARKET_REFUND']
            ]
        );
    }
}
