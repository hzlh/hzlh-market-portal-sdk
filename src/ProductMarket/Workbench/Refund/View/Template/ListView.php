<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\Refund\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $data = $this->getList();
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Workbench/Refund/List.tpl',
            [
                'list' => $data,
                'total' => $this->getTotal(),
                'permission' => $permission,
                'nav_left' => NavTrait::NAV_WORKBENCH['PRODUCT_MARKET_REFUND']
            ]
        );
    }
}
