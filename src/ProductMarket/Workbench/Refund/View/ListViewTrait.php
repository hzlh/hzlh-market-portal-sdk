<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\View;

use SuperMarket\ProductMarket\Workbench\Refund\Translator\RefundTranslator;

trait ListViewTrait
{
    private $refundList;

    private $count;

    private $translator;

    public function __construct(
        array $refundList,
        int $count
    ) {
        parent::__construct();
        $this->count = $count;
        $this->refundList = $refundList;
        $this->translator = new RefundTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->refundList);
        unset($this->translator);
    }

    public function getRefundList() : array
    {
        return $this->refundList;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : RefundTranslator
    {
        return $this->translator;
    }
    
    public function getList()
    {
        $translator = $this->getTranslator();

        $list = array();
        foreach ($this->getRefundList() as $refund) {
            $list[] = $translator->objectToArray(
                $refund,
                array(
                    'id',
                    'refundReason',
                    'refuseReason',
                    'voucher',
                    'status',
                    'order'=>[]
                )
            );
        }
        return $list;
    }
}
