<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;
use Common\Controller\Traits\GlobalCheckTrait;

use SuperMarket\ProductMarket\Workbench\Refund\Command\AgreeCommand;
use SuperMarket\ProductMarket\Workbench\Refund\Command\RefuseCommand;

use SuperMarket\ProductMarket\Workbench\Refund\CommandHandler\RefundCommandHandlerFactory;

use WidgetRules\Common\WidgetRules;

class OperationController extends Controller
{
    use WebTrait, GlobalCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new RefundCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    public function agree($id)
    {
        $id = marmot_decode($id);

        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }
        if (!$this->globalCheckEnterprise()) {
            $this->displayError();
            return false;
        }

        return $this->agreeAction($id) ;
    }

    protected function agreeAction(int $id) : bool
    {
        $command = new AgreeCommand(
            $id
        );

        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    public function refuse($id)
    {
        $id = marmot_decode($id);

        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }
        if (!$this->globalCheckEnterprise()) {
            $this->displayError();
            return false;
        }

        return $this->refuseAction($id) ;
    }

    protected function refuseAction(int $id) : bool
    {
        $request = $this->getRequest();
        $refuseReason = $request->post('refuseReason', '');

        $refuseReason = htmlspecialchars_decode($this->formatString($refuseReason), ENT_QUOTES);

        if ($this->validateOperationScenario($refuseReason)) {
            $command = new RefuseCommand(
                $refuseReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function formatString($str)
    {
      //ℑ为占位符，替换掉&lt;br&gt;，用来做字符串长度的验证
        $str = str_replace("&lt;br&gt;", "ℑ", $str);
        $str = str_replace("\'", "'", $str);

        return $str;
    }

    protected function validateOperationScenario(
        $refuseReason
    ) : bool {
        return $this->getWidgetRules()->reason($refuseReason);
    }
}
