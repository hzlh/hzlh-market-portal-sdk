<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\GlobalCheckTrait;

use SuperMarket\ProductMarket\Workbench\Refund\View\Json\JsonListView;
use SuperMarket\ProductMarket\Workbench\Refund\View\Template\ListView;
use SuperMarket\ProductMarket\Workbench\Refund\View\Template\DetailView;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;
use Sdk\ProductMarket\Order\RefundOrder\Adapter\IRefundOrderAdapter;
use Sdk\ProductMarket\Order\RefundOrder\Repository\RefundOrderRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new RefundOrderRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IRefundOrderAdapter
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }
        if (!$this->globalCheckEnterprise()) {
            return false;
        }

        list($page, $size) = $this->getPageAndSize(FORM_SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        $refundList = array();
        list($count, $refundList) = $this->getRepository()
            ->scenario(RefundOrderRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($refundList, $count));
            return true;
        }

        $this->render(new ListView($refundList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $orderNumber = $this->getRequest()->get('orderNumber', '');
        $orderCategory = $this->getRequest()->get('orderCategory', '');
        $goodsName = $this->getRequest()->get('goodsName', '');
        $createTime = $this->getRequest()->get('createTime', '');

        $sort = ['-updateTime'];
        $filter = array();

        $filter['enterprise'] = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $filter['status'] = RefundOrder::REFUND_STATUS['UNDER_REVIEW'];

        if ($orderNumber !== '') {
            $filter['orderNumber'] = $orderNumber;
        }
        if (!empty($orderCategory)) {
            $filter['orderCategory'] = $orderCategory;
        }
        if ($goodsName !== '') {
            $filter['goodsName'] = $goodsName;
        }
        if (!empty($createTime)) {
            $filter['createTime'] = strtotime($createTime);
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        if (!$this->globalCheck()) {
            return false;
        }
        if (!$this->globalCheckEnterprise()) {
            return false;
        }

        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $refund = $this->getRepository()
            ->scenario(RefundOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
        if ($refund instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DetailView($refund));
        return true;
    }
}
