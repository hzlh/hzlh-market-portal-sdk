<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\Command;

use Marmot\Interfaces\ICommand;

class RefuseCommand implements ICommand
{
    public $id;

    public $refuseReason;

    public function __construct(
        string $refuseReason,
        int $id = 0
    ) {
        $this->refuseReason = $refuseReason;
        $this->id = $id;
    }
}
