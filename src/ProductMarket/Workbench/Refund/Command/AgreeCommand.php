<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\Command;

use Marmot\Interfaces\ICommand;

class AgreeCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
