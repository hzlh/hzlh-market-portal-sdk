<?php
namespace SuperMarket\ProductMarket\Workbench\Refund\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;
use Sdk\ProductMarket\Order\RefundOrder\Model\NullRefundOrder;

use UserCenter\Member\Translator\MemberTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Translator\MerchantOrderTranslator;

class RefundTranslator implements ITranslator
{
    const STATUS_ZN = [
        RefundOrder::REFUND_STATUS['UNDER_REVIEW'] => '待处理',
        RefundOrder::REFUND_STATUS['AGREE'] => '待转账',
        RefundOrder::REFUND_STATUS['REFUSE'] => '退款失败',
        RefundOrder::REFUND_STATUS['PAYMENT'] => '退款到账'
    ];

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getMerchantOrderTranslator() : MerchantOrderTranslator
    {
        return new MerchantOrderTranslator();
    }
    
    public function arrayToObject(array $expression, $refund = null)
    {
        unset($refund);
        unset($expression);
        return NullRefundOrder::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function objectToArray($refund, array $keys = array())
    {
        if (!$refund instanceof RefundOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'refundReason',
                'refuseReason',
                'voucher',
                'orderNumber',
                'orderCategory',
                'goodsName',
                'timeRecord',
                'createTime',
                'updateTime',
                'statusTime',
                'status',
                'order'=>[],
                'member'=>[],
                'enterprise'=>[],
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] =  marmot_encode($refund->getId());
        }
        if (in_array('refundReason', $keys)) {
            $expression['refundReason'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $refund->getRefundReason()));//phpcs:ignore;
        }
        if (in_array('refuseReason', $keys)) {
            $expression['refuseReason'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $refund->getRefuseReason()));//phpcs:ignore;
        }
        if (in_array('voucher', $keys)) {
            $expression['voucher'] = $refund->getVoucher();
        }
        if (in_array('orderNumber', $keys)) {
            $expression['orderNumber'] = $refund->getOrderNumber();
        }
        if (in_array('orderCategory', $keys)) {
            $expression['orderCategory'] = $refund->getOrderCategory();
        }
        if (in_array('goodsName', $keys)) {
            $expression['goodsName'] = $refund->getGoodsName();
        }
        if (in_array('timeRecord', $keys)) {
            $expression['timeRecord'] = $refund->getTimeRecord();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $refund->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $refund->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $refund->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $refund->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $refund->getStatusTime();
            $expression['statusTimeFormat'] = date('Y-m-d H:i:s', $refund->getStatusTime());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $refund->getStatus();
            $expression['statusFormat'] = self::STATUS_ZN[$refund->getStatus()];
        }
        if (isset($keys['order'])) {
            $expression['order'] = $this->getMerchantOrderTranslator()->objectToArray(
                $refund->getOrder(),
                $keys['order']
            );
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $refund->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $refund->getEnterprise(),
                $keys['enterprise']
            );
        }

        return $expression;
    }
}
