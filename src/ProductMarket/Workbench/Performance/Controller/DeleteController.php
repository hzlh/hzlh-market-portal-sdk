<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\DeleteControllerTrait;
use Common\Controller\Interfaces\IDeleteAbleController;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\DeletePerformanceCommand;
use SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\PerformanceCommandHandlerFactory;

class DeleteController extends Controller implements IDeleteAbleController
{
    use WebTrait, DeleteControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new PerformanceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function deleteAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new DeletePerformanceCommand($id));
    }
}
