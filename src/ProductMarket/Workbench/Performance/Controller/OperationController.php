<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Controller;

use Common\Controller\Traits\CommonUtilsTrait;
use Common\Controller\Traits\GlobalCheckTrait;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use SuperMarket\ProductMarket\Workbench\Performance\Controller\OperationValidateTrait;
use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ApplyPerformanceCommand;
use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ReapplyPerformanceCommand;
use SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\PerformanceCommandHandlerFactory;

class OperationController extends Controller
{
    use WebTrait, OperationValidateTrait, GlobalCheckTrait, CommonUtilsTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new PerformanceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    public function apply(string $id) : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $request = $this->getRequest();

        $id = marmot_decode($id);
        $description = $request->post('description', '');
        $voucher = $request->post('voucher', []);

        $description = htmlspecialchars_decode($this->formatString($description), ENT_QUOTES);

        if ($this->validateOperationScenario(
            $description,
            $voucher
        )) {
            $voucher = $this->checkImage($voucher);
            $command = new ApplyPerformanceCommand(
                $description,
                $voucher,
                $id
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function reapply(string $id) : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $request = $this->getRequest();

        $id = marmot_decode($id);
        $description = $request->post('description', '');
        $voucher = $request->post('voucher', []);

        $description = htmlspecialchars_decode($this->formatString($description), ENT_QUOTES);

        if ($this->validateOperationScenario(
            $description,
            $voucher
        )) {
            $voucher = $this->checkImage($voucher);
            $command = new ReapplyPerformanceCommand(
                $description,
                $voucher,
                $id
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
