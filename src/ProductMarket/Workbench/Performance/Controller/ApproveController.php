<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Interfaces\IApproveAbleController;
use Common\Controller\Traits\ApproveControllerTrait;
use Common\Controller\Traits\CommonUtilsTrait;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ApprovePerformanceCommand;
use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\RejectPerformanceCommand;
use SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\PerformanceCommandHandlerFactory;

class ApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, PerformanceValidateTrait, CommonUtilsTrait;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new PerformanceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApprovePerformanceCommand($id);

        if ($this->getCommandBus()->send($command)) {
            return true;
        }

        return false;
    }

    protected function rejectAction(int $id) : bool
    {
        $rejectReason = $this->getRequest()->post('reason', '');

        $rejectReason = htmlspecialchars_decode($this->formatString($rejectReason), ENT_QUOTES);

        if ($this->validateApproveScenario($rejectReason)) {
            $command = new RejectPerformanceCommand(
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                return true;
            }
        }

        return false;
    }
}
