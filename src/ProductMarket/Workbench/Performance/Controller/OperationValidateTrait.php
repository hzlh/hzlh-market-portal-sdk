<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\Performance\WidgetRules as PerformanceWidgetRules;

trait OperationValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getPerformanceWidgetRules() : PerformanceWidgetRules
    {
        return PerformanceWidgetRules::getInstance();
    }

    protected function validateOperationScenario(
        $description,
        $voucher
    ) {
        return $this->getPerformanceWidgetRules()->description($description)
        && $this->getWidgetRules()->image($voucher, 'voucher');
    }
}
