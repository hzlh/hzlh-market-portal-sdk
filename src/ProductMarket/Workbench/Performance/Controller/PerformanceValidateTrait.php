<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\Performance\WidgetRules as PerformancetWidgetRules;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
trait PerformanceValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getPerformanceWidgetRules() : PerformancetWidgetRules
    {
        return PerformancetWidgetRules::getInstance();
    }

    protected function validateApproveScenario(
        $rejectReason
    ) {
        return $this->getWidgetRules()->reason($rejectReason);
    }
}
