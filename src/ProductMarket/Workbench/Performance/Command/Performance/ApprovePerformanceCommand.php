<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Command\Performance;

use Marmot\Interfaces\ICommand;

class ApprovePerformanceCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
