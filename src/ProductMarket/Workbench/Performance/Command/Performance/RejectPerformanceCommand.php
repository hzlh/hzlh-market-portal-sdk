<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Command\Performance;

use Marmot\Interfaces\ICommand;

class RejectPerformanceCommand implements ICommand
{
    public $rejectReason;

    public $id;

    public function __construct(
        string $rejectReason,
        int $id = 0
    ) {
        $this->rejectReason = $rejectReason;
        $this->id = $id;
    }
}
