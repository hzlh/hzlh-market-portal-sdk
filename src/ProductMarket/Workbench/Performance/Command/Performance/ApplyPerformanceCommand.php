<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\Command\Performance;

use Marmot\Interfaces\ICommand;

class ApplyPerformanceCommand implements ICommand
{
    public $id;

    public $description;

    public $voucher;//履约凭据

    public function __construct(
        string $description,
        array $voucher,
        int $id = 0
    ) {
        $this->description = $description;
        $this->voucher = $voucher;
        $this->id = $id;
    }
}
