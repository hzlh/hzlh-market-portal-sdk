<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\RejectPerformanceCommand;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Repository\DictionaryRepository;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class RejectPerformanceCommandHandler implements ICommandHandler, ILogAble
{
    use PerformanceBusinessNoticeTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RejectPerformanceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->performance = $this->fetchPerformance($command->id);
        $this->performance->setRejectReason($command->rejectReason);

        if ($this->performance->reject()) {
            $this->businessNotice($this->performance, ILogAble::OPERATION['OPERATION_REJECT']);
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REJECT'],
            ILogAble::CATEGORY['WORKBENCH_CONTRACT_PERFORMANCE'],
            $this->performance->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->performance->getRejectReason(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
