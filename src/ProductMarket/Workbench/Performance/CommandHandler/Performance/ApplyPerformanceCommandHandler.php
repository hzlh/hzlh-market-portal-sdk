<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ApplyPerformanceCommand;

class ApplyPerformanceCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait, PerformanceBusinessNoticeTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ApplyPerformanceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->performance = $this->fetchPerformance($command->id);
        $this->performance->setDescription($command->description);
        $this->performance->setVoucher($command->voucher);

        if ($this->performance->apply()) {
            $this->businessNotice($this->performance, ILogAble::OPERATION['OPERATION_PERFORMANCE_APPLY']);
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_PERFORMANCE_APPLY'],
            ILogAble::CATEGORY['WORKBENCH_CONTRACT_PERFORMANCE'],
            $this->performance->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->performance->getDescription(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
