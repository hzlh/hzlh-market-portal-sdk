<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Qxy\Contract\Performance\Model\Performance;
use Qxy\Contract\Performance\Repository\PerformanceRepository;
use Qxy\Contract\Contract\Model\Contract;

use Sdk\Contract\Repository\ContractRepository;
use Sdk\BusinessNotice\Model\BusinessNotice;

trait PerformanceCommandHandlerTrait
{
    private $performance;

    private $repository;

    private $businessNotice;

    private $contractRepository;

    public function __construct()
    {
        $this->performance = new Performance();
        $this->repository = new PerformanceRepository();
        $this->businessNotice = new BusinessNotice();
        $this->contractRepository = new ContractRepository();
    }

    public function __destruct()
    {
        unset($this->performance);
        unset($this->repository);
        unset($this->businessNotice);
        unset($this->contractRepository);
    }

    protected function getPerformance() : Performance
    {
        return $this->performance;
    }

    protected function getRepository() : PerformanceRepository
    {
        return $this->repository;
    }

    protected function fetchPerformance($id) : Performance
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getBusinessNotice() : BusinessNotice
    {
        return $this->businessNotice;
    }

    protected function getContractRepository() : ContractRepository
    {
        return $this->contractRepository;
    }

    protected function fetchContract($id) : Contract
    {
        return $this->getContractRepository()->fetchOne($id);
    }
}
