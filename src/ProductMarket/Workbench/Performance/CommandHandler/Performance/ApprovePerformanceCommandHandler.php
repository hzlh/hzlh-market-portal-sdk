<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Appointment\Model\TransactionInfo;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ApprovePerformanceCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class ApprovePerformanceCommandHandler implements ICommandHandler, ILogAble
{
    use PerformanceBusinessNoticeTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ApprovePerformanceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->performance = $this->fetchPerformance($command->id);

        if ($this->performance->approve()) {
            $this->businessNotice($this->performance, ILogAble::OPERATION['OPERATION_APPROVE']);
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::CATEGORY['WORKBENCH_CONTRACT_PERFORMANCE'],
            $this->performance->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->performance->getContent(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
