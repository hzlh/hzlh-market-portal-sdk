<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Sdk\BusinessNotice\Model\BusinessNotice;
use Sdk\Member\Model\Member;

use Sdk\Log\Model\ILogAble;

use Qxy\Contract\Performance\Model\Performance;

trait PerformanceBusinessNoticeTrait
{
    use PerformanceCommandHandlerTrait;

    protected function businessNotice($performanceData, $type)
    {
        $businessNotice = $this->getBusinessNotice();

      //获取合同的id，触发类型，触发模块
        $businessNotice->setRelationId($performanceData->getRelationId());
        $businessNotice->setTriggerModular(BusinessNotice::TRIGGER_MODULAR['CONTRACT']);
        $businessNotice->setTriggerType(BusinessNotice::TRIGGER_TYPE['CHANGE_CONTRACT_STATUS']);

      //获取消息内容
        $content = $this->getNoticeContents($performanceData, $type);
        $businessNotice->setContent($content);
        $businessNotice->setAppContent($content);

      //获取对方的id和路由
        list($memberId,$route) = $this->getNoticeMember($performanceData, $type);
        $member = new Member($memberId);
        $businessNotice->setMember($member);
        $businessNotice->setRoute($route);

        $businessNotice->add();
    }

    private function getNoticeContents($performanceData, $type)
    {
        $contract = $this->fetchContract($performanceData->getRelationId());
        $contractNumber = $contract->getNumber();

        $performanceNumber = $performanceData->getNumber();
        $serviceName = ($contract->getOrder()->getOrderCommodities()[0])->getCommodity()->getTitle();

        if ($type == ILogAble::OPERATION['OPERATION_PERFORMANCE_APPLY'] ||
         $type == ILogAble::OPERATION['OPERATION_PERFORMANCE_REAPPLY']) {
            $content = "当前服务名称为".$serviceName.",合同编号为".$contractNumber.
                     "的合同中履约项".$performanceNumber."对方已提交履约申请，请您及时确认";
        }
        if ($type == ILogAble::OPERATION['OPERATION_APPROVE']) {
            $content = "当前服务名称为".$serviceName.",合同编号为".$contractNumber.
                     "的合同,您提交的履约项".$performanceNumber."的履约申请，对方已通过，请您及时查看";
        }
        if ($type == ILogAble::OPERATION['OPERATION_REJECT']) {
            $content = "当前服务名称为".$serviceName.",合同编号为".$contractNumber.
                   "的合同,您提交的履约项".$performanceNumber."的履约申请，对方已驳回，请您及时处理";
        }

        return $content;
    }

    private function getNoticeMember($performanceData, $type)
    {
        $contract = $this->fetchContract($performanceData->getRelationId());

        $performingParty = $performanceData->getPerformingParty();
        if ($type == ILogAble::OPERATION['OPERATION_PERFORMANCE_APPLY'] ||
           $type == ILogAble::OPERATION['OPERATION_PERFORMANCE_REAPPLY']) {
            if ($performingParty == Performance::PERFORMING_PARTY['PARTY_A']) {
                $memberId = $contract->getPartB()->getId();
                $route = '/userCenter/contracts';
            }
            if ($performingParty == Performance::PERFORMING_PARTY['PARTY_B']) {
                $memberId = $contract->getPartA()->getId();
                $route = '/workbench/contracts';
            }
        }

        if ($type == ILogAble::OPERATION['OPERATION_APPROVE'] ||
           $type == ILogAble::OPERATION['OPERATION_REJECT']) {
            if ($performingParty == Performance::PERFORMING_PARTY['PARTY_A']) {
                $memberId = $contract->getPartA()->getId();
                $route = '/workbench/contracts';
            }
            if ($performingParty == Performance::PERFORMING_PARTY['PARTY_B']) {
                $memberId = $contract->getPartB()->getId();
                $route = '/userCenter/contracts';
            }
        }

        return [$memberId,$route];
    }
}
