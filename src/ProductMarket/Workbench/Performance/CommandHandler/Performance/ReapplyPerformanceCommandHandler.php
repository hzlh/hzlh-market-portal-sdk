<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ReapplyPerformanceCommand;

class ReapplyPerformanceCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait, PerformanceBusinessNoticeTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ReapplyPerformanceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->performance = $this->fetchPerformance($command->id);
        $this->performance->setDescription($command->description);
        $this->performance->setVoucher($command->voucher);

        if ($this->performance->reapply()) {
            $this->businessNotice($this->performance, ILogAble::OPERATION['OPERATION_PERFORMANCE_REAPPLY']);
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_PERFORMANCE_REAPPLY'],
            ILogAble::CATEGORY['WORKBENCH_CONTRACT_PERFORMANCE'],
            $this->performance->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->performance->getDescription(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
