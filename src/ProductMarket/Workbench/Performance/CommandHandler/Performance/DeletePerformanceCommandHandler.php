<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\DeletePerformanceCommand;
use SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\PerformanceCommandHandlerTrait;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class DeletePerformanceCommandHandler implements ICommandHandler, ILogAble
{
    use PerformanceCommandHandlerTrait,LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof DeletePerformanceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->performance = $this->fetchPerformance($command->id);

        if ($this->performance->deletePerformance()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_DELETE'],
            ILogAble::CATEGORY['WORKBENCH_CONTRACT_PERFORMANCE'],
            $this->performance->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->performance->getContent(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
