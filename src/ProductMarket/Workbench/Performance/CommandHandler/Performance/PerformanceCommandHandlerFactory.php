<?php
namespace SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class PerformanceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\DeletePerformanceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\DeletePerformanceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ApplyPerformanceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\ApplyPerformanceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ReapplyPerformanceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\ReapplyPerformanceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\RejectPerformanceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\RejectPerformanceCommandHandler',
        'SuperMarket\ProductMarket\Workbench\Performance\Command\Performance\ApprovePerformanceCommand'=>
        'SuperMarket\ProductMarket\Workbench\Performance\CommandHandler\Performance\ApprovePerformanceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
