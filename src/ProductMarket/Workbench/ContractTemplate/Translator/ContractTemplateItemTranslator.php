<?php
namespace SuperMarket\ProductMarket\Workbench\ContractTemplate\Translator;

use Marmot\Framework\Classes\Filter;
use Marmot\Interfaces\ITranslator;

use Qxy\Contract\Template\Model\TemplateItem;
use Qxy\Contract\Template\Model\NullTemplateItem;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class ContractTemplateItemTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $contractTemplateItem = null)
    {
        unset($expression);
        unset($contractTemplateItem);
        return NullTemplateItem::getInstance();
    }

    protected function getTemplateTranslator() : ContractTemplateTranslator
    {
        return new ContractTemplateTranslator();
    }

    public function objectToArray($contractTemplateItem, array $keys = array())
    {
        if (!$contractTemplateItem instanceof TemplateItem) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'item',
                'user',
                'account',
                'template'=>[],
                'status',
                'createTime',
                'updateTime'
            );
        }
        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($contractTemplateItem->getId());
        }
        if (in_array('item', $keys)) {
            $itemId = explode('_', $contractTemplateItem->getItem()->getId());

            $expression['item'] = marmot_encode($itemId[1]);
        }
        if (in_array('user', $keys)) {
            $expression['user'] = $contractTemplateItem->getUser()->getId();
        }
        if (in_array('account', $keys)) {
            $expression['account'] = $contractTemplateItem->getAccount()->getId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $contractTemplateItem->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $contractTemplateItem->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $contractTemplateItem->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $expression['updateTime']);
        }
        if (isset($keys['template'])) {
            $expression['template'] = $this->getTemplateTranslator()->objectToArray(
                $contractTemplateItem->getTemplate(),
                $keys['template']
            );
        }

        return $expression;
    }
}
