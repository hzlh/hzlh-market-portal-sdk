<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder;

use Marmot\Interfaces\ICommand;

class SellerCancelCommand implements ICommand
{
    public $cancelReason;

    public $id;

    public function __construct(
        int $cancelReason = 0,
        int $id = 0
    ) {
        $this->cancelReason = $cancelReason;
        $this->id = $id;
    }
}
