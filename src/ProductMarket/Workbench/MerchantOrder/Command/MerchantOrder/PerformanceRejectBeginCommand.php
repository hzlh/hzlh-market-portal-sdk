<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder;

use Marmot\Interfaces\ICommand;

class PerformanceRejectBeginCommand implements ICommand
{
    public $rejectReason;

    public $id;

    public function __construct(
        string $rejectReason,
        int $id = 0
    ) {
        $this->rejectReason = $rejectReason;
        $this->id = $id;
    }
}
