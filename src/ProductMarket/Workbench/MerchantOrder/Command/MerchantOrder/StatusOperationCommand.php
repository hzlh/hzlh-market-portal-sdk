<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder;

use Marmot\Interfaces\ICommand;

class StatusOperationCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
