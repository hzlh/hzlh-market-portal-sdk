<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder;

use Marmot\Interfaces\ICommand;

class UpdateOrderAmountCommand implements ICommand
{
    public $remark;

    public $amount;

    public $id;

    public function __construct(
        string $remark,
        float $amount = 0,
        int $id = 0
    ) {
        $this->remark = $remark;
        $this->amount = $amount;
        $this->id = $id;
    }
}
