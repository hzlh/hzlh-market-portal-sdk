<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\UpdateOrderAmountCommand;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerCancelCommand;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceBeginCommand;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceEndCommand;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerDeleteCommand;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerPermanentDeleteCommand;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceRejectBeginCommand;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\MerchantOrderCommandHandlerFactory;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Json\SaveCalculationJsonView;

class OperationOrderController extends Controller
{
    use WebTrait, GlobalCheckTrait, OperationValidateTrait, MerchantOrderTrait;

    const PROBABILITY = 100;

    const UPDATE_ORDER_AMOUNT_LOWER_LIMIT = 0.01;

    const MAX_COUPON_AMOUNT = 200;

    private $commandBus;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new MerchantOrderCommandHandlerFactory());
        $this->repository = new ServiceOrderRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
        unset($this->repository);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }
    /**
     * [saveCalculation 修改订单价格点击保存计算价格]
     *
     * @return [type] [bool]
     */
    public function saveCalculation(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = intval(marmot_decode($id));
        $amount = $this->getRequest()->post('amount', '0');

        $merchantOrder = $this->getRepository()
            ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if (empty($merchantOrder->getMemberCoupons())) {
            $coupons = $this->calculateAmount($amount, $merchantOrder->getMemberCoupons());
            $this->render(new SaveCalculationJsonView($coupons));
            return true;
        }

        $data = array(
            'platformPreferentialAmount' => '0.00', //平台优惠
            'businessPreferentialAmount' => '0.00', //店铺优惠
            'collectedAmount' => number_format($amount, 2, '.', ''), //买家应付款
            'paidAmount' => number_format($amount, 2, '.', '') //商家应收款
        );
        $this->render(new SaveCalculationJsonView($data));
        return true;
    }
    /**
     * [updateOrderAmount 卖家修改订单金额]
     *
     * @return [type] [bool]
     */
    public function updateOrderAmount(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = intval(marmot_decode($id));
        $amount = $this->getRequest()->post('amount', 0);
        $remark = $this->getRequest()->post('remark', '');

        if ($this->validateUpdateOrderAmountScenario(
            $amount
        )) {
            $command = new UpdateOrderAmountCommand(
                $remark,
                $amount,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
    /**
     * [sellerCancel 卖家取消订单]
     */
    public function sellerCancel(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = intval(marmot_decode($id));
        $cancelReason = $this->getRequest()->post('unbindReason', 0);
        $cancelReason = marmot_decode($cancelReason);

        if ($this->validateSellerCancelScenario(
            $cancelReason
        )) {
            $command = new SellerCancelCommand(
                $cancelReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
    /**
     * [performanceBegin 卖家接单]
     */
    public function performanceBegin(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if ($this->getCommandBus()->send(new PerformanceBeginCommand(intval(marmot_decode($id))))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
    /**
     * [performanceEnd 卖家确认结束订单]
     */
    public function performanceEnd(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if ($this->getCommandBus()->send(new PerformanceEndCommand(intval(marmot_decode($id))))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
    /**
     * [sellerDelete 卖家删除订单]
     */
    public function sellerDelete(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if ($this->getCommandBus()->send(new SellerDeleteCommand(intval(marmot_decode($id))))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
    /**
     * [sellerPermanent 卖家彻底删除订单]
     */
    public function sellerPermanentDelete(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if ($this->getCommandBus()->send(new SellerPermanentDeleteCommand(intval(marmot_decode($id))))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    /**
     * [sellerRefuse 卖家拒绝接单]
     */
    public function performanceRejectBegin(string $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = intval(marmot_decode($id));
        $rejectReason = $this->getRequest()->post('refuseReason', '');

        $rejectReason = htmlspecialchars_decode($this->formatString($rejectReason), ENT_QUOTES);

        if ($this->validateRejectReasonScenario(
            $rejectReason
        )) {
            $command = new PerformanceRejectBeginCommand(
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function formatString($str)
    {
      //ℑ为占位符，替换掉&lt;br&gt;，用来做字符串长度的验证
        $str = str_replace("&lt;br&gt;", "ℑ", $str);
        $str = str_replace("\'", "'", $str);

        return $str;
    }
}
