<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Controller;

use Sdk\MerchantCoupon\Model\MerchantCoupon;

trait MerchantOrderTrait
{
    /**
     *
     */
    protected function calculateAmount($totalPrice, $coupons)
    {
        $businessPreferential = $this->businessPreferential($totalPrice, $coupons);
        $platformPreferential = $this->platformPreferential($totalPrice, $coupons);

        $paidAmount = $totalPrice-$businessPreferential-$platformPreferential;
        $paidAmount = $paidAmount <= 0 ? self::UPDATE_ORDER_AMOUNT_LOWER_LIMIT : $paidAmount;

        $data = array(
            'platformPreferentialAmount' => number_format($platformPreferential, 2, '.', ''), //平台优惠
            'businessPreferentialAmount' => number_format($businessPreferential, 2, '.', ''), //店铺优惠
            'collectedAmount' => number_format($totalPrice, 2, '.', ''), //商家应收款
            'paidAmount' => number_format($paidAmount, 2, '.', '') //买家应付款
        );

        return $data;
    }

    /**
     *
     */
    protected function businessPreferential($totalPrice, $coupons)
    {
        $businessPreferential = 0;

        foreach ($coupons as $coupon) {
            $useStandard = $coupon->getMerchantCoupon()->getUseStandard();

            if ($coupon->getReleaseType() == MerchantCoupon::RELEASE_TYPE['MERCHANT']) {
                if ($coupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']) {//phpcs:ignore
                    $businessPreferential = $totalPrice >= $useStandard ? $coupon->getMerchantCoupon()->getDenomination() : 0;//phpcs:ignore
                }
                if ($coupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['DISCOUNT']) {
                    $discount = $coupon->getMerchantCoupon()->getDiscount();
                    $businessPreferential = $totalPrice >= $useStandard ? $totalPrice*((self::PROBABILITY-$discount)/100) : 0;//phpcs:ignore
                }
            }
        }

        return $businessPreferential;
    }

    /**
     *
     */
    protected function platformPreferential($totalPrice, $coupons)
    {
        $platformPreferential = 0;

        foreach ($coupons as $coupon) {
            $useStandard = $coupon->getMerchantCoupon()->getUseStandard();

            if ($coupon->getReleaseType() == MerchantCoupon::RELEASE_TYPE['PLATFORM']) {
                if ($coupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']) {//phpcs:ignore
                    $platformPreferential = $totalPrice >= $useStandard ? $coupon->getMerchantCoupon()->getDenomination() : 0;//phpcs:ignore
                }
                if ($coupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['DISCOUNT']) {
                    $discount = $coupon->getMerchantCoupon()->getDiscount();
                    $platformPreferential = $totalPrice >= $useStandard ? $totalPrice*((self::PROBABILITY-$discount)/100) : 0;//phpcs:ignore
                    $platformPreferential = $platformPreferential > self::MAX_COUPON_AMOUNT ? self::MAX_COUPON_AMOUNT : $platformPreferential;//phpcs:ignore
                }
            }
        }

        return $platformPreferential;
    }
}
