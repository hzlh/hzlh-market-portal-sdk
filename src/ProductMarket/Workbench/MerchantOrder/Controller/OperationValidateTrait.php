<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Controller;

use WidgetRules\Order\WidgetRules as OrderWidgetRules;
use WidgetRules\Common\WidgetRules;

trait OperationValidateTrait
{
    protected function getOrderWidgetRules() : OrderWidgetRules
    {
        return OrderWidgetRules::getInstance();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function validateUpdateOrderAmountScenario(
        $amount
    ) : bool {
        return $this->getOrderWidgetRules()->amount($amount);
    }

    protected function validateSellerCancelScenario(
        $cancelReason
    ) : bool {
        return $this->getOrderWidgetRules()->sellerCancelReason($cancelReason);
    }

    protected function validateRejectReasonScenario(
        $reason
    ) {
        return $this->getWidgetRules()->reason($reason);
    }
}
