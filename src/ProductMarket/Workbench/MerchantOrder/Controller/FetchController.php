<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Qxy\Contract\Common\Model\Account;
use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Template\ListView;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Template\DetailView;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Json\JsonListView;
use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Json\JsonDetailView;

use Sdk\Contract\Repository\ContractRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait, GlobalCheckRolesTrait;

    const DEFAULT_SIZE = 5;

    const SCENE = array(
        'ALL' => 1, //全部
        'PENDING_PAY' => 2, //等待买家付款
        'PENDING_TRANSACTION' => 3, //待接单
        'PENDING_FINISH' => 4, //进行中
        'PENDING_EVALUATE' => 5, //等待买家确认
        'DOWN' => 6, //已完成
        'DELETED' => 7, //回收站
    );

    const NOT_FROM_WORKBENCH_SCENE = array(
      'NOTICE' => 1
    );

    private $repository;

    private $contractRepository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
        $this->contractRepository = new ContractRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->contractRepository);
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getContractRepository() : ContractRepository
    {
        return $this->contractRepository;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /merchantOrders
     *
     * 订单列表
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        if (!$this->globalCheckEnterprise()) {
            return false;
        }

        list($page, $size) = $this->getPageAndSize(self::DEFAULT_SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        $merchantOrderList = array();
        list($count, $merchantOrderList) = $this->getServiceOrderRepository()
            ->scenario(ServiceOrderRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        $contract = [];

        $contract = $this->filterContract($merchantOrderList);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($count, $merchantOrderList, $contract));
            return true;
        }

        $this->render(new ListView($count, $merchantOrderList, $contract));
        return true;
    }

    protected function filterContract(array $merchantOrderList) : array
    {
        $sort = ['-updateTime'];

        $merchantOrdeIds = [];
        foreach ($merchantOrderList as $order) {
            $merchantOrdeIds[] = $order->getId();
        }

        $filter['order'] = join(',', $merchantOrdeIds);
        $filter['accountId'] = Account::ACCOUNT_ID['HZLH'];
        $filter['accountType'] = Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET'];

        list($count, $list) = $this->getContractRepository()->scenario(ContractRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);

        unset($count);

        return $list;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {
        $scene = $this->getRequest()->get('status', '');
        if (empty($scene)) {
            $scene = 'MA';
        }
        $scene = marmot_decode($scene);

        //订单号
        $orderno = $this->getRequest()->get('orderno', '');
        //商品名称
        $serviceName = $this->getRequest()->get('serviceName', '');
        //订单分类
        $category = $this->getRequest()->get('category', 0);
        //时间区间
        $startTime = $this->getRequest()->get('startTime', '');
        $endTime = $this->getRequest()->get('endTime', '');

        $sort = ['-updateTime'];
        $filter = array();

        $filter['sellerEnterprise'] = Core::$cacheDriver->fetch(
            'staffEnterpriseId:'.Core::$container->get('user')->getId()
        );
        $filter['sellerOrderStatusPortal'] = ServiceOrder::SELLER_ORDER_STATUS['NORMAL'];

        if (!empty($orderno)) {
            $filter['orderno'] = $orderno;
        }

        if (!empty($serviceName)) {
            $filter['serviceName'] = $serviceName;
        }

        if (!empty($category)) {
            $filter['category'] = $category;
        }

        if (!empty($startTime)) {
            $filter['startTime'] = $startTime;
        }

        if (!empty($endTime)) {
            $filter['endTime'] = $endTime;
        }

        // * PENDING  待付款 0  代付款
        // * PAID   已付款   2  待交易
        // * PERFORMANCE_END 履约结束  6  待完成
        // * BUYER_CONFIRMATION 买家确认  8  待评价

        if ($scene == self::SCENE['PENDING_PAY']) {
            $filter['status'] = ServiceOrder::STATUS['PENDING'];
        }

        if ($scene == self::SCENE['PENDING_TRANSACTION']) {
            $filter['status'] = ServiceOrder::STATUS['PAID'];
        }

        if ($scene == self::SCENE['PENDING_FINISH']) {
            $filter['status'] = ServiceOrder::STATUS['PERFORMANCE_BEGIN'];
        }

        if ($scene == self::SCENE['PENDING_EVALUATE']) {
            $filter['status'] = ServiceOrder::STATUS['PERFORMANCE_END'];
        }

        if ($scene == self::SCENE['DOWN']) {
            $filter['status'] = ServiceOrder::STATUS['BUYER_CONFIRMATION'].','
              .ServiceOrder::STATUS['PLATFORM_CONFIRMATION'] .','
              .ServiceOrder::STATUS['ORDER_COMPLETION'];//phpcs:ignore
        }

        if ($scene == self::SCENE['DELETED']) {
            $filter['sellerOrderStatusPortal'] = ServiceOrder::SELLER_ORDER_STATUS['DELETE'];
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction($id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $merchantOrder = $this->getServiceOrderRepository()
            ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($merchantOrder instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonDetailView($merchantOrder));
            return true;
        }

        $fromScene = $this->getRequest()->get('fromScene', '1');
        if (!empty($fromScene)) {
            $scene = marmot_decode($fromScene);
            $cacheKey = 'staffEnterpriseId:'. Core::$container->get('user')
                ->getId();

            if (in_array($scene, self::NOT_FROM_WORKBENCH_SCENE) ||
            empty(Core::$cacheDriver->fetch($cacheKey))
            ) {
                $this->resetEnterpriseId();
            }
        }

        $this->render(new DetailView($merchantOrder));
        return true;
    }
}
