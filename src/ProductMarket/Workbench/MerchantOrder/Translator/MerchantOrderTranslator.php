<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Model\NullServiceOrder;

use SuperMarket\ProductMarket\UserCenter\Enterprise\Translator\EnterpriseTranslator;
use SuperMarket\ProductMarket\Wallet\MemberAccount\Translator\MemberAccountTranslator;
use SuperMarket\ProductMarket\Wallet\Coupon\Translator\CouponTranslator;

use SuperMarket\ProductMarket\Order\Translator\OrderAddressTranslator;
use SuperMarket\ProductMarket\Order\Translator\OrderCommoditiesTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class MerchantOrderTranslator implements ITranslator
{
    protected function getMemberAccountTranslator() : MemberAccountTranslator
    {
        return new MemberAccountTranslator();
    }

    protected function getOrderAddressTranslator() : OrderAddressTranslator
    {
        return new OrderAddressTranslator();
    }

    protected function getOrderCommoditiesTranslator() : OrderCommoditiesTranslator
    {
        return new OrderCommoditiesTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    public function arrayToObject(array $expression, $merchantOrder = null)
    {
        unset($merchantOrder);
        unset($expression);
        return NullServiceOrder::getInstance();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceOrder, array $keys = array())
    {
//        if (count($keys) == 1) {
//            var_dump($keys);
//            var_dump($serviceOrder);
//            die();
//        }
        $expression = array();

        if (!$serviceOrder instanceof ServiceOrder) {
            return array();
        }


        if (empty($keys)) {
            $keys = array(
                'id',
                'orderno',
                'totalPrice',
                'paidAmount',
                'collectedAmount',
                'realAmount',
                'commission',
                'subsidy',
                'platformPreferentialAmount',
                'businessPreferentialAmount',
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'paymentTime',
                'status',
                'remark',
                'cancelReason',
                'buyerOrderStatus',
                'sellerOrderStatus',
                'timeRecord',
                'failureReason',
                'createTime',
                'updateTime',
                'statusTime',
                'sellerEnterprise'=>[],
                'buyerMemberAccount' => [],
                'orderAddress' => [],
                'orderCommodities' => []
            );
        }

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($serviceOrder->getId());
        }

        if (in_array('orderno', $keys)) {
            $expression['orderno'] = $serviceOrder->getOrderno();
        }

        if (in_array('totalPrice', $keys)) {
            $expression['totalPrice'] = number_format($serviceOrder->getTotalPrice(), 2, '.', '');
            $expression['totalPriceFormat'] =  sprintf("%.2f", $serviceOrder->getTotalPrice());
        }

        if (in_array('paidAmount', $keys)) {
            $expression['paidAmount'] = number_format($serviceOrder->getPaidAmount(), 2, '.', '');
        }

        if (in_array('collectedAmount', $keys)) {
            $expression['collectedAmount'] = number_format($serviceOrder->getCollectedAmount(), 2, '.', '');
        }

        if (in_array('realAmount', $keys)) {
            $expression['realAmount'] = number_format($serviceOrder->getRealAmount(), 2, '.', '');
        }

        if (in_array('commission', $keys)) {
            $expression['commission'] = number_format($serviceOrder->getCommission(), 2, '.', '');
        }

        if (in_array('subsidy', $keys)) {
            $expression['subsidy'] = number_format($serviceOrder->getSubsidy(), 2, '.', '');
        }

        if (in_array('platformPreferentialAmount', $keys)) {
            $platformPreferentialAmount = $serviceOrder->getPlatformPreferentialAmount();
            $expression['platformPreferentialAmount'] = number_format($platformPreferentialAmount, 2, '.', '');
        }

        if (in_array('businessPreferentialAmount', $keys)) {
            $businessPreferentialAmount = $serviceOrder->getBusinessPreferentialAmount();
            $expression['businessPreferentialAmount'] = number_format($businessPreferentialAmount, 2, '.', '');
        }

        if (in_array('paymentType', $keys)) {
            $expression['paymentType'] = $serviceOrder->getPayment()->getType();
        }

        if (in_array('transactionNumber', $keys)) {
            $expression['transactionNumber'] = $serviceOrder->getPayment()->getTransactionNumber();
        }

        if (in_array('transactionInfo', $keys)) {
            $expression['transactionInfo'] = $serviceOrder->getPayment()->getTransactionInfo();
        }

        if (in_array('paymentTime', $keys)) {
            $expression['paymentTime'] = $serviceOrder->getPayment()->getTime();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $serviceOrder->getStatus();
        }

        if (in_array('remark', $keys)) {
            $expression['remark'] = $serviceOrder->getRemark();
        }

        if (in_array('cancelReason', $keys)) {
            $expression['cancelReason'] = $serviceOrder->getCancelReason();
        }

        if (in_array('buyerOrderStatus', $keys)) {
            $expression['buyerOrderStatus'] = $serviceOrder->getBuyerOrderStatus();
        }

        if (in_array('sellerOrderStatus', $keys)) {
            $expression['sellerOrderStatus'] = $serviceOrder->getSellerOrderStatus();
        }

        if (in_array('timeRecord', $keys)) {
            $expression['timeRecord'] = $serviceOrder->getTimeRecord();
        }

        if (in_array('failureReason', $keys)) {
            $expression['failureReason'] = $serviceOrder->getFailureReason();
        }

        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $serviceOrder->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $expression['createTime']);
        }

        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $serviceOrder->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $expression['updateTime']);
        }

        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $serviceOrder->getStatusTime();
            $expression['statusTimeFormat'] = date('Y-m-d', $expression['statusTime']);
        }

        if (isset($keys['buyerMemberAccount'])) {
            $expression['buyerMemberAccount'] = $this->getMemberAccountTranslator()->objectToArray(
                $serviceOrder->getBuyerMemberAccount(),
                $keys['buyerMemberAccount']
            );
        }

        if (isset($keys['sellerEnterprise'])) {
            $expression['sellerEnterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $serviceOrder->getSellerEnterprise(),
                $keys['sellerEnterprise']
            );
        }

        if (isset($keys['orderAddress'])) {
            $expression['orderAddress'] = $this->getOrderAddressTranslator()->objectToArray(
                $serviceOrder->getOrderAddress(),
                $keys['orderAddress']
            );
        }

        if (isset($keys['orderCommodities'])) {
            $orderCommodities = $serviceOrder->getOrderCommodities();

            $tmp = [];

            foreach ($orderCommodities as $orderCommodity) {
                $tmp[] = $this->getOrderCommoditiesTranslator()->objectToArray(
                    $orderCommodity,
                    $keys['orderCommodities']
                );
            }

            $expression['orderCommodities'] = $tmp;
        }

        if (in_array('paidAmount', $keys) && in_array('platformPreferentialAmount', $keys) && in_array('businessPreferentialAmount', $keys)) {//phpcs:ignore
            $modifiedAmount = floatval($expression['paidAmount']) + floatval($expression['platformPreferentialAmount']) + floatval($expression['businessPreferentialAmount']); //phpcs:ignore
            $expression['modifiedAmount'] = number_format($modifiedAmount, 2, '.', '');
        }

        return $expression;
    }
}
