<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\View;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\Translator\MerchantOrderTranslator;

use Workbench\Contract\Translator\ContractTranslator;

trait ListViewTrait
{
    private $count;

    private $data;

    private $contracts;

    private $translator;

    private $contractTranslator;

    public function __construct($count, $data, $contracts)
    {
        parent::__construct();
        $this->count = $count;
        $this->data = $data;
        $this->contracts = $contracts;
        $this->translator = new MerchantOrderTranslator();
        $this->contractTranslator = new ContractTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->data);
        unset($this->contracts);
        unset($this->translator);
        unset($this->contractTranslator);
    }

    public function getMerchantOrderList() : array
    {
        return $this->data;
    }

    public function getContracts() : array
    {
        return $this->contracts;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : MerchantOrderTranslator
    {
        return $this->translator;
    }

    public function getContractTranslator() : ContractTranslator
    {
        return $this->contractTranslator;
    }

    public function getContractList()
    {
        $contractTranslator = $this->getContractTranslator();

        $list = array();
        foreach ($this->getContracts() as $contract) {

            $list[] = $contractTranslator->ObjectToArray(
                $contract,
                array('id', 'accountType', 'order'=>['id'], 'status')
            );
        }
        return $list;
    }

    public function getList()
    {
        $translator = $this->getTranslator();

        $list = array();
        foreach ($this->getMerchantOrderList() as $merchantOrder) {
            $list[] = $translator->objectToArray(
                $merchantOrder,
                array(
                    'id',
                    'orderno',
                    'status',
                    'orderAddress'=>['deliveryAddress'=>['realName','cellphone','area','address']],
                    'orderCommodities'=>['number','skuIndex','commodity'=>['id','title','cover','price']],
                    'totalPrice',
                    'paidAmount',
                    'platformPreferentialAmount',
                    'businessPreferentialAmount',
                    'collectedAmount',
                    'status',
                    'updateTime',
                )
            );
        }

        return $list;
    }
}
