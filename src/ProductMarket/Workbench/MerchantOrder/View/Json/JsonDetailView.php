<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\DetailViewTrait;

class JsonDetailView extends JsonView implements IView
{
    use DetailViewTrait;

    public function display() : void
    {
        $data = $this->getDetail();

        $this->encode($data);
    }
}
