<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\Distribution\View\ShareTemplateViewTrait;

class SaveCalculationJsonView extends JsonView implements IView
{

    private $coupons;

    public function __construct($coupons)
    {
        parent::__construct();
        $this->coupons = $coupons;
    }

    protected function getCoupons()
    {
        return $this->coupons;
    }

    public function display() : void
    {
        $data = $this->getCoupons();

        $this->encode($data);
    }
}
