<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\View;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\Translator\MerchantOrderTranslator;

trait DetailViewTrait
{
    private $service;

    private $translator;

    public function __construct(ServiceOrder $service)
    {
        parent::__construct();
        $this->service = $service;
        $this->translator = new MerchantOrderTranslator();
    }

    public function __destruct()
    {
        unset($this->service);
        unset($this->translator);
    }

    protected function getMerchantOrder() : ServiceOrder
    {
        return $this->service;
    }

    protected function getTranslator() : MerchantOrderTranslator
    {
        return $this->translator;
    }

    public function getDetail()
    {
        $translator = $this->getTranslator();
      
        $data = $translator->objectToArray(
            $this->getMerchantOrder(),
            array(
                'id',
                'status',
                'timeRecord',
                'sellerOrderStatus',
                'cancelReason',
                'orderno',
                'paymentType',
                'transactionNumber',
                'platformPreferentialAmount',
                'businessPreferentialAmount',
                'totalPrice',
                'paidAmount',
                'remark',
                'collectedAmount',
                'realAmount',
                'commission',
                'subsidy',
                'orderCommodities'=>[
                    'skuIndex',
                    'number',
                    'commodity'=>[
                        'id',
                        'title',
                        'cover',
                        'price',
                        'enterprise'=>['id','logo','name']
                    ],
                ],
                'orderAddress'=>['deliveryAddress'=>['realName','cellphone','area','address']],
            )
        );
       
        return $data;
    }
}
