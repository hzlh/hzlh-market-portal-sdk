<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $list = $this->getList();

        $permission = $this->workbenchesRoles();
        $contractList = $this->getContractList();

        foreach ($list as $key => $value) {
            foreach ($contractList as $item) {
                if ($value['id'] == $item['order']['id']) {
                    $list[$key]['contract']['id'] = $item['id'];
                    $list[$key]['contract']['status'] = $item['status'];
                    $list[$key]['contract']['accountType'] = $item['accountType'];
                    $list[$key]['contract']['statusFormat'] = $item['statusFormat'];
                }
            }
        }

        $this->getView()->display(
            'ProductMarket/Service/Workbench/MerchantOrder/List.tpl',
            [
                'list' => $list,
                'total' => $this->getTotal(),
                'permission' => $permission,
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_left' => NavTrait::NAV_WORKBENCH['PRODUCT_MARKET_ORDER'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_ORDER'],
            ]
        );
    }
}
