<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\View\DetailViewTrait;

class DetailView extends TemplateView implements IView
{
    use DetailViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $data = $this->getDetail();

        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Service/Workbench/MerchantOrder/Detail.tpl',
            [
                'data' => $data,
                'permission' => $permission,
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_left' => NavTrait::NAV_WORKBENCH['PRODUCT_MARKET_ORDER'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_ORDER'],
            ]
        );
    }
}
