<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\UpdateOrderAmountCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\ProductMarket\Log\Model\Log;
use Sdk\ProductMarket\Log\Model\ILogAble;

class UpdateOrderAmountCommandHandler implements ICommandHandler, ILogAble
{
    use MerchantOrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof UpdateOrderAmountCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceOrder = $this->fetchServiceOrder($command->id);

        $this->serviceOrder->setRemark($command->remark);
        $this->serviceOrder->setAmount($command->amount);

        if ($this->serviceOrder->updateOrderAmount()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_UPDATE_ORDER_AMOUNT'],
            ILogAble::CATEGORY['WORKBENCH_ORDER'],
            $this->serviceOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->serviceOrder->getOrderno(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
