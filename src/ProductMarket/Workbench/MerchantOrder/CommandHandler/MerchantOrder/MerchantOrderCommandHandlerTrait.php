<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder;

use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;
use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;

trait MerchantOrderCommandHandlerTrait
{
    private $serviceOrder;

    private $repository;

    public function __construct()
    {
        $this->serviceOrder = new ServiceOrder();
        $this->repository = new ServiceOrderRepository();
    }

    public function __destruct()
    {
        unset($this->serviceOrder);
        unset($this->repository);
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function fetchServiceOrder(int $id) : ServiceOrder
    {
        return $this->getRepository()->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }
}
