<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class MerchantOrderCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\UpdateOrderAmountCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\UpdateOrderAmountCommandHandler',
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerCancelCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\SellerCancelCommandHandler',
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceBeginCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\PerformanceBeginCommandHandler',
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceEndCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\PerformanceEndCommandHandler',
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerDeleteCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\SellerDeleteCommandHandler',
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerPermanentDeleteCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\SellerPermanentDeleteCommandHandler',
        'SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceRejectBeginCommand'=>
            'SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder\PerformanceRejectBeginCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
