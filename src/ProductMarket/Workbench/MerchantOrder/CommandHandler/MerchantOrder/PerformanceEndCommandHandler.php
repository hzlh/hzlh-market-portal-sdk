<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\PerformanceEndCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\ProductMarket\Log\Model\Log;
use Sdk\ProductMarket\Log\Model\ILogAble;

class PerformanceEndCommandHandler implements ICommandHandler, ILogAble
{
    use MerchantOrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof PerformanceEndCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceOrder = $this->fetchServiceOrder($command->id);

        if ($this->serviceOrder->performanceEnd()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_PERFORMANCE_END'],
            ILogAble::CATEGORY['WORKBENCH_ORDER'],
            $this->serviceOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->serviceOrder->getOrderno(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
