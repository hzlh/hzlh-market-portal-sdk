<?php
namespace SuperMarket\ProductMarket\Workbench\MerchantOrder\CommandHandler\MerchantOrder;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\Workbench\MerchantOrder\Command\MerchantOrder\SellerPermanentDeleteCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\ProductMarket\Log\Model\Log;
use Sdk\ProductMarket\Log\Model\ILogAble;

class SellerPermanentDeleteCommandHandler implements ICommandHandler, ILogAble
{
    use MerchantOrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof SellerPermanentDeleteCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceOrder = $this->fetchServiceOrder($command->id);

        if ($this->serviceOrder->sellerPermanentDelete()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_PERMANENT_DELETE'],
            ILogAble::CATEGORY['WORKBENCH_ORDER'],
            $this->serviceOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->serviceOrder->getOrderno(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
