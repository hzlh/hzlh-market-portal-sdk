<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\ServiceProvider\WidgetRules as ServiceProviderWidgetRules;

trait OperationValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getServiceProviderWidgetRules() : ServiceProviderWidgetRules
    {
        return ServiceProviderWidgetRules::getInstance();
    }

    protected function validateOperationScenario(
        $qualifications,
        $enterprise
    ) : bool {
        return $this->getServiceProviderWidgetRules()->qualifications($qualifications)
            && $this->getServiceProviderWidgetRules()->enterprise($enterprise);
    }

    protected function validateResubmitScenario(
        $image
    ) : bool {
        return (empty($image['identify']) ? true : $this->getWidgetRules()->image($image));
    }
}
