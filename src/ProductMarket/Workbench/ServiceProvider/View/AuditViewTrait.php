<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\View;

use SuperMarket\ProductMarket\Workbench\ServiceProvider\Translator\ServiceProviderTranslator;

trait AuditViewTrait
{
    private $data;

    private $translator;

    private $serviceAuthenticationCount;

    private $type;

    public function __construct($data, $serviceAuthenticationCount, $type)
    {
        parent::__construct();
        $this->data = $data;
        $this->serviceAuthenticationCount = $serviceAuthenticationCount;
        $this->type = $type;
        $this->translator = new ServiceProviderTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->serviceAuthenticationCount);
        unset($this->type);
        unset($this->translator);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getServiceAuthenticationCount()
    {
        return $this->serviceAuthenticationCount;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCount()
    {
        $data = $this->getData();
        $total = !empty($data) ? $data[0] : 0;

        return $total;
    }

    public function getTranslator() : ServiceProviderTranslator
    {
        return $this->translator;
    }

    public function getAuditList()
    {
        $data = $this->getData();
    
        $authentications = !empty($data) ? $data[1] : array();
        $translator = $this->getTranslator();
      
        $auditList = array();
        foreach ($authentications as $authentication) {
            
            $auditList[] = $translator->objectToArray(
                $authentication,
                array('id', 'number', 'serviceCategory'=>[], 'applyStatus', 'createTime', 'updateTime')
            );
        }
        
        return $auditList;
    }
}
