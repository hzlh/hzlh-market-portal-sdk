<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\Workbench\ServiceProvider\View\AuditViewTrait;

class JsonAuditView extends JsonView implements IView
{
    use AuditViewTrait;

    public function display() : void
    {
        $auditList = $this->getAuditList();
        
        $data = array(
            'list' => $auditList,
            'total' => $this->getCount()
        );

        $this->encode($data);
    }
}
