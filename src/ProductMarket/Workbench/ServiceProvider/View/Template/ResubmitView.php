<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\ProductMarket\Authentication\Model\Authentication;

use SuperMarket\ProductMarket\Workbench\ServiceProvider\Translator\ServiceProviderTranslator;

class ResubmitView extends TemplateView implements IView
{
    use GlobalCheckRolesTrait;

    private $authentication;

    private $translator;

    public function __construct(Authentication $authentication)
    {
        parent::__construct();
        $this->authentication = $authentication;
        $this->translator = new ServiceProviderTranslator();
    }

    public function __destruct()
    {
        unset($this->authentication);
        unset($this->translator);
    }

    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    protected function getTranslator() : ServiceProviderTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getAuthentication(),
            array('id', 'serviceCategory'=>[], 'qualificationImage')
        );

        $permission = $this->workbenchesRoles();
        
        $this->getView()->display(
            'ProductMarket/Service/Workbench/ServiceProvider/Resubmit.tpl',
            [
                'nav_left' => NavTrait::NAV_WORKBENCH['SUPPLIER'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['SUPPLIER'],
                'nav_phone' => NavTrait::NAV_PHONE['SUPPLIER'],
                'permission' => $permission,
                'data' => $data
            ]
        );
    }
}
