<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\Statistical\View\StatisticalViewTrait;

use SuperMarket\ProductMarket\Workbench\ServiceProvider\View\AuditViewTrait;

class AuditView extends TemplateView implements IView
{
    use AuditViewTrait, StatisticalViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $auditList = $this->getAuditList();

        $serviceAuthentication = $this->statisticalArray(
            $this->getType(),
            $this->getServiceAuthenticationCount()
        );

        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Service/Workbench/ServiceProvider/Audit.tpl',
            [
                'nav_left' => NavTrait::NAV_WORKBENCH['SUPPLIER'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['SUPPLIER'],
                'nav_phone' => NavTrait::NAV_PHONE['SUPPLIER'],
                'list' => $auditList,
                'total' => $this->getCount(),
                'permission' => $permission,
                'serviceAuthenticationCount' => $serviceAuthentication[0]
            ]
        );
    }
}
