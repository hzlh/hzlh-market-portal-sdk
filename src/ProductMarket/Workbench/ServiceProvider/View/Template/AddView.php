<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\ServiceProvider\View\ViewTrait;
use SuperMarket\ProductMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

class AddView extends TemplateView implements IView
{
    use ViewTrait, ServiceCategoryViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);
        $authenticationIds  = $this->getAuthenticationIds();
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Service/Workbench/ServiceProvider/Add.tpl',
            [
                'authenticationIds' => $authenticationIds,
                'serviceCategories' => $serviceCategories,
                'permission' => $permission,
                'nav_left' => NavTrait::NAV_WORKBENCH['SUPPLIER'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['SUPPLIER'],
                'nav_phone' => NavTrait::NAV_PHONE['SUPPLIER']
            ]
        );
    }
}
