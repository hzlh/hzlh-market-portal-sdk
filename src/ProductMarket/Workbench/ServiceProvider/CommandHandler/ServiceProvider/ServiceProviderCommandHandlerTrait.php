<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider;

use Marmot\Interfaces\ICommand;

use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;
use Sdk\ProductMarket\Authentication\Model\Authentication;
use Sdk\ProductMarket\Authentication\Model\Qualification;

trait ServiceProviderCommandHandlerTrait
{
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    protected function getQualification() : Qualification
    {
        return new Qualification();
    }

    protected function fetchEnterprise($id)
    {
        return $this->getEnterpriseRepository()->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    protected function fetchServiceCategory($ids)
    {
        return $this->getServiceCategoryRepository()->fetchList($ids);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function executeAction(ICommand $command, Authentication $authentication) : Authentication
    {
        $enterprise = $this->fetchEnterprise($command->enterprise);
        $authentication->setEnterprise($enterprise);

        $qualifications = $command->qualifications;

        $serviceCategoryIds = array();
        foreach ($qualifications as $qualification) {
            $serviceCategoryIds[] = $qualification['serviceCategoryId'];
            if (empty($qualification['image'])) {
                $qualificationImages[$qualification['serviceCategoryId']] = [];
            } else {
                $qualificationImages[$qualification['serviceCategoryId']] = $qualification['image'];
            }
        }

        list($count, $serviceCategoryList) = $this->fetchServiceCategory($serviceCategoryIds);
        unset($count);

        foreach ($serviceCategoryList as $category) {
            $qualificationModel = $this->getQualification();
            $qualificationModel->setImage($qualificationImages[$category->getId()]);
            $qualificationModel->setServiceCategory($category);
            $authentication->addQualification($qualificationModel);
        }

        return $authentication;
    }
}
