<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Authentication\Model\Authentication;
use Sdk\ProductMarket\Authentication\Model\NullAuthentication;

use SuperMarket\ProductMarket\Workbench\ServiceCategory\Translator\ServiceCategoryTranslator;

class ServiceProviderTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $serviceProvider = null)
    {
        unset($serviceProvider);
        unset($expression);
        return NullAuthentication::getInstance();
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceProvider, array $keys = array())
    {
        if (!$serviceProvider instanceof Authentication) {
            return array();
        }
       
        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'serviceCategory'=>[],
                'qualificationImage',
                'applyStatus',
                'rejectReason',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($serviceProvider->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $serviceProvider->getNumber();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $serviceProvider->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $serviceProvider->getRejectReason();
        }
        if (isset($keys['serviceCategory'])) {
            $expression['serviceCategory'] = $this->getServiceCategoryTranslator()->objectToArray(
                $serviceProvider->getServiceCategory(),
                $keys['serviceCategory']
            );
        }
        if (in_array('qualificationImage', $keys)) {
            $expression['qualificationImage'] = empty($serviceProvider->getQualificationImage())
                ? array('name'=>'','identify'=>'') : $serviceProvider->getQualificationImage();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $serviceProvider->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $serviceProvider->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $expression['updateTime']);
        }

        return $expression;
    }
}
