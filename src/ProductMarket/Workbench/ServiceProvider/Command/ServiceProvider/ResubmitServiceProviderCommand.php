<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\Command\ServiceProvider;

use Marmot\Interfaces\ICommand;

class ResubmitServiceProviderCommand implements ICommand
{
    public $qualificationImage;

    public $id;

    public function __construct(
        array $qualificationImage,
        int $id
    ) {
        $this->qualificationImage = $qualificationImage;
        $this->id = $id;
    }
}
