<?php
namespace SuperMarket\ProductMarket\Workbench\ServiceProvider\Command\ServiceProvider;

use Marmot\Interfaces\ICommand;

class AddServiceProviderCommand implements ICommand
{
    public $qualifications;

    public $enterprise;

    public function __construct(
        array $qualifications,
        int $enterprise
    ) {
        $this->qualifications = $qualifications;
        $this->enterprise = $enterprise;
    }
}
