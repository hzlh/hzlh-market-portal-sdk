<?php
namespace SuperMarket\ProductMarket\Dictionary\Translator;

use Sdk\ProductMarket\Dictionary\Model\Dictionary;
use Sdk\ProductMarket\Dictionary\Model\NullDictionary;

use Marmot\Interfaces\ITranslator;

class DictionaryTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $dictionary = null)
    {
        unset($dictionary);
        unset($expression);
        return NullDictionary::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($dictionary, array $keys = array())
    {
        if (!$dictionary instanceof Dictionary) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($dictionary->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $dictionary->getName();
        }

        return $expression;
    }
}
