<?php
namespace SuperMarket\ProductMarket\User\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\User\Model\User;

abstract class UserTranslator implements ITranslator
{
    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($user, array $keys = array())
    {
        if (!$user instanceof User) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'userName',
                'realName',
                'gender',
                'avatar',
                'createTime',
                'updateTime',
                'status',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($user->getId());
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $user->getCellphone();
        }
        if (in_array('realName', $keys)) {
            $expression['realName'] = $user->getRealName();
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $user->getUserName();
        }
        if (in_array('gender', $keys)) {
            $expression['gender'] = $user->getGender();
        }
        if (in_array('avatar', $keys)) {
            $expression['avatar'] = $user->getAvatar();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $user->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $user->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $user->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $user->getStatusTime();
        }

        return $expression;
    }
}
