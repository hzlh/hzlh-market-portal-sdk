<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IDeleteAbleController
{
    public function delete(int $id);
}
