<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IIndexController
{
    public function index();
}
