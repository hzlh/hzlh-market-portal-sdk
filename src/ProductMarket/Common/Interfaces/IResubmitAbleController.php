<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IResubmitAbleController
{
    public function resubmit(int $id);
}
