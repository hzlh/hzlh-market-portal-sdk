<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IApproveAbleController
{
    public function approve(int $id);

    public function reject(int $id);
}
