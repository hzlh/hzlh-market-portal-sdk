<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IRevokeAbleController
{
    public function revoke(int $id);
}
