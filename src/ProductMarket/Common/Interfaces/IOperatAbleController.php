<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IOperatAbleController
{
    public function add();

    public function edit(int $id);
}
