<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface IOnShelfAbleController
{
    public function onShelf(int $id);

    public function offStock(int $id);
}
