<?php
namespace SuperMarket\ProductMarket\Common\Controller\Interfaces;

interface ICloseAbleController
{
    public function close(int $id);
}
