<?php
namespace SuperMarket\ProductMarket\Common\View;

interface NavTrait
{
    // 主站一级导航
    const NAV_PORTAL = array(
        // 首页
        'HOME' => 1,
        // 政策、政策解读
        'POLICY' => 2,
        // 服务超市首页
        'SERVICE_HOME' => 3,
        // 金融超市首页
        'FINANCE_HOME' => 4,
        // 财智平台首页
        'MONEY_WISE_HOME' => 5,
    );

    // 主站二级导航
    const NAV_PORTAL_SECOND = array(
        // 服务分类
        'SERVICES_CATEGORY' => 1,
        // 服务商城
        'SERVICES_MALL' => 2,
        // 需求广场
        'REQUIREMENT_SQUARE' => 3,
        // 金融超市首页-贷款产品
        'LOAN_PRODUCT' => 4,
        // 财智平台首页-财智新规
        'NEWS' => 5,
        // 财智平台首页-财务问答
        'FINANCIAL_QA' => 6,
        // 财智平台首页-在线课堂
        'CLASS_ROOM' => 7
    );

    // 用户中心导航
    const NAV_USER_CENTER = array(
        'NULL' => 0,
        //账号设置
        'ACCOUNT_SETTINGS' => 1,
        //账号安全
        'ACCOUNT_SECURITY' => 2,
        //交易管理
        'TRANSACTION_MANAGEMENT' => 3,
        //资产管理
        'ASSET_MANAGEMENT' => 4,
        //我的关注
        'MY_CONCERNS' => 5,
        //我的参与
        'MY_PARTICIPATION' => 6
    );

    // 用户中心二级导航
    const NAV_USER_CENTER_SECOND = array(
        'NULL' => 0,
        //账号设置
        //基本信息
        'BASIC_INFO' => 1,
        //修改登录密码
        'UPDATE_PASSWORD' => 2,
        //修改绑定手机号
        'UPDATE_CELLPHONE' => 3,

        //账号安全
        //实名认证
        'NATURAL_PERSONS' => 4,
        //企业认证
        'ENTERPRISE' => 5,
        //身份认证
        'IDENTITY_AUTHENTICATION' => 6,

        //交易管理
        //我的订单
        'MY_ORDER' => 7,
        //我的评价
        'MY_EVALUATION' => 8,
        //我的发票
        'MY_INVOICE' => 9,
        //我的收货地址
        'MY_ADDRESS' => 10,
        //我的合同
        'MY_CONTRACT' => 20,
        //我的退款
        'REFUND' => 21,
        //合同修改申请
        'MY_CONTRACT_EDIT' => 22,

        //资产管理
        //我的账户
        'MY_ACCOUNT' => 11,

        //我的关注
        //我的收藏
        'MY_COLLECTION' => 12,
        //我的优惠劵
        'MY_COUPON' => 13,

        //我的参与
        //我的需求
        'SERVICE_REQUIREMENT' => 14,
        //我的申请
        'APPOINTMENT' => 15,
        //我的问题
        'FINANCIAL_QUESTION' => 16,
        //我的问答
        'FINANCIAL_ANSWER' => 17,
        //我的金融需求
        'LOAN_REQUIREMENT' => 18,
        //我的标签
        'MY_TAGS' => 19,
    );

    // 企业工作台导航
    const NAV_WORKBENCH = array(
        'FINANCE_SUPERMARKET' => null,
        // 店铺管理 父级菜单
        //我的店铺 0
        //店铺信息设置 1
        //店铺装修 2

        // 身份认证 父级菜单
        'IDENTITY' => 3,

        // 服务管理 父级菜单
        // 服务列表
        'SERVICE' => 4,

        // 身份认证管理 父级菜单
        // 认证服务商
        'AUTHENTICATION' => 5,
        // 认证金融机构
        'FINANCE_AUTHENTICATION' => 6,

        // 订单管理 父级菜单
        // 订单列表
        'MERCHANT_ORDER' => 7,
        // 退款管理
        'REFUND_MANAGEMENT' => 8,
        // 评价管理
        'EVALUATIONREPLY' => 9,
        // 发票管理
        'INVOICE_MANAGEMENT' => 10,
        // 电子合同
        'ELECTRONIC_CONTRACT' => 11,

        // 金融产品管理 父级菜单
        // 金融产品列表
        'LOAN_PRODUCT' => 12,
        // 产品模板库管理
        'LOAN_PRODUCT_TEMPLATE' => 13,
        // 金融申请管理
        'APPOINTMENT' => 14,

        // 融资需求库管理 父级菜单
        // 需求库列表
        'LOAN_REQUIREMENT' => 15,

        // 员工管理 父级菜单
        // 员工列表
        'STAFF' => 16,
        // 岗位管理
        'POSITION_MANAGEMENT' => 17,
        // 职称管理
        'TITLE_MANAGEMENT' => 18,

        // 营销管理
        // 会员中心
        'USER_CENTER' => 19,
        // 优惠劵营销
        'MERCHANT_COUPON' => 20,

        // 账套管理 父级菜单
        'ACCOUNT_TEMPLATES' => 22,

        // 查询日志
        'LOG' => 23,

        // 合同模版库
        'CONTRACT_TEMPLATE' => 24,
        // 合同库
        'CONTRACT' => 25,
        // 退款管理
        'REFUND' => 26,
        // 合同修改申请
        'CONTRACT_EDIT' => 27,
    );

    // 手机端导航
    const NAV_PHONE = array(
        // 主站导航  PORTAL_name
        // 主站首页
        'PORTAL_HOME' => 1,
        // 政策
        'PORTAL_POLICY' => 2,

        // 用户中心导航 USER_name
        // 安全设置
        'USER_SECURITY' => 3,
        // 基本信息
        'USER_BASIC_INFO' => 4,
        // 企业绑定
        'USER_ENTERPRISE' => 5,
        // 我的需求
        'USER_SERVICE_REQUIREMENT' => 6,
        // 我的账户
        'USER_ACCOUNT' => 7,
        // 地址管理
        'USER_ADDRESS' => 8,
        // 我的优惠劵
        'USER_COUPON' => 9,
        // 我的订单
        'USER_ORDER' => 10,
        // 我的申请
        'USER_APPOINTMENT' => 11,
        // 我的问题
        'USER_FINANCIAL_QUESTION' => 12,
        // 我的答案
        'USER_FINANCIAL_ANSWER' => 13,

        // 服务超市导航  超市名称_name
        // 服务超市首页
        'SERVICE_HOME' => 14,
        // 服务商城
        'SERVICES_MALL' => 15,
        // 需求广场
        'SERVICES_REQUIREMENT_SQUARE' => 16,

        // 企业工作台导航 WORKBENCH_name
        // 认证服务商
        'WORKBENCH_IDENTITY' => 20,
        // 服务超市
        'WORKBENCH_SUPERMARKET' => 23,
        // 认证服务商
        'WORKBENCH_AUTHENTICATION' => 24,
        // 服务管理
        'WORKBENCH_SERVICE' => 25,
        // 商家优惠劵
        'WORKBENCH_MERCHANT_COUPON'=> 27,
        // 商家订单
        'WORKBENCH_MERCHANT_ORDER' => 28,
        // 认证金融机构
        'WORKBENCH_FINANCE_AUTHENTICATION' => 30,
        // 金融产品管理
        'WORKBENCH_LOAN_PRODUCT' => 31,
        // 金融申请管理
        'WORKBENCH_APPOINTMENT' => 32,
        // 员工管理
        'WORKBENCH_STAFF' => 33,
        // 产品模板管理
        'WORKBENCH_LOAN_PRODUCT_TEMPLATE' => 34,
        //金融需求管理
        'WORKBENCH_LOAN_REQUIREMENT' => 35,
        // 岗位管理
        'WORKBENCH_ROLES' => 36,
        // 查询日志
        'WORKBENCH_LOG' => 37,
        // 合同模版库
        'WORKBENCH_CONTRACT_TEMPLATE' => 38,
        // 合同库
        'WORKBENCH_CONTRACT' => 39,
        // 金融超市导航  超市名称_name
        // 金融超市首页
        'FINANCE_HOME' => 40,
        // 金融超市贷款产品
        'FINANCE_LOAN_PRODUCT' => 41,

        //财智平台
        //财智平台首页
        'MONEY_WISE_HOME' => 50,
        //财智平台财智新规
        'MONEY_WISE_NEWS' => 51,
        //财智平台财务问答
        'MONEY_WISE_FINANCIAL_QA' => 52,
        //财智平台在线课堂
        'MONEY_WISE_CLASS_ROOM' => 53,

        //我的金融需求
        'USER_LOAN_REQUIREMENT' => 60,

        //我的消息通知
        'BUSINESS_NOTICE' => 70,
        //我的评价
        'USER_EVALUATION' => 71,
        //评价管理
        'WORKBENCH_EVALUATIONREPLY' => 72,
        //我的收藏
        'USER_COLLECTION' => 73,
        //我的标签
        'USER_TAGS' => 74,
        //合同修改申请
        'WORKBENCH_CONTRACT_EDIT' => 75,
    );

    // 帮助手册【父级菜单】
    const NAV_PARENT_HELP_MANUAL = array(
        // 用户中心
        'USER_CENTER' => 1,
        // 需求管理
        'REQUIREMENTS' => 2,
        // 服务管理
        'SERVICE' => 3,
        // 政策
        'POLICY' => 4,
        // 账户
        'ACCOUNT' => 5,
        // 订单
        'ORDER' => 6,
        // 优惠券
        'COUPON' => 7,
        // 金融超市
        'FINANCE' => 8
    );

    // 帮助手册
    const NAV_HELP_MANUAL = array(
      // 新手指南-注册
      'NOVICE_GUIDE_SIGN_UP' => 1,
      // 新手指南-登录
      'NOVICE_GUIDE_SIGN_IN' => 2,
      // 新手指南-忘记密码
      'NOVICE_GUIDE_RESET_PASSWORD' => 3,
      // 新手指南-修改密码
      'NOVICE_GUIDE_UPDATE_PASSWORD' => 4,
      // 新手指南-实名认证
      'NOVICE_GUIDE_NATURAL_PERSON' => 5,
      // 新手指南-修改绑定手机号
      'NOVICE_GUIDE_UPDATE_CELLPHONE' => 6,
      // 新手指南-企业认证
      'NOVICE_GUIDE_ENTERPRISE' => 7,
      // 新手指南-认证服务商
      'NOVICE_GUIDE_SERVICE_PROVIDER' => 8,
      // 新手指南-发布需求
      'NOVICE_GUIDE_PUBLISH_REQUIREMENTS' => 9,
      // 新手指南-我的需求
      'NOVICE_GUIDE_MY_REQUIREMENTS' => 10,
      // 新手指南-需求详情
      'NOVICE_GUIDE_REQUIREMENT_DETAIL' => 11,
      // 新手指南-发布服务
      'NOVICE_GUIDE_PUBLISH_SERVICE' => 12,
      // 新手指南-服务管理
      'NOVICE_GUIDE_SERVICE' => 13,
      // 新手指南-服务详情
      'NOVICE_GUIDE_SERVICE_DETAIL' => 14,
      // 新手指南-政策详情
      'NOVICE_GUIDE_POLICY_DETAIL' => 15,
      // 新手指南-绑定银行卡
      'NOVICE_GUIDE_BIND_BANK_CARD' => 16,
      // 新手指南-收货地址管理
      'NOVICE_GUIDE_ADDRESS' => 17,
      // 新手指南-设置支付密码
      'NOVICE_GUIDE_SET_UP_PAYMENT_PASSWORD' => 18,
      // 新手指南-修改支付密码
      'NOVICE_GUIDE_UPDATE_PAYMENT_PASSWORD' => 19,
      // 新手指南-重置支付密码
      'NOVICE_GUIDE_RESET_PAYMENT_PASSWORD' => 20,
      // 基本流程-发布服务
      'BASIC_PROCESS_PUBLISH_SERVICE' => 21,
      // 基本流程-发布需求
      'BASIC_PROCESS_PUBLISH_REQUIREMENTS' => 22,
      // 基本流程-企业认证
      'BASIC_PROCESS_ENTERPRISE' => 23,
      // 基本流程-服务商认证
      'BASIC_PROCESS_SERVICE_PROVIDER' => 24,
      // 基本流程-充值
      'BASIC_PROCESS_DEPOSIT' => 25,
      // 基本流程-评价
      'BASIC_PROCESS_EVALUATE' => 26,
      // 基本流程-绑定银行卡
      'BASIC_PROCESS_BIND_BANK_CARD' => 27,
      // 基本流程-购买商品
      'BASIC_PROCESS_BUY' => 28,
      // 基本流程-提现
      'BASIC_PROCESS_WITHDRAWAL' => 29,
      // 新手指南-我的订单(买家)
      'NOVICE_GUIDE_BUYER_ORDER' => 30,
      // 新手指南-我的订单(卖家)
      'NOVICE_GUIDE_SELLER_ORDER' => 31,
      // 新手指南-我的优惠券
      'NOVICE_GUIDE_MY_COUPON' => 32,
      // 新手指南-店铺优惠券
      'NOVICE_GUIDE_ENTERPRISE_COUPON' => 33,
      // 基本流程-发布店铺优惠券
      'BASIC_PROCESS_PUBLISH_ENTERPRISE_COUPON' => 34,
      // 基本流程-认证金融机构
      'BASIC_PROCESS_FINANCE_AUTHENTICATION' => 35,
      // 基本流程-发布金融产品
      'BASIC_PROCESS_LOAN_PRODUCT' => 36,
      // 基本流程-产品对比
      'BASIC_PROCESS_COMPARE' => 37,
      // 基本流程-贷款计算器
      'BASIC_PROCESS_CALCULATOR' => 38,
      // 基本流程-在线申请
      'BASIC_PROCESS_APPLICATION' => 39,
      // 基本流程-预约管理
      'BASIC_PROCESS_APPOINTMENT' => 40,
      // 新手指南-认证金融机构
      'NOVICE_GUIDE_FINANCE_AUTHENTICATION' => 41,
      // 新手指南-发布金融产品
      'NOVICE_GUIDE_LOAN_PRODUCT' => 42,
      // 新手指南-产品对比
      'NOVICE_GUIDE_COMPARE' => 43,
      // 新手指南-贷款计算器
      'NOVICE_GUIDE_CALCULATOR' => 44,
      // 新手指南-在线申请
      'NOVICE_GUIDE_APPLICATION' => 45,
      // 新手指南-预约管理
      'NOVICE_GUIDE_APPOINTMENT' => 46
    );
}
