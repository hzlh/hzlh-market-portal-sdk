<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait CloseControllerTrait
{
    use GlobalCheckTrait;

    public function close(int $id)
    {
        return $this->globalCheck() && $this->closeAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function closeAction(int $id) : bool;
}
