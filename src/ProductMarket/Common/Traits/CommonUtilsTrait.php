<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait CommonUtilsTrait
{
    public function formatString($str)
    {
      //ℑ为占位符，替换掉&lt;br&gt;，用来做字符串长度的验证
        $str = str_replace("&lt;br&gt;", "ℑ", $str);
        $str = str_replace("\'", "'", $str);

        return $str;
    }
}
