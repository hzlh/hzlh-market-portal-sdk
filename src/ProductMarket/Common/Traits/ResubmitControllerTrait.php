<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait ResubmitControllerTrait
{
    /**
     * 如果是GET请求返回页面, POST请求提交数据
     */
    public function resubmit(int $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if ($this->getRequest()->isGetMethod()) {
            return $this->resubmitView($id);
        }

        return $this->resubmitAction($id);
    }

    abstract protected function resubmitView(int $id) : bool;

    abstract protected function resubmitAction(int $id) : bool;
}
