<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait IndexControllerTrait
{
    public function getPageAndSize()
    {
        $page = $this->getRequest()->get('page', 1);
        $size = $this->getRequest()->get('limit', 10);

        return [$page, $size];
    }

    public function index() : bool
    {
        $this->indexAction();
        return true;
    }

    abstract protected function indexAction() : bool;
}
