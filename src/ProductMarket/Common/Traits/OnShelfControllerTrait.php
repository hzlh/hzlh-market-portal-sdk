<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait OnShelfControllerTrait
{
    use GlobalCheckTrait;
    
    public function onShelf(int $id)
    {
        return $this->globalCheck() && $this->onShelfAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function onShelfAction(int $id) : bool;

    public function offStock(int $id)
    {
        return $this->globalCheck() && $this->offStockAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function offStockAction(int $id) : bool;
}
