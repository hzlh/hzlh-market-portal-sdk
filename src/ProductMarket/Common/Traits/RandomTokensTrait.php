<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

use Utils\Utils\RandomTokenTrait;

trait RandomTokensTrait
{
    public function random(int $length)
    {
        return RandomTokenTrait::random($length);
    }

    public function randomNumber(int $length = 6)
    {
        $number = '0123456789';
        return substr(str_shuffle(str_repeat($number, 6)), 0, $length);
    }
}
