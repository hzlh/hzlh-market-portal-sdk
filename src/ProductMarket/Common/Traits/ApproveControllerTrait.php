<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait ApproveControllerTrait
{
    use GlobalCheckTrait;

    public function approve(int $id)
    {
        return $this->globalCheck() && $this->approveAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function approveAction(int $id) : bool;

    public function reject(int $id)
    {
        return $this->globalCheck() && $this->rejectAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function rejectAction(int $id) : bool;
}
