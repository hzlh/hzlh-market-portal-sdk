<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait RevokeControllerTrait
{
    use GlobalCheckTrait;

    public function revoke(int $id)
    {
        return $this->globalCheck() && $this->revokeAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function revokeAction(int $id) : bool;
}
