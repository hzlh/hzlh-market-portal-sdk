<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

use Marmot\Core;

use Sdk\Common\Model\IEnableAble;
use Sdk\ProductMarket\Staff\Repository\StaffRepository;
use Sdk\ProductMarket\Position\Repository\PositionRepository;

trait GlobalCheckRolesTrait
{
    protected function getStaffRepository() : StaffRepository
    {
        return new StaffRepository;
    }

    protected function getPositionRepository() : PositionRepository
    {
        return new PositionRepository;
    }

    public function workbenchesRoles()
    {
        $memberId = Core::$container->get('user')->getId();
        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'. $memberId);

        $staff = $this->getStaffInfo($memberId, $enterpriseId);

        if (empty($staff) || !$enterpriseId) {
            header('Location: /');
            return false;
        }

        $roleIds = array_shift($staff)->getRoles();

        if ($roleIds == "") {
            return $permissionArray = [];
        }

        if ($roleIds == '0') {
            return $permissionArray = [0];
        }

        $position = $this->getPositionListByRoleIds($roleIds);

        $permissionArray = [];
        foreach ($position as $item) {
            $permissionArray[] = $item->getPermission();
        }

        return array_reduce($permissionArray, 'array_merge', array());
    }

    public function resetEnterpriseId($memberId = 0, $enterpriseId = 0)
    {
        $memberId = $memberId > 0
          ? $memberId
          : Core::$container->get('user')->getId();

        $enterpriseId = $enterpriseId > 0
          ? $enterpriseId
          : $memberId;

        $enterpriseIdCacheKey = 'staffEnterpriseId:' . $memberId;

        if (!Core::$cacheDriver->save($enterpriseIdCacheKey, $enterpriseId)) {
            return false;
        }

        return true;
    }

    protected function getStaffInfo($memberId, $enterpriseId)
    {
        $filter['member'] = $memberId;
        $filter['enterprise'] = $enterpriseId;
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $staff = array();
        list($count, $staff) = $this->getStaffRepository()
            ->scenario(StaffRepository::LIST_MODEL_UN)
            ->search($filter, ['-updateTime'], PAGE, SIZE);
        unset($count);

        return $staff;
    }

    // 获得该用户岗位权限信息
    private function getPositionListByRoleIds($ids)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $filter['ids'] = $ids;

        $list = array();
        list($count, $list) = $this->getPositionRepository()
            ->scenario(PositionRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $list;
    }
}
