<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Utils\AlibabaCloud\AlibabaCloudFactory;
use Utils\AlibabaCloud\ImageCheck;
use Utils\Utils\Traits\CsrfTokenTrait;

use Sdk\ProductMarket\Enterprise\Model\NullEnterprise;
use Sdk\ProductMarket\Enterprise\Repository\EnterpriseRepository;

trait GlobalCheckTrait
{
    use CsrfTokenTrait;

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository;
    }

    public function globalCheck()
    {
        $flag = $this->validateCsrfToken()
            && $this->checkUserExist()
            && $this->checkUserStatusExist();

        if (!$flag && $this->getRequest()->isGetMethod() && !$this->getRequest()->isAjax()) {
            $this->setAuthCallbackUrl();
        }

        return $flag;
    }

    /**
     * 屏蔽所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function setAuthCallbackUrl()
    {
        return setcookie(
            'AUTH_CALLBACK_URI',
            $_SERVER['REQUEST_URI'],
            time() + 3600,
            Core::$container->get('cookie.path'),
            ltrim(Core::$container->get('cookie.domain'), '.')
        );
    }

    public function globalCsrfCheck()
    {
        return $this->validateCsrfToken();
    }

    private function checkUserExist()
    {
        if (Core::$container->get('user') instanceof INull) {
            Core::setLastError(NEED_SIGNIN);
            return false;
        }
        return true;
    }

    private function checkUserStatusExist() : bool
    {
        return Core::$container->get('user')->isEnabled();
    }

    // 用户判断用户是否认证企业
    public function globalCheckEnterprise()
    {
        return $this->checkUserEnterprise();
    }

    private function checkUserEnterprise() : bool
    {
        if (!$this->getEnterpriseInfo()) {
            return false;
        }

        return true;
    }

    /**
     * 用户判断用户是否认证企业，并获得企业信息
     * 注意: 此处代码不可改动，如需改动请反复斟酌，改动后需检查其他文件是否发生错误
     * 引用该方法地方：1.所以企业工作台文件 2.GlobalCheckTrait.php 3.用户中心
     */
    public function getEnterpriseInfo()
    {
        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $enterprise = $this->getEnterpriseRepository()
          ->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)
          ->fetchOne($enterpriseId);

        if ($enterprise instanceof NullEnterprise) {
            return false;
        }

        return $enterprise;
    }

    /**
     * 检测单个图片（营业执照可以通过检测）
     */
    public function checkImage(array $image) : array
    {
        return AlibabaCloudFactory::factory(AlibabaCloudFactory::MAPS['ImageCheck'])
            ->executeOne($image);
    }

    /**
     * 检测单个图片（营业执照不可以通过检测）
     */
    public function checkImageNormal(array $image) : array
    {
        return AlibabaCloudFactory::factory(AlibabaCloudFactory::MAPS['ImageCheck'])
            ->executeOne($image, ImageCheck::CHECK_TYPE['NORMAL']);
    }

    /**
     * 检测多个图片
     * @param array $details
     * @return array
     */
    public function checkImages(array $images) : array
    {
        return AlibabaCloudFactory::factory(AlibabaCloudFactory::MAPS['ImageCheck'])
            ->execute($images);
    }

    /**
     * 检测多个图片
     * @param array $details
     * @return array
     */
    public function checkImagesNormal(array $images) : array
    {
        return AlibabaCloudFactory::factory(AlibabaCloudFactory::MAPS['ImageCheck'])
            ->execute($images, ImageCheck::CHECK_TYPE['NORMAL']);
    }

    /**
     * 检测详情的图片
     */
    public function checkDetailImage(array $details) : array
    {
        foreach ($details as $key => $detail) {
            if ($detail['type'] == 'image') {
                $details[$key] = $this->checkImage($detail);
            }
        }

        return $details;
    }

    /**
     * 检测详情的图片
     */
    public function checkDetailImageNormal(array $details) : array
    {
        foreach ($details as $key => $detail) {
            if ($detail['type'] == 'image') {
                $details[$key] = $this->checkImageNormal($detail);
            }
        }

        return $details;
    }
}
