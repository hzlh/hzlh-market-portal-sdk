<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait DeleteControllerTrait
{
    use GlobalCheckTrait;

    public function delete(int $id)
    {
        return $this->globalCheck() && $this->deleteAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function deleteAction(int $id) : bool;
}
