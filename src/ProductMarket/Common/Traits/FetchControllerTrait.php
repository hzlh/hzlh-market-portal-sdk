<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait FetchControllerTrait
{
    public function getPageAndSize($size = '')
    {
        if (empty($size)) {
            $size = 6;
        }

        $page = $this->getRequest()->get('page', 1);
        $size = $this->getRequest()->get('limit', $size);

        return [$page, $size];
    }

    public function filter() : bool
    {
        if ($this->filterAction()) {
            return true;
        }

        $this->displayError();
        return false;
    }

    abstract protected function filterAction() : bool;

    public function fetchOne($id) : bool
    {
        if ($this->fetchOneAction($id)) {
            return true;
        }

        $this->displayError();
        return false;
    }

    abstract protected function fetchOneAction(int $id) : bool;
}
