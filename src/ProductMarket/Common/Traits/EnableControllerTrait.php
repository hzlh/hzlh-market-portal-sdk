<?php
namespace SuperMarket\ProductMarket\Common\Controller\Traits;

trait EnableControllerTrait
{
    use GlobalCheckTrait;
    
    public function enable(int $id)
    {
        return $this->globalCheck() && $this->enableAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function enableAction(int $id) : bool;

    public function disable(int $id)
    {
        return $this->globalCheck() && $this->disableAction($id) ? $this->displaySuccess() : $this->displayError();
    }

    abstract protected function disableAction(int $id) : bool;
}
