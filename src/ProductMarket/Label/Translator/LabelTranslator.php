<?php
namespace SuperMarket\ProductMarket\Label\Translator;

use Sdk\ProductMarket\Label\Model\Label;
use Sdk\ProductMarket\Label\Model\NullLabel;

use Marmot\Interfaces\ITranslator;

class LabelTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $label = null)
    {
        unset($label);
        unset($expression);
        return NullLabel::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($label, array $keys = array())
    {
        if (!$label instanceof Label) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($label->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $label->getName();
        }

        return $expression;
    }
}
