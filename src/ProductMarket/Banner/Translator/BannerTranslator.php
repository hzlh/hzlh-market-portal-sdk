<?php
namespace SuperMarket\ProductMarket\Banner\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Banner\Model\Banner;
use Sdk\ProductMarket\Banner\Model\BannerModelFactory;
use Sdk\ProductMarket\Banner\Model\NullBanner;

use SuperMarket\ProductMarket\Crew\Translator\CrewTranslator;

class BannerTranslator implements ITranslator
{
    const STATUS_CN = array(
        Banner::BANNER_STATUS['ON_SHELF'] => '已上架',
        Banner::BANNER_STATUS['OFF_STOCK'] => '待上架',
        Banner::BANNER_STATUS['DELETE'] => '已删除',
    );
    const LAUNCH_TYPE_CN = array(
        Banner::LAUNCH_TYPE['PC'] => 'PC',
        Banner::LAUNCH_TYPE['APP'] => 'APP'
    );
    const TYPE_CN = array(
        Banner::TYPE['ADVERTISEMENT'] => '广告位',
        Banner::TYPE['BANNER'] => 'banner图'
    );

    public function arrayToObject(array $expression, $banner = null)
    {
        unset($banner);
        unset($expression);
        return NullBanner::getInstance();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($banner, array $keys = array())
    {
        if (!$banner instanceof Banner) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'launchType',
                'bannerType',
                'place',
                'placeDetail',
                'image',
                'link',
                'remark',
                'crew'=>['id', 'realName'],
                'createTime',
                'updateTime',
                'statusTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] =  marmot_encode($banner->getId());
        }
        if (in_array('launchType', $keys)) {
            $launchType = $banner->getLaunchType();

            $expression['launchType'] = $launchType;
            $expression['launchTypeFormat'] = self::LAUNCH_TYPE_CN[$launchType];
        }
        if (in_array('bannerType', $keys)) {
            $bannerType = $banner->getBannerType();

            $expression['bannerType'] = $bannerType;
            $expression['bannerTypeFormat'] = self::TYPE_CN[$bannerType];
        }
        if (in_array('place', $keys) && in_array('launchType', $keys)) {
            $place = $banner->getPlace();
            $launchType = $banner->getLaunchType();

            $expression['place'] = $place;

            if ($launchType == Banner::LAUNCH_TYPE['PC']) {
                $expression['placeFormat'] = BannerModelFactory::PC_PLACE_DETAIL[$place]['title'];
            }
            if ($launchType == Banner::LAUNCH_TYPE['APP']) {
                $expression['placeFormat'] = BannerModelFactory::APP_PLACE_DETAIL[$place]['title'];
            }
        }
        if (in_array('placeDetail', $keys)) {
            $expression['placeDetail'] = $banner->getPlaceDetail();
        }
        if (in_array('image', $keys)) {
            $expression['image'] = $banner->getImage();
        }
        if (in_array('link', $keys)) {
            $expression['link'] = $banner->getLink();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $banner->getRemark();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $banner->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $banner->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $banner->getStatusTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $banner->getStatus();
            $expression['statusFormat'] = self::STATUS_CN[$banner->getStatus()];
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $banner->getCrew(),
                $keys['crew']
            );
        }

        return $expression;
    }
}
