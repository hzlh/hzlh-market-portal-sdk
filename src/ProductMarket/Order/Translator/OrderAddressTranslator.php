<?php
namespace SuperMarket\ProductMarket\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\CommonOrder\Model\OrderAddress;
use Sdk\ProductMarket\Order\CommonOrder\Model\NullOrderAddress;

use Snapshot\Translator\SnapshotTranslator;

use UserCenter\DeliveryAddress\Translator\DeliveryAddressTranslator;

class OrderAddressTranslator implements ITranslator
{
    protected function getDeliveryAddressTranslator() : DeliveryAddressTranslator
    {
        return new DeliveryAddressTranslator();
    }

    protected function getSnapshotTranslator() : SnapshotTranslator
    {
        return new SnapshotTranslator();
    }

    public function arrayToObject(array $expression, $orderAddress = null)
    {
        unset($orderAddress);
        unset($expression);
        return NullOrderAddress::getInstance();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderAddress, array $keys = array())
    {
        $expression = array();

        if (!$orderAddress instanceof OrderAddress) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'snapshot'=>[],
                'deliveryAddress'=>['area','address','realName','cellphone','postalCode']
            );
        }

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderAddress->getId());
        }

        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $orderAddress->getCellphone();
        }

        if (isset($keys['snapshot'])) {
            $expression['snapshot'] = $this->getSnapshotTranslator()->objectToArray(
                $orderAddress->getSnapshot(),
                $keys['snapshot']
            );
        }

        if (isset($keys['deliveryAddress'])) {
            $expression['deliveryAddress'] = $this->getDeliveryAddressTranslator()->objectToArray(
                $orderAddress->getDeliveryAddress(),
                $keys['deliveryAddress']
            );
        }
        
        return $expression;
    }
}
