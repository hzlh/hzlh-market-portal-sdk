<?php
namespace SuperMarket\ProductMarket\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\CommonOrder\Model\OrderCommodity;
use Sdk\ProductMarket\Order\CommonOrder\Model\NullOrderCommodity;

use SuperMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;
use Snapshot\Translator\SnapshotTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class OrderCommoditiesTranslator implements ITranslator
{
    protected function getServiceTranslator() : ServiceTranslator
    {
        return new ServiceTranslator();
    }

    protected function getSnapshotTranslator() : SnapshotTranslator
    {
        return new SnapshotTranslator();
    }

    public function arrayToObject(array $expression, $orderCommodities = null)
    {
        unset($orderCommodities);
        unset($expression);
        return NullOrderCommodity::getInstance();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderCommodities, array $keys = array())
    {
        $expression = array();

        if (!$orderCommodities instanceof OrderCommodity) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'skuIndex',
                'snapshot'=>[],
                'commodity'=>[]
            );
        }

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderCommodities->getId());
        }

        if (in_array('number', $keys)) {
            $expression['number'] = $orderCommodities->getNumber();
        }

        if (in_array('skuIndex', $keys)) {
            $expression['skuIndex'] = $orderCommodities->getSkuIndex();
        }

        if (isset($keys['snapshot'])) {
            $expression['snapshot'] = $this->getSnapshotTranslator()->objectToArray(
                $orderCommodities->getSnapshot(),
                $keys['snapshot']
            );
        }

        if (isset($keys['commodity'])) {
            $expression['commodity'] = $this->getServiceTranslator()->objectToArray(
                $orderCommodities->getCommodity(),
                $keys['commodity']
            );
        }

        return $expression;
    }
}
