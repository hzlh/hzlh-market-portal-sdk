<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\Translator;

use Sdk\ProductMarket\TradeRecord\Model\TradeRecord;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

class TranslatorFactory
{

    const TRADE_RECORD_TYPES = array(
        'NULL' => 0,
        'DEPOSIT' => 1, //用户充值
        'ORDER_PAY_ENTERPRISE' => 10,
        'ORDER_CONFIRMATION_BUYER_ENTERPRISE' => 14, //支付(订单）
        'ORDER_CONFIRMATION_SELLER_ENTERPRISE' => 16, //收入(卖家)
        'ORDER_CONFIRMATION_PLATFORM' => 18,
        'ORDER_CONFIRMATION_SUBSIDY' => 19,
        'BUYER_REFUND' => 24,
        'WITHDRAWAL' => 20, //用户提现
    );


    const MAPS = array(
        self::TRADE_RECORD_TYPES['DEPOSIT'] =>
            'Wallet\Deposit\Translator\DepositTranslator',
        self::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE'] =>
            'SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator',
        self::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE'] =>
            'SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator',
        self::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE'] =>
            'SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator',
        self::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM'] =>
            'SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator',
        self::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SUBSIDY'] =>
            'SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator',
        self::TRADE_RECORD_TYPES['BUYER_REFUND'] =>
            'SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator',
        self::TRADE_RECORD_TYPES['WITHDRAWAL'] =>
            'Wallet\Withdrawal\Translator\WithdrawalTranslator'
    );

    public function getTranslator(string $type) : ITranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
