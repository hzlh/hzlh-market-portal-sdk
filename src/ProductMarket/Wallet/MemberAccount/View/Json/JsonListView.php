<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\Wallet\MemberAccount\Translator\TradeRecordTranslator;

class JsonListView extends JsonView implements IView
{
    private $tradeRecordTranslator;

    public function __construct(
        array $tradeRecord,
        int $count = 0
    ) {
        $this->tradeRecord = $tradeRecord;
        $this->count = $count;
        $this->tradeRecordTranslator = new TradeRecordTranslator();
        parent::__construct();
    }

    protected function getCount()
    {
        return $this->count;
    }

    protected function getTradeRecord()
    {
        return $this->tradeRecord;
    }

    protected function getTradeRecordTranslator() : TradeRecordTranslator
    {
        return $this->tradeRecordTranslator;
    }

    protected function getTradeRecordList()
    {
        $tradeRecords = $this->getTradeRecord();
        $translator = $this->getTradeRecordTranslator();

        $tradeRecordList = array();

        foreach ($tradeRecords as $tradeRecord) {
            $tradeRecordList[] = $translator->objectToArray(
                $tradeRecord,
                array(
                    'id',
                    'tradeTime',
                    'tradeType',
                    'tradeMoney',
                    'balance',
                    'status',
                    'reference'=>['paymentType','status'],
                    'memberAccount'=>[]
                )
            );
        }

        return $tradeRecordList;
    }

    public function display() : void
    {
        $list = $this->getTradeRecordList();
        
        $data = array(
            'list' => $list,
            'total' => $this->getCount()
        );

        $this->encode($data);
    }
}
