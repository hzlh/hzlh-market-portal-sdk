<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View;

class ITplFactory
{
    const MAPS = array(
        '1'=>'Wallet/MemberAccount/Recharge.tpl',//充值
        '10'=>'Wallet/MemberAccount/OrderPayment.tpl',//第三方支付
        '14'=>'Wallet/MemberAccount/OrderPayment.tpl',//订单支付
        '16'=>'Wallet/MemberAccount/OrderReceivables.tpl',//订单收款
        '18'=>'Wallet/MemberAccount/OrderPayment.tpl',//交易服务费
        '19'=>'Wallet/MemberAccount/OrderReceivables.tpl',//补贴
        '20'=>'Wallet/MemberAccount/Withdrawal.tpl',//提现
        '24'=>'Wallet/MemberAccount/Refund.tpl',//退款
    );

    public function getTpl(int $type) : string
    {
        $tpl = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return $tpl;
    }
}
