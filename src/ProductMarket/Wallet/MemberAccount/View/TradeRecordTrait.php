<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View;

use Sdk\ProductMarket\TradeRecord\Model\TradeRecord;
use SuperMarket\ProductMarket\Wallet\MemberAccount\Translator\TradeRecordTranslator;

trait TradeRecordTrait
{
    private $type;

    private $tradeRecord;

    private $tplFactory;

    private $tradeRecordTranslator;

    public function __construct(int $type, TradeRecord $tradeRecord)
    {
        $this->type = $type;
        $this->tradeRecord = $tradeRecord;
        $this->tplFactory = new ITplFactory();
        $this->tradeRecordTranslator = new TradeRecordTranslator();
        parent::__construct();
    }

    protected function getTradeRecord() : TradeRecord
    {
        return $this->tradeRecord;
    }

    protected function getTradeRecordTranslator() : TradeRecordTranslator
    {
        return $this->tradeRecordTranslator;
    }

    protected function getTplFactory() : ITplFactory
    {
        return $this->tplFactory;
    }

    protected function getType() : int
    {
        return $this->type;
    }

    protected function getTradeRecordData()
    {
        $tradeRecordData = $this->getTradeRecord();
        $translator = $this->getTradeRecordTranslator();
        
        $tradeRecord = array();

        $tradeRecord = $translator->objectToArray($tradeRecordData);

        return $tradeRecord;
    }
}
