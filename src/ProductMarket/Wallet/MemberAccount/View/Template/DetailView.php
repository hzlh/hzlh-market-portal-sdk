<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Wallet\MemberAccount\View\TradeRecordTrait;

class DetailView extends TemplateView implements IView
{
    use TradeRecordTrait;

    public function display() : void
    {
        $tradeRecord = $this->getTradeRecordData();
        
        $tplFactory = $this->getTplFactory();

        $this->getView()->display(
            $tplFactory->getTpl($this->getType()),
            [
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['PRODUCT_MARKET_ACCOUNT'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_ACCOUNT'],
                'data' => $tradeRecord,
            ]
        );
    }
}
