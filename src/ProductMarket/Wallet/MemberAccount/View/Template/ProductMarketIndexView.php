<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\ProductMarket\MemberAccount\Model\MemberAccount;

use SuperMarket\ProductMarket\Wallet\MemberAccount\Translator\MemberAccountTranslator;
use SuperMarket\ProductMarket\Wallet\MemberAccount\Translator\TradeRecordTranslator;

class ProductMarketIndexView extends TemplateView implements IView
{
    private $memberAccountTranslator;

    private $tradeRecordTranslator;

    public function __construct(
        MemberAccount $memberAccount,
        array $tradeRecord,
        int $tradeRecordCount = 0
    ) {
        $this->memberAccount = $memberAccount;
        $this->tradeRecord = $tradeRecord;
        $this->tradeRecordCount = $tradeRecordCount;
        $this->memberAccountTranslator = new MemberAccountTranslator();
        $this->tradeRecordTranslator = new TradeRecordTranslator();
        parent::__construct();
    }

    protected function getTradeRecordCount()
    {
        return $this->tradeRecordCount;
    }

    protected function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }

    protected function getTradeRecord()
    {
        return $this->tradeRecord;
    }

    protected function getMemberAccountTranslator() : MemberAccountTranslator
    {
        return $this->memberAccountTranslator;
    }

    protected function getTradeRecordTranslator() : TradeRecordTranslator
    {
        return $this->tradeRecordTranslator;
    }

    protected function getMemberAccountData()
    {
        $memberAccountData = $this->getMemberAccount();
        $translator = $this->getMemberAccountTranslator();
        $memberAccount = array();

        $memberAccount = $translator->objectToArray($memberAccountData);

        return $memberAccount;
    }

    protected function getTradeRecordList()
    {
        $tradeRecords = $this->getTradeRecord();
        $translator = $this->getTradeRecordTranslator();

        $tradeRecordList = array();

        foreach ($tradeRecords as $tradeRecord) {
            $tradeRecordList[] = $translator->objectToArray(
                $tradeRecord,
                array('id','tradeTime','tradeType','tradeMoney','reference'=>['paymentType','status'],'balance')
            );
        }

        return $tradeRecordList;
    }

    public function display() : void
    {
        $memberAccount = $this->getMemberAccountData();

        $tradeRecordList = $this->getTradeRecordList();

        $this->getView()->display(
            'Wallet/MemberAccount/ProductMarketIndex.tpl',
            [
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['PRODUCT_MARKET_ACCOUNT'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_ACCOUNT'],
                'memberAccount' => $memberAccount,
                'tradeRecordList' => $tradeRecordList,
                'tradeRecordCount' => $this->getTradeRecordCount()
            ]
        );
    }
}
