<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class PayFailView extends TemplateView implements IView
{
    public function display() : void
    {
        $this->getView()->display(
            'PaymentResult/PayProductMarketFail.tpl'
        );
    }
}
