<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;
use SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

class PaySuccessView extends TemplateView implements IView
{
    private $data;

    private $type;

    private $translator;

    public function __construct($data, $type)
    {
        parent::__construct();
        $this->data = $data;
        $this->type = $type;
        $this->translator = new OrderTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->translator);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTranslator() : OrderTranslator
    {
        return $this->translator;
    }

    protected function getOrder()
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($this->getData());

        return $data;
    }

    public function display() : void
    {
        $type = $this->getType();

        $data = !empty($this->getData()) ? $this->getOrder() : array();

        $this->getView()->display(
            'PaymentResult/PayProductMarketSuccess.tpl',
            [
                'data' => $data,
                'type' => $type
            ]
        );
    }
}
