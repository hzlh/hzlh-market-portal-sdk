<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;

class ServiceView extends TemplateView implements IView
{
    private $data;

    private $serviceTranslator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->serviceTranslator = new ServiceTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->serviceTranslator);
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    public function getServiceList()
    {
        $serviceList = $this->getData();

        $list = array();
        if (!empty($serviceList)) {
            foreach ($serviceList as $service) {
                $list[] = $this->getServiceTranslator()->ObjectToArray(
                    $service,
                    array('id','title','cover','minPrice','serviceCategory'=>['name'],'volume',
                        'enterprise'=>['id','name','logo'])
                );
            }
        }

        return $list;
    }

    public function display()
    {
        $list = $this->getServiceList();

        $this->getView()->display(
            'PaymentResult/ProductService.tpl',
            [
                'list' => $list,
            ]
        );
    }
}
