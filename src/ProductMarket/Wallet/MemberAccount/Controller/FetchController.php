<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\Controller;

use Common\Controller\Traits\GlobalCheckTrait;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Common\Model\IApplyAble;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template\DetailView;
use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template\PaySuccessView;
use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template\PayFailView;
use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template\ServiceView;

use Sdk\ProductMarket\Service\Model\Service;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;
use Sdk\ProductMarket\TradeRecord\Repository\TradeRecordRepository;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait;

    const SERVICE_LIST = 4;

    const TYPE_PAYMENT = 1;

    private $repository;

    private $serviceRepository;

    private $serviceOrderRepository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new TradeRecordRepository();
        $this->serviceOrderRepository = new ServiceOrderRepository();
        $this->serviceRepository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->serviceOrderRepository);
        unset($this->serviceRepository);
    }

    protected function getRepository() : TradeRecordRepository
    {
        return $this->repository;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->serviceOrderRepository;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function filterAction()
    {
        return false;
    }

    protected function fetchOneAction($id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $tradeRecord = $this->getRepository()
            ->scenario(TradeRecordRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($tradeRecord instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $type = $tradeRecord->getType();

        $this->render(new DetailView($type, $tradeRecord));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /memberAccount/paySuccess
     *
     * 订单支付结果
     */
    public function paySuccess($paymentId)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if (empty($paymentId)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $paymentId = marmot_decode($paymentId);

        $temp = explode('_', $paymentId);
        $type = $temp[0];
        $id = $temp[1];

        if (!$id) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $data = array();
        if ($type == self::TYPE_PAYMENT) {
            $data = $this->getServiceOrderRepository()
                ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

            if ($data instanceof INull) {
                Core::setLastError(RESOURCE_NOT_EXIST);
                $this->displayError();
                return false;
            }
        }

        $this->render(new PaySuccessView($data, $type));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /memberAccount/payFail
     * 充值结果
     */
    public function payFail()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $this->render(new PayFailView());
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /memberAccount/fetchServices
     *
     * 获取服务信息
     */
    public function fetchServices()
    {
        $filter = array();
        $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];  //上架
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE']; //审核通过

        list($page, $size) = $this->getPageAndSize(self::SERVICE_LIST);

        // 获取服务列表
        $serviceList = array();
        list($count, $serviceList) = $this->getServiceRepository()
            ->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, ['-updateTime'], $page, $size);
        unset($count);

        $this->render(new ServiceView($serviceList));
        return true;
    }
}
