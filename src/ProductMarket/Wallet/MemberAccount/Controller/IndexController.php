<?php
namespace SuperMarket\ProductMarket\Wallet\MemberAccount\Controller;

use Marmot\Framework\Controller\WebTrait;
use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;

use Common\Controller\Interfaces\IIndexController;
use Common\Controller\Traits\GlobalCheckTrait;

use Sdk\ProductMarket\MemberAccount\Repository\MemberAccountRepository;
use Sdk\TradeRecord\Repository\TradeRecordRepository;
use Sdk\ProductMarket\TradeRecord\Repository\TradeRecordRepository as ProductMarketTradeRecordRepository;

use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template\IndexView;
use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Template\ProductMarketIndexView;
use SuperMarket\ProductMarket\Wallet\MemberAccount\View\Json\JsonListView;

class IndexController extends Controller implements IIndexController
{
    use GlobalCheckTrait, WebTrait;

    private $memberAccountRepository;

    private $tradeRecordRepository;

    const TRADERECORD_LIST = 10;

    const STATUS = [
        'DEPOSIT'=> 1,
        'ORDER_PAY_ENTERPRISE' => 10,
        'BUYER_ENTERPRISE'=> 14,
        'SELLER_ENTERPRISE'=> 16,
        'ORDER_CONFIRMATION_PLATFORM' => 18,
        'ORDER_CONFIRMATION_SUBSIDY' => 19,
        'WITHDRAWAL'=> 20,
        'BUYER_REFUND' => 24
    ];

    public function __construct()
    {
        parent::__construct();
        $this->memberAccountRepository = new MemberAccountRepository();
        $this->tradeRecordRepository = new TradeRecordRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->memberAccountRepository);
        unset($this->tradeRecordRepository);
    }

    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return $this->memberAccountRepository;
    }

    protected function getTradeRecordRepository() : TradeRecordRepository
    {
        return $this->tradeRecordRepository;
    }

    protected function getProductMarketTradeRecordRepository() : ProductMarketTradeRecordRepository
    {
        return new ProductMarketTradeRecordRepository();
    }

    public function index()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $memberId = Core::$container->get('user')->getId();

        $memberAccount = $this->getMemberAccountRepository()
            ->scenario(MemberAccountRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($memberId);

        if ($memberAccount instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        list($count, $tradeRecordList) = $this->fetchTradeRecord();

        $this->render(new IndexView($memberAccount, $tradeRecordList, $count));

        return true;
    }

    /**
     * [fetchTradeRecord 根据条件检索交易流水]
     * @return [type] [bool]
     * @param [GET]
     * @method /memberAccount/fetchTradeRecord
     *
     */
    public function fetchTradeRecord()
    {
        list($page, $size) = $this->getPageAndSize(self::TRADERECORD_LIST);
        list($filter, $sort) = $this->filterFormatChange();

        list($count, $tradeRecordList) = $this->getTradeRecordRepository()
            ->scenario(TradeRecordRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($tradeRecordList, $count));
            return true;
        }

        return [$count, $tradeRecordList];
    }


    # 产销流水
    public function fetchProductMarketTradeRecord()
    {
        list($page, $size) = $this->getPageAndSize(self::TRADERECORD_LIST);
        list($filter, $sort) = $this->filterFormatChange();

        list($count, $tradeRecordList) = $this->getProductMarketTradeRecordRepository()
            ->scenario(ProductMarketTradeRecordRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($tradeRecordList, $count));
            return true;
        }

        return [$count, $tradeRecordList];
    }

    # 产销账户首页
    public function productMarketIndex()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $memberId = Core::$container->get('user')->getId();

        $memberAccount = $this->getMemberAccountRepository()
            ->scenario(MemberAccountRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($memberId);

        if ($memberAccount instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        list($count, $tradeRecordList) = $this->fetchProductMarketTradeRecord();

        $this->render(new ProductMarketIndexView($memberAccount, $tradeRecordList, $count));

        return true;
    }

    public function getPageAndSize($size = '')
    {
        $page = $this->getRequest()->get('page', 1);
        $size = $this->getRequest()->get('limit', $size);

        if (empty($size)) {
            $size = 6;
            $page = 1;
        }

        return [$page, $size];
    }

    protected function filterFormatChange()
    {
        $type = $this->getRequest()->get('type', '');
        $startTime = $this->getRequest()->get('startTime', '');
        $endTime = $this->getRequest()->get('endTime', '');

        $sort = ['-id'];
        $filter = array();

        $filter['memberAccount'] = Core::$container->get('user')->getId();
        $filter['type'] = join(',', self::STATUS);

        if (!empty($type)) {
            $filter['type'] = $type;
        }

        if (!empty($startTime)) {
            $filter['startTime'] = $startTime;
        }

        if (!empty($endTime)) {
            $filter['endTime'] = $endTime;
        }

        return [$filter, $sort];
    }
}
