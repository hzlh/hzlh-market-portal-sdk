<?php
namespace SuperMarket\ProductMarket\Service\RequirementSquare\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Service\RequirementSquare\View\ListViewTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait;

    public function display() : void
    {
        $list = $this->getList();

        $data = array(
            'list' => $list,
            'total' => $this->getTotal()
        );

        $this->encode($data);
    }
}
