<?php
namespace SuperMarket\ProductMarket\Service\RequirementSquare\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;

use Sdk\ProductMarket\ServiceRequirement\Model\ServiceRequirement;

class DetailView extends TemplateView implements IView
{
    private $serviceRequirement;

    private $serviceRequirementList;

    private $translator;

    public function __construct(ServiceRequirement $serviceRequirement, $serviceRequirementList)
    {
        parent::__construct();
        $this->serviceRequirement = $serviceRequirement;
        $this->serviceRequirementList = $serviceRequirementList;
        $this->translator = new ServiceRequirementTranslator();
    }

    public function __destruct()
    {
        unset($this->serviceRequirement);
        unset($this->serviceRequirementList);
        unset($this->translator);
    }

    protected function getServiceRequirement() : ServiceRequirement
    {
        return $this->serviceRequirement;
    }

    protected function getServiceRequirementList() : array
    {
        return $this->serviceRequirementList;
    }

    protected function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray($this->getServiceRequirement());

        $requirementList = $this->getServiceRequirementList();

        $requirementArray = array();
        if (!empty($requirementList)) {
            foreach ($requirementList as $key => $requirement) {
                $requirementArray[$key] = $translator->objectToArray(
                    $requirement,
                    array(
                        'id', 'title', 'minPrice', 'maxPrice', 'serviceCategory'=>['name'],
                        'validityStartTime', 'validityEndTime', 'updateTime', 'createTime'
                    )
                );
                if ($requirementArray[$key]['id'] == $data['id']) {
                    unset($requirementArray[$key]);
                }
            }
        }
        $this->getView()->display(
            'ProductMarket/Service/RequirementSquare/Detail.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['PRODUCT_MARKET_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['SUPPLY_DEMAND'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_HOME'],
                'data' => $data,
                'list' => $requirementArray
            ]
        );
    }
}
