<?php
namespace SuperMarket\ProductMarket\Service\RequirementSquare\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Service\RequirementSquare\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getList();

        $this->getView()->display(
            'ProductMarket/Service/RequirementSquare/List.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['PRODUCT_MARKET_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['SUPPLY_DEMAND'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_HOME'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
