<?php
namespace SuperMarket\ProductMarket\Service\RequirementSquare\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

class CategoryView extends TemplateView implements IView
{
    use ServiceCategoryViewTrait;

    private $serviceCategories;

    public function __construct($serviceCategories)
    {
        parent::__construct();
        $this->serviceCategories = $serviceCategories;
    }

    public function __destruct()
    {
        unset($this->serviceCategories);
    }

    public function getServiceCategories() : array
    {
        return $this->serviceCategories;
    }

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $this->getView()->display(
            'ProductMarket/Service/RequirementSquare/Category.tpl',
            [
                'serviceCategories' => $serviceCategories
            ]
        );
    }
}
