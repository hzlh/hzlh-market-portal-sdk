<?php
namespace SuperMarket\ProductMarket\Service\RequirementSquare\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\ProductMarket\Service\RequirementSquare\View\Json\JsonListView;
use SuperMarket\ProductMarket\Service\RequirementSquare\View\Template\CategoryView;
use SuperMarket\ProductMarket\Service\RequirementSquare\View\Template\ListView;
use SuperMarket\ProductMarket\Service\RequirementSquare\View\Template\DetailView;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\ProductMarket\ServiceRequirement\Repository\ServiceRequirementRepository;

use SuperMarket\ProductMarket\Workbench\ServiceCategory\Controller\CategoryTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait, CategoryTrait;

    const SIZE = array(
        'REQUIREMENT_SIZE' => 12, //获取需求数据条数
        'SEARCH_SIZE' => 5 //获取推荐需求数
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /requirementSquare
     * 需求广场列表
     *
     */
    protected function filterAction()
    {
        list($page, $size) = $this->getPageAndSize(self::SIZE['REQUIREMENT_SIZE']);
        list($filter, $sort) = $this->filterFormatChange();

        $requirementsList = array();
        list($count, $requirementsList) = $this->getRepository()
            ->scenario(ServiceRequirementRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($count, $requirementsList));
            return true;
        }

        $this->render(new ListView($count, $requirementsList));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {

        $sort = $this->getRequest()->get('sort', array());
        $sort = empty($sort) ? ['-updateTime'] : $sort;
        //需求标题
        $title = $this->getRequest()->get('title', '');
        //需求分类
        $parentCategory = $this->getRequest()->get('parentCategory', '');
        $serviceCategory = $this->getRequest()->get('serviceCategory', array());
        //价格区间
        $minPrice = $this->getRequest()->get('minPrice', '');
        $maxPrice = $this->getRequest()->get('maxPrice', '');
        //有效时间
        $validityStartTime = $this->getRequest()->get('validityStartTime', '');
        $validityEndTime = $this->getRequest()->get('validityEndTime', '');
        //检索条件
        $filter = array();
        $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if (!empty($parentCategory) && !empty($serviceCategory)) {
            unset($parentCategory);
        }

        if (!empty($parentCategory)) {
            $filter['parentCategory'] = marmot_decode($parentCategory);
        }
        if (!empty($serviceCategory)) {
            if (is_string($serviceCategory)) {
                $serviceCategory = array($serviceCategory);
            }
            $filter['serviceCategory'] = $this->implodeArray($serviceCategory);
        }
        if (!empty($minPrice)) {
            $filter['minPrice'] = $minPrice;
        }
        if (!empty($maxPrice)) {
            $filter['maxPrice'] = $maxPrice;
        }
        if (!empty($validityStartTime)) {
            $filter['validityStartTime'] = $validityStartTime;
        }
        if (!empty($validityEndTime)) {
            $filter['validityEndTime'] = $validityEndTime;
        }

        return [$filter, $sort];
    }

    /**
     *
     */
    protected function implodeArray(array $array) : string
    {
        $dataDecode = array();
        foreach ($array as $val) {
            $dataDecode[] = marmot_decode($val);
        }

        return implode(',', $dataDecode);
    }

    /**
     * @return bool
     * @param [GET]
     * @method /requirementSquare/MA
     * 需求详情
     *
     */
    protected function fetchOneAction($id)
    {
        $requirements = $this->getRepository()
            ->scenario(ServiceRequirementRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($requirements instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        //获取相似需求列表【按服务分类id检索】
        $serviceCategoryId =  $requirements->getServiceCategory()->getId();
        $requirementList = $this->fetchRequirementList($serviceCategoryId);

        $this->render(new DetailView($requirements, $requirementList));
        return true;
    }

    /**
     * @param $serviceCategoryId
     * @return array
     * 获取条件为[服务分类id]的需求列表
     *
     */
    protected function fetchRequirementList($serviceCategoryId)
    {
        list($filter, $sort) = $this->otherFilterFormatChange($serviceCategoryId);

        $requirementList = array();
        list($count, $requirementList) = $this->getRepository()
            ->scenario(ServiceRequirementRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, self::SIZE['SEARCH_SIZE']);
        unset($count);

        return $requirementList;
    }

    /**
     * @param $serviceCategoryId
     * @return array
     *
     * 获取需求详情中需求列表条件
     */
    protected function otherFilterFormatChange($serviceCategoryId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['serviceCategory'] = $serviceCategoryId;
        $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /requirementSquares/serviceCategory
     *
     * 需求分类列表
     */
    public function serviceCategory()
    {
        $serviceCategory = $this->fetchServiceCategory();

        $this->render(new CategoryView($serviceCategory));
        return true;
    }

    public function fetchList(string $ids)
    {
        unset($ids);
        $this->displayError();
        return false;
    }
}
