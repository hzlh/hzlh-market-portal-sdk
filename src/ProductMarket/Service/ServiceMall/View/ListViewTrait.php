<?php
namespace SuperMarket\ProductMarket\Service\ServiceMall\View;

use SuperMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;
use SuperMarket\ProductMarket\Workbench\ServiceCategory\Translator\ServiceCategoryTranslator;

trait ListViewTrait
{
    private $count;

    private $list;

    private $searchParameters;

    private $serviceTranslator;

    private $serviceCategoryTranslator;

    public function __construct(int $count, array $list, array $searchParameters)
    {
        parent::__construct();
        $this->count = $count;
        $this->list = $list;
        $this->searchParameters = $searchParameters;
        $this->serviceTranslator = new ServiceTranslator();
        $this->serviceCategoryTranslator = new ServiceCategoryTranslator();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->count);
        unset($this->list);
        unset($this->searchParameters);
        unset($this->serviceTranslator);
        unset($this->serviceCategoryTranslator);
    }

    public function getCount() : int
    {
        return $this->count;
    }

    public function getList() : array
    {
        return $this->list;
    }

    public function getSearchParameters() : array
    {
        return $this->searchParameters;
    }

    public function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    public function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return $this->serviceCategoryTranslator;
    }

    public function searchParameters()
    {
        $searchParameters = $this->getSearchParameters();

        $serviceCategories = empty($searchParameters['serviceCategories']) ?
                                [] : $searchParameters['serviceCategories'];

        $serviceCategoryArray = array();

        foreach ($serviceCategories as $serviceCategory) {
            $serviceCategoryArray[] = $this->getServiceCategoryTranslator()->ObjectToArray(
                $serviceCategory,
                array('id', 'name', 'parentCategory'=>['id', 'name'])
            );
        }

        $serviceCategoryArray = $this->formatArray($serviceCategoryArray);
        
        $searchParameters['serviceCategories'] = $serviceCategoryArray;

        return $searchParameters;
    }

    public function serviceList()
    {
        $serviceList = array();

        $translator = $this->getServiceTranslator();

        foreach ($this->getList() as $val) {
            $serviceList[] = $translator->objectToArray(
                $val,
                array(
                    'id',
                    'title',
                    'minPrice',
                    'volume',
                    'pageViews',
                    'cover',
                    'enterprise'=>['id','logo','name'],
                    'serviceCategory'=>['name']
                )
            );
        }

        return $serviceList;
    }

    private function formatArray(array $serviceCategoryArray)
    {
        $parentCategories = array_unique(array_column($serviceCategoryArray, 'parentCategory'), SORT_REGULAR);
        
        foreach ($parentCategories as $key => $parentCategory) {
            foreach ($serviceCategoryArray as $val) {
                if ($val['parentCategory']['id'] == $parentCategory['id']) {
                    unset($val['parentCategory']);
                    $parentCategories[$key]['serviceCategory'][] = $val;
                }
            }
        };

        return $parentCategories;
    }
}
