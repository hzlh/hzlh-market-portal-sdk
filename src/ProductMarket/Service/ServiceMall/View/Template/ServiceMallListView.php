<?php
namespace SuperMarket\ProductMarket\Service\ServiceMall\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Service\ServiceMall\View\ListViewTrait;

class ServiceMallListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $searchParameters = $this->searchParameters();

        $serviceList = $this->serviceList();

        $this->getView()->display(
            'ProductMarket/Service/ServiceMall/Index.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['PRODUCT_MARKET_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['COMMODITY'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_HOME'],
                'searchParameters' => $searchParameters,
                'list' => $serviceList,
                'count' => $this->getCount()
            ]
        );
    }
}
