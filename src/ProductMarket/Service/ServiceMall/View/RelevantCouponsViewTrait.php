<?php
namespace SuperMarket\ProductMarket\Service\ServiceMall\View;

use SuperMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;
use SuperMarket\ProductMarket\Workbench\MerchantCoupon\Translator\MerchantCouponTranslator;

trait RelevantCouponsViewTrait
{
    private $count;

    private $data;

    private $translator;

    public function __construct($count, $data)
    {
        parent::__construct();
        $this->count = $count;
        $this->data = $data;
        $this->translator = new MerchantCouponTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->data);
        unset($this->translator);
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : MerchantCouponTranslator
    {
        return $this->translator;
    }

    public function getCouponList()
    {
        $couponList = $this->getData();

        $list = array();
        if (!empty($couponList)) {
            foreach ($couponList as $coupon) {
                $list[] = $this->getTranslator()->ObjectToArray(
                    $coupon,
                    array('id','releaseType','couponType','denomination','discount','useStandard','validityStartTime',
                      'validityEndTime','status','isSuperposition','reference'=>['id','name'])
                );
            }
        }

        return $list;
    }
}
