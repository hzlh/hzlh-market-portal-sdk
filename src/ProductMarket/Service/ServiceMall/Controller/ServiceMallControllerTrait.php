<?php
namespace SuperMarket\ProductMarket\Service\ServiceMall\Controller;

use Marmot\Core;

use Sdk\Common\Model\IEnableAble;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;
use Sdk\Policy\Model\PolicyModelFactory;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\Distribution\Repository\SystemTemplateRepository;
use Sdk\Distribution\Model\SystemTemplate;

trait ServiceMallControllerTrait
{
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    public function fetchSearchParameters() : array
    {
        $searchParameters = array(
            'serviceObjects' => $this->fetchServiceObjects(),
            'serviceCategories' => $this->fetchServiceCategories()
        );

        return $searchParameters;
    }

    private function fetchServiceObjects() : array
    {
        $serviceObjects = array();

        foreach (PolicyModelFactory::POLICY_APPLICABLE_OBJECT as $key => $serviceObject) {
            $serviceObjects[] = array(
                'id' => marmot_encode($key),
                'name' => $serviceObject
            );
        }

        return $serviceObjects;
    }

    private function fetchServiceCategories($filter = []) : array
    {
        //获取二级服务分类
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $sort = ['id'];
        $page = 0;
        $size = 1000;

        $serviceCategories = array();
        list($count, $serviceCategories) = $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        unset($count);

        return $serviceCategories;
    }

    protected function getSystemTemplateRepository() : SystemTemplateRepository
    {
        return new SystemTemplateRepository();
    }

    public function getSystemTemplateData()
    {
        $filter = [];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $filter['category'] = SystemTemplate::CATEGORY['SERVICE'];

        $count = 0;
        list($count, $list) = $this->getSystemTemplateRepository()
            ->scenario(SystemTemplateRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, ['-updateTime'], PAGE, FORM_SIZE);
        unset($list);
        return $count;
    }
}
