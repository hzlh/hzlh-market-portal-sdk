<?php
namespace SuperMarket\ProductMarket\Service\ServiceMall\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use Sdk\Collection\Model\Collection;
use Sdk\Common\Model\IApplyAble;
use Sdk\ProductMarket\Service\Model\Service;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;

use SuperMarket\ProductMarket\Service\ServiceMall\View\Template\ServiceMallListView;
use SuperMarket\ProductMarket\Service\ServiceMall\View\Template\ServiceMallView;
use SuperMarket\ProductMarket\Service\ServiceMall\View\Json\ServiceMallJsonListView;
use SuperMarket\ProductMarket\Service\ServiceMall\View\Template\RelevantCouponsListView;
use SuperMarket\ProductMarket\Service\ServiceMall\View\Json\RelevantCouponsJsonListView;

use Sdk\MerchantCoupon\Repository\MerchantCouponRepository;
use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\Evaluation\Model\Evaluation;
use Sdk\Evaluation\Repository\EvaluationRepository;
use Sdk\EvaluationReply\Repository\EvaluationReplyRepository;
use Enterprise\Repository\EnterpriseRepository;

use Statistical\Controller\StatisticalControllerTrait;

use SuperMarket\ProductMarket\Service\ServiceMall\View\Json\ServiceEvaluationListView;
use UserCenter\Collection\Controller\CollectionTrait;

use Workbench\Service\Controller\ContractTemplateItemTrait;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait,
        FetchControllerTrait,
        ServiceMallControllerTrait,
        CollectionTrait,
        ContractTemplateItemTrait;

    const STATICS_TYPE = [
      'GOODS_SCORE' => 'staticsCommodityScore',
      'ENTERPRISE_SCORE' => 'staticsEnterpriseScore',
      'CONTRACT_PERFORMANCE_COUNT' => 'staticsContractPerformanceCount',
      'CONTRACT_USAGE_COUNT' =>'staticsContractUsageCount',
      'ENTERPRISE_CONTRACT_COUNT' =>'staticsEnterpriseContractCount'
    ];

    const PAGE = 0;

    const SIZE = [
        'SERVICE_LIST' => 20, //服务商城列表数量
        'RECOMMENDED_SERVICES' => 4 //详情页面同企业推荐服务数量
    ];

    private $serviceCategoryRepository;

    private $serviceRepository;

    private $merchantCouponRepository;

    private $concurrentAdapter;

    public function __construct()
    {
        parent::__construct();
        $this->merchantCouponRepository = new MerchantCouponRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->merchantCouponRepository);
        unset($this->serviceCategoryRepository);
        unset($this->serviceRepository);
        unset($this->concurrentAdapter);
    }

    protected function getMerchantCouponRepository() : MerchantCouponRepository
    {
        return $this->merchantCouponRepository;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function getConcurrentAdapter()
    {
        return $this->concurrentAdapter;
    }

    protected function getEvaluationRepository() : EvaluationRepository
    {
        return new EvaluationRepository();
    }

    protected function getEvaluationReplyRepository() : EvaluationReplyRepository
    {
        return new EvaluationReplyRepository();
    }
    /**
     *
     */
    protected function filterAction()
    {
        // 获取搜索参数列表
        $searchParameters = $this->fetchSearchParameters();

        list($page, $size) = $this->getPageAndSize(self::SIZE['SERVICE_LIST']);
        list($filter, $sort) = $this->filterFormatChange();

        // 获取服务列表
        list($count, $list) = $this->getServiceRepository()
           ->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
           ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new ServiceMallJsonListView($count, $list));
            return true;
        }

        $this->render(new ServiceMallListView($count, $list, $searchParameters));
        return true;
    }

    protected function filterFormatChange()
    {
        $parentCategory = $this->getRequest()->get('parentCategory', '');
        $serviceCategory = $this->getRequest()->get('serviceCategory', array());
        $serviceObjects = $this->getRequest()->get('serviceObject', array());
        $minPrice = $this->getRequest()->get('minPrice', 0);
        $maxPrice = $this->getRequest()->get('maxPrice', 0);
        $contract = $this->getRequest()->get('contract', 0);
        $title = $this->getRequest()->get('title', '');
        $sort = $this->getRequest()->get('sort', array());

        $tag = Core::$container->get('user')->getBinaries();
        if (!empty($tag)) {
            $sort['tag'] = $tag;
        }

        $sort = empty($sort) ? ['-updateTime'] : $sort;

        $filter = [];
        $filter['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        $filter['title'] = $title;
        $filter['minPriceRange'] = $minPrice;
        $filter['maxPriceRange'] = $maxPrice;
        $filter['contract'] = $contract;

        if (!empty($parentCategory) && !empty($serviceCategory)) {
            unset($parentCategory);
        }

        if (!empty($parentCategory)) {
            $filter['parentCategory'] = marmot_decode($parentCategory);
        }
        if (!empty($serviceCategory)) {
            $filter['serviceCategory'] = $this->implodeArray($serviceCategory);
        }
        if (!empty($serviceObjects)) {
            $filter['serviceObjects'] = $this->implodeArray($serviceObjects);
        }

        return [$filter, $sort];
    }

    protected function implodeArray(array $array) : string
    {
        $idDecodeData = array();

        foreach ($array as $val) {
            $idDecodeData[] = marmot_decode($val);
        }

        return implode(',', $idDecodeData);
    }

    protected function fetchOneAction(int $id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        $serviceRepository = $this->getServiceRepository();

        $service = $serviceRepository->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)->portalFetchOne($id);
        
        if ($service instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        list($filterStore, $filterSame ,$sort) = $this->filterRecommendedFormatChange($service);

        // 获取推荐服务列表
        $this->getConcurrentAdapter()->addPromise(
            'storeServices',
            $serviceRepository->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($filterStore, $sort, self::PAGE, self::SIZE['RECOMMENDED_SERVICES']),
            $serviceRepository->getAdapter()
        );
        $this->getConcurrentAdapter()->addPromise(
            'sameServices',
            $serviceRepository->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($filterSame, $sort, self::PAGE, self::SIZE['RECOMMENDED_SERVICES']),
            $serviceRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        //获取商品评价数据
        $evaluationCount = 0;
        $evaluations = [];
        list($evaluationCount,$evaluations) = $this->filterEvaluation($id);
        $staticsList = [];

        list($count, $couponList) = $this->fetchCoupon($service->getEnterprise()->getId());
        unset($count);

        //@TODO 屏蔽
        $staticsList = $this->fetchStatics($id);

        //@TODO 屏蔽
        $collection = [];
        $collectionAll= 0;
        list($collection, $collectionAll) = $this->collectionInfoList(
            Collection::COLLECTION_CATEGORY['PRODUCT_MARKET'],
            $id
        );
        list($enterpriseCollection, $enterpriseCollectionAll) = $this->collectionInfoList(
            Collection::COLLECTION_CATEGORY['ENTERPRISE'],
            $service->getEnterprise()->getId()
        );

        $evaluationReplies = array();
        $enterpriseStaticsList = array();

        if (!empty($evaluations)) {
            $evaluationReplies = $this->filterEvaluationReplies($evaluations);
            $enterpriseStaticsList = $this->filterEnterpriseStatics($evaluations);
        }

        $collectionArray = array(
            'collection' => $collection,//用户收藏列表
            'collectionAll' => $collectionAll,//商品收藏总数
            'enterpriseCollection' => $enterpriseCollection,
            'enterpriseCollectionAll' => $enterpriseCollectionAll,
        );
        $contractTemplate = [];
        $contractTemplate = $this->filterContractTemplateItems($service);

        $contractPerformanceStatics = [];
        $enterpriseList = [];
        $contractUsageList = [];
        if (!empty($contractTemplate)) {
            //获取合同模版履约情况和纠纷率的统计
            $templateId = $this->getTemplateIds($contractTemplate);
            $contractPerformanceStatics = $this->fetchPerformanceStatics($templateId, self::STATICS_TYPE['CONTRACT_PERFORMANCE_COUNT']);//phpcs:ignore
            //获取合同模版的违约次数和使用次数
            $contractUsageList = $this->fetchPerformanceStatics($templateId, self::STATICS_TYPE['CONTRACT_USAGE_COUNT']);//phpcs:ignore
            if (!empty($contractUsageList)) {
                $enterpriseIds = $this->getListIds($contractUsageList['data']->getResult());
                $enterpriseList = $this->getEnterpriseList([$enterpriseIds]);
            }
        }

        //获取商家履约情况数据
        $enterpriseContractStatics = [];
        $enterpriseContractStatics = $this->fetchPerformanceStatics($service->getEnterprise()->getId(), self::STATICS_TYPE['ENTERPRISE_CONTRACT_COUNT']);//phpcs:ignore

        //获取系统模版数量
        $shareTemplateCount = $this->getSystemTemplateData();
        if ($this->getRequest()->isAjax()) {
            $this->render(new ServiceEvaluationListView(
                $service,
                $data,
                $couponList,
                $staticsList,
                $evaluationReplies,
                $enterpriseStaticsList,
                $evaluations,
                $evaluationCount,
                $collectionArray,
                $contractTemplate
            ));
            return true;
        }

        $this->render(new ServiceMallView(
            $service,
            $data,
            $couponList,
            $staticsList,
            $evaluationReplies,
            $enterpriseStaticsList,
            $evaluations,
            $evaluationCount,
            $collectionArray,
            $contractTemplate,
            $enterpriseList,
            $contractUsageList,
            $contractPerformanceStatics,
            $enterpriseContractStatics,
            $shareTemplateCount
        ));
        return true;
    }
    /**
     *
     */
    public function filterRecommendedFormatChange(Service $service)
    {
        $enterpriseId = $service->getEnterprise()->getId();
        $serviceCategoryId = $service->getServiceCategory()->getId();

        $sort = ['-updateTime'];

        $filterStore['enterprise'] = $enterpriseId;
        $filterStore['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $filterStore['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        $filterSame['serviceCategory'] = $serviceCategoryId;
        $filterSame['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $filterSame['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        return [$filterStore, $filterSame, $sort];
    }

    /**
     *
     */
    public function fetchRelevantCoupons()
    {
        $enterprise = $this->getRequest()->get('enterpriseId', '');

        list($count, $couponList) = $this->fetchCoupon(marmot_decode($enterprise));

        if ($this->getRequest()->isAjax()) {
            $this->render(new RelevantCouponsJsonListView($count, $couponList));
            return true;
        }

        $this->render(new RelevantCouponsListView($count, $couponList));
        return true;
    }

    /**
     *
     */
    protected function fetchCoupon($enterpriseId)
    {
        list($page, $size) = $this->getPageAndSize(COMMON_SIZE);
        list($filter, $sort) = $this->filterRelevantCouponsFormatChange($enterpriseId);

        $couponList = array();
        list($count, $couponList) = $this->getMerchantCouponRepository()
            ->scenario(MerchantCouponRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        return [$count, $couponList];
    }

    /**
     *
     */
    protected function filterRelevantCouponsFormatChange($enterpriseId)
    {
        $sort = ['-updateTime'];
        $filter = array();

        $filter['releaseType'] = MerchantCoupon::RELEASE_TYPE['MERCHANT'];  //默认商家优惠劵
        $filter['status'] = MerchantCoupon::STATUS['NORMAL'];   //默认正常使用
        $filter['enterpriseId'] = $enterpriseId;

        return [$filter, $sort];
    }

    protected function filterEvaluation(int $serviceId) : array
    {
        $filter = [];

        $filter['relation'] = $serviceId;
        $filter['relationCategory'] = Evaluation::RELATION_CATEGORY['MARKET'];
        $filter['status'] = Evaluation::STATUS['NORMAL'];
        $sort = ['-createTime'];

        list($page, $size) = $this->getPageAndSize(FORM_SIZE);

        $evaluationType = marmot_decode($this->getRequest()->get('evaluationType', ''));

        if (!empty($evaluationType)) {
            $filter['evaluationType'] = $evaluationType;
        }


        list($count, $list) = $this->getEvaluationRepository()
            ->scenario(EvaluationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        return [$count,$list];
    }

    protected function filterEvaluationReplies(array $evaluations) : array
    {
        $evaluationIds = [];
        foreach ($evaluations as $evaluation) {
            $evaluationIds[] = $evaluation->getId();
        }
        $sort = ['-createTime'];

        $filter=[];
        $filter['evaluation'] = join(',', $evaluationIds);

        list($count, $evaluationReplyList) = $this->getEvaluationReplyRepository()
             ->scenario(EvaluationReplyRepository::LIST_MODEL_UN)
             ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $evaluationReplyList;
    }

    protected function filterEnterpriseStatics(array $evaluations) : array
    {
        $enterpriseIds = [];
        foreach ($evaluations as $evaluation) {
            $enterpriseIds[] = $evaluation->getEnterprise()->getId();
        }

        $filter=[];
        $filter['enterpriseId'] = $enterpriseIds[0];

        $enterpriseStaticsList = $this->analyse(self::STATICS_TYPE['ENTERPRISE_SCORE'], $filter);

        return  ["data"=>$enterpriseStaticsList,"type"=>self::STATICS_TYPE['ENTERPRISE_SCORE']];
    }

    protected function fetchStatics(int $serviceId)
    {
        $filter = [];
        $filter['serviceId'] = $serviceId;
        $filter['relationCategory'] = Evaluation::RELATION_CATEGORY['MARKET'];
        $staticsList = $this->analyse(self::STATICS_TYPE['GOODS_SCORE'], $filter);
        return ["data"=>$staticsList,"type"=>self::STATICS_TYPE['GOODS_SCORE']];
    }
}
