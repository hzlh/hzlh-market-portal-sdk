<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Command;

use Marmot\Interfaces\ICommand;

class AddShareRecordCommand implements ICommand
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }
}
