<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Translator;

use Sdk\Common\Translator\CategoryTranslator;
use Sdk\Common\Model\Category;
use Sdk\ProductMarket\Policy\Model\NullPolicyCategory;

class PolicyCategoryTranslator extends CategoryTranslator
{
    public function arrayToObject(array $expression, $category = null)
    {
        unset($category);
        unset($expression);
        return NullPolicyCategory::getInstance();
    }

    public function objectToArray($category, array $keys = array())
    {
        if (!$category instanceof Category) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = $category->getId();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $category->getName();
        }

        return $expression;
    }
}
