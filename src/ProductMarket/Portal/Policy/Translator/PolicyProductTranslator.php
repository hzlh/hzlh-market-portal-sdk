<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Translator;

use Sdk\ProductMarket\PolicyProduct\Model\PolicyProduct;
use Sdk\ProductMarket\PolicyProduct\Model\NullPolicyProduct;

use Marmot\Interfaces\ITranslator;

use SuperMarket\ProductMarket\UserCenter\Enterprise\Translator\EnterpriseTranslator;

class PolicyProductTranslator implements ITranslator
{
    use PolicyTranslatorTrait;

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    public function getPolicyProductTranslatorFactory() : PolicyProductTranslatorFactory
    {
        return new PolicyProductTranslatorFactory();
    }

    public function arrayToObject(array $expression, $policyProduct = null)
    {
        unset($policyProduct);
        unset($expression);
        return NullPolicyProduct::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($policyProduct, array $keys = array())
    {
        if (!$policyProduct instanceof PolicyProduct) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'cover',
                'attribute',
                'category',
                'enterprise'=>['id','name','logo'],
                'product'=>[]
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($policyProduct->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $policyProduct->getTitle();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $policyProduct->getCover();
        }
        if (in_array('attribute', $keys)) {
            $expression['attribute'] = $policyProduct->getAttribute();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $policyProduct->getCategory();
        }

        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $policyProduct->getEnterprise(),
                $keys['enterprise']
            );
        }

        if (isset($keys['product'])) {
            $translator = $this->getPolicyProductTranslatorFactory()->getTranslator($policyProduct->getCategory());
            $expression['product'] = $translator->objectToArray(
                $policyProduct->getProduct(),
                $keys['product']
            );
        }

        return $expression;
    }
}
