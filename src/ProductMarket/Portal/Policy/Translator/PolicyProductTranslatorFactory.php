<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

use Sdk\ProductMarket\PolicyProduct\Model\PolicyProduct;

class PolicyProductTranslatorFactory
{
    const MAPS = array(
        PolicyProduct::CATEGORY['SERVICE']=>
            'ProductMarket\Workbench\Service\Translator\ServiceTranslator',
        PolicyProduct::CATEGORY['FINANCE']=>
            'ProductMarket\Workbench\LoanProduct\Translator\LoanProductTranslator',
    );

    public function getTranslator(string $type) : ITranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
