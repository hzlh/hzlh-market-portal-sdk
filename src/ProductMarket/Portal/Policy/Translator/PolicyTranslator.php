<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Translator;

use Sdk\ProductMarket\Policy\Model\Policy;
use Sdk\ProductMarket\Policy\Model\NullPolicy;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

class PolicyTranslator implements ITranslator
{
    use PolicyTranslatorTrait;

    public function arrayToObject(array $expression, $policy = null)
    {
        unset($policy);
        unset($expression);
        return NullPolicy::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function objectToArray($policy, array $keys = array())
    {
        if (!$policy instanceof Policy) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'title',
                'image',
                'dispatchDepartments'=>[],
                'labels'=>[],
                'applicableObjects'=>[],
                'applicableIndustries'=>[],
                'classifies'=>[],
                'level',
                'detail',
                'description',
                'attachments',
                'admissibleAddress',
                'processingFlow',
                'policyProducts'=>[],
                'createTime',
                'updateTime',
                'pageViews',
                'shareVolume',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($policy->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $policy->getNumber();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $policy->getTitle();
        }
        if (in_array('image', $keys)) {
            $expression['image'] = empty($policy->getImage())
            ? array('name'=>'','identify'=>'') : $policy->getImage();
        }
        if (isset($keys['dispatchDepartments'])) {
            $expression['dispatchDepartments'] = $this->setUpDispatchDepartments(
                $policy->getDispatchDepartments(),
                $keys['dispatchDepartments']
            );
        }
        if (isset($keys['labels'])) {
            $expression['labels'] = $this->setUpLabels($policy->getLabels(), $keys['labels']);
        }
        if (isset($keys['applicableObjects'])) {
            $expression['applicableObjects'] = $this->setUpApplicableObjects(
                $policy->getApplicableObjects(),
                $keys['applicableObjects']
            );
        }
        if (isset($keys['applicableIndustries'])) {
            $expression['applicableIndustries'] = $this->setUpApplicableIndustries(
                $policy->getApplicableIndustries(),
                $keys['applicableIndustries']
            );
        }
        if (isset($keys['policyProducts'])) {
            $expression['policyProducts'] = $this->setUpPolicyProducts(
                $policy->getPolicyProducts(),
                $keys['policyProducts']
            );
        }
        if (isset($keys['classifies'])) {
            $expression['classifies'] = $this->setUpClassifies($policy->getClassifies(), $keys['classifies']);
        }
        if (in_array('level', $keys)) {
            $expression['level'] = $this->getDictionaryTranslator()->objectToArray($policy->getLevel());
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = Filter::dhtmlspecialchars($policy->getDetail());
        }
        if (in_array('description', $keys)) {
            $expression['description'] = Filter::dhtmlspecialchars($policy->getDescription());
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $policy->getAttachments();
        }
        if (in_array('admissibleAddress', $keys)) {
            $expression['admissibleAddress'] = $policy->getAdmissibleAddress();
        }
        if (in_array('processingFlow', $keys)) {
            $expression['processingFlow'] = Filter::dhtmlspecialchars($policy->getProcessingFlow());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $policy->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $policy->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $policy->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $policy->getUpdateTime());
        }
        if (in_array('pageViews', $keys)) {
            $expression['pageViews'] = $policy->getPageViews();
        }
        if (in_array('shareVolume', $keys)) {
            $expression['shareVolume'] = $policy->getShareVolume();
        }

        return $expression;
    }
}
