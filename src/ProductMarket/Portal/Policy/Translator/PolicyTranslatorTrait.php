<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Translator;

use SuperMarket\ProductMarket\Label\Translator\LabelTranslator;
use SuperMarket\ProductMarket\Dictionary\Translator\DictionaryTranslator;
use SuperMarket\ProductMarket\Portal\DispatchDepartment\Translator\DispatchDepartmentTranslator;

trait PolicyTranslatorTrait
{
    protected function getPolicyCategoryTranslator() : PolicyCategoryTranslator
    {
        return new PolicyCategoryTranslator();
    }

    protected function getDispatchDepartmentTranslator() : DispatchDepartmentTranslator
    {
        return new DispatchDepartmentTranslator();
    }

    protected function getLabelTranslator() : LabelTranslator
    {
        return new LabelTranslator();
    }

    protected function getDictionaryTranslator() : DictionaryTranslator
    {
        return new DictionaryTranslator();
    }

    protected function getPolicyProductTranslator() : PolicyProductTranslator
    {
        return new PolicyProductTranslator();
    }

    public function setUpDispatchDepartments($dispatchDepartments, $key)
    {
        $dispatchDepartmentsArray = array();
        foreach ($dispatchDepartments as $dispatchDepartmentsVal) {
            $dispatchDepartmentsArray[] = $this->getDispatchDepartmentTranslator()->objectToArray(
                $dispatchDepartmentsVal,
                $key
            );
        }

        return $dispatchDepartmentsArray;
    }

    public function setUpLabels($labels, $key)
    {
        $labelsArray = array();
        foreach ($labels as $labelsVal) {
            $labelsArray[] = $this->getLabelTranslator()->objectToArray(
                $labelsVal,
                $key
            );
        }

        return $labelsArray;
    }

    // 注意此处需要 public 方法，在服务的 ViewTrait 文件中用到
    public function setUpApplicableObjects($applicableObjects, $key)
    {
        $applicableObjectsArray = array();
        foreach ($applicableObjects as $applicableObject) {
            $applicableObjectsArray[] = $this->getDictionaryTranslator()->objectToArray(
                $applicableObject,
                $key
            );
        }

        return $applicableObjectsArray;
    }

    public function setUpApplicableIndustries($applicableIndustries, $key)
    {
        $applicableIndustriesArray = array();
        foreach ($applicableIndustries as $applicableIndustry) {
            $applicableIndustriesArray[] = $this->getDictionaryTranslator()->objectToArray(
                $applicableIndustry,
                $key
            );
        }

        return $applicableIndustriesArray;
    }

    public function setUpClassifies($classifies, $key)
    {
        $classifiesArray = array();
        foreach ($classifies as $classify) {
            $classifiesArray[] = $this->getDictionaryTranslator()->objectToArray(
                $classify,
                $key
            );
        }

        return $classifiesArray;
    }

    public function setUpPolicyProducts($policyProducts, $key)
    {
        $policyProductsArray = array();
        foreach ($policyProducts as $policyProduct) {
            $policyProductsArray[] = $this->getPolicyProductTranslator()->objectToArray(
                $policyProduct,
                $key
            );
        }

        return $policyProductsArray;
    }
}
