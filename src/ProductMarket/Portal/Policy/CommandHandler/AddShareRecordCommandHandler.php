<?php
namespace SuperMarket\ProductMarket\Portal\Policy\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ProductMarket\Log\Model\Log;
use Sdk\ProductMarket\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

use Sdk\ProductMarket\Policy\Model\Policy;
use Sdk\ProductMarket\Policy\Model\NullPolicy;
use Sdk\ProductMarket\Policy\Repository\PolicyRepository;

use SuperMarket\ProductMarket\Portal\Policy\Command\AddShareRecordCommand;

class AddShareRecordCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    const FETCH_TYPE = 2;

    private $repository;

    private $policy;

    public function __construct()
    {
        $this->policy = new NullPolicy();
        $this->repository = new PolicyRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->policy);
    }

    protected function getPolicy() : Policy
    {
        return $this->policy;
    }

    protected function getRepository() : PolicyRepository
    {
        return $this->repository;
    }

    protected function fetchPolicy(int $id) : Policy
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddShareRecordCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->policy = $this->fetchPolicy($command->id);

        $type = self::FETCH_TYPE;
        if ($this->policy->addShareRecord($type)) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['POLICY'],
            $this->policy->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->policy->getTitle()
        );
    }
}
