<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Adapter\ConcurrentAdapter;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Common\Model\IOnShelfAble;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;
use Sdk\ProductMarket\Policy\Repository\PolicyRepository;
use Sdk\ProductMarket\PolicyInterpretation\Repository\PolicyInterpretationRepository;
use Sdk\ProductMarket\DispatchDepartment\Repository\DispatchDepartmentRepository;

use Common\Controller\Traits\ToolTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Controller\Interfaces\IFetchAbleController;

use SuperMarket\ProductMarket\Portal\Policy\View\Template\PolicyListView;
use SuperMarket\ProductMarket\Portal\Policy\View\Template\PolicyView;
use SuperMarket\ProductMarket\Portal\Policy\View\Json\PolicyJsonListView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, PolicyControllerTrait, ToolTrait;

    const DEFAULT_PARENT_ID = 0;  // 政策字典项父级id

    const SIZE = array(
        'POLICY_LIST_SIZE' => 6, //政策列表:政策显示个数
        'SERVICE_SIZE' => 3, //政策详情:相关服务显示个数
        'POLICY_SIZE' => 3, //政策详情:相关政策显示个数
    );

    const FETCH_TYPE = 2;

    private $policyRepository;

    private $dispatchDepartmentRepository;

    private $policyInterpretationRepository;

    private $concurrentAdapter;

    public function __construct()
    {
        parent::__construct();
        $this->policyRepository = new PolicyRepository();
        $this->dispatchDepartmentRepository = new DispatchDepartmentRepository();
        $this->policyInterpretationRepository = new PolicyInterpretationRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->policyRepository);
        unset($this->dispatchDepartmentRepository);
        unset($this->policyInterpretationRepository);
        unset($this->concurrentAdapter);
    }

    protected function getPolicyRepository() : PolicyRepository
    {
        return $this->policyRepository;
    }

    protected function getDispatchDepartmentRepository() : DispatchDepartmentRepository
    {
        return $this->dispatchDepartmentRepository;
    }

    protected function getPolicyInterpretationRepository() : PolicyInterpretationRepository
    {
         return $this->policyInterpretationRepository;
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }

    /**
     *
     */
    protected function filterAction()
    {
        // 获取搜索参数列表
        $searchParameters = $this->fetchSearchParameters();

        list($page, $size) = $this->getPageAndSize();

        list($filter, $sort) = $this->filterFormatChange();

        // 获取政策列表
        list($count, $list) = $this->getPolicyRepository()
             ->scenario(PolicyRepository::PORTAL_LIST_MODEL_UN)
             ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new PolicyJsonListView($count, $list, $searchParameters));
            return true;
        }

        $this->render(new PolicyListView($count, $list, $searchParameters));
        return true;
    }

    /**
     *
     */
    protected function filterFormatChange()
    {
        $title = $this->getRequest()->get('title', '');
        $dispatchDepartments = $this->getRequest()->get('dispatchDepartment', array());
        $applicableObjects = $this->getRequest()->get('applicableObject', array());
        $applicableIndustries = $this->getRequest()->get('applicableIndustries', array());
        $classifies = $this->getRequest()->get('classify', array());
        $labels = $this->getRequest()->get('labels', array());
        $level = $this->getRequest()->get('level', '');
        $specials = $this->getRequest()->get('specials', '');

        $sort = ['-updateTime'];

        $filter = array();
        $filter['status'] = IOnShelfAble::STATUS['ONSHELF'];

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if (!empty($specials)) {
            $filter['specials'] = marmot_decode($specials);
        }

        if (!empty($dispatchDepartments)) {
            $filter['dispatchDepartments'] = $this->implodeArray($dispatchDepartments);
        }

        if (!empty($applicableObjects)) {
            $filter['applicableObjects'] = $this->implodeArray($applicableObjects);
        }

        if (!empty($applicableIndustries)) {
            $filter['applicableIndustries'] = $this->implodeArray($applicableIndustries);
        }

        if (!empty($classifies)) {
            $filter['classifies'] = $this->implodeArray($classifies);
        }

        if (!empty($labels)) {
            $filter['labels'] = $this->implodeArray($labels);
        }

        if (!empty($level)) {
            $filter['level'] = $this->implodeArray($level);
        }

        return [$filter, $sort];
    }

    protected function implodeArray(array $array) : string
    {
        $idDecodeData = array();
        foreach ($array as $val) {
            $idDecodeData[] = marmot_decode($val);
        }

        return  implode(',', $idDecodeData);
    }

    /**
     *
     */
    protected function fetchOneAction(int $id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $type = self::FETCH_TYPE;

        $policy = $this->getPolicyRepository()
            ->scenario(PolicyRepository::FETCH_ONE_MODEL_UN)
            ->portalFetchOne($id, $type);

        if ($policy instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        // 获取政策分类，用于查询条件为政策分类的相关政策
        $classifies = $policy->getClassifies();
        $classifyIds = $this->getImplodeIds($classifies);

        $sort = ['-updateTime'];
        $filter['policy'] = $id;

        $policyFilter['classifies'] = $classifyIds;
        $policyFilter['status'] = IOnShelfAble::STATUS['ONSHELF'];

        $policyRepository = $this->getPolicyRepository();
        $policyInterpretationRepository = $this->getPolicyInterpretationRepository();

        $this->getConcurrentAdapter()->addPromise(
            'policies',
            $policyRepository->scenario(PolicyRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($policyFilter, $sort, PAGE, self::SIZE['POLICY_SIZE']),
            $policyRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'policyInterpretations',
            $policyInterpretationRepository->scenario(PolicyInterpretationRepository::LIST_MODEL_UN)
                ->searchAsync($filter, $sort),
            $policyInterpretationRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        $this->render(new PolicyView($policy, $data));
        return true;
    }
}
