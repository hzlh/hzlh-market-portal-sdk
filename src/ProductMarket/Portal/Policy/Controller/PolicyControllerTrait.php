<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Controller;

use Sdk\Common\Model\IEnableAble;
use Sdk\ProductMarket\Label\Model\Label;
use Sdk\ProductMarket\Label\Repository\LabelRepository;
use Sdk\ProductMarket\Dictionary\Model\Dictionary;
use Sdk\ProductMarket\Dictionary\Repository\DictionaryRepository;
use Sdk\ProductMarket\DispatchDepartment\Repository\DispatchDepartmentRepository;

trait PolicyControllerTrait
{
    protected function getLabelRepository() : LabelRepository
    {
        return new LabelRepository();
    }

    protected function getDispatchDepartmentRepository() : DispatchDepartmentRepository
    {
        return new DispatchDepartmentRepository();
    }

    protected function getDictionaryRepository() : DictionaryRepository
    {
        return new DictionaryRepository();
    }

    protected function fetchSearchParameters()
    {
        $labelRepository = $this->getLabelRepository();
        $dictionaryRepository = $this->getDictionaryRepository();
        $dispatchDepartmentRepository= $this->getDispatchDepartmentRepository();

        list($label, $dispatchDepartment, $applicableObject, $applicableIndustry, $level, $classify) = $this->filterDictionaryFormatChange();//phpcs:ignore

        // 政策标签
        $this->getConcurrentAdapter()->addPromise(
            'labels',
            $labelRepository->scenario(LabelRepository::LIST_MODEL_UN)
                ->searchAsync($label, ['id'], PAGE, COMMON_SIZE),
            $labelRepository->getAdapter()
        );

        // 发文部门
        $this->getConcurrentAdapter()->addPromise(
            'dispatchDepartments',
            $dispatchDepartmentRepository->scenario(DispatchDepartmentRepository::LIST_MODEL_UN)
                ->searchAsync($dispatchDepartment, ['id'], PAGE, COMMON_SIZE),
            $dispatchDepartmentRepository->getAdapter()
        );

        // 适用对象
        $this->getConcurrentAdapter()->addPromise(
            'applicableObjects',
            $dictionaryRepository->scenario(DictionaryRepository::LIST_MODEL_UN)
                ->searchAsync($applicableObject, ['id'], PAGE, COMMON_SIZE),
            $dictionaryRepository->getAdapter()
        );

        // 适用行业
        $this->getConcurrentAdapter()->addPromise(
            'applicableIndustries',
            $dictionaryRepository->scenario(DictionaryRepository::LIST_MODEL_UN)
                ->searchAsync($applicableIndustry, ['id'], PAGE, COMMON_SIZE),
            $dictionaryRepository->getAdapter()
        );

        // 政策级别
        $this->getConcurrentAdapter()->addPromise(
            'levels',
            $dictionaryRepository->scenario(DictionaryRepository::LIST_MODEL_UN)
                ->searchAsync($level, ['id'], PAGE, COMMON_SIZE),
            $dictionaryRepository->getAdapter()
        );

        // 政策分类
        $this->getConcurrentAdapter()->addPromise(
            'classifies',
            $dictionaryRepository->scenario(DictionaryRepository::LIST_MODEL_UN)
                ->searchAsync($classify, ['id'], PAGE, COMMON_SIZE),
            $dictionaryRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        return $data;
    }

    protected function filterDictionaryFormatChange()
    {
        //获取公共标签,政策标签
        $label['category'] = Label::CATEAGORY_LABEL['COMMON'].Label::CATEAGORY_LABEL['POLICY'];
        $label['status'] = IEnableAble::STATUS['ENABLED'];
        //发文部门
        $dispatchDepartment['status'] = IEnableAble::STATUS['ENABLED'];
        //适用对象字典项
        $applicableObject['category'] = Dictionary::CATEGORY['APPLICABLE_OBJECTS'];
        $applicableObject['status'] = IEnableAble::STATUS['ENABLED'];
        //适用行业字典项
        $applicableIndustry['category'] = Dictionary::CATEGORY['APPLICABLE_INDUSTRIES'];
        $applicableIndustry['status'] = IEnableAble::STATUS['ENABLED'];
        //政策级别字典项
        $level['category'] = Dictionary::CATEGORY['LEVEL'];
        $level['status'] = IEnableAble::STATUS['ENABLED'];
        //政策分类字典项
        $classify['category'] = Dictionary::CATEGORY['CLASSIFIES'];
        $classify['status'] = IEnableAble::STATUS['ENABLED'];

        return [$label, $dispatchDepartment, $applicableObject, $applicableIndustry, $level, $classify];
    }
}
