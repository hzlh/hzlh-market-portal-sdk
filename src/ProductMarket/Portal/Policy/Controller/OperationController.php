<?php
namespace SuperMarket\ProductMarket\Portal\Policy\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Interfaces\IOperatAbleController;
use Common\Controller\Traits\OperatControllerTrait;
use Controller\Traits\GlobalCheckTrait;

use SuperMarket\ProductMarket\Portal\Policy\Command\AddShareRecordCommand;
use SuperMarket\ProductMarket\Portal\Policy\CommandHandler\PolicyCommandHandlerFactory;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, GlobalCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new PolicyCommandHandlerFactory());
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function addView(): bool
    {
        return false;
    }

    protected function editView(int $id): bool
    {
        unset($id);
        return false;
    }

    protected function addAction(): bool
    {
        return false;
    }

    protected function editAction(int $id): bool
    {
        unset($id);
        return false;
    }

    public function addShareRecord(string $id) : bool
    {
        $id = marmot_decode($id);
        if ($this->getCommandBus()->send(new AddShareRecordCommand($id))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
