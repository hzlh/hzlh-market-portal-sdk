<?php
namespace SuperMarket\ProductMarket\Portal\Policy\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\Portal\Policy\View\ListViewTrait;

class PolicyJsonListView extends JsonView implements IView
{
    use ListViewTrait;

    public function display() : void
    {
        $policyList = $this->policyList();

        $data = array(
            'list' => $policyList,
            'count' => $this->getCount()
        );

        $this->encode($data);
    }
}
