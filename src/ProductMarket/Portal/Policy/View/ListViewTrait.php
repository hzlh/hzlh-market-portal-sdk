<?php
namespace SuperMarket\ProductMarket\Portal\Policy\View;

use SuperMarket\ProductMarket\Portal\Policy\Translator\PolicyTranslator;

use SuperMarket\ProductMarket\Label\Translator\LabelTranslator;

use SuperMarket\ProductMarket\Dictionary\Translator\DictionaryTranslator;

use SuperMarket\ProductMarket\Portal\DispatchDepartment\Translator\DispatchDepartmentTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
trait ListViewTrait
{
    private $count;

    private $policyList;

    private $searchParameters;

    private $policyTranslator;

    private $labelTranslator;

    private $dictionaryTranslator;

    private $dispatchDepartmentTranslator;

    public function __construct(
        int $count,
        array $policyList,
        array $searchParameters
    ) {
        parent::__construct();
        $this->count = $count;
        $this->policyList = $policyList;
        $this->searchParameters = $searchParameters;
        $this->policyTranslator = new PolicyTranslator();
        $this->labelTranslator = new LabelTranslator();
        $this->dictionaryTranslator = new DictionaryTranslator();
        $this->dispatchDepartmentTranslator = new DispatchDepartmentTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->policyList);
        unset($this->searchParameters);
        unset($this->policyTranslator);
        unset($this->labelTranslator);
        unset($this->dictionaryTranslator);
        unset($this->dispatchDepartmentTranslator);
    }

    public function getPolicyList() : array
    {
        return $this->policyList;
    }

    public function getSearchParameters() : array
    {
        return $this->searchParameters;
    }

    public function getPolicyTranslator() : PolicyTranslator
    {
        return $this->policyTranslator;
    }

    public function getLabelTranslator() : LabelTranslator
    {
        return $this->labelTranslator;
    }

    public function getDispatchDepartmentTranslator() : DispatchDepartmentTranslator
    {
        return $this->dispatchDepartmentTranslator;
    }

    protected function getDictionaryTranslator() : DictionaryTranslator
    {
        return $this->dictionaryTranslator;
    }

    public function getCount() : int
    {
        return $this->count;
    }

    // 搜索条件信息
    public function searchParameters()
    {
        $parameters = $this->getSearchParameters();

        // 发文部门
        $dispatchDepartments = !empty($parameters['dispatchDepartments']) ?
            $parameters['dispatchDepartments'][1] : array();
        $dispatchDepartmentArray = array();
        foreach ($dispatchDepartments as $item) {
            $dispatchDepartmentArray[] = $this->getDispatchDepartmentTranslator()->ObjectToArray($item);
        }

        // 政策标签
        $labels = !empty($parameters['labels']) ? $parameters['labels'][1] : array();
        $labelArray = array();
        foreach ($labels as $item) {
            $labelArray[] = $this->getLabelTranslator()->ObjectToArray($item);
        }

        // 适用对象
        $applicableObjects = !empty($parameters['applicableObjects']) ? $parameters['applicableObjects'][1] : array();
        $applicableObjectArray = array();
        if (!empty($applicableObjects)) {
            foreach ($applicableObjects as $item) {
                $applicableObjectArray[] = $this->getDictionaryTranslator()->objectToArray($item);
            }
        }

        // 适用行业
        $applicableIndustries = !empty($parameters['applicableIndustries']) ?
            $parameters['applicableIndustries'][1] : array();
        $applicableIndustryArray = array();
        if (!empty($applicableIndustries)) {
            foreach ($applicableIndustries as $item) {
                $applicableIndustryArray[] = $this->getDictionaryTranslator()->objectToArray($item);
            }
        }

        // 政策级别
        $levels = !empty($parameters['levels']) ? $parameters['levels'][1] : array();
        $levelArray = array();
        if (!empty($levels)) {
            foreach ($levels as $item) {
                $levelArray[] = $this->getDictionaryTranslator()->objectToArray($item);
            }
        }

        // 政策分类
        $classifies = !empty($parameters['classifies']) ? $parameters['classifies'][1] : array();
        $classifyArray = array();
        if (!empty($classifies)) {
            foreach ($classifies as $item) {
                $classifyArray[] = $this->getDictionaryTranslator()->objectToArray($item);
            }
        }

        $data = array();
        $data['levels'] = $levelArray;
        $data['labels'] = $labelArray;
        $data['classifies'] = $classifyArray;
        $data['applicableObjects'] = $applicableObjectArray;
        $data['dispatchDepartments'] = $dispatchDepartmentArray;
        $data['applicableIndustries'] = $applicableIndustryArray;

        return $data;
    }

    // 政策列表信息
    public function policyList()
    {
        $policyList = array();
        $translator = $this->getPolicyTranslator();

        foreach ($this->getPolicyList() as $item) {
            $policyList[] = $translator->objectToArray(
                $item,
                array('id','title','image','level','labels'=>[],'description','updateTime')
            );
        }

        return $policyList;
    }
}
