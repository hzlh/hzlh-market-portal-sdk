<?php
namespace SuperMarket\ProductMarket\Portal\Policy\View;

use Sdk\ProductMarket\Policy\Model\Policy;

use SuperMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;

use SuperMarket\ProductMarket\Portal\Policy\Translator\PolicyTranslator;
use SuperMarket\ProductMarket\Portal\PolicyInterpretation\Translator\PolicyInterpretationTranslator;

trait ViewTrait
{
    private $data;

    private $policy;

    private $policyTranslator;

    private $serviceTranslator;

    private $policyInterpretationTranslator;

    public function __construct(Policy $policy, array $data)
    {
        parent::__construct();
        $this->policy = $policy;
        $this->data = $data;
        $this->policyTranslator = new PolicyTranslator();
        $this->serviceTranslator = new ServiceTranslator();
        $this->policyInterpretationTranslator = new PolicyInterpretationTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->policy);
        unset($this->policyTranslator);
        unset($this->serviceTranslator);
        unset($this->policyInterpretationTranslator);
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getPolicy() : Policy
    {
        return $this->policy;
    }

    public function getPolicyTranslator() : PolicyTranslator
    {
        return $this->policyTranslator;
    }

    public function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    public function getPolicyInterpretationTranslator() : PolicyInterpretationTranslator
    {
        return $this->policyInterpretationTranslator;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function data()
    {
        $data = $this->getData();

        $policies = !empty($data['policies']) ? $data['policies'][1] : array();
        $policyInterpretations = !empty($data['policyInterpretations']) ? $data['policyInterpretations'][1] : array();

        $policyTranslator = $this->getPolicyTranslator();
        $policyInterpretationTranslator = $this->getPolicyInterpretationTranslator();

        $policyData = $policyTranslator->objectToArray($this->getPolicy());

        // 获取相关政策列表信息
        $policyArray = array();
        if (!empty($policies)) {
            foreach ($policies as $key => $policy) {
                $policyArray[$key] = $policyTranslator->objectToArray(
                    $policy,
                    array('id','title','image','level','description','updateTime')
                );
                if ($policyArray[$key]['id'] == $policyData['id']) {
                    unset($policyArray[$key]);
                }
            }
        }

        // 获取政策相关解读列表信息
        $policyInterpretationArray = array();
        if (!empty($policyInterpretations)) {
            foreach ($policyInterpretations as $policyInterpretation) {
                $policyInterpretationArray[] = $policyInterpretationTranslator->objectToArray(
                    $policyInterpretation,
                    array('id', 'title', 'cover', 'level', 'description', 'updateTime')
                );
            }
        }

        $data = array(
            'policy' => $policyData,
            'policies' => $policyArray,
            'policyInterpretations' => $policyInterpretationArray
        );

        return $data;
    }
}
