<?php
namespace SuperMarket\ProductMarket\Portal\Policy\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Portal\Policy\View\ViewTrait;

class PolicyView extends TemplateView implements IView
{
    use ViewTrait;

    public function display()
    {
        $data = $this->data();

        $this->getView()->display(
            'PolicyHall/Policy/Policy.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['POLICY'],
                'nav_phone' => NavTrait::NAV_PHONE['PORTAL_POLICY'],
                'policy' => $data['policy'],
                'policies' => $data['policies'],
                'policyInterpretations' => $data['policyInterpretations'],
            ]
        );
    }
}
