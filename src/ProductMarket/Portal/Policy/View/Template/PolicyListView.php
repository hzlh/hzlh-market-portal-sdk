<?php
namespace SuperMarket\ProductMarket\Portal\Policy\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Portal\Policy\View\ListViewTrait;

class PolicyListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $searchParameters = $this->searchParameters();

        $policyList = $this->policyList();

        $this->getView()->display(
            'PolicyHall/Policy/PolicyList.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['POLICY'],
                'nav_phone' => NavTrait::NAV_PHONE['PORTAL_POLICY'],
                'searchParameters' => $searchParameters,
                'list' => $policyList,
                'count' => $this->getCount()
            ]
        );
    }
}
