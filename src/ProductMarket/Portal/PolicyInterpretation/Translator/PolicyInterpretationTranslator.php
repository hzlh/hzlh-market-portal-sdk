<?php
namespace SuperMarket\ProductMarket\Portal\PolicyInterpretation\Translator;

use Sdk\ProductMarket\PolicyInterpretation\Model\PolicyInterpretation;
use Sdk\ProductMarket\PolicyInterpretation\Model\NullPolicyInterpretation;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use SuperMarket\ProductMarket\Portal\Policy\Translator\PolicyTranslator;

class PolicyInterpretationTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $policyInterpretation = null)
    {
        unset($policyInterpretation);
        unset($expression);
        return new NullPolicyInterpretation();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getPolicyTranslator() : PolicyTranslator
    {
        return new PolicyTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($policyInterpretation, array $keys = array())
    {
        if (!$policyInterpretation instanceof PolicyInterpretation) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'cover',
                'description',
                'detail',
                'attachments',
                'createTime',
                'updateTime',
                'policy'=>[],
                'pageViews',
                'shareVolume',
             );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($policyInterpretation->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $policyInterpretation->getTitle();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $policyInterpretation->getCover();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = Filter::dhtmlspecialchars($policyInterpretation->getDescription());
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = Filter::dhtmlspecialchars($policyInterpretation->getDetail());
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $policyInterpretation->getAttachments();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $policyInterpretation->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $policyInterpretation->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $policyInterpretation->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $policyInterpretation->getUpdateTime());
        }
        if (isset($keys['policy'])) {
            $expression['policy'] = $this->getPolicyTranslator()->objectToArray(
                $policyInterpretation->getPolicy(),
                $keys['policy']
            );
        }
        if (in_array('pageViews', $keys)) {
            $expression['pageViews'] = $policyInterpretation->getPageViews();
        }
        if (in_array('shareVolume', $keys)) {
            $expression['shareVolume'] = $policyInterpretation->getShareVolume();
        }

        return $expression;
    }
}
