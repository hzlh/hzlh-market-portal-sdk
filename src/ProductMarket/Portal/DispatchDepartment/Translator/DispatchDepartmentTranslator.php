<?php
namespace SuperMarket\ProductMarket\Portal\DispatchDepartment\Translator;

use Sdk\ProductMarket\DispatchDepartment\Model\DispatchDepartment;
use Sdk\ProductMarket\DispatchDepartment\Model\NullDispatchDepartment;

use Marmot\Interfaces\ITranslator;

class DispatchDepartmentTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $dispatchDepartment = null)
    {
        unset($dispatchDepartment);
        unset($expression);
        return NullDispatchDepartment::getInstance();
    }

    public function objectToArray($dispatchDepartment, array $keys = array())
    {
        if (!$dispatchDepartment instanceof DispatchDepartment) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($dispatchDepartment->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $dispatchDepartment->getName();
        }

        return $expression;
    }
}
