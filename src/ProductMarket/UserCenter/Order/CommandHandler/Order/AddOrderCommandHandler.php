<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\AddOrderCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Qxy\Contract\Common\Model\Account;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

use Qxy\Contract\Contract\Model\Part;
use Qxy\Contract\Contract\Model\Order;
use Qxy\Contract\Template\Model\Template;
use Qxy\Contract\Performance\Model\Performance;

use Sdk\Enterprise\Model\Enterprise;

class AddOrderCommandHandler implements ICommandHandler, ILogAble
{
    use OrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof AddOrderCommand)) {
            throw new \InvalidArgumentException;
        }
        // 获取收货地址信息
        $orderAddressArray = $command->address;

        // 获取收货地址快照信息
        $orderAddress = $this->getOrderAddress();
        $orderAddress->setCellphone($orderAddressArray['cellphone']);

        // 该服务无需地址时可不填写
        if (!empty($orderAddressArray['snapshotId'])) {
            // 获取地址快照信息
            
            $addressSnapshot = $this->fetchSnapshot(marmot_decode($orderAddressArray['snapshotId']));
            
            $orderAddress->setSnapshot($addressSnapshot);
        }

        // 获取商品信息
        $commodityArray = $command->commodity;

        $orderCommodity = $this->getOrderCommodity();
   
        foreach ($commodityArray as $item) {
            // 获取商品快照信息
            $commoditySnapshot = $this->fetchSnapshot(marmot_decode($item['snapshotId']));
            $orderCommodity->setNumber($item['number']);
            $orderCommodity->setSkuIndex($item['sKuIndex']);
            $orderCommodity->setSnapshot($commoditySnapshot);
        }

        //服务快照信息
        $snapshotObject = $commoditySnapshot->getSnapshotObject();
        // 获取优惠劵信息
        $couponIds = array();
        foreach ($command->couponIds as $coupon) {
            $couponIds[] = marmot_decode($coupon);
        }

        list($count, $couponList) = $this->fetchCoupon($couponIds);
        unset($count);

        // 获取用户账户信息
        $memberAccount = $this->fetchMemberAccount($command->memberAccountId);
      
        $order = $this->getServiceOrder();
       
        $order->setRemark($command->remark);

        $order->setOrderAddress($orderAddress);
        $order->addOrderCommodity($orderCommodity);

        $order->setBuyerMemberAccount($memberAccount);
        foreach ($couponList as $coupon) {
            $order->addMemberCoupon($coupon);
        }

        $realNameAuthenticationStatus = Core::$container->get('user')->getEnterpriseStatus();

        if ($order->placeOrder()) {
            if ($realNameAuthenticationStatus == Enterprise::AUTHENTICATION_STATUS['APPROVE']) {
                $this->addContract($snapshotObject, $command, $order);
            }
            
            $command->id = $order->getId();
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    protected function addContract($snapshotObject, $command, $order)
    {
        $templateItem = $this->filterTemplateItem($snapshotObject->getId());

        if (!empty($templateItem)) {
            $templateItem = array_shift($templateItem);

            $contract = $this->getContract();
            $contract->setAccountType(Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET']);
            $contract->setAccountId(Account::ACCOUNT_ID['HZLH']);
            $contract->setPartA(new Part($snapshotObject->getEnterprise()->getId()));
            $contract->setPartB(new Part($command->memberAccountId));
            $contract->setOrder(new Order($order->getId()));
            $contract->setOrderNumber($order->getOrderno());
            $contract->setTemplate(new Template($templateItem->getTemplate()->getId()));
            $contract->setTitle($templateItem->getTemplate()->getTitle());
            $contract->setContent($templateItem->getTemplate()->getContent());

            $performanceItems = $templateItem->getTemplate()->getPerformance();

            foreach ($performanceItems as $value) {
                $performance = new Performance(0);
                $performance->setCategory($performance::CATEGORY['CONTRATE']);
                $performance->setAccountType(1);
                // $performance->setAccountId(0);
                $performance->setNumber($value->getNumber());
                $performance->setPerformingParty($value->getPerformingParty());
                $performance->setTriggerConditionType($value->getTriggerConditionType());
                $performance->setTriggerCondition($value->getTriggerCondition());
                $performance->setTerm($value->getTerm());
                $performance->setContent($value->getContent());

                $contract->addPerformance($performance);
            }

            $contract->add();
        }
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ORDER'],
            ILogAble::CATEGORY['ORDER'],
            $this->getServiceOrder()->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->getServiceOrder()->getOrderno()
        );
    }
}
