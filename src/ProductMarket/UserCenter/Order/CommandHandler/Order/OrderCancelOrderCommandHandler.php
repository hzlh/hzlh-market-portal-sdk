<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\OrderCancelOrderCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class OrderCancelOrderCommandHandler implements ICommandHandler, ILogAble
{
    use OrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof OrderCancelOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceOrder = $this->fetchOrder($command->id);
        $this->serviceOrder->setCancelReason($command->cancelReason);

        if ($this->serviceOrder->buyerCancel()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_CANCEL'],
            ILogAble::CATEGORY['ORDER'],
            $this->serviceOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->serviceOrder->getOrderno()
        );
    }
}
