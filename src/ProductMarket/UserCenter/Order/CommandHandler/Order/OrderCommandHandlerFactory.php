<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class OrderCommandHandlerFactory implements ICommandHandlerFactory
{
    /**
     * 提交订单
     * 确认订单
     * 删除订单
     * 永久删除订单
     * 订单取消
     */
    const MAPS = array(
        'SuperMarket\ProductMarket\UserCenter\Order\Command\Order\AddOrderCommand'=>
        'SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\AddOrderCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\Order\Command\Order\ConfirmationOrderCommand'=>
        'SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\ConfirmationOrderCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\Order\Command\Order\OrderCancelOrderCommand'=>
        'SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\OrderCancelOrderCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\Order\Command\Order\DeleteOrderCommand'=>
        'SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\DeleteOrderCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\Order\Command\Order\PermanentDeleteOrderCommand'=>
        'SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\PermanentDeleteOrderCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\Order\Command\Order\UpdateAddressOrderCommand'=>
        'SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\UpdateAddressOrderCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
