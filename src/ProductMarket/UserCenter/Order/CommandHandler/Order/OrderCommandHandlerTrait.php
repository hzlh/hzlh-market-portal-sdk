<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Sdk\Coupon\Repository\CouponRepository;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;
use Sdk\ProductMarket\MemberAccount\Repository\MemberAccountRepository;
use Sdk\DeliveryAddress\Repository\DeliveryAddressRepository;

use Sdk\ProductMarket\Order\CommonOrder\Model\OrderAddress;
use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;
use Sdk\ProductMarket\Order\CommonOrder\Model\OrderCommodity;
use Sdk\Snapshot\Repository\SnapshotRepository;
use Sdk\Snapshot\Model\Snapshot;
use Qxy\Contract\Contract\Model\Contract;
use Qxy\Contract\Template\Model\TemplateItem;
use Qxy\Contract\Template\Repository\TemplateItemRepository;
use Qxy\Contract\Common\Model\Account;

trait OrderCommandHandlerTrait
{
    private $orderAddress;

    private $serviceOrder;

    private $orderCommodity;

    private $contract;

    private $templateItemRepository;

    private $orderRepository;

    private $memberAccountRepository;

    private $couponRepository;

    private $serviceRepository;

    private $deliveryAddressRepository;

    private $snapshotRepository;

    public function __construct()
    {
        $this->orderAddress = new OrderAddress();
        $this->serviceOrder = new ServiceOrder();
        $this->orderCommodity = new OrderCommodity();
        $this->contract = new Contract();
        $this->templateItemRepository = new TemplateItemRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->orderRepository = new ServiceOrderRepository();
        $this->deliveryAddressRepository = new DeliveryAddressRepository();
        $this->couponRepository = new CouponRepository();
        $this->memberAccountRepository = new MemberAccountRepository();
        $this->snapshotRepository = new SnapshotRepository();
    }

    public function __destruct()
    {
        unset($this->serviceOrder);
        unset($this->serviceRepository);
        unset($this->orderRepository);
        unset($this->deliveryAddressRepository);
        unset($this->couponRepository);
        unset($this->memberAccountRepository);
        unset($this->snapshotRepository);
    }
    protected function getSnapshot() : Snapshot
    {
        return new Snapshot();
    }

    protected function getOrderAddress() : OrderAddress
    {
        return $this->orderAddress;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getOrderCommodity() : OrderCommodity
    {
        return $this->orderCommodity;
    }

    protected function getContract()
    {
        return new Contract();
    }

    protected function getTemplateItemRepository()
    {
        return new TemplateItemRepository();
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function getSnapshotRepository() : SnapshotRepository
    {
        return $this->snapshotRepository;
    }

    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return $this->memberAccountRepository;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->orderRepository;
    }

    protected function getCouponRepository() : CouponRepository
    {
        return $this->couponRepository;
    }

    protected function fetchOrder(int $id) : ServiceOrder
    {
        return $this->getServiceOrderRepository()->fetchOne($id);
    }

    protected function fetchMemberAccount($id)
    {
        return $this->getMemberAccountRepository()
            ->scenario(MemberAccountRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    protected function fetchCoupon($ids)
    {
        return $this->getCouponRepository()
            ->scenario(CouponRepository::PORTAL_LIST_MODEL_UN)->fetchList($ids);
    }

    protected function fetchSnapshot($id)
    {
        return $this->getSnapshotRepository()
            ->scenario(SnapshotRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    protected function filterTemplateItem(int $serviceId) : array
    {
        $sort = ['-updateTime'];
        $filter['itemId'] = $serviceId;
        $filter['status'] = TemplateItem::STATUS['BIND'];
        $filter['accountType'] = Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET'];

        list($count, $list) = $this->getTemplateItemRepository()
            ->scenario(TemplateItemRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $list;
    }
}
