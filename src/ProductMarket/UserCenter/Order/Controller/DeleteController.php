<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\DeleteControllerTrait;
use Common\Controller\Interfaces\IDeleteAbleController;

use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\DeleteOrderCommand;
use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\PermanentDeleteOrderCommand;
use SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\OrderCommandHandlerFactory;

class DeleteController extends Controller implements IDeleteAbleController
{
    use WebTrait, DeleteControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new OrderCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function deleteAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new DeleteOrderCommand($id));
    }

    /**
     * @return bool
     * @param [POST]
     * @method /orders/{id}/permanentDelete
     * 永久删除订单
     */
    public function permanentDelete($id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = marmot_decode($id);

        if ($this->getCommandBus()->send(new PermanentDeleteOrderCommand($id))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
