<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Controller;

use Utils\Utils\Traits\CryptojsTrait;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Interfaces\IOperatAbleController;

use SuperMarket\ProductMarket\UserCenter\Order\View\Json\JsonOrderView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Json\JsonAddressView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Template\AddOrderView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Template\AddressView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Json\JsonCouponsView;

use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\AddOrderCommand;
use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\UpdateAddressOrderCommand;
use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\ConfirmationOrderCommand;
use SuperMarket\ProductMarket\UserCenter\Order\Command\Order\OrderCancelOrderCommand;
use SuperMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\OrderCommandHandlerFactory;

use Sdk\Coupon\Model\Coupon;
use Sdk\Coupon\Repository\CouponRepository;
use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Model\NullServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;

use Sdk\DeliveryAddress\Model\DeliveryAddress;
use Sdk\DeliveryAddress\Repository\DeliveryAddressRepository;

use Qxy\Contract\Template\Model\TemplateItem;
use Qxy\Contract\Template\Repository\TemplateItemRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, GlobalCheckTrait, CryptojsTrait, OperationValidateTrait, OrderTrait;

    const PROBABILITY = 100;

    const UPDATE_ORDER_AMOUNT_LOWER_LIMIT = 0.01;

    const MAX_COUPON_AMOUNT = 200;

    private $commandBus;

    private $deliveryAddressRepository;

    private $couponRepository;

    private $serviceRepository;

    private $concurrentAdapter;

    private $serviceOrderRepository;

    private $templateItemRepository;

    public function __construct()
    {
        parent::__construct();
        $this->couponRepository = new CouponRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
        $this->serviceOrderRepository = new ServiceOrderRepository();
        $this->deliveryAddressRepository = new DeliveryAddressRepository();
        $this->templateItemRepository = new TemplateItemRepository();
        $this->commandBus = new CommandBus(new OrderCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->couponRepository);
        unset($this->concurrentAdapter);
        unset($this->serviceRepository);
        unset($this->serviceOrderRepository);
        unset($this->deliveryAddressRepository);
        unset($this->templateItemRepository);
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getCouponRepository() : CouponRepository
    {
        return $this->couponRepository;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->serviceOrderRepository;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function getAddressRepository() : DeliveryAddressRepository
    {
        return $this->deliveryAddressRepository;
    }

    protected function getTemplateItemRepository() : TemplateItemRepository
    {
        return $this->templateItemRepository;
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }
    /**
     * @return bool
     * @param [GET, POST]
     * @method /orders/add
     *
     * 提交订单
     */
    protected function addView() : bool
    {
        $sort = ['-updateTime'];
        $serviceData = $this->getServiceRequest();

        if (empty($serviceData['serviceId']) || empty($serviceData['number']) || !isset($serviceData['sKuIndex'])) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $serviceData['serviceId'] = marmot_decode($serviceData['serviceId']);

        $memberId = Core::$container->get('user')->getId();
        $merchantFilter['member'] = $memberId;
        $merchantFilter['status'] = Coupon::STATUS['NORMAL'];   //默认正常使用
        $merchantFilter['releaseType'] = MerchantCoupon::RELEASE_TYPE['MERCHANT'];  //商家优惠劵

        $platformFilter['member'] = $memberId;
        $platformFilter['status'] = Coupon::STATUS['NORMAL'];   //默认正常使用
        $platformFilter['releaseType'] = MerchantCoupon::RELEASE_TYPE['PLATFORM'];  //平台优惠劵

        $templateFilter['itemId'] = $serviceData['serviceId'];
        $templateFilter['status'] = TemplateItem::STATUS['BIND'];

        $couponRepository = $this->getCouponRepository();
        $serviceRepository = $this->getServiceRepository();
        $templateItemRepository = $this->getTemplateItemRepository();

        $this->getConcurrentAdapter()->addPromise(
            'platformCoupons',
            $couponRepository->scenario(CouponRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($platformFilter, $sort, PAGE, COMMON_SIZE),
            $couponRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'merchantCoupons',
            $couponRepository->scenario(CouponRepository::PORTAL_LIST_MODEL_UN)
                ->searchAsync($merchantFilter, $sort, PAGE, COMMON_SIZE),
            $couponRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'service',
            $serviceRepository->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
                ->fetchOneAsync($serviceData['serviceId']),
            $serviceRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'templateItem',
            $templateItemRepository->scenario(TemplateItemRepository::LIST_MODEL_UN)
                ->searchAsync($templateFilter, $sort, PAGE, COMMON_SIZE),
            $templateItemRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();
        $data['number'] = $serviceData['number'];
        $data['sKuIndex'] = $serviceData['sKuIndex'];

        if (empty($data['service'])) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        //获取企业实名认证状态
        $data['realNameAuthenticationStatus'] = Core::$container->get('user')->getEnterpriseStatus();

        $this->render(new AddOrderView($data, $serviceData));
        return true;
    }

    protected function getServiceRequest()
    {
        $request = $this->getRequest();
        $requestData = array();
        $requestData['serviceId'] = $request->get('serviceId', '');
        $requestData['number'] = $request->get('number', 0);
        $requestData['sKuIndex'] = $request->get('sKuIndex', '');

        return $requestData;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /orderAddress
     *
     * 订单页面获取用户地址
     */
    public function orderAddress()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        list($filter, $sort) = $this->filterAddressFormatChange();

        list($count, $addressList) = $this->getAddressRepository()
            ->scenario(DeliveryAddressRepository::LIST_MODEL_UN)
            ->search($filter, $sort);
        unset($count);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonAddressView($addressList));
            return true;
        }

        $this->render(new AddressView($addressList));
        return true;
    }

    protected function filterAddressFormatChange()
    {
        $sort = ['-updateTime'];
        $filter = array();

        $filter['member'] = Core::$container->get('user')->getId();
        $filter['status'] = DeliveryAddress::STATUS['NORMAL'];

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /add
     *
     * 提交订单
     */
    protected function addAction()
    {
        $request = $this->getRequest();
        $couponIds = $request->post('couponIds', array());
        $address = $request->post('address', array());
        $commodity = $request->post('commodity', array());
        $remark = $request->post('remark', '');
        $memberAccount = Core::$container->get('user')->getId();

        if ($this->validateOperationScenario(
            $commodity,
            $address
        )) {
            $command = new AddOrderCommand(
                $remark,
                $couponIds,
                $commodity,
                $address,
                $memberAccount
            );

            if ($this->getCommandBus()->send($command)) {
                $order = $this->fetchOrder($command->id);
                
                if ($order instanceof INull) {
                    Core::setLastError(RESOURCE_NOT_EXIST);
                    return false;
                }

                $this->render(new JsonOrderView($order));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function fetchOrder($id)
    {
        return $this->getServiceOrderRepository()
            ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
    }

    /**
     * @return bool
     * @param [POST]
     * @method /orders/id/updateAddress
     * 修改订单收货地址
     */
    public function updateAddress($id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = marmot_decode($id);

        $request = $this->getRequest();
        $cellphone = $request->post('cellphone', '');
        $snapshotId = $request->post('snapshotId', '');

        if ($this->validateCellphone($cellphone)) {
            $command = new UpdateAddressOrderCommand($cellphone, $snapshotId, $id);
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * @return bool
     * @param [POST]
     * @method /orders/id/orderCancel
     * 取消订单
     */
    public function orderCancel($id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = marmot_decode($id);

        $request = $this->getRequest();
        $cancelReason = $request->post('unbindReason', '');

        $cancelReason = marmot_decode($cancelReason);

        $command = new OrderCancelOrderCommand($cancelReason, $id);
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    /**
     * @return bool
     * @param [POST]
     * @method /orders/id/confirmation
     * 确认订单
     */
    public function confirmation($id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = marmot_decode($id);

        $command = new ConfirmationOrderCommand($id);
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    /**
     * @return bool
     * @param [POST]
     * @method /orders/couponPrice
     *
     * 获取优惠劵价格
     */
    public function couponPrice()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }
        $request = $this->getRequest();
        $commodity = $request->post('commodity', array());
        $coupons = $request->post('coupons', array());

        if (empty($commodity['number']) || empty($commodity['price']) || empty($coupons)) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT);
            $this->displayError();
            return false;
        }

        $totalPrice = $commodity['number'] * $commodity['price'];
        $resultPrise = $this->calculateAmount($totalPrice, $coupons);
        $this->render(new JsonCouponsView($resultPrise));
        return true;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        return false;
    }

    protected function editAction(int $id) : bool
    {
        unset($id);
        return false;
    }
}
