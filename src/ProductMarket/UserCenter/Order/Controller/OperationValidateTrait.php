<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\User\WidgetRules as UserWidgetRules;
use WidgetRules\Order\WidgetRules as OrderWidgetRules;

trait OperationValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getUserWidgetRules() : UserWidgetRules
    {
        return UserWidgetRules::getInstance();
    }

    protected function getOrderWidgetRules() : OrderWidgetRules
    {
        return OrderWidgetRules::getInstance();
    }

    protected function validateCellphone(
        $cellphone
    ) {
        return $this->getUserWidgetRules()->cellphone($cellphone);
    }

    protected function validateOperationScenario(
        $commodity,
        $orderAddress
    ) {
        return $this->getOrderWidgetRules()->orderCommodityList($commodity)
            && $this->getOrderWidgetRules()->orderAddress($orderAddress);
    }
}
