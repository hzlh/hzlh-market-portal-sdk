<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Coupon\Repository\CouponRepository;
use Sdk\MerchantCoupon\Model\MerchantCoupon;

trait OrderTrait
{
    protected function getCouponRepository() : CouponRepository
    {
        return new CouponRepository();
    }

    /**
     *
     */
    protected function getCouponInfo($couponId)
    {
        $coupon = $this->getCouponRepository()
            ->scenario(CouponRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne(marmot_decode($couponId));

        if ($coupon instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        return $coupon;
    }

    /**
     *
     */
    protected function calculateAmount($totalPrice, $coupons)
    {
        $businessPreferential = $this->businessPreferential($totalPrice, $coupons);
        $platformPreferential = $this->platformPreferential($totalPrice, $coupons);

        $paidAmount = $totalPrice-$businessPreferential-$platformPreferential;
        $paidAmount = $paidAmount <= 0 ? self::UPDATE_ORDER_AMOUNT_LOWER_LIMIT : $paidAmount;

        return $paidAmount;
    }

    /**
     *
     */
    protected function businessPreferential($totalPrice, $coupons)
    {
        $businessPreferential = 0;

        foreach ($coupons as $couponId) {
            $memberCoupon = $this->getCouponInfo($couponId);
            $useStandard = $memberCoupon->getMerchantCoupon()->getUseStandard();

            if ($memberCoupon->getReleaseType() == MerchantCoupon::RELEASE_TYPE['MERCHANT']) {
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']) {//phpcs:ignore
                    $businessPreferential = $totalPrice >= $useStandard ? $memberCoupon->getMerchantCoupon()->getDenomination() : 0;//phpcs:ignore
                }
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['DISCOUNT']) {
                    $discount = $memberCoupon->getMerchantCoupon()->getDiscount();
                    $businessPreferential = $totalPrice >= $useStandard ? $totalPrice*((self::PROBABILITY-$discount)/100) : 0;//phpcs:ignore
                }
            }
        }

        return $businessPreferential;
    }

    /**
     *
     */
    protected function platformPreferential($totalPrice, $coupons)
    {
        $platformPreferential = 0;

        foreach ($coupons as $couponId) {
            $memberCoupon = $this->getCouponInfo($couponId);
            $useStandard = $memberCoupon->getMerchantCoupon()->getUseStandard();

            if ($memberCoupon->getReleaseType() == MerchantCoupon::RELEASE_TYPE['PLATFORM']) {
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']) {//phpcs:ignore
                    $platformPreferential = $totalPrice >= $useStandard ? $memberCoupon->getMerchantCoupon()->getDenomination() : 0;//phpcs:ignore
                }
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['DISCOUNT']) {
                    $discount = $memberCoupon->getMerchantCoupon()->getDiscount();
                    $platformPreferential = $totalPrice >= $useStandard ? $totalPrice*((self::PROBABILITY-$discount)/100) : 0;//phpcs:ignore
                    $platformPreferential = $platformPreferential > self::MAX_COUPON_AMOUNT ? self::MAX_COUPON_AMOUNT : $platformPreferential;//phpcs:ignore
                }
            }
        }

        return $platformPreferential;
    }
}
