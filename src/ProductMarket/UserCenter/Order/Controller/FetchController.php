<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\ProductMarket\UserCenter\Order\View\Template\ListView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Json\JsonListView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Template\DetailView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Template\SnapshotView;
use SuperMarket\ProductMarket\UserCenter\Order\View\Template\ContractView;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;
use Qxy\Contract\Common\Model\Account;

use Sdk\ProductMarket\Contract\Repository\ContractRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait;

    private $repository;

    private $contractRepository;

    const SCENE = array(
        'ALL' => 1, //全部  MA
        'PENDING_PAY' => 2, //代付款  MQ
        'PENDING_TRANSACTION' => 3, //待交易  Mg
        'PENDING_FINISH' => 4, //待完成  Mw
        'PENDING_EVALUATE' => 5, //待评价  NA
        'DELETED' => 6, //回收站 NQ
    );

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
        $this->contractRepository = new ContractRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->contractRepository);
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getContractRepository() : ContractRepository
    {
        return $this->contractRepository;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /orders
     * 买家订单列表
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        list($page, $size) = $this->getPageAndSize(FORM_SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        $orderList = array();
        list($count, $orderList) = $this->getServiceOrderRepository()
            ->scenario(ServiceOrderRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        $contract = [];
        //@TODO 屏蔽合同
        $contract = $this->filterContract($orderList);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($count, $orderList, $contract));
            return true;
        }

        $this->render(new ListView($count, $orderList, $contract));
        return true;
    }

    protected function filterContract(array $orderList) : array
    {
        $sort = ['-updateTime'];

        $orderIds = [];
        foreach ($orderList as $order) {
            $orderIds[] = $order->getId();
        }

        $filter['order'] = join(',', $orderIds);
        $filter['accountType'] = Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET'];

        list($count, $list) = $this->getContractRepository()->scenario(ContractRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $list;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {
        $scene = $this->getRequest()->get('scene', '');
        if (empty($scene)) {
            $scene = 'MA';
        }
        $scene = marmot_decode($scene);

        $serviceName = $this->getRequest()->get('serviceName', '');
        $sellerEnterprise = $this->getRequest()->get('sellerEnterprise', '');
        $orderno = $this->getRequest()->get('orderno', '');
        // 成交时间
        $dealStartTime = $this->getRequest()->get('dealStartTime', '');
        $dealEndTime = $this->getRequest()->get('dealEndTime', '');

        $sort = ['-updateTime'];
        $filter = array();
        $filter['buyerMemberAccount'] = Core::$container->get('user')->getId();
        $filter['buyerOrderStatusPortal'] = ServiceOrder::BUYER_ORDER_STATUS['NORMAL'];

        if (!empty($serviceName)) {
            $filter['serviceName'] = $serviceName;
        }

        if (!empty($sellerEnterprise)) {
            $filter['sellerEnterpriseName'] = $sellerEnterprise;
        }

        if (!empty($orderno)) {
            $filter['orderno'] = $orderno;
        }

        if (!empty($dealStartTime)) {
            $filter['dealStartTime'] = $dealStartTime;
        }

        if (!empty($dealEndTime)) {
            $filter['dealEndTime'] = $dealEndTime;
        }

        // * PENDING  待付款 0  代付款
        // * PAID   已付款   2  待交易
        // * PERFORMANCE_END 履约结束  6  待完成
        // * BUYER_CONFIRMATION 买家确认  8  待评价

        if ($scene == self::SCENE['PENDING_PAY']) {
            $filter['status'] = ServiceOrder::STATUS['PENDING'];
        }

        if ($scene == self::SCENE['PENDING_TRANSACTION']) {
            $filter['status'] = ServiceOrder::STATUS['PAID'];
        }

        if ($scene == self::SCENE['PENDING_FINISH']) {
            $filter['status'] = ServiceOrder::STATUS['PERFORMANCE_BEGIN'].','.ServiceOrder::STATUS['PERFORMANCE_END'];
        }

        if ($scene == self::SCENE['PENDING_EVALUATE']) {
            $status = [ServiceOrder::STATUS['BUYER_CONFIRMATION'],ServiceOrder::STATUS['PLATFORM_CONFIRMATION']];
            $filter['status'] = implode(',', $status);
        }

        if ($scene == self::SCENE['DELETED']) {
            $filter['buyerOrderStatusPortal'] = ServiceOrder::BUYER_ORDER_STATUS['DELETE'];
        }

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /orders/snapshot
     *
     * 获取交易快照信息
     */
    public function snapshot($id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $data = $this->getServiceOrderRepository()
            ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne(marmot_decode($id));

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $this->render(new SnapshotView($data));
        return true;
    }
    /**
     * @return bool
     * @param [GET]
     * @method /orders/confirmContract
     *
     * 确认合同页面
     */
    public function confirmContract()
    {
        $sort = ['-updateTime'];
        $filter['order'] = marmot_decode($this->getRequest()->get('orderId', ''));
        $filter['accountType'] = Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET'];

        list($count, $list) = $this->getContractRepository()->scenario(ContractRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        if (empty($list)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }
        $contract = array_shift($list);

        $this->render(new ContractView($contract));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /orders/MA
     * 买家订单详情
     */
    protected function fetchOneAction($id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $data = $this->getServiceOrderRepository()
            ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DetailView($data));
        return true;
    }
}
