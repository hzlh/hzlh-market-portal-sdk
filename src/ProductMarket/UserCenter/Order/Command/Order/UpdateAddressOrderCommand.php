<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Command\Order;

use Marmot\Interfaces\ICommand;

class UpdateAddressOrderCommand implements ICommand
{
    public $cellphone;

    public $snapshotId;

    public $id;

    public function __construct(
        string $cellphone,
        string $snapshotId,
        int $id = 0
    ) {
        $this->cellphone = $cellphone;
        $this->snapshotId = $snapshotId;
        $this->id = $id;
    }
}
