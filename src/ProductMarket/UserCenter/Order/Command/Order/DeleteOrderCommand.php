<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Command\Order;

use Marmot\Interfaces\ICommand;

class DeleteOrderCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
