<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Command\Order;

use Marmot\Interfaces\ICommand;

class PermanentDeleteOrderCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
