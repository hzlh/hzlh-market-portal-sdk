<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

class JsonCouponsView extends JsonView implements IView
{
    private $resultPrise;

    public function __construct($resultPrise)
    {
        parent::__construct();
        $this->resultPrise = $resultPrise;
    }

    protected function getResultPrise()
    {
        return $this->resultPrise;
    }

    public function display() : void
    {
        $data = array(
            'data' => $this->getResultPrise(),
        );

        $this->encode($data);
    }
}
