<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\UserCenter\Order\View\ListViewTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait;

    public function display() : void
    {
        $list = $this->getList();
        $contract = $this->getContractList();

        foreach ($list as $key => $value) {
            foreach ($contract as $item) {
                if ($value['id'] == $item['order']['id']) {
                    $list[$key]['contract']['id'] = $item['id'];
                    $list[$key]['contract']['status'] = $item['status'];
                    $list[$key]['contract']['statusFormat'] = $item['statusFormat'];
                }
            }
        }

         $data = array(
             'list' => $list,
             'total' => $this->getTotal()
         );

         $this->encode($data);
    }
}
