<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\Order\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getList();
        $contractList = $this->getContractList();

        foreach ($list as $key => $value) {
            foreach ($contractList as $item) {
                if ($value['id'] == $item['order']['id']) {
                    $list[$key]['contract']['id'] = $item['id'];
                    $list[$key]['contract']['status'] = $item['status'];
                    $list[$key]['contract']['statusFormat'] = $item['statusFormat'];
                }
            }
        }

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/List.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['PRODUCT_MARKET_ORDER'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_ORDER'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
