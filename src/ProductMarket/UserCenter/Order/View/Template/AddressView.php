<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\Order\View\AddressViewTrait;

class AddressView extends TemplateView implements IView
{
    use AddressViewTrait;

    public function display()
    {
        $data = $this->getAddressList();

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/Address.tpl',
            [
                'list' => $data
            ]
        );
    }
}
