<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

class SnapshotView extends TemplateView implements IView
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->translator = new OrderTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->translator);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getTranslator() : OrderTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getData()
        );

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/Snapshot.tpl',
            [
                'data' => $data
            ]
        );
    }
}
