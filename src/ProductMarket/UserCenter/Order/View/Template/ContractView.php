<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Qxy\Contract\Contract\Model\Contract;

use SuperMarket\ProductMarket\UserCenter\Contract\Translator\ContractTranslator;

use Qxy\Contract\Template\Model\TemplateFactory;

class ContractView extends TemplateView implements IView
{
    private $contract;

    private $contractTranslator;

    public function __construct(Contract $contract)
    {
        parent::__construct();
        $this->contract = $contract;
        $this->contractTranslator = new ContractTranslator();
    }

    public function __destruct()
    {
        unset($this->contract);
        unset($this->contractTranslator);
    }

    public function getContract()
    {
        return $this->contract;
    }

    public function getContractTranslator() : ContractTranslator
    {
        return $this->contractTranslator;
    }

    public function getContractLsit()
    {
        $data = $this->getContractTranslator()->objectToArray(
            $this->getContract(),
            array(
                'id',
                'number',
                'orderNumber',
                'title',
                'status',
                'updateTime',
                'createTime',
                'content',
                'partA' => ['id', 'name', 'unifiedSocialCreditCode'],
                'partB' => ['id', 'name', 'unifiedSocialCreditCode'],
                'order' => [
                    'id',
                    'number',
                    'orderno',
                    'paymentId',
                    'totalPrice',
                    'status',
                    'createTime',
                    'orderCommodities'=>[]
                ],
                'performanceItems'=>[]
            )
        );

        return $data;
    }

    public function display()
    {
        $contract = $this->getContractLsit();

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/ConfirmContract.tpl',
            [
                'contract' => $contract
            ]
        );
    }
}
