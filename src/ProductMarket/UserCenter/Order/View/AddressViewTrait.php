<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\View;

use UserCenter\DeliveryAddress\Translator\DeliveryAddressTranslator;

trait AddressViewTrait
{
    private $deliveryAddressList;

    private $translator;

    public function __construct(array $deliveryAddress)
    {
        parent::__construct();
        $this->deliveryAddressList = $deliveryAddress;
        $this->translator = new DeliveryAddressTranslator();
    }

    public function __destruct()
    {
        unset($this->deliveryAddressList);
        unset($this->translator);
    }

    public function getDeliveryAddressList() : array
    {
        return $this->deliveryAddressList;
    }

    public function getTranslator() : DeliveryAddressTranslator
    {
        return $this->translator;
    }

    public function getAddressList()
    {
        $translator = $this->getTranslator();

        $addressList = array();
        if (!empty($this->getDeliveryAddressList())) {
            foreach ($this->getDeliveryAddressList() as $deliveryAddress) {
                $addressList[] = $translator->objectToArray(
                    $deliveryAddress,
                    array('id','area','address','realName','cellphone','postalCode','isDefaultAddress','snapshots'=>[])
                );
            }

            //排序，将isDefaultAddress为2的排在第一
            array_multisort(
                array_column($addressList, 'isDefaultAddress'),
                SORT_DESC,
                $addressList
            );
        }

        return $addressList;
    }
}
