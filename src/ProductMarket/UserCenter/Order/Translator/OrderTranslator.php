<?php
namespace SuperMarket\ProductMarket\UserCenter\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Model\NullServiceOrder;

use UserCenter\Enterprise\Translator\EnterpriseTranslator;
use SuperMarket\ProductMarket\Wallet\MemberAccount\Translator\MemberAccountTranslator;

use SuperMarket\ProductMarket\Order\Translator\OrderAddressTranslator;
use SuperMarket\ProductMarket\Order\Translator\OrderCommoditiesTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class OrderTranslator implements ITranslator
{
    protected function getMemberAccountTranslator() : MemberAccountTranslator
    {
        return new MemberAccountTranslator();
    }

    protected function getOrderAddressTranslator() : OrderAddressTranslator
    {
        return new OrderAddressTranslator();
    }

    protected function getOrderCommoditiesTranslator() : OrderCommoditiesTranslator
    {
        return new OrderCommoditiesTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    public function arrayToObject(array $expression, $merchantOrder = null)
    {
        unset($merchantOrder);
        unset($expression);
        return NullServiceOrder::getInstance();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceOrder, array $keys = array())
    {
        $orderExpression = array();

        if (!$serviceOrder instanceof ServiceOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'orderno',
                'paymentId',
                'totalPrice',
                'paidAmount',
                'collectedAmount',
                'platformPreferentialAmount',
                'businessPreferentialAmount',
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'paymentTime',
                'status',
                'remark',
                'cancelReason',
                'timeRecord',
                'buyerOrderStatus',
                'sellerOrderStatus',
                'createTime',
                'updateTime',
                'statusTime',
                'sellerEnterprise'=>[],
                'buyerMemberAccount' => [],
                'orderAddress' => [],
                'orderCommodities' => []
            );
        }

        if (in_array('id', $keys)) {
            $orderExpression['id'] = marmot_encode($serviceOrder->getId());
        }

        if (in_array('orderno', $keys)) {
            $orderExpression['orderno'] = $serviceOrder->getOrderno();
        }

        if (in_array('paymentId', $keys)) {
            $orderExpression['paymentId'] = marmot_encode($serviceOrder->getPaymentId());
        }

        if (in_array('timeRecord', $keys)) {
            $orderExpression['timeRecord'] = $serviceOrder->getTimeRecord();
        }

        if (in_array('totalPrice', $keys)) {
            $orderExpression['totalPrice'] =  $serviceOrder->getTotalPrice();
            $orderExpression['totalPriceFormat'] =  sprintf("%.2f", $serviceOrder->getTotalPrice());
        }

        if (in_array('paidAmount', $keys)) {
            $orderExpression['paidAmount'] = $serviceOrder->getPaidAmount();
        }

        if (in_array('collectedAmount', $keys)) {
            $orderExpression['collectedAmount'] = $serviceOrder->getCollectedAmount();
        }

        if (in_array('platformPreferentialAmount', $keys)) {
            $orderExpression['platformPreferentialAmount'] = $serviceOrder->getPlatformPreferentialAmount();
        }

        if (in_array('businessPreferentialAmount', $keys)) {
            $orderExpression['businessPreferentialAmount'] = $serviceOrder->getBusinessPreferentialAmount();
        }

        if (in_array('paymentType', $keys)) {
            $orderExpression['paymentType'] = $serviceOrder->getPayment()->getType();
        }

        if (in_array('transactionNumber', $keys)) {
            $orderExpression['transactionNumber'] = $serviceOrder->getPayment()->getTransactionNumber();
        }

        if (in_array('transactionInfo', $keys)) {
            $orderExpression['transactionInfo'] = $serviceOrder->getPayment()->getTransactionInfo();
        }

        if (in_array('paymentTime', $keys)) {
            $orderExpression['paymentTime'] = $serviceOrder->getPayment()->getTime();
        }

        if (in_array('status', $keys)) {
            $orderExpression['status'] = $serviceOrder->getStatus();
        }

        if (in_array('remark', $keys)) {
            $orderExpression['remark'] = $serviceOrder->getRemark();
        }

        if (in_array('cancelReason', $keys)) {
            $orderExpression['cancelReason'] = $serviceOrder->getCancelReason();
        }

        if (in_array('buyerOrderStatus', $keys)) {
            $orderExpression['buyerOrderStatus'] = $serviceOrder->getBuyerOrderStatus();
        }

        if (in_array('sellerOrderStatus', $keys)) {
            $orderExpression['sellerOrderStatus'] = $serviceOrder->getSellerOrderStatus();
        }

        if (in_array('createTime', $keys)) {
            $orderExpression['createTime'] = $serviceOrder->getCreateTime();
            $orderExpression['createTimeFormat'] = date('Y-m-d', $serviceOrder->getCreateTime());
        }

        if (in_array('updateTime', $keys)) {
            $orderExpression['updateTime'] = $serviceOrder->getUpdateTime();
            $orderExpression['updateTimeFormat'] = date('Y-m-d', $serviceOrder->getUpdateTime());
        }

        if (in_array('statusTime', $keys)) {
            $orderExpression['statusTime'] = $serviceOrder->getStatusTime();
        }

        if (isset($keys['buyerMemberAccount'])) {
            $orderExpression['buyerMemberAccount'] = $this->getMemberAccountTranslator()->objectToArray(
                $serviceOrder->getBuyerMemberAccount(),
                $keys['buyerMemberAccount']
            );
        }

        if (isset($keys['sellerEnterprise'])) {
            $orderExpression['sellerEnterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $serviceOrder->getSellerEnterprise(),
                $keys['sellerEnterprise']
            );
        }

        if (isset($keys['orderAddress'])) {
            $orderExpression['orderAddress'] = $this->getOrderAddressTranslator()->objectToArray(
                $serviceOrder->getOrderAddress(),
                $keys['orderAddress']
            );
        }

        if (isset($keys['orderCommodities'])) {
            $orderCommodities = $serviceOrder->getOrderCommodities();

            $tmp = [];

            foreach ($orderCommodities as $orderCommodity) {
                $tmp[] = $this->getOrderCommoditiesTranslator()->objectToArray(
                    $orderCommodity,
                    $keys['orderCommodities']
                );
            }

            $orderExpression['orderCommodities'] = $tmp;
        }

        return $orderExpression;
    }
}
