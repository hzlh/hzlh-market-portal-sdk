<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\View;

use Workbench\ItemsRevision\Translator\ItemsRevisionTranslator;

trait ListViewTrait
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        parent::__construct();
        $this->list = $list;
        $this->count = $count;
        $this->translator = new ItemsRevisionTranslator();
    }

    public function __destruct()
    {
        unset($this->list);
        unset($this->count);
        unset($this->translator);
    }

    public function getMyList() : array
    {
        return $this->list;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : ItemsRevisionTranslator
    {
        return $this->translator;
    }

    public function getMyContractEditList()
    {
        $translator = $this->getTranslator();

        $list = array();
        foreach ($this->getMyList() as $value) {
            $list[] = $translator->objectToArray(
                $value,
                array(
                  'id',
                  'number',
                  'title',
                  'performanceItems'=>[
                    'id',
                    'number'
                  ],
                  'status',
                  'updateTime',
                  'createTime',
                )
            );
        }

        return $list;
    }
}
