<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use UserCenter\ItemsRevision\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getMyContractEditList();

        $this->getView()->display(
            'ProductMarket/UserCenter/ItemsRevision/List.tpl',
            [
                'list' => $list,
                'total' => $this->getTotal(),
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['PRODUCT_MARKET_CONTRACT_EDIT'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_CONTRACT_EDIT']
            ]
        );
    }
}
