<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\UserCenter\ItemsRevision\Translator\ItemsRevisionTranslator;

class JsonDetailView extends JsonView implements IView
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->translator = new ItemsRevisionTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->translator);
    }

    public function getMyContractData()
    {
        return $this->data;
    }

    public function getTranslator() : ItemsRevisionTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray(
            $this->getMyContractData(),
            array(
                'id',
                'number',
                'title',
                'term',
                'content',
                'reason',
                'agreement',
                'rejectReason',
                'performanceItems'=>[
                  'id',
                  'number',
                  'content',
                  'triggerConditionType',
                  'triggerCondition',
                  'term',
                ],
                'status',
                'updateTime',
                'createTime',
            )
        );

        $this->encode($data);
    }
}
