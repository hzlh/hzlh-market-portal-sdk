<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\ListViewTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait;

    public function display() : void
    {
        $list = $this->getMyContractEditList();

        $data = array(
            'list' => $list,
            'total' => $this->getTotal()
        );

        $this->encode($data);
    }
}
