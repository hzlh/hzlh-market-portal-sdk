<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Qxy\Contract\Common\Model\Account;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use Qxy\Contract\ItemsRevision\Repository\ItemsRevisionRepository;
use Qxy\Contract\ItemsRevision\Model\ItemsRevision;

use SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\Template\ListView;
use SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\Json\JsonDetailView;
use SuperMarket\ProductMarket\UserCenter\ItemsRevision\View\Json\JsonListView;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait;

    const SCENE = array(
      'NULL' => 0, //全部  Lw
      'NORMAL' => 1, //待确认  MA
      'APPROVE' => 2, //已通过   MQ
      'REJRECT' => 3, //已驳回  Mg
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ItemsRevisionRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ItemsRevisionRepository
    {
        return $this->repository;
    }
    /**
     * @return bool
     * @param [GET]
     * @method /userCenter/itemsRevisions/{id}
     * 合同修改申请详情
     */
    protected function fetchOneAction($id):bool
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $data = $this->getRepository()
            ->scenario(ItemsRevisionRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new JsonDetailView($data));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {
        $scene = marmot_decode($this->getRequest()->get('scene', ''));
        $title = $this->getRequest()->get('title', '');
        $number = $this->getRequest()->get('number', '');
        $accountType = Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET'];

        $filter = array();
        // * NORMAL  待确认 0  待审核
        // * APPROVE   已通过   2  已通过
        // * REJRECT 已驳回  -2  已驳回
        if (!empty($scene)) {
            if ($scene == self::SCENE['NORMAL']) {
                $filter['status'] = ItemsRevision::STATUS['PENDING'];
            }

            if ($scene == self::SCENE['APPROVE']) {
                $filter['status'] = ItemsRevision::STATUS['APPROVE'];
            }

            if ($scene == self::SCENE['REJRECT']) {
                $filter['status'] = ItemsRevision::STATUS['REJRECT'];
            }
        }

        $sort = ['-updateTime'];

        $filter['partB'] = Core::$container->get('user')->getId();

        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if (!empty($accountType)) {
            $filter['accountType'] = $accountType;
        }
        if (!empty($number)) {
            $filter['number'] = $number;
        }

        return [$filter, $sort];
    }

    /**
     * @return bool
     * @param [GET]
     * @method /userCenter/itemsRevisions
     * 合同修改申请列表
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        list($filter, $sort) = $this->filterFormatChange();
        list($page, $size) = $this->getPageAndSize(FORM_SIZE);

        list($count, $list) = $this->getRepository()
            ->scenario(ItemsRevisionRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($list, $count));
            return true;
        }

        $this->render(new ListView($list, $count));
        return true;
    }
}
