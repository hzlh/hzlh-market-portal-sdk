<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Interfaces\IApproveAbleController;
use Common\Controller\Traits\ApproveControllerTrait;
use Common\Controller\Traits\CommonUtilsTrait;

use WidgetRules\Common\WidgetRules;

use SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision\ApproveItemsRevisionCommand;
use SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision\RejectItemsRevisionCommand;
use SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision\ItemsRevisionCommandHandlerFactory;

class ApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, CommonUtilsTrait;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ItemsRevisionCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApproveItemsRevisionCommand($id);

        if ($this->getCommandBus()->send($command)) {
            return true;
        }

        return false;
    }

    protected function rejectAction(int $id) : bool
    {
        $rejectReason = $this->getRequest()->post('rejectReason', '');

        $rejectReason = htmlspecialchars_decode($this->formatString($rejectReason), ENT_QUOTES);

        if ($this->validateApproveScenario($rejectReason)) {
            $command = new RejectItemsRevisionCommand(
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                return true;
            }
        }

        return false;
    }

    protected function validateApproveScenario(
        $rejectReason
    ) {
        return $this->getWidgetRules()->reason($rejectReason);
    }
}
