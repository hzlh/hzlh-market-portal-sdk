<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision\ApproveItemsRevisionCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class ApproveItemsRevisionCommandHandler implements ICommandHandler, ILogAble
{
    use ItemsRevisionCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ApproveItemsRevisionCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->itemsRevision = $this->fetchItemsRevision($command->id);

        if ($this->itemsRevision->approve()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::CATEGORY['CONTRACT_EDIT_APPLT'],
            $this->itemsRevision->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->itemsRevision->getContent()
        );
    }
}
