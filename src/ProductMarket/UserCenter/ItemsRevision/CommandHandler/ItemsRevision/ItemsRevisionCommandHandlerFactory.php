<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ItemsRevisionCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
          'SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision\RejectItemsRevisionCommand'=>
          'SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision\RejectItemsRevisionCommandHandler',
          'SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision\ApproveItemsRevisionCommand'=>
          'SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision\ApproveItemsRevisionCommandHandler',
        );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);

        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
