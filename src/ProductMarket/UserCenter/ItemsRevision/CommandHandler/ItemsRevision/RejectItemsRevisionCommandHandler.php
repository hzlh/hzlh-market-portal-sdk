<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision\RejectItemsRevisionCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class RejectItemsRevisionCommandHandler implements ICommandHandler, ILogAble
{
    use ItemsRevisionCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RejectItemsRevisionCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->itemsRevision = $this->fetchItemsRevision($command->id);
        $this->itemsRevision->setRejectReason($command->rejectReason);

        if ($this->itemsRevision->reject()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REJECT'],
            ILogAble::CATEGORY['CONTRACT_EDIT_APPLT'],
            $this->itemsRevision->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->itemsRevision->getContent()
        );
    }
}
