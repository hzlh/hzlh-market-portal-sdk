<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\CommandHandler\ItemsRevision;

use Qxy\Contract\ItemsRevision\Model\ItemsRevision;
use Qxy\Contract\ItemsRevision\Repository\ItemsRevisionRepository;

trait ItemsRevisionCommandHandlerTrait
{
    private $itemsRevision;

    private $repository;

    public function __construct()
    {
        $this->itemsRevision = new ItemsRevision();
        $this->repository = new ItemsRevisionRepository();
    }

    public function __destruct()
    {
        unset($this->itemsRevision);
        unset($this->repository);
    }

    protected function getItemsRevision() : ItemsRevision
    {
        return $this->itemsRevision;
    }

    protected function getRepository() : ItemsRevisionRepository
    {
        return $this->repository;
    }

    protected function fetchItemsRevision($id) : ItemsRevision
    {
        return $this->getRepository()->fetchOne($id);
    }
}
