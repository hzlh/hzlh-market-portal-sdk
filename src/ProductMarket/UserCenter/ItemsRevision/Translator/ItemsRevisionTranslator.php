<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Qxy\Contract\ItemsRevision\Model\ItemsRevision;
use Qxy\Contract\ItemsRevision\Model\NullItemsRevision;

use Qxy\Contract\Template\Model\Template;

use Workbench\ContractTemplate\Translator\ContractTemplateTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;
use Workbench\MerchantOrder\Translator\MerchantOrderTranslator;

use Workbench\Performance\Translator\PerformanceTranslator;

class ItemsRevisionTranslator implements ITranslator
{
    const STATUS_ZN = [
      ItemsRevision::STATUS['PENDING'] => '待审核',
      ItemsRevision::STATUS['APPROVE'] => '已通过',
      ItemsRevision::STATUS['REJRECT'] => '已驳回',
    ];

    protected function getPerformanceTranslator() : PerformanceTranslator
    {
        return new PerformanceTranslator();
    }

    public function arrayToObject(array $expression, $itemsRevision = null)
    {
        unset($itemsRevision);
        unset($expression);
        return NullItemsRevision::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * 屏蔽类中所有PMD警告
     *
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($itemsRevision, array $keys = array())
    {
        if (!$itemsRevision instanceof ItemsRevision) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'title',
                'content',
                'term',
                'reason',
                'agreement',
                'rejectReason',
                'status',
                'performanceItems'=>[],
                'updateTime',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($itemsRevision->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $itemsRevision->getNumber();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $itemsRevision->getTitle();
        }
        if (in_array('agreement', $keys)) {
            $expression['agreement'] = $itemsRevision->getAgreement();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $itemsRevision->getRejectReason();
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $itemsRevision->getReason();
        }
        if (in_array('term', $keys)) {
            $expression['term'] = $itemsRevision->getTerm();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars($itemsRevision->getContent());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $itemsRevision->getStatus();
            $expression['statusFormat'] = self::STATUS_ZN[$itemsRevision->getStatus()];
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $itemsRevision->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $itemsRevision->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $itemsRevision->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $itemsRevision->getUpdateTime());
        }
        if (isset($keys['performanceItems'])) {
            $expression['performanceItems'] = $this->getPerformanceTranslator()->objectToArray(
                $itemsRevision->getPerformance(),
                $keys['performanceItems']
            );
        }

        return $expression;
    }
}
