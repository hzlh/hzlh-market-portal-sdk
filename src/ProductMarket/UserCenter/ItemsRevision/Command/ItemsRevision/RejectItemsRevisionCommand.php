<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision;

use Marmot\Interfaces\ICommand;

class RejectItemsRevisionCommand implements ICommand
{
    public $rejectReason;

    public $id;

    public function __construct(
        string $rejectReason,
        int $id = 0
    ) {
        $this->rejectReason = $rejectReason;
        $this->id = $id;
    }
}
