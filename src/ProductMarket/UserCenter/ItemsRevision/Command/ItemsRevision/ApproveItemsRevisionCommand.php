<?php
namespace SuperMarket\ProductMarket\UserCenter\ItemsRevision\Command\ItemsRevision;

use Marmot\Interfaces\ICommand;

class ApproveItemsRevisionCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
