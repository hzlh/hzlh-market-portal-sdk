<?php
namespace SuperMarket\ProductMarket\UserCenter\Refund\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\UserCenter\Refund\View\ListViewTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait;

    public function display() : void
    {
        $list = $this->getRefundList();

        $data = array(
            'list' => $list,
            'total' => $this->getRefundTotal()
            );
        $this->encode($data);
    }
}
