<?php
namespace SuperMarket\ProductMarket\UserCenter\DeliveryAddress\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\DeliveryAddress\Model\DeliveryAddress;
use Sdk\ProductMarket\DeliveryAddress\Model\NullDeliveryAddress;

use Sdk\Snapshot\Model\Snapshot;

use Snapshot\Translator\SnapshotTranslator;

class DeliveryAddressTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $deliveryAddress = null)
    {
        unset($deliveryAddress);
        unset($expression);
        return NullDeliveryAddress::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getSnapshotsTranslator() : SnapshotTranslator
    {
        return new SnapshotTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($deliveryAddress, array $keys = array())
    {
        if (!$deliveryAddress instanceof DeliveryAddress) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'area',
                'address',
                'postalCode',
                'realName',
                'cellphone',
                'isDefaultAddress',
                'createTime',
                'updateTime',
                'snapshots'=>[]
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($deliveryAddress->getId());
        }
        if (in_array('area', $keys)) {
            $expression['area'] = $deliveryAddress->getArea();
        }
        if (in_array('address', $keys)) {
            $expression['address'] = $deliveryAddress->getAddress();
        }
        if (in_array('postalCode', $keys)) {
            $expression['postalCode'] = $deliveryAddress->getPostalCode()==0 ?
            '000000' : $deliveryAddress->getPostalCode();
        }
        if (in_array('realName', $keys)) {
            $expression['realName'] = $deliveryAddress->getRealName();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $deliveryAddress->getCellphone();
        }
        if (in_array('isDefaultAddress', $keys)) {
            $expression['isDefaultAddress'] = $deliveryAddress->getIsDefaultAddress();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $deliveryAddress->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $deliveryAddress->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $deliveryAddress->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $deliveryAddress->getUpdateTime());
        }
        if (isset($keys['snapshots'])) {
            $expression['snapshots'] = $this->getSnapshotsTranslator()
              ->objectToArray($deliveryAddress->getSnapshots()[0]);
        }

        return $expression;
    }
}
