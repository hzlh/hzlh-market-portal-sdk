<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ContractCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\ProductMarket\UserCenter\Contract\Command\ConfirmContractCommand'=>
            'SuperMarket\ProductMarket\UserCenter\Contract\CommandHandler\ConfirmContractCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
