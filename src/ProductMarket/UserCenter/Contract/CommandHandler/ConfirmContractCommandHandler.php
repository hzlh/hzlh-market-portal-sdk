<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\ProductMarket\UserCenter\Contract\Command\ConfirmContractCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Qxy\Contract\Common\Model\Account;

use Sdk\ProductMarket\Contract\Repository\ContractRepository;

use Qxy\Contract\Contract\Model\Contract;
use Qxy\Contract\Contract\Model\NullContract;

class ConfirmContractCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    private $repository;

    private $contract;

    public function __construct()
    {
        $this->repository = new ContractRepository();
        $this->contract = NullContract::getInstance();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->contract);
    }

    protected function getRepository() : ContractRepository
    {
        return $this->repository;
    }

    protected function fetchContract(int $id) : Contract
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function getContract()
    {
        return new Contract();
    }

    protected function executeAction($command)
    {
        if (!($command instanceof ConfirmContractCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->contract = $this->fetchContract($command->id);

        $contract = $this->getContract();
        $contract->setAccountId(Account::ACCOUNT_ID['HZLH']);

        if ($this->contract->confirm()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_CONFIRMATION'],
            ILogAble::CATEGORY['CONTRACT'],
            $this->contract->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->contract->getTitle()
        );
    }
}
