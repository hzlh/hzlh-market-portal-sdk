<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\View;

use SuperMarket\ProductMarket\UserCenter\Contract\Translator\ContractTranslator;

use Qxy\Contract\Contract\Model\Contract;

trait ViewTrait
{
    private $contract;

    private $translator;

    public function __construct(
        Contract $contract
    ) {
        parent::__construct();
        $this->translator = new ContractTranslator();
        $this->contract = $contract;
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->contract);
    }

    public function getTranslator() : ContractTranslator
    {
        return $this->translator;
    }

    public function getContract() : Contract
    {
        return $this->contract;
    }
  /**
   * 屏蔽类中所有PMD警告
   *
   * @SuppressWarnings(PHPMD)
   */
    public function getData()
    {
        $data = $this->getTranslator()->objectToArray(
            $this->getContract(),
            array(
                'id',
                'number',
                'orderNumber',
                'title',
                'status',
                'updateTime',
                'createTime',
                'content',
                'partA' => ['id', 'name', 'unifiedSocialCreditCode'],
                'partB' => ['id', 'name', 'unifiedSocialCreditCode'],
                'order' => ['id', 'orderno', 'paymentId', 'totalPrice', 'status', 'orderCommodities'=>[]],
                'performanceItems'=>[]
            )
        );

        return $data;
    }
}
