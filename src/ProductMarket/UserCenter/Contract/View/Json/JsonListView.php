<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\ProductMarket\UserCenter\Contract\View\ListViewTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait;

    const REMIND_STATUS = [
        'NO_REMIND' => 0,
        'REMIND' => 1
    ];

    public function display() : void
    {
        $list = $this->getContractPerformanceItemsList();

        $data = array(
            'earlyWarningStatus' => $this->getEarlyWarningStatus(),
            'handlerStatus' => $this->getHandlerStatus(),
            'list' => $list,
            'total' => $this->getTotal()
        );

        $this->encode($data);
    }
}
