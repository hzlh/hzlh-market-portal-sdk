<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\View;

use SuperMarket\ProductMarket\UserCenter\Contract\Translator\ContractTranslator;

use Workbench\Contract\View\PerformanceListViewTrait;

use Qxy\Contract\Contract\Model\Contract;
use Qxy\Contract\Performance\Model\Performance;

trait ListViewTrait
{
    use PerformanceListViewTrait;

    private $contractList;

    private $count;

    private $translator;

    private $scene;

    private $earlyWarningStatus;

    private $handlerStatus;

    public function __construct(
        array $contractList,
        string $scene,
        int $count = 0,
        int $handlerStatus = 0,
        int $earlyWarningStatus = 0
    ) {
        parent::__construct();
        $this->contractList = $contractList;
        $this->count = $count;
        $this->earlyWarningStatus = $earlyWarningStatus;
        $this->handlerStatus = $handlerStatus;
        $this->translator = new ContractTranslator();
        $this->scene = $scene;
    }

    public function __destruct()
    {
        unset($this->contractList);
        unset($this->count);
        unset($this->earlyWarningStatus);
        unset($this->handlerStatus);
        unset($this->translator);
        unset($this->scene);
    }

    public function getContractList() : array
    {
        return $this->contractList;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : ContractTranslator
    {
        return $this->translator;
    }

    public function getMyContractList()
    {
        $translator = $this->getTranslator();

        $list = array();
        foreach ($this->getContractList() as $contract) {
            $list[] = $translator->objectToArray(
                $contract,
                array(
                    'id',
                    'number',
                    'orderNumber',
                    'title',
                    'status',
                    'updateTime',
                    'earlyWarningStatus',
                    'performanceItems'=>[
                      'id',
                      'number',
                      'category',
                      'performingParty',
                      'triggerConditionType',
                      'triggerCondition',
                      'status',
                      'startTime',
                      'endTime',
                    ],
                    'partAEvaluateStatus',
                    'partBEvaluateStatus'
                )
            );
        }

        return $list;
    }

    public function getScene()
    {
        return $this->scene;
    }

    /**
     * 屏蔽类中所有PMD警告
     *
     * @SuppressWarnings(PHPMD)
     */
    public function getContractPerformanceItemsList():array
    {
        $contractList = $this->getMyContractList();

        $currentList = [];
        $newContractList = [];

        $scene = 0;
        if (!empty($this->getScene())) {
            $scene = $this->getScene();
        }
        $currentConstArray = array(
          'SCENCE'=>array(
            "PROCESSE"=>5
          )
        );

        foreach ($contractList as $key => $data) {
            $notWaringConst = -1;
            if ($data['status'] != Contract::STATUS['WAIT_PERFORMANCE']) {
                $data['earlyWarningStatus'] = $notWaringConst;
            }

            $currentList[$key] = [
              'progress' => []
            ];
          //获取买家当前合同履约条目的履约进度
            $progress = [];
            if ($data['status']== Contract::STATUS['WAIT_PERFORMANCE'] && !empty($data['performanceItems'])) {
                $performanceNumber = $this->getPerformanceItemByMinTime($data['performanceItems']);

                foreach ($performanceNumber as $numKey => $number) {
                    foreach ($data['performanceItems'] as $performanceItems) {
                        if ($performanceItems['number'] == $numKey) {
                            if ($performanceItems['status'] == Performance::PERFORMANCE_STATUS['EXECUTORY'] ||
                                $performanceItems['status'] == Performance::PERFORMANCE_STATUS['REJECT']) {
                                $part = (marmot_decode($performanceItems['performingParty']) ==
                                        Performance::PERFORMING_PARTY['PARTY_B']) ? "我" : "甲方";
                                $progress[$numKey]['value'] = $numKey." "."-"." "."待".$part."履约";
                                $progress[$numKey]['status'] = $number;
                                $progress[$numKey]['performingParty'] = $performanceItems['performingParty'];

                                if (!empty($scene) && $scene == $currentConstArray['SCENCE']['PROCESSE']) {
                                    if ($part == "甲方") {
                                        unset($progress[$numKey]);
                                    }
                                }
                            }
                            if ($performanceItems['status'] == Performance::PERFORMANCE_STATUS['PENDING']) {
                                $part = (marmot_decode($performanceItems['performingParty']) ==
                                        Performance::PERFORMING_PARTY['PARTY_B']) ? "甲方" : "我";
                                $progress[$numKey]['value'] = $numKey." "."-"." "."待".$part."确认";
                                $progress[$numKey]['status'] = $number;
                                $progress[$numKey]['performingParty'] = $performanceItems['performingParty'];

                                if (!empty($scene) && $scene == $currentConstArray['SCENCE']['PROCESSE']) {
                                    if ($part == "甲方") {
                                        unset($progress[$numKey]);
                                    }
                                }
                            }
                        }
                    }
                }
                if (empty($progress)) {
                    $data['earlyWarningStatus'] = $notWaringConst;
                }

                $currentList[$key]['progress'] = array_values($progress);
                //根据判断设置提醒状态
                $currentList[$key]['remindStatus'] = self::REMIND_STATUS['NO_REMIND'];
                if (in_array('MA', array_column($currentList[$key]['progress'], 'performingParty'))) {
                    $currentList[$key]['remindStatus'] = self::REMIND_STATUS['REMIND'];
                }
            }
            $newContractList[$key] = array_merge($currentList[$key], $data);
        }

        return $newContractList;
    }

    public function getEarlyWarningStatus()
    {
        return $this->earlyWarningStatus;
    }

    public function getHandlerStatus()
    {
        return $this->handlerStatus;
    }
}
