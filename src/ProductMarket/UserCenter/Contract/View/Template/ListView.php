<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\Contract\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    const REMIND_STATUS = [
        'NO_REMIND' => 0,
        'REMIND' => 1
    ];

    public function display()
    {
        $list = $this->getContractPerformanceItemsList();

        $this->getView()->display(
            'ProductMarket/UserCenter/Contract/List.tpl',
            [
                'list' => $list,
                'earlyWarningStatus' => $this->getEarlyWarningStatus(),
                'handlerStatus' => $this->getHandlerStatus(),
                'total' => $this->getTotal(),
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['PRODUCT_MARKET_CONTRACT'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_CONTRACT']
            ]
        );
    }
}
