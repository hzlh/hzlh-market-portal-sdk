<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\View\Template;

use Qxy\Contract\Template\Model\TemplateFactory;

/**
  * 屏蔽类中所有PMD警告
  *
  * @SuppressWarnings(PHPMD)
  */
trait ReplaceLableTrait
{
    protected function replaceLableToActual(array $data) : array
    {
        $lable = TemplateFactory::TEMPLATE_LABEL;

        $lableKeys = array_keys($lable);

        foreach ($lableKeys as $value) {
            $preg = '/<span class="contract-item-key mce-non-editable" data-key="'.$value.'">[\w\W]*?<\/span>/';

            switch ($value) {
                case $lableKeys[0]:
                    $data['content'] = preg_replace($preg, $data['number'], $data['content']);
                    break;
                case $lableKeys[1]:
                    $data['content'] = preg_replace($preg, $data['order']['orderCommodities'][0]['commodity']['title'], $data['content']);//phpcs:ignore
                    break;
                case $lableKeys[2]:
                    $data['content'] = preg_replace($preg, $data['order']['totalPriceFormat'], $data['content']);
                    break;
                case $lableKeys[3]:
                    $data['content'] = preg_replace($preg, $data['orderNumber'], $data['content']);
                    break;
                case $lableKeys[4]:
                    $data['content'] = preg_replace($preg, $data['partA']['name'], $data['content']);
                    break;
                case $lableKeys[5]:
                    $data['content'] = preg_replace($preg, $data['partA']['unifiedSocialCreditCode'], $data['content']);
                    break;
                case $lableKeys[6]:
                    $data['content'] = preg_replace($preg, $data['partB']['name'], $data['content']);
                    break;
                case $lableKeys[7]:
                    $data['content'] = preg_replace($preg, $data['partB']['unifiedSocialCreditCode'], $data['content']);
                    break;
            }
        }

        return $data;
    }
}
