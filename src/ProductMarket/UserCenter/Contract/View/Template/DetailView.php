<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\Contract\View\ViewTrait;

class DetailView extends TemplateView implements IView
{
    use ViewTrait, ReplaceLableTrait;

    public function display()
    {
        $data = $this->replaceLableToActual($this->getData());

        $perfomanceStatusArray = $this->performanceItemsStatus($data['performanceItems']);

        $this->getView()->display(
            'ProductMarket/UserCenter/Contract/Detail.tpl',
            [
                'data' => $data,
                'perfomanceStatusArray'=>$perfomanceStatusArray,
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['PRODUCT_MARKET_CONTRACT'],
                'nav_phone' => NavTrait::NAV_PHONE['PRODUCT_MARKET_CONTRACT']
            ]
        );
    }

    protected function performanceItemsStatus(array $performanceItems):array
    {
        $dataArray = [];
        foreach ($performanceItems as $value) {
            $dataArray[$value['number']] = $value['status'];
        }

        return $dataArray;
    }
}
