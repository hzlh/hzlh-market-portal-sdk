<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\Controller;

use Common\Controller\Traits\GlobalCheckTrait;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use SuperMarket\ProductMarket\UserCenter\Contract\Command\ConfirmContractCommand;
use SuperMarket\ProductMarket\UserCenter\Contract\CommandHandler\ContractCommandHandlerFactory;

class StatusController extends Controller
{
    use WebTrait, GlobalCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ContractCommandHandlerFactory());
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    /**
     *
     * [confirm 确认合同]
     */
    public function confirm($id) : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = marmot_decode($id);

        if ($this->getCommandBus()->send(new ConfirmContractCommand($id))) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
