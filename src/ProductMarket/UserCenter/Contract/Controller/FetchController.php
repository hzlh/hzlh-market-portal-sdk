<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use SuperMarket\ProductMarket\UserCenter\Contract\View\Template\ListView;
use SuperMarket\ProductMarket\UserCenter\Contract\View\Template\DetailView;
use SuperMarket\ProductMarket\UserCenter\Contract\View\Json\JsonListView;
use Qxy\Contract\Common\Model\Account;

use Sdk\ProductMarket\Contract\Repository\ContractRepository;

use Qxy\Contract\Contract\Model\Contract;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait, ContractControllerTrait;

    const SCENE = array(
      'NULL' => 0, //全部  Lw
      'NORMAL' => 1, //待确认  MA
      'WAIT_PERFORMANCE' => 2, //待履约  MQ
      'IS_PERFORMANCE' => 3, //已履约   Mg
      'BREAK_CONTRACT' => 4, //已违约  Mw
      'PROCESSE' => 5, //待处理  NA
      'WARNING' => 6, //预警状态  NQ
      'EVLUATION' => 7, //待评价  Ng
    );

    protected function fetchOneAction($id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $contract = $this->getRepository()
            ->scenario(ContractRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($contract instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DetailView($contract));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /contracts
     * 我的合同
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }
        list($filter, $sort, $scene) = $this->filterFormatChange();
        list($page, $size) = $this->getPageAndSize(FORM_SIZE);

        list($count, $contractList) = $this->getRepository()
            ->scenario(ContractRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        //获取预警场景状态和待处理场景状态
        $earlyWarningStatus = $this->getEarlyWarningStatus();
        $handlerStatus = $this->getHandlerStatus();

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView(
                $contractList,
                $scene,
                $count,
                $handlerStatus,
                $earlyWarningStatus
            ));
            return true;
        }

        $this->render(new ListView(
            $contractList,
            $scene,
            $count,
            $handlerStatus,
            $earlyWarningStatus
        ));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {
        $sort = ['-updateTime'];
        $filter = array();

        $scene = marmot_decode($this->getRequest()->get('scene', ''));

        $title = $this->getRequest()->get('title', '');
        $orderNumber = $this->getRequest()->get('orderNumber', '');
        $accountType = marmot_decode($this->getRequest()->get('accountType', ''));
        $accountType = Account::ACCOUNT_CHILDREN_TYPE['PRODUCT_MARKET'];
        $accountId = Account::ACCOUNT_ID['HZLH'];

        $filter['partB'] = Core::$container->get('user')->getId();

        // * NORMAL  待确认 0  待确认
        // * WAIT_PERFORMANCE   待履约   2  待履约
        // * IS_PERFORMANCE 已履约  4  已履约
        // * BREAK_CONTRACT 已违约 -4  已违约

        if (!empty($scene)) {
            if ($scene == self::SCENE['NORMAL']) {
                $filter['status'] = Contract::STATUS['NORMAL'];
            }

            if ($scene == self::SCENE['WAIT_PERFORMANCE']) {
                $filter['status'] = Contract::STATUS['WAIT_PERFORMANCE'];
            }

            if ($scene == self::SCENE['IS_PERFORMANCE']) {
                $filter['status'] = Contract::STATUS['IS_PERFORMANCE'];
            }

            if ($scene == self::SCENE['BREAK_CONTRACT']) {
                $filter['status'] = Contract::STATUS['BREAK_CONTRACT'];
            }
            if ($scene == self::SCENE['PROCESSE']) {
                $filter['handlerPartB'] = Core::$container->get('user')->getId();
            }
            if ($scene == self::SCENE['EVLUATION']) {
                $filter['status'] = (Contract::STATUS['IS_PERFORMANCE'].','.Contract::STATUS['BREAK_CONTRACT']);
                $filter['partBEvaluateStatus'] = Contract::EVALUATE_STATUS['WAIT_EVALUATE'];
            }
            // * WARNING 预警状态 2  到期预警
            if ($scene == self::SCENE['WARNING']) {
                $filter['earlyWarningStatus'] = Contract::EARLY_WARNING_STATUS['WARNING'];
            }
        }

        if ($title !== '') {
            $filter['title'] = $title;
        }
        if ($accountId !== '') {
            $filter['accountId'] = $accountId;
        }
        if ($accountType!== '') {
            $filter['accountType'] = $accountType;
        }
        if ($orderNumber !== '') {
            $filter['orderNumber'] = $orderNumber;
        }

        return [$filter, $sort, $scene];
    }
}
