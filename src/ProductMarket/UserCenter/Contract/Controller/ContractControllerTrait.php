<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\Controller;

use Marmot\Core;

use Sdk\ProductMarket\Contract\Repository\ContractRepository;

use Qxy\Contract\Contract\Model\Contract;

trait ContractControllerTrait
{
    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ContractRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ContractRepository
    {
        return $this->repository;
    }

    protected function getStatusByFilter($filter)
    {
        $filter['partB'] = Core::$container->get('user')->getId();

        list($count, $list) = $this->getRepository()
            ->scenario(ContractRepository::LIST_MODEL_UN)
            ->search($filter, ['-id'], PAGE, COMMON_SIZE);
        unset($list);

        $status = 0;
        if ($count > 0) {
            $status = 1;
        }
        return $status;
    }

    protected function getHandlerStatus()
    {
        $filter = array();
        $filter['handlerPartB'] = Core::$container->get('user')->getId();

        return $this->getStatusByFilter($filter);
    }

    protected function getEarlyWarningStatus()
    {
        $filter['earlyWarningStatus'] = Contract::EARLY_WARNING_STATUS['WARNING'];

        return $this->getStatusByFilter($filter);
    }
}
