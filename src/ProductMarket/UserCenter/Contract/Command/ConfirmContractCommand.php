<?php
namespace SuperMarket\ProductMarket\UserCenter\Contract\Command;

use Marmot\Interfaces\ICommand;

class ConfirmContractCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
