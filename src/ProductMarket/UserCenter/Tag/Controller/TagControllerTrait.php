<?php
namespace SuperMarket\ProductMarket\UserCenter\Tag\Controller;

use Sdk\Tag\Repository\Tag\TagRepository;

trait TagControllerTrait
{
    protected function getTagRepository() : TagRepository
    {
        return new TagRepository();
    }

    protected function getTags($tagIds) : array
    {
        $tagList = array();
        if (!empty($tagIds)) {
            $tagIds = explode(',', $tagIds);
            list($count, $tagList) =
                $this->getTagRepository()
                    ->scenario(TagRepository::LIST_MODEL_UN)
                    ->fetchList($tagIds);
            unset($count);
        }
        
        return $tagList;
    }
}
