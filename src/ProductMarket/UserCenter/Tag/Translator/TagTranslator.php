<?php
namespace SuperMarket\ProductMarket\UserCenter\Tag\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\Tag\Model\Tag\Tag;
use Sdk\Tag\Model\Tag\NullTag;

use SuperMarket\ProductMarket\Crew\Translator\CrewTranslator;

class TagTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $tag = null)
    {
        unset($tag);
        unset($expression);
        return NullTag::getInstance();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($tag, array $keys = array())
    {
        if (!$tag instanceof Tag) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'remark',
                'pid',
                'createTime',
                'updateTime',
                'statusTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] =  marmot_encode($tag->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $tag->getName();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $tag->getRemark();
        }
        if (in_array('pid', $keys)) {
            $expression['pid'] = marmot_encode($tag->getPid());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $tag->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $tag->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $tag->getStatusTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $tag->getStatus();
        }

        return $expression;
    }
}
