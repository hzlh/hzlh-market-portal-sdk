<?php
namespace SuperMarket\ProductMarket\UserCenter\Tag\View;

use UserCenter\Tag\Translator\TagTranslator;

trait TagTrait
{
    protected function getTagTranslator() : TagTranslator
    {
        return new TagTranslator();
    }

    protected function getTagList() : array
    {
        return $this->tagList;
    }

    protected function tagObjectToArray()
    {
        $tag = array();
        $tagTranslator = $this->getTagTranslator();
        $tagList = $this->getTagList();
        foreach ($tagList as $val) {
            $tag[] = $tagTranslator->objectToArray(
                $val
            );
        }
        $tag = $this->generateTree($tag);
        return $tag;
    }

    protected function generateTree($data)
    {
        $tree = array();
        foreach ($data as $v) {
            $tree[$v['id']] = $v['name'];
        }
        return $tree;
    }

    protected function tagDetailList()
    {
        $tagDetail = array();
        $tagTranslator = $this->getTagTranslator();
        $tagList = $this->getTagList();
        foreach ($tagList as $val) {
            $tagDetail[] = $tagTranslator->objectToArray(
                $val,
                array('id','name')
            );
        }

        return $tagDetail;
    }
}
