<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Core;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\CommandHandler\DeleteCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class DeleteServiceRequirementCommandHandler extends DeleteCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;

    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchRequirement($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_DELETE'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            $this->deleteAble->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->deleteAble->getNumber()
        );
    }
}
