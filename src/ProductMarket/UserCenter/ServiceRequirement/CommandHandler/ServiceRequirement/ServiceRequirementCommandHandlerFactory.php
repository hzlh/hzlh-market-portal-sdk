<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
