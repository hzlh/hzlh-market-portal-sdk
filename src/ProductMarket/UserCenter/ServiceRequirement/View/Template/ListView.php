<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\View\ListViewTrait;
use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\View\StatusTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait, StatusTrait;

    //自定义状态
    const STATUS = array(
        'REVOKED' => -4, //撤销
        'CLOSED' => -6, //关闭
        'DELETED' => -8 //删除
    );

    public function display()
    {
        $list = $this->getList();

        if (!empty($list)) {
            $list = $this->stateTransitionByArray($list);
        }

        $this->getView()->display(
            'ProductMarket/UserCenter/ServiceRequirement/Audit.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['MY_PARTICIPATION'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['SUPPLY'],
                'nav_phone' => NavTrait::NAV_PHONE['SUPPLY'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
