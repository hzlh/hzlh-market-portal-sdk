<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\ProductMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

use SuperMarket\Statistical\View\StatisticalViewTrait;

class SearchView extends TemplateView implements IView
{
    use ServiceCategoryViewTrait, StatisticalViewTrait;

    private $serviceCategories;

    private $staticsServiceRequirementCount;

    private $type;

    public function __construct($serviceCategories, $staticsServiceRequirementCount, $type)
    {
        parent::__construct();
        $this->staticsServiceRequirementCount = $staticsServiceRequirementCount;
        $this->type = $type;
        $this->serviceCategories = $serviceCategories;
    }

    public function __destruct()
    {
        unset($this->serviceCategories);
        unset($this->staticsServiceRequirementCount);
        unset($this->type);
    }

    public function getServiceCategories() : array
    {
        return $this->serviceCategories;
    }

    public function getStaticsServiceRequirementCount()
    {
        return $this->staticsServiceRequirementCount;
    }

    public function getType()
    {
        return $this->type;
    }

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $staticsServiceRequirementCount = $this->statisticalArray(
            $this->getType(),
            $this->getStaticsServiceRequirementCount()
        );
        $staticsServiceRequirementCount = empty($staticsServiceRequirementCount) ?
                                [] : array_shift($staticsServiceRequirementCount);
        $this->getView()->display(
            'ProductMarket/UserCenter/ServiceRequirement/Search.tpl',
            [
                'serviceCategoryList' => $serviceCategories,
                'serviceRequirementCount' => $staticsServiceRequirementCount
            ]
        );
    }
}
