<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\View;

use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;

trait ListViewTrait
{
    private $count;

    private $data;

    private $translator;

    public function __construct($count, $data)
    {
        parent::__construct();
        $this->count = $count;
        $this->data = $data;
        $this->translator = new ServiceRequirementTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->data);
        unset($this->translator);
    }

    public function getServiceRequirementList() : array
    {
        return $this->data;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }

    public function getList()
    {
        $translator = $this->getTranslator();
        
        $list = array();
        foreach ($this->getServiceRequirementList() as $serviceRequirement) {
            $list[] = $translator->objectToArray(
                $serviceRequirement,
                array(
                    'id','number','title','serviceCategory'=>[],'applyStatus', 'status',
                    'validityStartTime','validityEndTime','createTime','updateTime'
                )
            );
        }

        return $list;
    }
}
