<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ProductMarket\ServiceRequirement\Model\ServiceRequirement;
use Sdk\ProductMarket\ServiceRequirement\Model\NullServiceRequirement;

use SuperMarket\ProductMarket\UserCenter\Member\Translator\MemberTranslator;
use SuperMarket\ProductMarket\Workbench\ServiceCategory\Translator\ServiceCategoryTranslator;

class ServiceRequirementTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $serviceRequirement = null)
    {
        unset($expression);
        unset($serviceRequirement);
        return NullServiceRequirement::getInstance();
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceRequirement, array $keys = array())
    {
        if (!$serviceRequirement instanceof ServiceRequirement) {
            return array();
        }
        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'title',
                'minPrice',
                'maxPrice',
                'detail',
                'contactName',
                'contactPhone',
                'applyStatus',
                'rejectReason',
                'status',
                'validityStartTime',
                'validityEndTime',
                'createTime',
                'updateTime',
                'serviceCategory'=>[],
                'member'=>[],
            );
        }
        $expression = array();
        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($serviceRequirement->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $serviceRequirement->getTitle();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $serviceRequirement->getNumber();
        }
        if (in_array('minPrice', $keys)) {
            $expression['minPrice'] = empty($serviceRequirement->getMinPrice())
            ? '' : number_format($serviceRequirement->getMinPrice(), 2, '.', '');
        }
        if (in_array('maxPrice', $keys)) {
            $expression['maxPrice'] = empty($serviceRequirement->getMaxPrice())
            ? '' : number_format($serviceRequirement->getMaxPrice(), 2, '.', '');
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = Filter::dhtmlspecialchars($serviceRequirement->getDetail());
        }
        if (in_array('contactName', $keys)) {
            $expression['contactName'] = $serviceRequirement->getContactName();
        }
        if (in_array('contactPhone', $keys)) {
            $expression['contactPhone'] = $serviceRequirement->getContactPhone();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $serviceRequirement->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $serviceRequirement->getRejectReason();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $serviceRequirement->getStatus();
        }
        if (in_array('validityStartTime', $keys)) {
            $expression['validityStartTime'] = empty($serviceRequirement->getValidityStartTime())
              ? '' : $serviceRequirement->getValidityStartTime();
            $expression['validityStartTimeFormat'] = empty($serviceRequirement->getValidityStartTime())
              ? '' : date('Y.m.d', $expression['validityStartTime']);
        }
        if (in_array('validityEndTime', $keys)) {
            $expression['validityEndTime'] = empty($serviceRequirement->getValidityEndTime())
              ? '' : $serviceRequirement->getValidityEndTime();
            $expression['validityEndTimeFormat'] = empty($serviceRequirement->getValidityEndTime())
              ? '' : date('Y.m.d', $expression['validityEndTime']);
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $serviceRequirement->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $serviceRequirement->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $expression['updateTime']);
        }
        if (isset($keys['serviceCategory'])) {
            $expression['serviceCategory'] = $this->getServiceCategoryTranslator()->objectToArray(
                $serviceRequirement->getServiceCategory(),
                $keys['serviceCategory']
            );
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $serviceRequirement->getMember(),
                $keys['member']
            );
        }
        return $expression;
    }
}
