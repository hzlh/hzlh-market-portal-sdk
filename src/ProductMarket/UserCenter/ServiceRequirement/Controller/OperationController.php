<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Interfaces\IOperatAbleController;

use SuperMarket\ProductMarket\Workbench\ServiceCategory\Controller\CategoryTrait;

use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\View\Template\AddView;
use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand;
use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, GlobalCheckTrait, OperationValidateTrait, CategoryTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * @return bool
     * @param [GET, POST]
     * @method /serviceRequirements/add
     * 发布需求
     */
    protected function addView() : bool
    {
        $serviceCategory = $this->fetchServiceCategory();

        $this->render(new AddView($serviceCategory));
        return true;
    }

    protected function addAction()
    {
        $request = $this->getRequest();
        $serviceCategory = $request->post('serviceCategory', '');
        $serviceCategory = marmot_decode($serviceCategory);
        $title = $request->post('title', '');
        $detail = $request->post('detail', array());
        $minPrice = $request->post('minPrice', 0.00);
        $maxPrice = $request->post('maxPrice', 0.00);
        $validityStartTime = $request->post('validityStartTime', 0);
        $validityEndTime = $request->post('validityEndTime', 0);
        $contactName = $request->post('contactName', '');
        $contactPhone = $request->post('contactPhone', '');
        if ($this->validateOperationScenario(
            $title,
            $detail,
            $contactName,
            $contactPhone,
            $minPrice,
            $maxPrice,
            $serviceCategory
        ) ) {
            $detail = $this->checkDetailImage($detail);
            $command = new AddServiceRequirementCommand(
                $title,
                $contactName,
                $contactPhone,
                $detail,
                $serviceCategory,
                $validityStartTime,
                $validityEndTime,
                $minPrice,
                $maxPrice
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id) : bool
    {
        unset($id);
        $this->displayError();
        return false;
    }
}
