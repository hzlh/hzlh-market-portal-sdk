<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\User\WidgetRules as UserWidgetRules;
use WidgetRules\ServiceRequirement\WidgetRules as ServiceRequirementWidgetRules;

trait OperationValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getUserWidgetRules() : UserWidgetRules
    {
        return UserWidgetRules::getInstance();
    }

    protected function getServiceRequirementWidgetRules() : ServiceRequirementWidgetRules
    {
        return ServiceRequirementWidgetRules::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function validateOperationScenario(
        $title,
        $detail,
        $contactName,
        $contactPhone,
        $minPrice,
        $maxPrice,
        $serviceCategory
    ) : bool {
        return $this->getServiceRequirementWidgetRules()->title($title)
            && $this->getWidgetRules()->detail($detail)
            && $this->getWidgetRules()->realName($contactName)
            && $this->getUserWidgetRules()->cellphone($contactPhone)
            && (empty($minPrice) ? true : $this->getWidgetRules()->price($minPrice, 'minPrice'))
            && (empty($maxPrice) ? true : $this->getWidgetRules()->price($maxPrice, 'maxPrice'))
            && $this->getWidgetRules()->comparePrice($minPrice, $maxPrice, 'comparePrice')
            && $this->getWidgetRules()->formatNumeric($serviceCategory, 'serviceCategoryId');
    }
}
