<?php
namespace SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\CloseControllerTrait;
use Common\Controller\Interfaces\ICloseAbleController;

use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand;
use SuperMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

class CloseController extends Controller implements ICloseAbleController
{
    use WebTrait, CloseControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function closeAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new CloseServiceRequirementCommand($id));
    }
}
