<?php
namespace SuperMarket\ProductMarket\UserCenter\Enterprise\Translator;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Model\NullEnterprise;

use SuperMarket\ProductMarket\UserCenter\Member\Translator\MemberTranslator;

use Marmot\Interfaces\ITranslator;

use Utils\Utils\Mask;

class EnterpriseTranslator implements ITranslator
{
    const NAME_SIZE = array(
      'LEN_TWO' => 2,
      'LEN_FIVE' => 5,
    );

    public function arrayToObject(array $expression, $enterprise = null)
    {
        unset($enterprise);
        unset($expression);
        return NullEnterprise::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($enterprise, array $keys = array())
    {
        if (!$enterprise instanceof Enterprise || $enterprise instanceof NullEnterprise) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'unifiedSocialCreditCode',
                'tag',
                'logo',
                'businessLicense',
                'powerAttorney',
                'contactsInfo'=>[],
                'legalPersonInfo'=>[],
                'member'=>[],
                'realNameAuthenticationStatus'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($enterprise->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $enterprise->getName();

            $nameLength = mb_strlen($expression['name']);
            if ($nameLength == self::NAME_SIZE['LEN_TWO']) {
                $expression['enterpriseName'] = Mask::mask($expression['name'], 0, 1);
            }
            if ($nameLength > self::NAME_SIZE['LEN_TWO'] && $nameLength <= self::NAME_SIZE['LEN_FIVE']) {
                $expression['enterpriseName'] = Mask::substrReplaceByMiddle($expression['name'], 1, 1);
            }
            if ($nameLength > self::NAME_SIZE['LEN_FIVE']) {
                $expression['enterpriseName'] = Mask::substrReplaceByNumMiddle($expression['name'], 1, 1);
            }
        }
        if (in_array('unifiedSocialCreditCode', $keys)) {
            $expression['unifiedSocialCreditCode'] = $enterprise->getUnifiedSocialCreditCode();
        }
        if (in_array('tag', $keys)) {
            $expression['tag'] = $enterprise->getTag();
        }
        if (in_array('logo', $keys)) {
            $expression['logo'] = empty($enterprise->getLogo())
            ? array('name'=>'','identify'=>'') : $enterprise->getLogo();
        }
        if (in_array('businessLicense', $keys)) {
            $expression['businessLicense'] = empty($enterprise->getBusinessLicense())
            ? array('name'=>'','identify'=>'') : $enterprise->getBusinessLicense();
        }
        if (in_array('powerAttorney', $keys)) {
            $expression['powerAttorney'] = empty($enterprise->getPowerAttorney())
            ? array('name'=>'','identify'=>'') : $enterprise->getPowerAttorney();
        }
        //联系人信息
        if (isset($keys['contactsInfo'])) {
            $contactsInfo = $enterprise->getContactsInfo();
            $expression['contactsName'] = $contactsInfo->getName();
            $expression['contactsCellphone'] = $contactsInfo->getCellphone();
            $expression['contactsArea'] = $contactsInfo->getArea();
            $expression['contactsAddress'] = $contactsInfo->getAddress();
        }
        //法人信息
        if (isset($keys['legalPersonInfo'])) {
            $legalPersonInfo = $enterprise->getLegalPersonInfo();
            $expression['legalPersonName'] = $legalPersonInfo->getName();
            $expression['legalPersonCardId'] = $legalPersonInfo->getCardId();
            $expression['legalPersonPositivePhoto'] = empty($legalPersonInfo->getPositivePhoto())
            ? array('name'=>'','identify'=>'') : $legalPersonInfo->getPositivePhoto();

            $expression['legalPersonReversePhoto'] = empty($legalPersonInfo->getReversePhoto())
            ? array('name'=>'','identify'=>'') : $legalPersonInfo->getReversePhoto();

            $expression['legalPersonHandheldPhoto'] = empty($legalPersonInfo->getHandheldPhoto())
            ? array('name'=>'','identify'=>'') : $legalPersonInfo->getHandheldPhoto();
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $enterprise->getMember(),
                $keys['member']
            );
        }
        if (in_array('realNameAuthenticationStatus', $keys)) {
            $expression['realNameAuthenticationStatus'] = $enterprise->getRealNameAuthenticationStatus();
        }

        return $expression;
    }
}
