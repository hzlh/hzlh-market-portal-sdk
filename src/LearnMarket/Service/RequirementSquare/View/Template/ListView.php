<?php
namespace SuperMarket\LearnMarket\Service\RequirementSquare\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\LearnMarket\Service\RequirementSquare\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getList();

        $this->getView()->display(
            'LearnMarket/Service/RequirementSquare/List.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['LEARN_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['COURSE_REQUIREMENTS'],
                'nav_phone' => NavTrait::NAV_PHONE['LEARN_HOME'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
