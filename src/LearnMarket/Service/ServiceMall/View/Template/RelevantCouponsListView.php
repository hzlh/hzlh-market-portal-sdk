<?php
namespace SuperMarket\LearnMarket\Service\ServiceMall\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\LearnMarket\Service\ServiceMall\View\RelevantCouponsViewTrait;

class RelevantCouponsListView extends TemplateView implements IView
{
    use RelevantCouponsViewTrait;
    
    public function display()
    {
        $list = $this->getCouponList();

        $this->getView()->display(
            'LearnMarket/Service/ServiceMall/RelevantCoupons.tpl',
            [
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}
