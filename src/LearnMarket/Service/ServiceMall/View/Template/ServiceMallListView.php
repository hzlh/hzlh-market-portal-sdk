<?php
namespace SuperMarket\LearnMarket\Service\ServiceMall\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\LearnMarket\Service\ServiceMall\View\ListViewTrait;

class ServiceMallListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $searchParameters = $this->searchParameters();

        $serviceList = $this->serviceList();

        $this->getView()->display(
            'LearnMarket/Service/ServiceMall/Index.tpl',
            [
                'nav' => NavTrait::NAV_PORTAL['LEARN_HOME'],
                'nav_second' => NavTrait::NAV_PORTAL_SECOND['COURSE_SQUARE'],
                'nav_phone' => NavTrait::NAV_PHONE['LEARN_HOME'],
                'searchParameters' => $searchParameters,
                'list' => $serviceList,
                'count' => $this->getCount()
            ]
        );
    }
}
