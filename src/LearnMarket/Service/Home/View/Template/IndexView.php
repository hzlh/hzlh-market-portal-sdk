<?php
namespace SuperMarket\LearnMarket\Service\Home\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\ProductMarket\Banner\Model\Banner;

use Banner\Translator\BannerTranslator;
use SuperMarket\LearnMarket\Workbench\Service\Translator\ServiceTranslator;
use SuperMarket\LearnMarket\Workbench\ServiceCategory\Translator\ParentCategoryTranslator;
use SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;
use SuperMarket\LearnMarket\Portal\Policy\Translator\PolicyTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;

class IndexView extends TemplateView implements IView
{
    const MONTH_EN = array(
        '01' => "Jan",
        '02' => "Feb",
        '03' => "Mar",
        '04' => "Apr",
        '05' => "May",
        '06' => "Jun",
        '07' => "Jul",
        '08' => "Aug",
        '09' => "Sep",
        '10' => "Oct",
        '11' => "Nov",
        '12' => "Dec"
    );

    private $cacheData;

    private $parentCategoryList;

    private $enterpriseList;

    private $bannerList;

    private $staticsActiveEnterpriseMap;

    private $parentCategoryTranslator;

    private $enterpriseTranslator;

    private $bannerTranslator;

    public function __construct(
        array $parentCategoryList,
        array $cacheData,
        array $enterpriseList,
        array $staticsActiveEnterpriseMap,
        array $bannerList
    ) {
        $this->cacheData = $cacheData;
        $this->parentCategoryList = $parentCategoryList;
        $this->enterpriseList = $enterpriseList;
        $this->staticsActiveEnterpriseMap = $staticsActiveEnterpriseMap;
        $this->bannerList = $bannerList;
        $this->parentCategoryTranslator = new ParentCategoryTranslator();
        $this->enterpriseTranslator = new EnterpriseTranslator();
        $this->bannerTranslator = new BannerTranslator();
        parent::__construct();
    }
    
    public function __destruct()
    {
        unset($this->cacheData);
        unset($this->parentCategoryList);
        unset($this->enterpriseList);
        unset($this->staticsActiveEnterpriseMap);
        unset($this->bannerList);
        unset($this->parentCategoryTranslator);
        unset($this->enterpriseTranslator);
        unset($this->bannerTranslator);
        parent::__destruct();
    }

    protected function getCacheData() : array
    {
        return $this->cacheData;
    }

    protected function getParentCategoryList() : array
    {
        return $this->parentCategoryList;
    }

    protected function getEnterpriseList() : array
    {
        return $this->enterpriseList;
    }

    protected function getStaticsActiveEnterpriseMap() : array
    {
        return $this->staticsActiveEnterpriseMap;
    }

    protected function getBannerList() : array
    {
        return $this->bannerList;
    }

    protected function getParentCategoryTranslator() : ParentCategoryTranslator
    {
        return $this->parentCategoryTranslator;
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return $this->enterpriseTranslator;
    }

    protected function getBannerTranslator() : BannerTranslator
    {
        return $this->bannerTranslator;
    }

    protected function getBanners() : array
    {
        $translator = $this->getBannerTranslator();

        $data = [];
        foreach ($this->getBannerList() as $val) {
            $data[] = $translator->objectToArray(
                $val,
                array(
                    'id',
                    'launchType',
                    'bannerType',
                    'place',
                    'image',
                    'link',
                )
            );
        }

        $banners = [];
        foreach ($data as $val) {
            if ($val['bannerType'] == Banner::TYPE['BANNER']
                && $val['place'] == Banner::PC_PLACE['SERVICE_BANNER']
            ) { //服务超市首页banner
                $banners['banner'][] = $val;
            }
        }

        return $banners;
    }

    public function display() : void
    {
        $parentCategories = $this->parentCategoryRender();
        $enterprises = $this->enterpriseRender();
        $cacheData = $this->getCacheData();
        $banners = $this->getBanners();

        $services = $cacheData['service'];
        $serviceCount = $cacheData['serviceCount'];
        $policies = $this->policies($cacheData['policy']);
        $policyCount = $cacheData['policyCount'];
        $enterpriseCount = $cacheData['enterpriseCount'];
        $serviceRequirements = $cacheData['requirements'];
        $serviceRequirementCount = $cacheData['requirementCount'];
        
        $this->getView()->display(
            'LearnPort/Home/Index.tpl',
            [
            'nav' => NavTrait::NAV_PORTAL['LEARN_HOME'],
            'nav_phone' => NavTrait::NAV_PHONE['LEARN_HOME'],
            'parentCategories' => $parentCategories,
            'services' => $services,
            'serviceCount' => $serviceCount,
            'policies' => $policies,
            'policyCount' => $policyCount,
            'enterprises' => $enterprises,
            'enterpriseCount' => $enterpriseCount,
            'serviceRequirements' => $serviceRequirements,
            'serviceRequirementCount' => $serviceRequirementCount,
            'banners' => $banners
            ]
        );
    }

    private function enterpriseRender() : array
    {
        $translator = $this->getEnterpriseTranslator();

        $staticsActiveEnterpriseMap = $this->getStaticsActiveEnterpriseMap();

        $enterprises = array();
        foreach ($this->getEnterpriseList() as $enterprise) {
            $enterprises[] = $translator->objectToArray(
                $enterprise,
                array(
                  'id',
                  'name',
                  'logo'
                )
            );
        }

        $staticsActiveEnterprises = array();
        foreach ($enterprises as $key => $enterprise) {
            $staticsActiveEnterprises[$key]['enterprise'] = $enterprise;
            $staticsActiveEnterprises[$key]['serviceCount'] = $staticsActiveEnterpriseMap[$enterprise['id']]['serviceCount'];//phpcs:ignore
            $staticsActiveEnterprises[$key]['serviceEmployer'] = $staticsActiveEnterpriseMap[$enterprise['id']]['serviceEmployer'];//phpcs:ignore
        }

        return $staticsActiveEnterprises;
    }

    private function parentCategoryRender() : array
    {
        $translator = $this->getParentCategoryTranslator();
        $parentCategories = array();

        foreach ($this->getParentCategoryList() as $parentCategory) {
            $parentCategories[] = $translator->objectToArray(
                $parentCategory,
                array(
                  'id','name'
                )
            );
        }

        return $parentCategories;
    }

    private function policies(array $policies) : array
    {
        foreach ($policies as $key => $policy) {
            $policies[$key]['year'] = date('Y', $policy['updateTime']);
            $policies[$key]['month'] = self::MONTH_EN[date('m', $policy['updateTime'])];
            $policies[$key]['day'] = date('d', $policy['updateTime']);
        }

        return $policies;
    }
}
