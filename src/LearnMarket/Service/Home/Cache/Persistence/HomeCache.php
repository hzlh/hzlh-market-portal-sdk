<?php
namespace SuperMarket\LearnMarket\Service\Home\Cache\Persistence;

use Marmot\Core;
use Marmot\Framework\Classes\Cache;

class HomeCache extends Cache
{
    public function __construct()
    {
        parent::__construct('home'.Core::$container->get('user')->getId());
    }
}
