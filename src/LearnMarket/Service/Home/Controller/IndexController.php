<?php
namespace SuperMarket\LearnMarket\Service\Home\Controller;

use Common\Controller\Interfaces\IIndexController;

use Marmot\Framework\Adapter\ConcurrentAdapter;
use Marmot\Framework\Classes\Controller;

use Sdk\Banner\Model\Banner;

use Sdk\Banner\Repository\BannerRepository;
use Sdk\LearnMarket\Service\Repository\ServiceRepository;
use Sdk\LearnMarket\ServiceRequirement\Repository\ServiceRequirementRepository;
use Sdk\Policy\Repository\PolicyRepository;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\LearnMarket\ServiceCategory\Repository\ParentCategoryRepository;
use Sdk\LearnMarket\ServiceCategory\Repository\ServiceCategoryRepository;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;

use SuperMarket\LearnMarket\Service\Home\Cache\Query\HomeFragmentCacheQuery;
use SuperMarket\LearnMarket\Service\Home\View\Template\IndexView;
use Service\Home\View\Template\BannerIndexView;
use SuperMarket\LearnMarket\Service\Home\View\Json\BannerIndexJsonView;

use Statistical\Controller\StatisticalControllerTrait;

use Statistical\View\StatisticalViewTrait;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class IndexController extends Controller implements IIndexController
{
    use StatisticalControllerTrait, StatisticalViewTrait;

    const ACTIVE_ENTERPRISE_TYPE = 'staticsActiveEnterprise';

    private $serviceRepository;

    private $bannerRepository;

    private $requirementsRepository;

    private $policyRepository;

    private $enterpriseRepository;

    private $parentCategoryRepository;

    private $serviceCategoryRepository;

    public function __construct()
    {
        parent::__construct();
        $this->serviceRepository = new ServiceRepository();
        $this->bannerRepository = new BannerRepository();
        $this->requirementsRepository = new ServiceRequirementRepository();
        $this->policyRepository = new PolicyRepository();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->parentCategoryRepository = new ParentCategoryRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->serviceRepository);
        unset($this->bannerRepository);
        unset($this->requirementsRepository);
        unset($this->policyRepository);
        unset($this->enterpriseRepository);
        unset($this->parentCategoryRepository);
        unset($this->serviceCategoryRepository);
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function getServiceRequirementRepository() : ServiceRequirementRepository
    {
        return $this->requirementsRepository;
    }

    protected function getPolicyRepository() : PolicyRepository
    {
        return $this->policyRepository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getParentCategoryRepository() : ParentCategoryRepository
    {
        return $this->parentCategoryRepository;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function getBannerRepository() : BannerRepository
    {
        return $this->bannerRepository;
    }

    protected function getHomeFragmentCacheQuery()
    {
        return new HomeFragmentCacheQuery();
    }

    public function index()
    {
        $parentCategoryList = $this->bannerIndex();
        list($cacheData, $enterpriseList, $staticsActiveEnterpriseMap) = $this->cacheIndex();
        //获取首页banner和广告图
        $bannerList = $this->fetchBanners();

        $this->render(new IndexView(
            $parentCategoryList,
            $cacheData,
            $enterpriseList,
            $staticsActiveEnterpriseMap,
            $bannerList
        ));

        return true;
    }

    public function fetchBannerServices()
    {
        $parentCategoryId = $this->getRequest()->get('parentCategoryId', '');
        $parentCategoryId = marmot_decode($parentCategoryId);

        $serviceCategoryList = $this->serviceCategoryList($parentCategoryId);

        $serviceCategoryIds = array();
        foreach ($serviceCategoryList as $serviceCategory) {
            array_push($serviceCategoryIds, $serviceCategory->getId());
        }

        $bannerServiceList = $this->bannerServiceList($serviceCategoryIds);

        $this->render(new BannerIndexJsonView($serviceCategoryList, $bannerServiceList));

        return true;
    }

    private function bannerIndex()
    {
        $parentCategoryList = $this->parentCategory();

        return $parentCategoryList;
    }

    private function cacheIndex()
    {
        $cacheData = array();

        $cacheData = $this->getHomeFragmentCacheQuery()->get();

        list($enterpriseList, $staticsActiveEnterpriseMap) = $this->fetchStaticsActiveEnterprise();

        return [$cacheData, $enterpriseList, $staticsActiveEnterpriseMap];
    }

    private function fetchStaticsActiveEnterprise() : array
    {
      //获取企业 ids
        $staticsActiveEnterprise = $this->analyse(self::ACTIVE_ENTERPRISE_TYPE);
        $staticsActiveEnterprise = $this->statisticalArray(self::ACTIVE_ENTERPRISE_TYPE, $staticsActiveEnterprise);

        $enterpriseIds = array();
        $staticsActiveEnterpriseMap = array();

        foreach ($staticsActiveEnterprise as $val) {
            if (!empty($val['enterprise_id'])) {
                $enterpriseIds[] = $val['enterprise_id'];
            }

            $staticsActiveEnterpriseMap[marmot_encode($val['enterprise_id'])]['serviceCount'] = $val['service_count'];
            $staticsActiveEnterpriseMap[marmot_encode($val['enterprise_id'])]['serviceEmployer'] = $val['service_employer'];//phpcs:ignore
        }

        list($count, $enterpriseList) = $this->getEnterpriseRepository()
        ->scenario(EnterpriseRepository::PORTAL_LIST_MODEL_UN)
        ->fetchList($enterpriseIds);
        unset($count);

        return [$enterpriseList, $staticsActiveEnterpriseMap];
    }

    private function parentCategory() : array
    {
        $sort = ['id'];

        list($count, $parentCategoryList) = $this->getParentCategoryRepository()->search(array(), $sort);

        unset($count);
        return $parentCategoryList;
    }

    private function serviceCategoryList($parentCategoryId) : array
    {
        $filter['parentCategory'] = $parentCategoryId;
        $sort = ['id'];

        list($count, $serviceCategoryList) = $this->getServiceCategoryRepository()->search($filter, $sort, 0, 100);

        unset($count);
        return $serviceCategoryList;
    }

    private function bannerServiceList($serviceCategoryIds) : array
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        $filter['serviceCategory'] = implode(",", $serviceCategoryIds);

        list($count, $bannerServiceList) = $this->getServiceRepository()
        ->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)
        ->search($filter, $sort, 0, 4);
        unset($count);

        return $bannerServiceList;
    }

    protected function fetchBanners() : array
    {
        $sort = ['-updateTime'];

        $filter = [];
        $filter['status'] = Banner::BANNER_STATUS['ON_SHELF'];
        $filter['launchType'] = Banner::LAUNCH_TYPE['PC'];
        $filter['place'] = Banner::PC_PLACE['SERVICE_BANNER'];

        list($count, $bannerList) =
            $this->getBannerRepository()
            ->scenario(BannerRepository::LIST_MODEL_UN)
            ->search($filter, $sort, 0, COMMON_SIZE);
        unset($count);

        return $bannerList;
    }
}
