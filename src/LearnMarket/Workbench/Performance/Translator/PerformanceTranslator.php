<?php
namespace SuperMarket\LearnMarket\Workbench\Performance\Translator;

use Marmot\Core;
use Marmot\Interfaces\ITranslator;

use Qxy\Contract\Performance\Model\Performance;
use Qxy\Contract\Performance\Model\NullPerformance;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class PerformanceTranslator implements ITranslator
{
    const TRIGGER_CONDITION_TYPE_CN = [
        1 => "签订合同后" ,
        2 => "付款后",
        3 => "履约项已履行后"
    ];

    public function arrayToObject(array $expression, $performance = null)
    {
        unset($expression);
        unset($performance);
        return NullPerformance::getInstance();
    }

    public function objectToArray($performance, array $keys = array())
    {
        if (!$performance instanceof Performance) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'content',
                'category',
                'performingParty',
                'triggerConditionType',
                'triggerCondition',
                'term',
                'status',
                'createTime',
                'updateTime',
                'startTime',
                'endTime',
                'rejectReason',
                'description',
                'voucher'
            );
        }
        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($performance->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $performance->getNumber();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $performance->getContent();
        }
        if (in_array('category', $keys)) {
            $expression['category']= $performance->getCategory();
        }
        if (in_array('performingParty', $keys)) {
            $expression['performingParty'] = marmot_encode($performance->getPerformingParty());
        }
        if (in_array('triggerConditionType', $keys)) {
            $expression['triggerConditionType'] = marmot_encode($performance->getTriggerConditionType());
            if (!empty($performance->getTriggerConditionType())) {
                $triggerConditionType = self::TRIGGER_CONDITION_TYPE_CN[$performance->getTriggerConditionType()];
                $expression['triggerConditionTypeFormat'] = $triggerConditionType;

                if ($performance->getTriggerConditionType() == Performance::TRIGGER_CONDITION_TYPE['EXECUTED']) {
                    $expression['triggerConditionTypeFormat'] =
                      "待履约项-".$performance->getTriggerCondition()."履行后";
                }
            }
        }
        if (in_array('triggerCondition', $keys)) {
            $expression['triggerCondition'] = $performance->getTriggerCondition();
        }
        if (in_array('term', $keys)) {
            $expression['term'] = $performance->getTerm();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $performance->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $performance->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $performance->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $expression['updateTime']);
        }
        if (in_array('startTime', $keys)) {
            $expression['startTime'] = $performance->getStartTime();
            $expression['startTimeFormat'] = date('Y-m-d H:i:s', $expression['startTime']);
        }
        if (in_array('endTime', $keys)) {
            $expression['endTime'] = $performance->getEndTime();
            $expression['endTimeFormat'] = date('Y-m-d H:i:s', $expression['endTime']);
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $performance->getRejectReason();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $performance->getDescription();
        }
        if (in_array('voucher', $keys)) {
            $expression['voucher'] = $performance->getVoucher();
        }

        return $expression;
    }
}
