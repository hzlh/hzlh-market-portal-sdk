<?php
namespace SuperMarket\LearnMarket\Workbench\ServiceCategory\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\LearnMarket\ServiceCategory\Model\ParentCategory;
use Sdk\LearnMarket\ServiceCategory\Model\NullParentCategory;

class ParentCategoryTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $parentCategory = null)
    {
        unset($parentCategory);
        unset($expression);
        return NullParentCategory::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($parentCategory, array $keys = array())
    {
        if (!$parentCategory instanceof ParentCategory) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($parentCategory->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $parentCategory->getName();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $parentCategory->getStatus();
        }

        return $expression;
    }
}
