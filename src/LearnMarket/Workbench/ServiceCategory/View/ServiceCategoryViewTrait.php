<?php
namespace SuperMarket\LearnMarket\Workbench\ServiceCategory\View;

use SuperMarket\LearnMarket\Workbench\ServiceCategory\Translator\ServiceCategoryTranslator;

trait ServiceCategoryViewTrait
{
    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator;
    }

    public function getCategoryList($serviceCategory)
    {
        $serviceCategoryTranslator = $this->getServiceCategoryTranslator();

        $serviceCategoryList = array();
        foreach ($serviceCategory as $category) {
            $serviceCategoryList[] = $serviceCategoryTranslator->objectToArray(
                $category,
                array('id','name','parentCategory'=>['id','name'],'commission','isQualification','qualificationName')
            );
        }

        $parentCategories = array_unique(array_column($serviceCategoryList, 'parentCategory'), SORT_REGULAR);
        $parentCategories = array_values($parentCategories);

        foreach ($parentCategories as $key => $parent) {
            foreach ($serviceCategoryList as $children) {
                if ($parent['id'] == $children['parentCategory']['id']) {
                    unset($children['parentCategory']);
                    $parentCategories[$key]['list'][] = $children;
                }
            }
        };

        return $parentCategories;
    }
}
