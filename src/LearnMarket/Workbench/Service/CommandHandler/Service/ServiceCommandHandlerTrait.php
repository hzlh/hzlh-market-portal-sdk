<?php
namespace SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;

use Sdk\LearnMarket\Service\Model\Service;
use Sdk\LearnMarket\Service\Repository\ServiceRepository;
use Sdk\LearnMarket\ServiceCategory\Repository\ServiceCategoryRepository;

trait ServiceCommandHandlerTrait
{
    private $service;

    private $repository;

    public function __construct()
    {
        $this->service = new Service();
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        unset($this->service);
        unset($this->repository);
    }

    protected function getService() : Service
    {
        return $this->service;
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchService(int $id) : Service
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function fetchServiceCategory($id)
    {
        return $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    protected function executeAction(ICommand $command, Service $service) : Service
    {
        $serviceCategory = $this->fetchServiceCategory($command->serviceCategory);

        $service->setTitle($command->title);
        $service->setCover($command->cover);
        $service->setServiceObjects($command->serviceObjects);
        $service->setPrice($command->price);
        $service->setDetail($command->detail);
        $service->setContract($command->contract);
        $service->setServiceCategory($serviceCategory);
        $service->setTag($command->tag);

        return $service;
    }
}
