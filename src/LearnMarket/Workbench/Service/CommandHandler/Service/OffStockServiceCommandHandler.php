<?php
namespace SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use SuperMarket\LearnMarket\Workbench\Service\Command\Service\OffStockServiceCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\ProductMarket\Log\Model\Log;
use Sdk\ProductMarket\Log\Model\ILogAble;

class OffStockServiceCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof OffStockServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->service = $this->fetchService($command->id);

        if ($this->service->offStock()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_OFF_STOCK'],
            ILogAble::CATEGORY['SERVICE'],
            $this->service->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->service->getNumber(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
