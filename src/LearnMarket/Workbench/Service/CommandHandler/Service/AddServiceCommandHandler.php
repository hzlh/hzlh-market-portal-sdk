<?php
namespace SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\LearnMarket\Service\Model\Service;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Repository\EnterpriseRepository;

use SuperMarket\LearnMarket\Workbench\Service\Command\Service\AddServiceCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class AddServiceCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    private $service;

    private $enterpriseRepository;

    public function __construct()
    {
        $this->service = new Service();
        $this->enterpriseRepository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        unset($this->service);
        unset($this->enterpriseRepository);
    }

    protected function getService() : Service
    {
        return $this->service;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function fetchEnterprise($id) : Enterprise
    {
        return $this->getEnterpriseRepository()->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $service = $this->getService();
        $service = $this->executeAction($command, $service);

        $enterprise = $this->fetchEnterprise($command->enterprise);
        $service->setEnterprise($enterprise);
        if ($service->add()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['SERVICE'],
            $this->getService()->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->getService()->getNumber(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}
