<?php
namespace SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\AddServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\AddServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\EditServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\EditServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\ResubmitServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\RevokeServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\DeleteServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\CloseServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\OnShelfServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'SuperMarket\LearnMarket\Workbench\Service\Command\Service\OffStockServiceCommand'=>
        'SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
