<?php
namespace SuperMarket\LearnMarket\Workbench\Service\View;

use Sdk\LearnMarket\Service\Model\Service;

trait StatusTrait
{
    public function stateTransitionByOne($data)
    {
        $data['serviceStatus'] = $this->statusTransition($data['status'], $data['applyStatus']);

        return $data;
    }

    public function stateTransitionByArray($data)
    {
        foreach ($data as $key => $item) {
            $data[$key]['serviceStatus'] = $this->statusTransition($item['status'], $item['applyStatus']);
        }

        return $data;
    }

    protected function statusTransition($status, $applyStatus)
    {
        switch ($status) {
            case Service::SERVICE_STATUS['OFFSTOCK']:
                $serviceStatus = self::STATUS['OFFSTOCK'];
                break;
            case Service::SERVICE_STATUS['REVOKED']:
                $serviceStatus = self::STATUS['REVOKED'];
                break;
            case Service::SERVICE_STATUS['CLOSED']:
                $serviceStatus = self::STATUS['CLOSED'];
                break;
            case Service::SERVICE_STATUS['DELETED']:
                $serviceStatus = self::STATUS['DELETED'];
                break;
            default:
                $serviceStatus = $applyStatus;
        }

        return $serviceStatus;
    }
}
