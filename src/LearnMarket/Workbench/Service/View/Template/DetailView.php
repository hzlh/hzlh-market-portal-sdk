<?php
namespace SuperMarket\LearnMarket\Workbench\Service\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\LearnMarket\Workbench\Service\View\StatusTrait;
use SuperMarket\LearnMarket\Workbench\Service\View\DetailViewTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use UserCenter\Tag\View\TagTrait;

class DetailView extends TemplateView implements IView
{
    use DetailViewTrait, StatusTrait, GlobalCheckRolesTrait, TagTrait;

    const STATUS = array(
        'OFFSTOCK' => -3,
        'REVOKED' => -4,
        'CLOSED' => -6,
        'DELETED' => -8
    );

    public function display()
    {
        $data = $this->getDetail();
        $data = $this->stateTransitionByOne($data);
        $contractTemplateItem = $this->getContractTemplateItemList();

        $data = $this->contractTemplateItemStatus($data, $contractTemplateItem);

        $tag = $this->tagDetailList();

        $data['tag'] = !empty($tag) ? $tag : [];

        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'LearnMarket/Service/Workbench/Service/Detail.tpl',
            [
                'data' => $data,
                'permission' => $permission,
                'nav_left' => NavTrait::NAV_WORKBENCH['SERVICE'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_SERVICE']
            ]
        );
    }

    protected function contractTemplateItemStatus(array $data, array $templateItem) : array
    {
        $data['template'] = [];

        foreach ($templateItem as $val) {
            if ($val['item'] == $data['id']) {
                $data['template'] = $val;
            }
        }

        return $data;
    }
}
