<?php
namespace SuperMarket\LearnMarket\Workbench\Service\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\LearnMarket\Workbench\Service\View\ViewTrait;
use SuperMarket\LearnMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

use Common\Controller\Traits\GlobalCheckRolesTrait;

use UserCenter\Tag\View\TagTrait;

class ResubmitView extends TemplateView implements IView
{
    use ViewTrait, ServiceCategoryViewTrait, GlobalCheckRolesTrait, TagTrait;

    public function display()
    {
        $data = $this->getData();

        $tag = $this->tagDetailList();

        $data['tag'] = !empty($tag) ? $tag : [];

        $serviceCategories = $this->getServiceCategory();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $serviceObjects = $this->getServiceObjects();
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'LearnMarket/Service/Workbench/Service/Resubmit.tpl',
            [
                'data' => $data,
                'permission' => $permission,
                'serviceCategories' => $serviceCategories,
                'serviceObjects' => $serviceObjects,
                'nav_left' => NavTrait::NAV_WORKBENCH['SERVICE'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_SERVICE']
            ]
        );
    }
}
