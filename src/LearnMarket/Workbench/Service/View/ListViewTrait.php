<?php
namespace SuperMarket\LearnMarket\Workbench\Service\View;

use SuperMarket\LearnMarket\Workbench\Service\Translator\ServiceTranslator;
use Workbench\ContractTemplate\Translator\ContractTemplateItemTranslator;

use Statistical\View\StatisticalViewTrait;

trait ListViewTrait
{
    use StatisticalViewTrait;

    private $count;

    private $data;

    private $type;

    private $contractTemplateItem;

    private $translator;

    private $staticsServiceCount;

    private $contractTemplateItemTranslator;

    public function __construct(
        $count,
        $data,
        $staticsServiceCount,
        $type,
        $contractTemplateItem
    ) {
        parent::__construct();
        $this->count = $count;
        $this->data = $data;
        $this->type = $type;
        $this->contractTemplateItem = $contractTemplateItem;
        $this->staticsServiceCount = $staticsServiceCount;
        $this->translator = new ServiceTranslator();
        $this->contractTemplateItemTranslator = new ContractTemplateItemTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->data);
        unset($this->type);
        unset($this->contractTemplateItem);
        unset($this->staticsServiceCount);
        unset($this->translator);
        unset($this->contractTemplateItemTranslator);
    }

    public function getServiceList() : array
    {
        return $this->data;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getStaticsServiceCount()
    {
        return $this->staticsServiceCount;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getContractTemplateItem() : array
    {
        return $this->contractTemplateItem;
    }

    public function getTranslator() : ServiceTranslator
    {
        return $this->translator;
    }

    public function getContractTemplateItemTranslator() : ContractTemplateItemTranslator
    {
        return $this->contractTemplateItemTranslator;
    }

    public function getStaticsService()
    {
        $serviceCount = $this->statisticalArray(
            $this->getType(),
            $this->getStaticsServiceCount()
        );

        $serviceCount = empty($serviceCount) ? [] : array_shift($serviceCount);

        return $serviceCount;
    }

    public function getContractTemplateItemList()
    {
        $translator = $this->getContractTemplateItemTranslator();

        $list = array();
        foreach ($this->getContractTemplateItem() as $contractTemplateItem) {
            $list[] = $translator->objectToArray(
                $contractTemplateItem,
                array(
                    'id',
                    'item',
                    'template' => ['id']
                )
            );
        }

        return $list;
    }

    public function getList()
    {
        $translator = $this->getTranslator();

        $list = array();
        foreach ($this->getServiceList() as $serviceRequirement) {
            $list[] = $translator->objectToArray(
                $serviceRequirement,
                array(
                    'id',
                    'number',
                    'title',
                    'serviceCategory'=>['name'],
                    'applyStatus',
                    'volume',
                    'pageViews',
                    'status',
                    'updateTime'
                )
            );
        }

        return $list;
    }
}
