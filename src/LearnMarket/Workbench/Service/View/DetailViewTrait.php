<?php
namespace SuperMarket\LearnMarket\Workbench\Service\View;

use Sdk\LearnMarket\Service\Model\Service;

use SuperMarket\LearnMarket\Workbench\Service\Translator\ServiceTranslator;
use SuperMarket\LearnMarket\Workbench\ContractTemplate\Translator\ContractTemplateItemTranslator;

trait DetailViewTrait
{
    private $service;

    private $tagList;

    private $collectionCount;

    private $contractTemplateItem;

    private $translator;

    private $contractTemplateItemTranslator;

    public function __construct(
        Service $service,
        array $contractTemplateItem = array(),
        $tagList = array(),
        $collectionCount = 0
    ) {
        parent::__construct();
        $this->service = $service;
        $this->tagList = $tagList;
        $this->contractTemplateItem = $contractTemplateItem;
        $this->collectionCount = $collectionCount;
        $this->translator = new ServiceTranslator();
        $this->contractTemplateItemTranslator = new ContractTemplateItemTranslator();
    }

    public function __destruct()
    {
        unset($this->service);
        unset($this->tagList);
        unset($this->contractTemplateItem);
        unset($this->collectionCount);
        unset($this->translator);
        unset($this->contractTemplateItemTranslator);
    }

    protected function getService() : Service
    {
        return $this->service;
    }

    protected function getContractTemplateItem() : array
    {
        return $this->contractTemplateItem;
    }

    protected function getCollectionCount() : int
    {
        return $this->collectionCount;
    }

    protected function getTranslator() : ServiceTranslator
    {
        return $this->translator;
    }

    public function getContractTemplateItemTranslator() : ContractTemplateItemTranslator
    {
        return $this->contractTemplateItemTranslator;
    }

    public function getContractTemplateItemList()
    {
        $translator = $this->getContractTemplateItemTranslator();

        $list = array();
        foreach ($this->getContractTemplateItem() as $contractTemplateItem) {
            $list[] = $translator->objectToArray(
                $contractTemplateItem
            );
        }

        return $list;
    }

    public function getDetail()
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray($this->getService());

        $data['collectionCount'] = $this->getCollectionCount();

        return $data;
    }
}
