<?php
namespace SuperMarket\LearnMarket\Workbench\Service\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use SuperMarket\LearnMarket\Workbench\Service\View\ListViewTrait;
use SuperMarket\LearnMarket\Workbench\Service\View\StatusTrait;

class JsonListView extends JsonView implements IView
{
    use ListViewTrait, StatusTrait;

    const STATUS = array(
        'OFFSTOCK' => -3,
        'REVOKED' => -4,
        'CLOSED' => -6,
        'DELETED' => -8
    );

    const TEMPLATE_STATUS = [
        'UNCORRELATED' => 0,
        'ASSOCIATED' => 2
    ];

    const TEMPLATE_STATUS_ZN = [
        self::TEMPLATE_STATUS['UNCORRELATED'] => '去关联',
        self::TEMPLATE_STATUS['ASSOCIATED'] => '已关联'
    ];

    public function display() : void
    {
        $list = $this->getList();
        $contractTemplateItem = $this->getContractTemplateItemList();

        $template = [];
        foreach ($list as $key => $val) {
            $template['id'] = '';
            $template['templateStatus'] = marmot_encode(self::TEMPLATE_STATUS['UNCORRELATED']);
            $template['templateStatusFormat'] = self::TEMPLATE_STATUS_ZN[self::TEMPLATE_STATUS['UNCORRELATED']];

            foreach ($contractTemplateItem as $itemVal) {
                if ($val['id'] == $itemVal['item']) {
                    $template['id'] = $itemVal['template']['id'];
                    $template['templateStatus'] = marmot_encode(self::TEMPLATE_STATUS['ASSOCIATED']);
                    $template['templateStatusFormat'] = self::TEMPLATE_STATUS_ZN[self::TEMPLATE_STATUS['ASSOCIATED']];
                }
            }

            $list[$key]['template'] = $template;
        }

        if (!empty($list)) {
            $list = $this->stateTransitionByArray($list);
        }

        $data = array(
            'list' => $list,
            'total' => $this->getTotal(),
            'serviceCount' =>$this->getStaticsService()
        );

        $this->encode($data);
    }
}
