<?php
namespace SuperMarket\LearnMarket\Workbench\Service\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Interfaces\IOperatAbleController;

use Utils\AlibabaCloud\AlibabaCloudFactory;
use SuperMarket\LearnMarket\Workbench\Service\View\Template\AddView;
use SuperMarket\LearnMarket\Workbench\Service\View\Template\EditView;
use SuperMarket\LearnMarket\Workbench\Service\Command\Service\AddServiceCommand;
use SuperMarket\LearnMarket\Workbench\Service\Command\Service\EditServiceCommand;
use SuperMarket\LearnMarket\Workbench\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

use Sdk\LearnMarket\Service\Repository\ServiceRepository;
use Sdk\LearnMarket\Service\Model\NullService;

use UserCenter\Tag\Controller\TagControllerTrait;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, GlobalCheckTrait, OperationValidateTrait, ServiceTrait, TagControllerTrait;

    private $commandBus;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
        unset($this->repository);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->repository;
    }

    /**
     * @return bool
     * @param [GET, POST]
     * @method /services/add
     * 发布需求
     */
    protected function addView() : bool
    {
        if (!$this->globalCheckEnterprise()) {
            $this->displayError();
            return false;
        }

        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $serviceCategoryIds  = $this->getServiceCategoryIds($enterpriseId);
        $serviceCategoryArray = $this->fetchServiceCategoryByIds($serviceCategoryIds);
        
        $serviceObjects = $this->fetchServiceObjects();


        $this->render(new AddView($serviceCategoryArray, $serviceObjects));
        return true;
    }

    protected function addAction()
    {
        $request = $this->getCommonRequest();

        if ($this->validateOperationScenario(
            $request['title'],
            $request['cover'],
            $request['price'],
            $request['contract'],
            $request['serviceObjects'],
            $request['detail'],
            $request['serviceCategoryId']
        ) ) {
            list($cover, $detail) = $this->getCoverAndDetail($request['cover'], $request['detail']);
            $command = new AddServiceCommand(
                $request['title'],
                $request['tag'],
                $cover,
                $request['serviceObjects'],
                $request['price'],
                $detail,
                $request['contract'],
                $request['serviceCategoryId'],
                $request['enterpriseId']
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * @return bool
     * @param [GET, POST]
     * @method /services/{$id}/edit
     * 编辑需求
     */
    protected function editView(int $id) : bool
    {
        $service = $this->getServiceRepository()
            ->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($service instanceof NullService) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $tag = $service->getTag();
        $tagList = $this->getTags($tag);

        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $serviceCategoryIds  = $this->getServiceCategoryIds($enterpriseId);
        $serviceCategoryArray = $this->fetchServiceCategoryByIds($serviceCategoryIds);

        $serviceObjects = $this->fetchServiceObjects();

        $this->render(new EditView($service, $serviceCategoryArray, $serviceObjects, $tagList));
        return true;
    }

    protected function editAction(int $id) : bool
    {
        $request = $this->getCommonRequest();

        if ($this->validateOperationScenario(
            $request['title'],
            $request['cover'],
            $request['price'],
            $request['contract'],
            $request['serviceObjects'],
            $request['detail'],
            $request['serviceCategoryId']
        ) ) {
            list($cover, $detail) = $this->getCoverAndDetail($request['cover'], $request['detail']);
            $command = new EditServiceCommand(
                $request['title'],
                $request['tag'],
                $cover,
                $request['serviceObjects'],
                $request['price'],
                $detail,
                $request['contract'],
                $request['serviceCategoryId'],
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
