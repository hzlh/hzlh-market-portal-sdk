<?php
namespace SuperMarket\LearnMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\Authentication\Model\Authentication;

use Workbench\ServiceProvider\Translator\ServiceProviderTranslator;

class ResubmitView extends TemplateView implements IView
{
    use GlobalCheckRolesTrait;

    private $authentication;

    private $translator;

    public function __construct(Authentication $authentication)
    {
        parent::__construct();
        $this->authentication = $authentication;
        $this->translator = new ServiceProviderTranslator();
    }

    public function __destruct()
    {
        unset($this->authentication);
        unset($this->translator);
    }

    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    protected function getTranslator() : ServiceProviderTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getAuthentication(),
            array('id', 'serviceCategory'=>[], 'qualificationImage')
        );

        $permission = $this->workbenchesRoles();
        
        $this->getView()->display(
            'LearnMarket/Service/Workbench/ServiceProvider/Resubmit.tpl',
            [
                'nav_left' => NavTrait::NAV_WORKBENCH['AUTHENTICATION'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_AUTHENTICATION'],
                'permission' => $permission,
                'data' => $data
            ]
        );
    }
}
