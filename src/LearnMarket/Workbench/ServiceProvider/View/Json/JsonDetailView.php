<?php
namespace SuperMarket\LearnMarket\Workbench\ServiceProvider\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Authentication\Model\Authentication;

use Workbench\ServiceProvider\Translator\ServiceProviderTranslator;

class JsonDetailView extends JsonView implements IView
{
    private $authentication;

    private $translator;

    public function __construct(Authentication $authentication)
    {
        parent::__construct();
        $this->authentication = $authentication;
        $this->translator = new ServiceProviderTranslator();
    }

    public function __destruct()
    {
        unset($this->authentication);
        unset($this->translator);
    }
    
    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    protected function getTranslator() : ServiceProviderTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($this->getAuthentication());

        $this->encode($data);
    }
}
