<?php
namespace SuperMarket\LearnMarket\Workbench\ContractTemplate\Translator;

use Marmot\Framework\Classes\Filter;
use Marmot\Interfaces\ITranslator;

use Qxy\Contract\Template\Model\Template;
use Qxy\Contract\Template\Model\NullTemplate;

use UserCenter\Enterprise\Translator\EnterpriseTranslator;

use Qxy\Contract\Template\Model\TemplateFactory;

use SuperMarket\LearnMarket\Workbench\Performance\Translator\PerformanceTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class ContractTemplateTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $contractTemplate = null)
    {
        unset($expression);
        unset($contractTemplate);
        return NullTemplate::getInstance();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getPerformanceTranslator() : PerformanceTranslator
    {
        return new PerformanceTranslator();
    }

    public function objectToArray($contractTemplate, array $keys = array())
    {
        if (!$contractTemplate instanceof Template) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'number',
                'content',
                'category',
                'industry',
                'invisible',
                'enterprise'=>[],
                'performanceItems'=>[],
                'status',
                'createTime',
                'updateTime'
            );
        }
        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($contractTemplate->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $contractTemplate->getTitle();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $contractTemplate->getNumber();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars($contractTemplate->getContent());
        }
        if (in_array('category', $keys)) {
            $category = explode(',', $contractTemplate->getCategory());

            $categoryZn = [];
            foreach ($category as $val) {
                $categoryZn[] = [
                    'id' => marmot_encode($val),
                    'name' => TemplateFactory::CATEGORY_ZN[$val]
                ];
            }
            $expression['category'] = $categoryZn;
        }
        if (in_array('industry', $keys)) {
            $industry = explode(',', $contractTemplate->getIndustry());

            $industryZn = [];
            foreach ($industry as $val) {
                $industryZn[] = [
                    'id' => marmot_encode($val),
                    'name' => TemplateFactory::INDUSTRY_ZN[$val]
                ];
            }
            $expression['industry'] = $industryZn;
        }
        if (in_array('invisible', $keys)) {
            $expression['invisible'] = marmot_encode($contractTemplate->getInvisible());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $contractTemplate->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $contractTemplate->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $contractTemplate->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $expression['updateTime']);
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $contractTemplate->getUser(),
                $keys['enterprise']
            );
        }
        if (isset($keys['performanceItems'])) {
            $performanceItemsArray = [];
            foreach ($contractTemplate->getPerformance() as $performanceItems) {
                $performanceItemsArray[] = $this->getPerformanceTranslator()->objectToArray(
                    $performanceItems,
                    $keys['performanceItems']
                );
            }
            $expression['performanceItems'] = $performanceItemsArray;
        }

        return $expression;
    }
}
