<?php
namespace SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement;

use Marmot\Interfaces\ICommand;

class AddServiceRequirementCommand implements ICommand
{
    public $title;

    public $contactName;

    public $contactPhone;

    public $detail;

    public $serviceCategoryId;

    public $validityStartTime;

    public $validityEndTime;

    public $minPrice;

    public $maxPrice;

    public function __construct(
        string $title,
        string $contactName,
        string $contactPhone,
        array $detail,
        int $serviceCategoryId = 0,
        int $validityStartTime = 0,
        int $validityEndTime = 0,
        float $minPrice = 0.00,
        float $maxPrice = 0.00
    ) {
        $this->title = $title;
        $this->contactName = $contactName;
        $this->contactPhone = $contactPhone;
        $this->detail = $detail;
        $this->serviceCategoryId = $serviceCategoryId;
        $this->validityStartTime = $validityStartTime;
        $this->validityEndTime = $validityEndTime;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    }
}
