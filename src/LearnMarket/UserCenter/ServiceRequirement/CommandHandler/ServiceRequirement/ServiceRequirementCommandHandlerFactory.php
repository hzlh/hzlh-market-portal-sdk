<?php
namespace SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
