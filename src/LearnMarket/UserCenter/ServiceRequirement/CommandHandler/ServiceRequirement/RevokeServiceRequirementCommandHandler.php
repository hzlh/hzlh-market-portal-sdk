<?php
namespace SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Core;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\CommandHandler\RevokeCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class RevokeServiceRequirementCommandHandler extends RevokeCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;

    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchRequirement($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REVOKE'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            $this->revokeAble->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->revokeAble->getNumber()
        );
    }
}
