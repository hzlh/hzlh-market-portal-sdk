<?php
namespace SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\DeleteControllerTrait;
use Common\Controller\Interfaces\IDeleteAbleController;

use SuperMarket\LearnMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand;
use SuperMarket\LearnMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

class DeleteController extends Controller implements IDeleteAbleController
{
    use WebTrait, DeleteControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function deleteAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new DeleteServiceRequirementCommand($id));
    }
}
