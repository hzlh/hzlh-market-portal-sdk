<?php
namespace SuperMarket\LearnMarket\UserCenter\ServiceRequirement\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use SuperMarket\LearnMarket\Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

class AddView extends TemplateView implements IView
{
    use ServiceCategoryViewTrait;

    private $serviceCategories;

    public function __construct($serviceCategories)
    {
        parent::__construct();
        $this->serviceCategories = $serviceCategories;
    }

    public function __destruct()
    {
        unset($this->serviceCategories);
    }

    public function getServiceCategories() : array
    {
        return $this->serviceCategories;
    }

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);

        $this->getView()->display(
            'LearnMarket/UserCenter/ServiceRequirement/Add.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['MY_PARTICIPATION'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['COURSE_REQUIREMENTS'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_SERVICE_REQUIREMENT'],
                'serviceCategoryList' => $serviceCategories
            ]
        );
    }
}
